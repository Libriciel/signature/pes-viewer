/*
 * PES-Viewer
 * Copyright (C) 2019-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.pesviewer.service;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
@ExtendWith(SpringExtension.class)
public class StorageServiceTest {


    private @Autowired StorageService storageService;


    @Test
    void launchingApp_shouldCreatePJDirectory() {
        assertTrue(Paths.get(storageService.getProperties().getPath()).toFile().exists());
    }


    @Test
    void storingFile_shouldCreatePJDirectory() throws IOException {

        FileUtils.deleteDirectory(new File(storageService.getProperties().getPath()));

        storageService.store("testing", new FileInputStream("src/test/resources/minimal.html"));

        assertTrue(Paths.get(storageService.getProperties().getPath()).toFile().exists());
    }


    @Test
    void storingFile_shouldCreateFileWithId() throws IOException {
        storageService.store("testing", new FileInputStream("src/test/resources/minimal.html"));

        assertTrue(Paths.get(storageService.getProperties().getPath() + "/testing").toFile().exists());

        // It should not store it again
        storageService.store("testing", new FileInputStream("src/test/resources/minimal.html"));

        assertTrue(Paths.get(storageService.getProperties().getPath() + "/testing").toFile().exists());
    }


    @Test
    void gettingUnknownFile_shouldReturnNull() throws IOException {
        assertNull(storageService.get("hey !"));
    }


    @Test
    void gettingFile_shouldReturnStream() throws IOException {
        storageService.store("testing", new FileInputStream("src/test/resources/minimal.html"));
        assertTrue(Paths.get(storageService.getProperties().getPath() + "/testing").toFile().exists());

        assertNotNull(storageService.get("testing"));
    }


    @Test
    void cleanUpJob_shouldRemoveOnlyOldFiles() throws IOException {
        storageService.store("testing", new FileInputStream("src/test/resources/minimal.html"));
        assertTrue(Paths.get((storageService.getProperties().getPath() + "/testing")).toFile().exists());

        // Keeping files is by default 30 days
        assertEquals(30, storageService.getProperties().getKeepingTime());

        storageService.cleanupPJDirectory();

        assertTrue(Paths.get(storageService.getProperties().getPath() + "/testing").toFile().exists());

        storageService.getProperties().setKeepingTime(0);
        storageService.cleanupPJDirectory();

        assertFalse(Paths.get(storageService.getProperties().getPath() + "/testing").toFile().exists());
    }


}
