Contributing
============

## Importing project

##### Setup in IntelliJ :

- Preferences → Plugins...
- Search and install `Lombok`
- File → Open...
- Open the `build.gradle` file
- Enable Auto-import
- Go to Preferences → Build, Execution, Deployment → Compiler → Annotation Processors
- Check Enable annotation processing


## Code style and project structure

The `.idea` folder has been committed. It should bring the appropriate code-style.  
Obviously, every modified file should be auto-formatted before any git push.  
No hook would reject a mis-formatted file for now... But if everything goes bananas, we may set it up.


## Unit tests

```bash
./gradlew clean test
```

Note : Unit tests and integration tests are included in the standard unit tests, for now.  
Those should be split into a dedicated `integrationTests` task in the future.


## Xemelios resources

This project is based on the XSLT files provided by xemelios (see [http://xemelios.org](http://xemelios.org) for further information).

To retrieve the latest version, first check on [the update page](http://xemelios.org/updatesV5/pes-aller/PRODUCTION),
 then use the python script XemeliosUpdater.py to retrieve the version of the resources you want

Those resources are embed in pes-viewer through a custom jar dependency. It is referenced in the build as a maven artifact : `fr.gouv.finances:xemelios`.  
Therefore, to update the resources for a new pes-viewer  version, one must produce a new version of this jar artifact, and reference this new version in the build.

To do that : 
 - retrieve the jar file, probably in your local maven repository (eg `~/.m2/repository/fr/gouv/finances/xemelios/1.1/xemelios-1.1.jar`)
 - extract it (right-click - extract)
 - zip the resource directory retrieved with the update script, naming the archive xemelios-resources.zip
 - replace the zip in the extracted jar with this new one (location : `xemelios-1.1/fr/gouv/finances/xemelios-resources.zip`)
 - change the name to update the version, and repack the jar :
 ```bash
mv xemelios-1.1 xemelios-1.2
cd xemelios-1.2
jar cf ../xemelios-1.2.jar *
``` 
 - upload this jar to a maven repository (currently Libriciel Nexus : nexus.libriciel.fr)
```shell
mv xemelios-1.2.jar mvn-repo-update/
cd mvn-repo-update
export PACKNAME="xemelios" VERSION="1.2" && mvn deploy:deploy-file -Dfile=$PACKNAME-$VERSION.jar -DrepositoryId=libriciel-nexus-releases \
-Durl=https://nexus.libriciel.fr/repository/maven-releases/ -Dpackaging=jar \
-DgroupId=fr.gouv.finances -DartifactId=$PACKNAME -Dversion=$VERSION
```
Note that the credentials used are those matching the specified repository in the file `$HOME/.m2/settings.xml`

 - update the artifact version in the build
 ```
 - implementation group: 'fr.gouv.finances', name: 'xemelios', version: '1.1'
 + implementation group: 'fr.gouv.finances', name: 'xemelios', version: '1.2'
```

Note : We have customized versions of some XSL files, found under resources/static.  
Those might need to be updated "manually", by viewing the diff between the old and new resources files - comparing the "official" version only,
as our customized versions have a LOT of differences... 
