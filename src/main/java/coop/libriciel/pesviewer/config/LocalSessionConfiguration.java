/*
 * PES-Viewer
 * Copyright (C) 2019-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.pesviewer.config;


import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import java.util.UUID;

import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;


@Log4j2
@Order(1)
@Configuration
@EnableScheduling
@EnableWebSecurity
public class LocalSessionConfiguration extends WebSecurityConfigurerAdapter {


    @Value("${auth.token-header-name:libricielauth}")
    private String principalRequestHeader;

    @Value("${http.token:none}")
    private String principalRequestValue;


    @Autowired
    public LocalSessionConfiguration() {
    }


    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {

        if (StringUtils.equalsIgnoreCase(principalRequestValue, "none")) {
            principalRequestValue = UUID.randomUUID().toString();
            log.info("\n\nAuth Token : " + principalRequestValue + "\n\n");
        }

        LocalSessionApiKeyAuthFilter filter = new LocalSessionApiKeyAuthFilter(principalRequestHeader);
        filter.setAuthenticationManager(authentication -> {
            String principal = (String) authentication.getPrincipal();
            if (!principalRequestValue.equals(principal)) {
                throw new BadCredentialsException("The API key was not found or not the expected value.");
            }
            authentication.setAuthenticated(true);
            return authentication;
        });

        httpSecurity.antMatcher("/maintenance/**")
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(STATELESS)
                .and().addFilter(filter).authorizeRequests()
                .anyRequest().authenticated();
    }


}
