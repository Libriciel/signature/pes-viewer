/*
 * PES-Viewer
 * Copyright (C) 2019-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.pesviewer.service;

import coop.libriciel.pesviewer.exception.PJNotFoundException;
import coop.libriciel.pesviewer.exception.UnableToCreateRichPesException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.Document;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static coop.libriciel.pesviewer.util.XemeliosConstants.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.util.MimeTypeUtils.TEXT_PLAIN_VALUE;


@SpringBootTest
@ExtendWith(SpringExtension.class)
public class XemeliosServiceTest {


    private @Autowired XemeliosService xemeliosService;
    private @Autowired StorageService storageService;


    @Test
    void invalidXml_shouldThrowUnableToCreateRichPesException() throws IOException {
        MultipartFile multipartFile = new MockMultipartFile("test.xml", "test.xml", TEXT_PLAIN_VALUE, new FileInputStream("src/test/resources/minimal.html"));
        assertThrows(UnableToCreateRichPesException.class, () -> xemeliosService.createRichPesFileAndExtractPj(multipartFile.getInputStream()));
    }


    @Test
    void minimalPes_shouldBeParsed() throws IOException, UnableToCreateRichPesException {
        MultipartFile multipartFile = new MockMultipartFile("test.xml", "test.xml", TEXT_PLAIN_VALUE, new FileInputStream("src/test/resources/peswithoutpj.xml"));
        Document d = xemeliosService.createRichPesFileAndExtractPj(multipartFile.getInputStream());
        assertNotNull(d);
    }


    @Test
    void PJInPes_shouldBeAccessibleAfterward() throws Exception {
        // This id is defined in peswithpj.xml file
        String id = "thisisunique";
        // This id is defined in peswithpj.xml file
        String idColl = "1002";

        MultipartFile multipartFile = new MockMultipartFile("test.xml", "test.xml", TEXT_PLAIN_VALUE, new FileInputStream("src/test/resources/peswithpj.xml"));
        Document d = xemeliosService.createRichPesFileAndExtractPj(multipartFile.getInputStream());
        assertNotNull(d);

        assertTrue(storageService.fileExists(idColl + id));
        assertNotNull(xemeliosService.getAttachmentInRichFile(d, Collections.singletonMap(PJ_ID, id)));
    }


    @Test
    void PjASAPInPes_shouldBeAccessibleAfterward() throws Exception {
        // This id is defined in pesWithASAP-minimal.xml file
        String id = "asap-pj-id";
        // This id is defined in pesWithASAP-minimal.xml file
        String idColl = "123456789";

        Document d = xemeliosService.createRichPesFileAndExtractPj(new FileInputStream("src/test/resources/pesWithASAP-minimal.xml"));
        assertNotNull(d);

        assertTrue(storageService.fileExists(idColl + id));
        assertNotNull(xemeliosService.getAttachmentInRichFile(d, Collections.singletonMap(PJ_ID, id)));
    }


    @Test
    void gettingPJWithFakeId_shouldThrowException() throws Exception {
        // This id is not defined in peswithpj.xml file
        String id = "thisisfake";

        MultipartFile multipartFile = new MockMultipartFile("test.xml", "test.xml", TEXT_PLAIN_VALUE, new FileInputStream("src/test/resources/peswithpj.xml"));
        Document d = xemeliosService.createRichPesFileAndExtractPj(multipartFile.getInputStream());
        assertNotNull(d);

        assertFalse(storageService.fileExists(id));
        assertThrows(PJNotFoundException.class, () -> xemeliosService.getAttachmentInRichFile(d, Collections.singletonMap(PJ_ID, id)));
    }


    @Test
    void gettingPJWithoutPresence_shouldThrowException() throws Exception {

        // This id is defined in peswithoutpj.xml file
        String id = "thisisunique1";

        MultipartFile multipartFile = new MockMultipartFile("test.xml", "test.xml", TEXT_PLAIN_VALUE, new FileInputStream("src/test/resources/peswithpjnotinside.xml"));
        Document d = xemeliosService.createRichPesFileAndExtractPj(multipartFile.getInputStream());
        assertNotNull(d);
        assertFalse(storageService.fileExists(id));

        assertThrows(PJNotFoundException.class, () -> xemeliosService.getAttachmentInRichFile(d, Collections.singletonMap(PJ_ID, id)));
    }


    @Test
    void givenMinimalPes_shouldBeAbleToBrowse() throws Exception {

        MultipartFile multipartFile = new MockMultipartFile("test.xml", "test.xml", TEXT_PLAIN_VALUE, new FileInputStream("src/test/resources/peswithoutpj.xml"));
        Document d = xemeliosService.createRichPesFileAndExtractPj(multipartFile.getInputStream());
        assertNotNull(d);

        Map<String, String> parameters = new HashMap<>();
        parameters.put("docId", DOC_ID);
        parameters.put("etatId", GENERAL_VIEW);

        assertNotNull(xemeliosService.browseInRichFile(d, parameters));
    }


    @Test
    void givenSignedPes_shouldUseXemeliosFunctionsAdapterFunctions() throws Exception {

        MultipartFile multipartFile = new MockMultipartFile("test.xml", "test.xml", TEXT_PLAIN_VALUE, new FileInputStream("src/test/resources/pessigned.xml"));
        Document d = xemeliosService.createRichPesFileAndExtractPj(multipartFile.getInputStream());
        assertNotNull(d);

        // http://localhost:8888/bl-xemwebviewer/browse?docId=pes-aller&etatId=PES_DepenseAller&elementId=BordereauDepense&collectivite=27870851600028&budget=00&path=[@added:primary-key=%272018-01-1204%27]&xsl:param=(elementId,Bordereau)&xwvSession=b2b3321c-2999-454d-b0b4-d122bfcd63fd
        Map<String, String> parameters = new HashMap<>();
        parameters.put("docId", DOC_ID);
        parameters.put("etatId", GENERAL_VIEW);

        assertNotNull(xemeliosService.browseInRichFile(d, parameters));
    }


    @Test
    void givenBadBase64PJPes_shouldBeBrowsable() throws Exception {
        MultipartFile multipartFile = new MockMultipartFile("test.xml", "test.xml", TEXT_PLAIN_VALUE, new FileInputStream("src/test/resources/peswithpjbadbase64.xml"));
        Document d = xemeliosService.createRichPesFileAndExtractPj(multipartFile.getInputStream());
        assertNotNull(d);
    }


    @Test
    void givenMinimalPes_shouldBeAbleToBrowseWithPath() throws Exception {

        MultipartFile multipartFile = new MockMultipartFile("test.xml", "test.xml", TEXT_PLAIN_VALUE, new FileInputStream("src/test/resources/peswithoutpj.xml"));
        Document d = xemeliosService.createRichPesFileAndExtractPj(multipartFile.getInputStream());
        assertNotNull(d);

        Map<String, String> parameters = new HashMap<>();
        parameters.put("docId", DOC_ID);
        parameters.put("etatId", "PES_DepenseAller");
        parameters.put("elementId", "BordereauDepense");

        parameters.put("collectivite", "1");
        parameters.put("budget", "1");
        parameters.put("path", "[@added:primary-key='2019-01-887']");
        parameters.put("xsl:param", "(mandatId, 1)");

        parameters.put("sp1", "1");
        parameters.put("sp2", "1");

        assertNotNull(xemeliosService.browseInRichFile(d, parameters));
    }


    @Test
    void givenPjId_shouldFindMimeType() throws Exception {

        MultipartFile multipartFile = new MockMultipartFile("testAttach.xml", "testAttach.xml", TEXT_PLAIN_VALUE, new FileInputStream("src/test/resources/peswithMultiplepj.xml"));
        Document d = xemeliosService.createRichPesFileAndExtractPj(multipartFile.getInputStream());
        assertNotNull(d);

        String idToTest = "PdfPjUniqueId";
        String result = xemeliosService.getMimeTypeForPj(d, idToTest);
        assertEquals("application/pdf", result);

        idToTest = "XmlPjUniqueId";
        result = xemeliosService.getMimeTypeForPj(d, idToTest);
        assertEquals("application/xhtml+xml", result);

        idToTest = "ZipPjUniqueId";
        result = xemeliosService.getMimeTypeForPj(d, idToTest);
        assertEquals("application/zip", result);
    }


    @Test
    void givenPjId_shouldFindOriginalFilename() throws Exception {

        MultipartFile multipartFile = new MockMultipartFile("testAttach.xml", "testAttach.xml", TEXT_PLAIN_VALUE, new FileInputStream("src/test/resources/peswithMultiplepj.xml"));
        Document d = xemeliosService.createRichPesFileAndExtractPj(multipartFile.getInputStream());
        assertNotNull(d);

        String idToTest = "PdfPjUniqueId";
        String result = xemeliosService.getOriginalFileNameForPj(d, idToTest);
        assertEquals("maPJ.pdf", result);

        idToTest = "XmlPjUniqueId";
        result = xemeliosService.getOriginalFileNameForPj(d, idToTest);
        assertEquals("maPJ.xml", result);

        idToTest = "ZipPjUniqueId";
        result = xemeliosService.getOriginalFileNameForPj(d, idToTest);
        assertEquals("maPJ.zip", result);
    }


    @Test
    void asapContent_shouldBeParsed() throws IOException, UnableToCreateRichPesException {

        Document d = xemeliosService.createRichPesFileAndExtractPj(new FileInputStream("src/test/resources/pesASAPContent.xml"));
        assertNotNull(d);

        Map<String, String> parameters = new HashMap<>();
        parameters.put("docId", DOC_ID);
        parameters.put("etatId", STATE_ASAP);
        parameters.put("elementId", ELEM_ASAP_FACTINDIV);

        ByteArrayOutputStream res = xemeliosService.browseInRichFile(d, parameters);
        assertNotNull(res);
    }

}
