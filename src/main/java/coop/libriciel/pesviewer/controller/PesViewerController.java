/*
 * PES-Viewer
 * Copyright (C) 2019-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.pesviewer.controller;

import coop.libriciel.pesviewer.bean.UserViewerSession;
import coop.libriciel.pesviewer.exception.PJNotFoundException;
import coop.libriciel.pesviewer.exception.PesSessionExpiredException;
import coop.libriciel.pesviewer.exception.PjUnreadableException;
import coop.libriciel.pesviewer.exception.UnableToCreateRichPesException;
import coop.libriciel.pesviewer.service.HtmlAdapterService;
import coop.libriciel.pesviewer.service.StorageService;
import coop.libriciel.pesviewer.service.XemeliosService;
import coop.libriciel.pesviewer.util.FileUtils;
import coop.libriciel.pesviewer.util.MimeTypeUtils;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.Document;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import static coop.libriciel.pesviewer.util.XemeliosConstants.*;
import static java.nio.charset.StandardCharsets.ISO_8859_1;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.springframework.http.HttpStatus.MOVED_TEMPORARILY;


@Log4j2
@Controller
public class PesViewerController {


    private final HtmlAdapterService htmlAdapterService;
    private final UserViewerSession userViewerSession;
    private final XemeliosService xemeliosService;
    private final StorageService storageService;


    // <editor-fold desc="LifeCycle">


    @Autowired
    public PesViewerController(UserViewerSession userViewerSession,
                               XemeliosService xemeliosService,
                               HtmlAdapterService htmlAdapterService,
                               StorageService storageService) {
        this.userViewerSession = userViewerSession;
        this.xemeliosService = xemeliosService;
        this.htmlAdapterService = htmlAdapterService;
        this.storageService = storageService;
    }


    // </editor-fold desc="LifeCycle">


    @PostMapping(path = "prepare")
    @ResponseStatus(MOVED_TEMPORARILY)
    public String doPrepare(@RequestParam MultipartFile file) {
        String sessionId = UUID.randomUUID().toString();

        log.info("Preparing " + file.getName());
        log.info("Session id : " + sessionId);

        // Add current file to userViewerSession's session
        try {
            Document d = xemeliosService.createRichPesFileAndExtractPj(file.getInputStream());
            this.userViewerSession.getRichPesFiles().put(sessionId, d);
        } catch (IOException e) {
            throw new UnableToCreateRichPesException("Cannot create rich PES file, missing file content", e);
        }

        // Redirect to browse file
        return String.format("redirect:browse?%s=%s&docId=%s&etatId=%s", SESSION_KEY, sessionId, DOC_ID, GENERAL_VIEW);
    }


    @GetMapping(path = "browse")
    @ResponseBody
    public ResponseEntity<String> browse(@RequestParam Map<String, String> allRequestParams) {
        log.info("Browsing in session " + allRequestParams.get(SESSION_KEY));

        checkSession(allRequestParams.get(SESSION_KEY));

        ByteArrayOutputStream out = xemeliosService.browseInRichFile(
                this.userViewerSession.getRichPesFiles().get(allRequestParams.get(SESSION_KEY)),
                allRequestParams);

        String htmlContent = null;
        try {
            htmlContent = out.toString(String.valueOf(ISO_8859_1));
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException("Cannot read html content in appropriate encoding");
        }

        // The main document in UTF-8
        org.jsoup.nodes.Document document = htmlAdapterService.prepareHtml(htmlContent);
        // The main document with all links modified
        document = htmlAdapterService.replaceLinks(document, allRequestParams.get(SESSION_KEY));

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(MediaType.TEXT_HTML);

        return ResponseEntity
                .ok()
                .headers(responseHeaders)
                .cacheControl(CacheControl.maxAge(3600, SECONDS))
                .body(document.toString());
    }


    @GetMapping(path = "browse-asap-content")
    @ResponseBody
    public ResponseEntity<String> browseASAPContent(@RequestParam Map<String, String> allRequestParams) {
        String pjId = allRequestParams.get(PJ_ID);
        String xwvSession = allRequestParams.get(SESSION_KEY);
        log.info("browseASAPContent pjId:{} session:{}", pjId, xwvSession);

        checkSession(xwvSession);
        Document pesDocument = this.userViewerSession.getRichPesFiles().get(xwvSession);

        InputStream bis = getPJStreamFromDocument(pjId, pesDocument, allRequestParams);

        Document asapDocument = xemeliosService.createRichPesFileAndExtractPj(bis);

        Map<String, String> asapBrowseParams = new HashMap<>();
        asapBrowseParams.put("docId", DOC_ID);
        asapBrowseParams.put("etatId", STATE_ASAP);
        asapBrowseParams.put("elementId", ELEM_ASAP_FACTINDIV);

        ByteArrayOutputStream out = xemeliosService.browseInRichFile(asapDocument, asapBrowseParams);

        String htmlContent = null;
        try {
            htmlContent = out.toString(String.valueOf(ISO_8859_1));
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException("Cannot read html content in appropriate encoding");
        }

        //The main document in UTF-8
        org.jsoup.nodes.Document document = htmlAdapterService.prepareHtml(htmlContent);
        // The main document with all links modified
        document = htmlAdapterService.replaceLinks(document, allRequestParams.get(SESSION_KEY));

        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(MediaType.TEXT_HTML);

        return ResponseEntity
                .ok()
                .headers(responseHeaders)
                .cacheControl(CacheControl.maxAge(3600, SECONDS))
                .body(document.toString());
    }


    @GetMapping(path = "attachment")
    @ResponseBody
    public ResponseEntity<? extends Object> attachment(@RequestParam Map<String, String> allRequestParams) throws IOException {
        String pjId = allRequestParams.get(PJ_ID);
        String xwvSession = allRequestParams.get(SESSION_KEY);
        log.info("Get attachment pjId:{} session:{}", pjId, xwvSession);

        checkSession(xwvSession);
        Document pesDocument = this.userViewerSession.getRichPesFiles().get(xwvSession);

        if (xemeliosService.pjIsAsap(pesDocument, pjId)) {
            return browseASAPContent(allRequestParams);
        }

        InputStream bis = getPJStreamFromDocument(pjId, pesDocument, allRequestParams);

        // Define InputStream to return request
        InputStreamResource isr = new InputStreamResource(bis);
        HttpHeaders respHeaders = createHttpResponseHeadersForPJ(pesDocument, pjId);

        return ResponseEntity
                .ok()
                .headers(respHeaders)
                .cacheControl(CacheControl.maxAge(3600, SECONDS))
                .body(isr);
    }


    private InputStream getPJStreamFromDocument(String pjId, Document pesDocument, final Map<String, String> allRequestParams) {

        Path attachmentPath = xemeliosService.getAttachmentInRichFile(pesDocument, allRequestParams);
        InputStream result;

        try {
            result = Files.newInputStream(attachmentPath);
        } catch (IOException | PJNotFoundException e) {
            if (allRequestParams.containsKey("state") && allRequestParams.get("state").equalsIgnoreCase("present")) {
                throw new PjUnreadableException(pjId);
            } else {
                throw new RuntimeException(e);
            }
        }

        return result;
    }


    @GetMapping(path = "exists")
    @ResponseBody
    public ResponseEntity<Boolean> attachmentExists(@RequestParam Map<String, String> allRequestParams) {
        log.info("Check attachment presence " + allRequestParams.get(PJ_ID) + " in session " + allRequestParams.get(SESSION_KEY));

        checkSession(allRequestParams.get(SESSION_KEY));

        return ResponseEntity
                .ok()
                .cacheControl(CacheControl.maxAge(3600, SECONDS))
                .body(storageService.fileExists(allRequestParams.get("collId") + allRequestParams.get(PJ_ID)));
    }


    private void checkSession(String sessionKey) {
        // Unable to get session, throw exception to show error page
        if (!userViewerSession.getRichPesFiles().containsKey(sessionKey)) {
            throw new PesSessionExpiredException();
        }
    }


    /**
     * Create appropriate HTTP Headers of the response containing an attachment file.
     * The two info set in the header are the mimeType, and the filename with appropriate extension.
     * <p>
     * We first attempt to determine them by looking up in the source document, and if it fails,
     * by analysing the filename passed as parameter (requestFilename)
     *
     * @param pesDocument The source PES document
     * @param pjId        The id of the attachment
     * @return a valid HttpHeaders instance with Contentype and ContentDispositionFormData set.
     */
    private HttpHeaders createHttpResponseHeadersForPJ(Document pesDocument, String pjId) {
        log.debug("createHttpResponseHeadersForPJ pjId:{}", pjId);

        // Get values from XML

        final String mimeTypeString = xemeliosService.getMimeTypeForPj(pesDocument, pjId);

        String pjName = Optional.ofNullable(xemeliosService.getOriginalFileNameForPj(pesDocument, pjId))
                .filter(StringUtils::isNotEmpty)
                .orElseGet(() -> xemeliosService.getPjName(pesDocument, pjId));

        // Compute values

        String fileName = Optional.ofNullable(pjName)
                .filter(StringUtils::isNotEmpty)
                .map(FilenameUtils::removeExtension)
                .map(FileUtils::cleanFileSystemForbiddenCharacters)
                .orElse("PJ");

        String fileExtension = Optional.ofNullable(pjName)
                .filter(StringUtils::isNotEmpty)
                .map(FilenameUtils::getExtension)
                .orElseGet(() -> MimeTypeUtils.getExtensionFromMimeType(mimeTypeString));

        // TODO : Wrap this with an Optional.or(() -> ...) in Java9+
        fileExtension = Optional.ofNullable(fileExtension)
                .filter(StringUtils::isNotEmpty)
                .map(String::toLowerCase)
                .orElse("pdf");

        final String targetFilename = String.format("%s.%s", fileName, fileExtension);

        log.debug("    pjName         : {}", pjName);
        log.debug("    pjMimeType     : {}", mimeTypeString);
        log.debug("    fileName       : {}", fileName);
        log.debug("    fileExtension  : {}", fileExtension);
        log.debug("    targetFilename : {}", targetFilename);

        // Build response

        final String finalFileExtension = fileExtension;
        String mimeType = Optional.ofNullable(mimeTypeString)
                .filter(StringUtils::isNotEmpty)
                .orElseGet(() ->
                        // TODO : Flatten this with a glorious Optional.or(() -> ...) in Java9+
                        Optional.of(finalFileExtension)
                                .map(MimeTypeUtils::getMimeTypeFromExtension)
                                .filter(StringUtils::isNotEmpty)
                                .orElseGet(() -> MimeTypeUtils.probeMimeTypeFromExtension(finalFileExtension)));

        HttpHeaders respHeaders = new HttpHeaders();
        MediaType mediaType = MediaType.APPLICATION_PDF;
        try {
            mediaType = MediaType.valueOf(mimeType);
        } catch (InvalidMediaTypeException e) {
            log.warn("Cannot get mediaType from nomFic field : {}", e.getMessage());
        }

        log.debug("    mimeType       : {}", mimeType);

        respHeaders.setContentType(mediaType);
        respHeaders.setContentDispositionFormData("attachment", targetFilename);

        return respHeaders;
    }


}
