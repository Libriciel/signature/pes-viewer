/*
 * PES-Viewer
 * Copyright (C) 2019-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package coop.libriciel.pesviewer.util;

import lombok.NoArgsConstructor;

import static lombok.AccessLevel.PRIVATE;


@NoArgsConstructor(access = PRIVATE)
public final class XemeliosConstants {

    public static final String SESSION_KEY = "xwvSession";
    public static final String DOC_ID = "pes-aller";
    public static final String GENERAL_VIEW = "general_view";
    public static final String STATE_ASAP = "PES_Facture";
    public static final String ELEM_ASAP_FACTINDIV = "FactureIndiv";
    public static final String PJ_ID = "pjId";

}
