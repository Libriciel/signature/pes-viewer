<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xsl:stylesheet [
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:n="http://www.minefi.gouv.fr/cp/helios/pes_v2/Rev0/aller"
	xmlns:added="http://projets.admisource.gouv.fr/xemelios/namespaces#added"
	xmlns:ano="http://projets.admisource.gouv.fr/xemelios/namespaces#anomally"
	xmlns:data="data.uri"
	xmlns:fn="http://projets.admisource.gouv.fr/xemelios/namespaces#functions"
	version="2.0">
	<xsl:param name="elementId" /> <!-- 'PES_Facture' pour afficher le releve, vide sinon -->
	<xsl:param name="numeroFacture" />  <!-- dans le cas de l'affichage d'une FactureIndiv FactureIndiv/NumeroFacture/@V -->
	<xsl:param name="browser-destination" />
	<xsl:param name="anoId" />    <!-- id de l'anomalie a mettre en surbrillance -->
	<xsl:output encoding="ISO-8859-1" method="xhtml"
		exclude-result-prefixes="n added ano data" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
		include-content-type="no" indent="yes" />
	<xsl:decimal-format name="decformat"
		decimal-separator="," grouping-separator=" " digit="#"
		pattern-separator=";" NaN="NaN" minus-sign="-" />
	<xsl:variable name="tags-ano">
		<xsl:for-each select="//ano:Anomalie[@ano:anoId = $anoId]/ano:node">
			<xsl:element name="node">
				<xsl:attribute name="id" select="./@ano:id" />
			</xsl:element>
		</xsl:for-each>
	</xsl:variable>
	<!-- Donnees pour les equivalences des codes -->
	<xsl:variable name="data">
		<data:data>
			<data:typeBordereau>
				<data:t type="01">Ordinaire</data:t>
				<data:t type="02">Annulation/r&#233;duction</data:t>
				<data:t type="03">Ordres de paiement</data:t>
				<data:t type="04">Bordereau de r&#233;gularisation</data:t>
			</data:typeBordereau>
			<data:typePiece>
				<data:t type="01">Mandat ordinaire</data:t>
				<data:t type="02">Mandat correctif</data:t>
				<data:t type="03">Mandat d&apos;ordre budg&#233;taire</data:t>
				<data:t type="04">Mandat d&apos;ordre mixte</data:t>
				<data:t type="05">Mandat &#233;mis apr&#232;s paiement</data:t>
				<data:t type="06">Mandat global</data:t>
				<data:t type="07">Mandat d&apos;admission en non valeurs</data:t>
				<data:t type="08">Mandat collectif</data:t>
				<data:t type="09">Mandat sur march&#233;</data:t>
				<data:t type="10">Mandat de rattachement</data:t>
				<data:t type="11">Ordre de paiement</data:t>
				<data:t type="12">Demande &#233;mission mandat</data:t>
				<data:t type="13">Charges constat&#233;es d&apos;avance</data:t>
			</data:typePiece>
			<data:naturePiece>
				<data:n type="01">Fonctionnement</data:n>
				<data:n type="02">Investissement</data:n>
				<data:n type="03">Inventaire</data:n>
				<data:n type="04">Emprunt</data:n>
				<data:n type="05">R&#233;gie</data:n>
				<data:n type="06">Annulation-R&#233;duction</data:n>
				<data:n type="07">Compl&#233;mentaire</data:n>
				<data:n type="08">R&#233;&#233;mis</data:n>
				<data:n type="09">Annulant un titre</data:n>
				<data:n type="10">Annulation du mandat de rattachement</data:n>
				<data:n type="11">Paie</data:n>
				<data:n type="12">Retenue de garantie</data:n>
				<data:n type="13">Dernier acompte sur march&#233;</data:n>
				<data:n type="14">Avance forfaitaire</data:n>
				<data:n type="15">Autres</data:n>
				<data:n type="18">Cession</data:n>
			</data:naturePiece>
			<data:typePieceOrigine>
				<data:t type="01">Mandat</data:t>
				<data:t type="02">Titre</data:t>
				<data:t type="03">Paiement</data:t>
				<data:t type="04">Liste non valeur</data:t>
			</data:typePieceOrigine>
			<data:typeTiersNational>
				<data:t type="01">Siret</data:t>
				<data:t type="02">Siren</data:t>
				<data:t type="03">Finess</data:t>
				<data:t type="04">Nir</data:t>
			</data:typeTiersNational>
			<data:categorieTiers>
				<data:c V="01">Personnes physiques</data:c>
				<data:c V="20">&#233;tat et &#233;tablissements publics nationaux</data:c>
				<data:c V="21">R&#233;gions</data:c>
				<data:c V="22">D&#233;partements</data:c>
				<data:c V="23">Communes</data:c>
				<data:c V="24">Groupements de collectivit&#233;s</data:c>
				<data:c V="25">Caisses des &#233;coles</data:c>
				<data:c V="26">CCAS</data:c>
				<data:c V="27">Etablissements publics de sant&#233;</data:c>
				<data:c V="28">Ecole nationale de la sant&#233; publique</data:c>
				<data:c V="29">Autres &#233;tablissements publics et organismes
					internationaux</data:c>
				<data:c V="50">Personnes morales de droit priv&#233; autres
					qu'organismes sociaux</data:c>
				<data:c V="60">Caisses de s&#233;curit&#233; sociale r&#233;gime
					g&#233;n&#233;ral</data:c>
				<data:c V="61">Caisses de s&#233;curit&#233; sociale r&#233;gime
					agricole</data:c>
				<data:c V="62">S&#233;curit&#233; sociale des travailleurs non
					salari&#233;s et professions non agricoles</data:c>
				<data:c V="63">Autres r&#233;gimes obligatoires de
					s&#233;curit&#233; sociale</data:c>
				<data:c V="64">Mutuelles et organismes d'assurance</data:c>
				<data:c V="65">Divers autres tiers payants</data:c>
				<data:c V="70">CNRACL</data:c>
				<data:c V="71">IRCANTEC</data:c>
				<data:c V="72">ASSEDIC</data:c>
				<data:c V="73">Caisses mutualistes de retraite
					compl&#233;mentaires</data:c>
				<data:c V="74">Autres organismes sociaux</data:c>
			</data:categorieTiers>
			<data:natureJuridiqueTiers>
				<data:t V="00">Inconnue</data:t>
				<data:t V="01">Particuliers</data:t>
				<data:t V="02">Artisan / Commerçant / Agriculteur</data:t>
				<data:t V="03">Soci&#233;t&#233;</data:t>
				<data:t V="04">CAM ou caisse appliquant les m&#234;mes r&#232;gles</data:t>
				<data:t V="05">Caisse compl&#233;mentaire</data:t>
				<data:t V="06">Association</data:t>
				<data:t V="07">Etat ou organisme d&apos;Etat</data:t>
				<data:t V="08">Etablissement public national</data:t>
				<data:t V="09">Collectivit&#233; territoriale / EPL / EPS</data:t>
				<data:t V="10">Etat &#233;tranger / ambassade</data:t>
				<data:t V="11">CAF</data:t>
			</data:natureJuridiqueTiers>
			<data:codePaysINSEE>
				<data:p V="99125">ALBANIE</data:p>
				<data:p V="99109">ALLEMAGNE</data:p>
				<data:p V="99130">ANDORRE</data:p>
				<data:p V="99110">AUTRICHE</data:p>
				<data:p V="99131">BELGIQUE</data:p>
				<data:p V="99148">BIELORUSSIE</data:p>
				<data:p V="99118">BOSNIE-HERZEGOVINE</data:p>
				<data:p V="99103">BOUVET (ILE)</data:p>
				<data:p V="99111">BULGARIE</data:p>
				<data:p V="99119">CROATIE</data:p>
				<data:p V="99101">DANEMARK</data:p>
				<data:p V="99134">ESPAGNE</data:p>
				<data:p V="99106">ESTONIE</data:p>
				<data:p V="99156">EX-REPUBLIQUE YOUGOSLAVE DE MACEDOINE</data:p>
				<data:p V="99101">FEROE (ILES)</data:p>
				<data:p V="99105">FINLANDE</data:p>
				<data:p V="99133">GIBRALTAR</data:p>
				<data:p V="99126">GRECE</data:p>
				<data:p V="99112">HONGRIE</data:p>
				<data:p V="99136">IRLANDE, ou EIRE</data:p>
				<data:p V="99102">ISLANDE</data:p>
				<data:p V="99127">ITALIE</data:p>
				<data:p V="99107">LETTONIE</data:p>
				<data:p V="99113">LIECHTENSTEIN</data:p>
				<data:p V="99108">LITUANIE</data:p>
				<data:p V="99137">LUXEMBOURG</data:p>
				<data:p V="99144">MALTE</data:p>
				<data:p V="99151">MOLDAVIE</data:p>
				<data:p V="99138">MONACO</data:p>
				<data:p V="99120">MONTENEGRO</data:p>
				<data:p V="99103">NORVEGE</data:p>
				<data:p V="99135">PAYS-BAS</data:p>
				<data:p V="99122">POLOGNE</data:p>
				<data:p V="99139">PORTUGAL</data:p>
				<data:p V="99141">REPUBLIQUE DEMOCRATIQUE ALLEMANDE</data:p>
				<data:p V="99142">REPUBLIQUE FEDERALE D'ALLEMAGNE</data:p>
				<data:p V="99114">ROUMANIE</data:p>
				<data:p V="99132">ROYAUME-UNI</data:p>
				<data:p V="99123">RUSSIE</data:p>
				<data:p V="99128">SAINT-MARIN</data:p>
				<data:p V="99121">SERBIE-ET-MONTENEGRO</data:p>
				<data:p V="99117">SLOVAQUIE</data:p>
				<data:p V="99145">SLOVENIE</data:p>
				<data:p V="99104">SUEDE</data:p>
				<data:p V="99140">SUISSE</data:p>
				<data:p V="99103">SVALBARD et ILE JAN MAYEN</data:p>
				<data:p V="99115">TCHECOSLOVAQUIE</data:p>
				<data:p V="99116">TCHEQUE (REPUBLIQUE)</data:p>
				<data:p V="99124">TURQUIE D'EUROPE</data:p>
				<data:p V="99155">UKRAINE</data:p>
				<data:p V="99129">VATICAN, ou SAINT-SIEGE</data:p>
				<data:p V="99212">AFGHANISTAN</data:p>
				<data:p V="99201">ARABIE SAOUDITE</data:p>
				<data:p V="99252">ARMENIE</data:p>
				<data:p V="99253">AZERBAIDJAN</data:p>
				<data:p V="99249">BAHREIN</data:p>
				<data:p V="99246">BANGLADESH</data:p>
				<data:p V="99214">BHOUTAN</data:p>
				<data:p V="99224">BIRMANIE</data:p>
				<data:p V="99225">BRUNEI</data:p>
				<data:p V="99234">CAMBODGE</data:p>
				<data:p V="99216">CHINE</data:p>
				<data:p V="99254">CHYPRE</data:p>
				<data:p V="99237">COREE</data:p>
				<data:p V="99239">COREE (REPUBLIQUE DE)</data:p>
				<data:p V="99238">COREE (REPUBLIQUE POPULAIRE DEMOCRATIQUE DE)</data:p>
				<data:p V="99247">EMIRATS ARABES UNIS</data:p>
				<data:p V="99228">ETATS MALAIS NON FEDERES</data:p>
				<data:p V="99255">GEORGIE</data:p>
				<data:p V="99223">GOA</data:p>
				<data:p V="99230">HONG-KONG</data:p>
				<data:p V="99223">INDE</data:p>
				<data:p V="99231">INDONESIE</data:p>
				<data:p V="99204">IRAN</data:p>
				<data:p V="99203">IRAQ</data:p>
				<data:p V="99207">ISRAEL</data:p>
				<data:p V="99217">JAPON</data:p>
				<data:p V="99222">JORDANIE</data:p>
				<data:p V="99211">KAMTCHATKA</data:p>
				<data:p V="99256">KAZAKHSTAN</data:p>
				<data:p V="99257">KIRGHIZISTAN</data:p>
				<data:p V="99240">KOWEIT</data:p>
				<data:p V="99241">LAOS</data:p>
				<data:p V="99205">LIBAN</data:p>
				<data:p V="99232">MACAO</data:p>
				<data:p V="99227">MALAISIE</data:p>
				<data:p V="99229">MALDIVES</data:p>
				<data:p V="99218">MANDCHOURIE</data:p>
				<data:p V="99242">MONGOLIE</data:p>
				<data:p V="99215">NEPAL</data:p>
				<data:p V="99250">OMAN</data:p>
				<data:p V="99258">OUZBEKISTAN</data:p>
				<data:p V="99213">PAKISTAN</data:p>
				<data:p V="99261">PALESTINE</data:p>
				<data:p V="99220">PHILIPPINES</data:p>
				<data:p V="99221">POSSESSIONS BRITANNIQUES AU PROCHE-ORIENT</data:p>
				<data:p V="99248">QATAR</data:p>
				<data:p V="99209">SIBERIE</data:p>
				<data:p V="99226">SINGAPOUR</data:p>
				<data:p V="99235">SRI LANKA</data:p>
				<data:p V="99206">SYRIE</data:p>
				<data:p V="99259">TADJIKISTAN</data:p>
				<data:p V="99236">TAIWAN</data:p>
				<data:p V="99219">THAILANDE</data:p>
				<data:p V="99262">TIMOR ORIENTAL</data:p>
				<data:p V="99210">TURKESTAN RUSSE</data:p>
				<data:p V="99260">TURKMENISTAN</data:p>
				<data:p V="99208">TURQUIE</data:p>
				<data:p V="99243">VIET NAM</data:p>
				<data:p V="99244">VIET NAM DU NORD</data:p>
				<data:p V="99245">VIET NAM DU SUD</data:p>
				<data:p V="99251">YEMEN</data:p>
				<data:p V="99233">YEMEN DEMOCRATIQUE</data:p>
				<data:p V="99202">YEMEN (REPUBLIQUE ARABE DU)</data:p>
				<data:p V="99319">ACORES, MADERE</data:p>
				<data:p V="99303">AFRIQUE DU SUD</data:p>
				<data:p V="99352">ALGERIE</data:p>
				<data:p V="99395">ANGOLA</data:p>
				<data:p V="99327">BENIN</data:p>
				<data:p V="99347">BOTSWANA</data:p>
				<data:p V="99331">BURKINA</data:p>
				<data:p V="99321">BURUNDI</data:p>
				<data:p V="99322">CAMEROUN</data:p>
				<data:p V="99305">CAMEROUN ET TOGO</data:p>
				<data:p V="99313">CANARIES (ILES)</data:p>
				<data:p V="99396">CAP-VERT</data:p>
				<data:p V="99323">CENTRAFRICAINE (REPUBLIQUE)</data:p>
				<data:p V="99397">COMORES</data:p>
				<data:p V="99324">CONGO</data:p>
				<data:p V="99312">CONGO (REPUBLIQUE DEMOCRATIQUE)</data:p>
				<data:p V="99326">COTE D'IVOIRE</data:p>
				<data:p V="99399">DJIBOUTI</data:p>
				<data:p V="99301">EGYPTE</data:p>
				<data:p V="99317">ERYTHREE</data:p>
				<data:p V="99315">ETHIOPIE</data:p>
				<data:p V="99328">GABON</data:p>
				<data:p V="99304">GAMBIE</data:p>
				<data:p V="99329">GHANA</data:p>
				<data:p V="99330">GUINEE</data:p>
				<data:p V="99314">GUINEE EQUATORIALE</data:p>
				<data:p V="99392">GUINEE-BISSAU</data:p>
				<data:p V="99320">ILES PORTUGAISES DE L'OCEAN INDIEN</data:p>
				<data:p V="99332">KENYA</data:p>
				<data:p V="99348">LESOTHO</data:p>
				<data:p V="99302">LIBERIA</data:p>
				<data:p V="99316">LIBYE</data:p>
				<data:p V="99333">MADAGASCAR</data:p>
				<data:p V="99334">MALAWI</data:p>
				<data:p V="99335">MALI</data:p>
				<data:p V="99350">MAROC</data:p>
				<data:p V="99390">MAURICE</data:p>
				<data:p V="99336">MAURITANIE</data:p>
				<data:p V="99393">MOZAMBIQUE</data:p>
				<data:p V="99311">NAMIBIE</data:p>
				<data:p V="99337">NIGER</data:p>
				<data:p V="99338">NIGERIA</data:p>
				<data:p V="99308">OCEAN INDIEN (TERRITOIRE BRITANNIQUE DE L')</data:p>
				<data:p V="99339">OUGANDA</data:p>
				<data:p V="99313">PRESIDES</data:p>
				<data:p V="99313">PROVINCES ESPAGNOLES D'AFRIQUE</data:p>
				<data:p V="99340">RWANDA</data:p>
				<data:p V="99389">SAHARA OCCIDENTAL</data:p>
				<data:p V="99306">SAINTE-HELENE</data:p>
				<data:p V="99394">SAO TOME-ET-PRINCIPE</data:p>
				<data:p V="99341">SENEGAL</data:p>
				<data:p V="99398">SEYCHELLES</data:p>
				<data:p V="99342">SIERRA LEONE</data:p>
				<data:p V="99318">SOMALIE</data:p>
				<data:p V="99343">SOUDAN</data:p>
				<data:p V="99307">SOUDAN ANGLO-EGYPTIEN, KENYA, OUGANDA</data:p>
				<data:p V="99391">SWAZILAND</data:p>
				<data:p V="99325">TANGER</data:p>
				<data:p V="99309">TANZANIE</data:p>
				<data:p V="99344">TCHAD</data:p>
				<data:p V="99345">TOGO</data:p>
				<data:p V="99351">TUNISIE</data:p>
				<data:p V="99346">ZAMBIE</data:p>
				<data:p V="99308">ZANZIBAR</data:p>
				<data:p V="99310">ZIMBABWE</data:p>
				<data:p V="99404">ALASKA</data:p>
				<data:p V="99425">ANGUILLA</data:p>
				<data:p V="99441">ANTIGUA-ET-BARBUDA</data:p>
				<data:p V="99431">ANTILLES NEERLANDAISES</data:p>
				<data:p V="99415">ARGENTINE</data:p>
				<data:p V="99431">ARUBA</data:p>
				<data:p V="99436">BAHAMAS</data:p>
				<data:p V="99434">BARBADE</data:p>
				<data:p V="99429">BELIZE</data:p>
				<data:p V="99425">BERMUDES</data:p>
				<data:p V="99418">BOLIVIE</data:p>
				<data:p V="99416">BRESIL</data:p>
				<data:p V="99425">CAIMAN (ILES)</data:p>
				<data:p V="99401">CANADA</data:p>
				<data:p V="99417">CHILI</data:p>
				<data:p V="99419">COLOMBIE</data:p>
				<data:p V="99406">COSTA RICA</data:p>
				<data:p V="99407">CUBA</data:p>
				<data:p V="99408">DOMINICAINE (REPUBLIQUE)</data:p>
				<data:p V="99438">DOMINIQUE</data:p>
				<data:p V="99414">EL SALVADOR</data:p>
				<data:p V="99420">EQUATEUR</data:p>
				<data:p V="99404">ETATS-UNIS</data:p>
				<data:p V="99427">GEORGIE DU SUD ET LES ILES SANDWICH DU SUD</data:p>
				<data:p V="99435">GRENADE</data:p>
				<data:p V="99430">GROENLAND</data:p>
				<data:p V="99409">GUATEMALA</data:p>
				<data:p V="99428">GUYANA</data:p>
				<data:p V="99410">HAITI</data:p>
				<data:p V="99411">HONDURAS</data:p>
				<data:p V="99426">JAMAIQUE</data:p>
				<data:p V="99403">LABRADOR</data:p>
				<data:p V="99427">MALOUINES, OU FALKLAND (ILES)</data:p>
				<data:p V="99405">MEXIQUE</data:p>
				<data:p V="99425">MONTSERRAT</data:p>
				<data:p V="99412">NICARAGUA</data:p>
				<data:p V="99413">PANAMA</data:p>
				<data:p V="99421">PARAGUAY</data:p>
				<data:p V="99422">PEROU</data:p>
				<data:p V="99432">PORTO RICO</data:p>
				<data:p V="99442">SAINT-CHRISTOPHE-ET-NIEVES</data:p>
				<data:p V="99439">SAINTE-LUCIE</data:p>
				<data:p V="99440">SAINT-VINCENT-ET-LES GRENADINES</data:p>
				<data:p V="99437">SURINAME</data:p>
				<data:p V="99432">TERR. DES ETATS-UNIS D'AMERIQUE EN AMERIQUE</data:p>
				<data:p V="99427">TERR. DU ROYAUME-UNI DANS L'ATLANTIQUE SUD</data:p>
				<data:p V="99402">TERRE-NEUVE</data:p>
				<data:p V="99431">TERRITOIRE DES PAYS-BAS</data:p>
				<data:p V="99425">TERRITOIRES DU ROYAUME-UNI AUX ANTILLES</data:p>
				<data:p V="99433">TRINITE-ET-TOBAGO</data:p>
				<data:p V="99425">TURKS ET CAIQUES (ILES)</data:p>
				<data:p V="99423">URUGUAY</data:p>
				<data:p V="99424">VENEZUELA</data:p>
				<data:p V="99425">VIERGES BRITANNIQUES (ILES)</data:p>
				<data:p V="99432">VIERGES DES ETATS-UNIS (ILES)</data:p>
				<data:p V="99501">AUSTRALIE</data:p>
				<data:p V="99501">CHRISTMAS (ILE)</data:p>
				<data:p V="99501">COCOS ou KEELING (ILES)</data:p>
				<data:p V="99502">COOK (ILES)</data:p>
				<data:p V="99508">FIDJI</data:p>
				<data:p V="99505">GUAM</data:p>
				<data:p V="99504">HAWAII (ILES)</data:p>
				<data:p V="99501">HEARD ET MACDONALD (ILES)</data:p>
				<data:p V="99513">KIRIBATI</data:p>
				<data:p V="99505">MARIANNES DU NORD (ILES)</data:p>
				<data:p V="99515">MARSHALL (ILES)</data:p>
				<data:p V="99516">MICRONESIE (ETATS FEDERES DE)</data:p>
				<data:p V="99507">NAURU</data:p>
				<data:p V="99502">NIUE</data:p>
				<data:p V="99501">NORFOLK (ILE)</data:p>
				<data:p V="99502">NOUVELLE-ZELANDE</data:p>
				<data:p V="99517">PALAOS (ILES)</data:p>
				<data:p V="99510">PAPOUASIE-NOUVELLE-GUINEE</data:p>
				<data:p V="99503">PITCAIRN (ILE)</data:p>
				<data:p V="99512">SALOMON (ILES)</data:p>
				<data:p V="99505">SAMOA AMERICAINES</data:p>
				<data:p V="99506">SAMOA OCCIDENTALES</data:p>
				<data:p V="99505">TERR. DES ETATS-UNIS D'AMERIQUE EN OCEANIE</data:p>
				<data:p V="99502">TOKELAU</data:p>
				<data:p V="99509">TONGA</data:p>
				<data:p V="99511">TUVALU</data:p>
				<data:p V="99514">VANUATU</data:p>
			</data:codePaysINSEE>
			<data:typeAdresse>
				<data:a V="0"></data:a>
				<data:a V="1">principal</data:a>
				<data:a V="2">secondaire</data:a>
			</data:typeAdresse>
			<data:moyenPaiement>
				<data:m type="01">Num&#233;raire</data:m>
				<data:m type="02">Ch&#232;que</data:m>
				<data:m type="03">Virement</data:m>
				<data:m type="04">Virement appli externe</data:m>
				<data:m type="05">Virement gros montant</data:m>
				<data:m type="06">Virement &#224; l&apos;&#233;tranger</data:m>
				<data:m type="07">Op&#233;ration budget rattach&#233;</data:m>
				<data:m type="08">Op&#233;ration d&apos;ordre</data:m>
				<data:m type="09">Autres</data:m>
				<data:m type="10">Pr&#233;l&#232;vement</data:m>
				<data:m type="11">Virement interne</data:m>
			</data:moyenPaiement>
		</data:data>
	</xsl:variable>

	<!-- Correspondance entre le code de pays INSEE et son nom -->
	<xsl:template name="codePaysINSEE" xmlns:data="data.uri">
		<xsl:param name="codePays" />
		<xsl:value-of
			select="$data/data:data/data:codePaysINSEE/data:p[@V=$codePays]/text()" />
	</xsl:template>

	<xsl:template name="slash-date">
		<xsl:param name="datebrute" />
		<date xmlns:data="data.uri" xsl:exclude-result-prefixes="data">
			<xsl:value-of select="substring($datebrute, 9, 2)" />
			<xsl:text>/</xsl:text>
			<xsl:value-of select="substring($datebrute, 6, 2)" />
			<xsl:text>/</xsl:text>
			<xsl:value-of select="substring($datebrute, 1, 4)" />
		</date>
	</xsl:template>
	<xsl:template name="number">
		<xsl:param name="num" />
		<xsl:choose>
			<xsl:when test="string-length(string($num)) = 0" />
			<xsl:when test="number($num) = 0">
				0,00
			</xsl:when>
			<xsl:when test="string(number($num)) = 'NaN'" />
			<xsl:otherwise>
				<xsl:value-of select="format-number($num,'# ##0,00;-# ##0,00','decformat')" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>


	<xsl:template name="CadreEmetteurCreance">
		<xsl:param name="emetteur" />
		<fieldset style="border-color : blue;">
			<legend>Emetteur de la créance</legend>
			<table class="noborder">
				<xsl:if test="string-length($emetteur/n:InfoTiers/n:Nom/@V) &gt; 0">
					<tr>
						<td>
							<xsl:value-of select="$emetteur/n:InfoTiers/n:Nom/@V" />
						</td>
					</tr>
				</xsl:if>
				<xsl:if test="string-length($emetteur/n:InfoTiers/n:ComplNom/@V) &gt; 0">
					<tr>
						<td>
							<xsl:value-of select="$emetteur/n:InfoTiers/n:ComplNom/@V" />
						</td>
					</tr>
				</xsl:if>
				<xsl:if test="string-length($emetteur/n:Adresse/n:Adr1/@V) &gt; 0">
					<tr>
						<td>
							<xsl:value-of select="$emetteur/n:Adresse/n:Adr1/@V" />
						</td>
					</tr>
				</xsl:if>
				<xsl:if test="string-length($emetteur/n:Adresse/n:Adr2/@V) &gt; 0">
					<tr>
						<td>
							<xsl:value-of select="$emetteur/n:Adresse/n:Adr2/@V" />
						</td>
					</tr>
				</xsl:if>
				<xsl:if test="string-length($emetteur/n:Adresse/n:Adr3/@V) &gt; 0">
					<tr>
						<td>
							<xsl:value-of select="$emetteur/n:Adresse/n:Adr3/@V" />
						</td>
					</tr>
				</xsl:if>
				<tr>
					<td>
						<xsl:if test="string-length($emetteur/n:Adresse/n:CP/@V) &gt; 0">
							<xsl:value-of select="$emetteur/n:Adresse/n:CP/@V" /> &nbsp;
						</xsl:if>
						<xsl:if test="string-length($emetteur/n:Adresse/n:Ville/@V) &gt; 0">
							<xsl:value-of select="$emetteur/n:Adresse/n:Ville/@V" />
						</xsl:if>
					</td>
				</tr>
				<xsl:if test="string-length($emetteur/n:InfoSuppl/n:NumTel/@V) &gt; 0">
					<tr>
						<td>
							Téléphone :
							<xsl:value-of select="$emetteur/n:InfoSuppl/n:NumTel/@V" />
						</td>
					</tr>
				</xsl:if>
				<xsl:if
					test="string-length($emetteur/n:InfoSuppl/n:HorairesOuv/@V) &gt; 0">
					<tr>
						<td>
							Horaires d'ouverture :
							<xsl:value-of select="$emetteur/n:InfoSuppl/n:HorairesOuv/@V" />
						</td>
					</tr>
				</xsl:if>
			</table>
		</fieldset>
	</xsl:template>



	<xsl:template name="CadreDestinatairePaiement">
		<xsl:param name="destinataire" />
		<xsl:param name="entetePes" />
		<fieldset style="border-color : red;">
			<legend>Destinataire de votre paiement</legend>
			<table class="noborder">
				<tr>
					<td>Centre des Finances Publiques</td>
				</tr>

				<xsl:if test="$destinataire/n:InfoTiers/n:TypTiers/@V = '26'">
					<xsl:if test="string-length($destinataire/n:InfoTiers/n:Nom/@V) &gt; 0">
						<tr>
							<td>
								<xsl:value-of select="$destinataire/n:InfoTiers/n:Nom/@V" />
							</td>
						</tr>
					</xsl:if>
					<xsl:if
						test="string-length($destinataire/n:InfoTiers/n:ComplNom/@V) &gt; 0">
						<tr>
							<td>
								<xsl:value-of select="$destinataire/n:InfoTiers/n:ComplNom/@V" />
							</td>
						</tr>
					</xsl:if>
				</xsl:if>

				<xsl:if test="string-length($destinataire/n:Adresse/n:Adr1/@V) &gt; 0">
					<tr>
						<td>
							<xsl:value-of select="$destinataire/n:Adresse/n:Adr1/@V" />
						</td>
					</tr>
				</xsl:if>
				<xsl:if test="string-length($destinataire/n:Adresse/n:Adr2/@V) &gt; 0">
					<tr>
						<td>
							<xsl:value-of select="$destinataire/n:Adresse/n:Adr2/@V" />
						</td>
					</tr>
				</xsl:if>
				<xsl:if test="string-length($destinataire/n:Adresse/n:Adr3/@V) &gt; 0">
					<tr>
						<td>
							<xsl:value-of select="$destinataire/n:Adresse/n:Adr3/@V" />
						</td>
					</tr>
				</xsl:if>
				<tr>
					<td>
						<xsl:if test="string-length($destinataire/n:Adresse/n:CP/@V) &gt; 0">
							<xsl:value-of select="$destinataire/n:Adresse/n:CP/@V" /> &nbsp;
						</xsl:if>
						<xsl:if test="string-length($destinataire/n:Adresse/n:Ville/@V) &gt; 0">
							<xsl:value-of select="$destinataire/n:Adresse/n:Ville/@V" />
						</xsl:if>
					</td>
				</tr>
				<xsl:if
					test="string-length($destinataire/n:InfoSuppl/n:NumTel/@V) &gt; 0">
					<tr>
						<td>
							Téléphone :
							<xsl:value-of select="$destinataire/n:InfoSuppl/n:NumTel/@V" />
						</td>
					</tr>
				</xsl:if>
				<xsl:if
					test="string-length($destinataire/n:InfoSuppl/n:HorairesOuv/@V) &gt; 0">
					<tr>
						<td>
							Horaires d'ouverture :
							<xsl:value-of select="$destinataire/n:InfoSuppl/n:HorairesOuv/@V" />
						</td>
					</tr>
				</xsl:if>
			</table>
		</fieldset>
	</xsl:template>





	<xsl:template name="CadreReferenceARappeler">
		<xsl:param name="factureIndiv" />
		<xsl:param name="entetePes" />

		Références à rappeler
		<table class="withborder" cellpadding="0" cellspacing="0">
			<tr>
				<td>Budget</td>
				<td>Exercice</td>
				<td>N°bordereau</td>
				<td>N°titre</td>
			</tr>
			<tr>
				<td>
					<xsl:if test="string-length($entetePes/n:CodCol/@V) &gt; 0">
						<xsl:value-of select="$entetePes/n:CodCol/@V" />
					</xsl:if>
					<xsl:if test="string-length($entetePes/n:CodBud/@V) &gt; 0">
						<xsl:value-of select="$entetePes/n:CodBud/@V" />
					</xsl:if>
				</td>
				<td>
					<xsl:if test="string-length($factureIndiv/../n:Annee/@V) &gt; 0">
						<xsl:value-of select="$factureIndiv/../n:Annee/@V" />
					</xsl:if>
				</td>
				<td>
					<xsl:if
						test="string-length($factureIndiv/n:RefTitre/n:NumBordereau/@V) &gt; 0">
						<xsl:value-of select="$factureIndiv/n:RefTitre/n:NumBordereau/@V" />
					</xsl:if>
				</td>
				<td>
					<xsl:if
						test="string-length($factureIndiv/n:RefTitre/n:NumTitre/@V) &gt; 0">
						<xsl:value-of select="$factureIndiv/n:RefTitre/n:NumTitre/@V" />
					</xsl:if>
				</td>
			</tr>
		</table>
		<br />

		Date d'émission du titre de recette :
		<xsl:if test="string-length($factureIndiv/n:DateEmission/@V) &gt; 0">
			<xsl:value-of select="$factureIndiv/n:DateEmission/@V" />
		</xsl:if>

	</xsl:template>


	<xsl:template name="CadrePaiement">
		<xsl:param name="pesFacture" />
		<xsl:param name="refTitre" />

		<fieldset>
			<table class="noborder">
				<tr>
					<td>
						<span class="thick">Adresse de paiement par Internet : </span>
						<xsl:if
							test="string-length($pesFacture/n:Emetteur/n:InfoSuppl/n:AdrTipi/@V) &gt; 0">
							<xsl:value-of select="$pesFacture/n:Emetteur/n:InfoSuppl/n:AdrTipi/@V" />
						</xsl:if>
					</td>
				</tr>
				<tr>
					<td>
						<span class="thick">Identifiant collectivité : </span>
						<xsl:if
							test="string-length($pesFacture/n:Emetteur/n:InfoSuppl/n:CodeTipi/@V) &gt; 0">
							<xsl:value-of select="$pesFacture/n:Emetteur/n:InfoSuppl/n:CodeTipi/@V" />
						</xsl:if>
					</td>
				</tr>
				<tr>
					<td>
						<span class="thick">Référence : </span>
						<xsl:if
							test="$pesFacture/n:Emetteur/n:InfoSuppl/n:AdrTipi/@V = 'www.tipi.budget.gouv.fr'">
							<xsl:value-of select="$refTitre/n:Exercice/@V" />
							-
							<xsl:value-of select="$refTitre/n:NumTitre/@V" />
							-1
						</xsl:if>
					</td>
				</tr>
			</table>
		</fieldset>
	</xsl:template>

	<xsl:template name="CadreSignataire">
		<xsl:param name="signataire" />

		<FONT size="10pt">
			<xsl:if test="string-length($signataire/n:Personne/n:Nom/@V) &gt; 0">
				<xsl:value-of select="$signataire/n:Personne/n:Nom/@V" />
			</xsl:if> &nbsp;
			<xsl:if test="string-length($signataire/n:Personne/n:Prenom/@V) &gt; 0">
				<xsl:value-of select="$signataire/n:Personne/n:Prenom/@V" />
			</xsl:if>,&nbsp;
			<xsl:if test="string-length($signataire/n:Role/@V) &gt; 0">
				<xsl:value-of select="$signataire/n:Role/@V" />
			</xsl:if>

		</FONT>
	</xsl:template>


	<xsl:template name="CadreTalonPaiement">
		<xsl:param name="encaissement" />
		<xsl:param name="entetePes" />
		<xsl:param name="facture" />
		<xsl:param name="pesFacture" />

		<fieldset style="border-color : red;">
			<legend>Talon de paiement / TIP SEPA</legend>
			<table class="noborder">
				<tr>
					<td>Centre d'encaissement :</td>
				</tr>

				<xsl:if test="$encaissement/n:InfoTiers/n:TypTiers/@V = '25'">
					<xsl:if test="string-length($encaissement/n:InfoTiers/n:Nom/@V) &gt; 0">
						<tr>
							<td>
								<xsl:value-of select="$encaissement/n:InfoTiers/n:Nom/@V" />
							</td>
						</tr>
					</xsl:if>
				</xsl:if>

				<xsl:if test="string-length($encaissement/n:Adresse/n:Adr1/@V) &gt; 0">
					<tr>
						<td>
							<xsl:value-of select="$encaissement/n:Adresse/n:Adr1/@V" />
						</td>
					</tr>
				</xsl:if>
				<xsl:if test="string-length($encaissement/n:Adresse/n:Adr2/@V) &gt; 0">
					<tr>
						<td>
							<xsl:value-of select="$encaissement/n:Adresse/n:Adr2/@V" />
						</td>
					</tr>
				</xsl:if>
				<xsl:if test="string-length($encaissement/n:Adresse/n:Adr3/@V) &gt; 0">
					<tr>
						<td>
							<xsl:value-of select="$encaissement/n:Adresse/n:Adr3/@V" />
						</td>
					</tr>
				</xsl:if>
				<tr>
					<td>
						<xsl:if test="string-length($encaissement/n:Adresse/n:CP/@V) &gt; 0">
							<xsl:value-of select="$encaissement/n:Adresse/n:CP/@V" /> &nbsp;
						</xsl:if>
						<xsl:if test="string-length($encaissement/n:Adresse/n:Ville/@V) &gt; 0">
							<xsl:value-of select="$encaissement/n:Adresse/n:Ville/@V" />
						</xsl:if>
					</td>
				</tr>


				<tr>
					<td>Emetteur :</td>
				</tr>

				<tr>
					<td>
						ICS :
						<xsl:if test="$facture/n:ModeRecouvrement/@V = 0">
							<xsl:if
								test="string-length($pesFacture/n:Emetteur/n:InfoSuppl/n:RefBdf/@V) &gt; 0">
								<xsl:value-of select="$pesFacture/n:Emetteur/n:InfoSuppl/n:RefBdf/@V" />
							</xsl:if>
						</xsl:if>
					</td>
				</tr>

				<tr>
					<td>
						RUM : TIPSEPA
						<xsl:call-template name="substringLastChar">
							<xsl:with-param name="nbChar" select="6" />
							<xsl:with-param name="input" select="$entetePes/n:IdPost/@V" />
						</xsl:call-template>
						<xsl:value-of select="$entetePes/n:CodCol/@V" />
						<xsl:value-of select="$entetePes/n:CodBud/@V" />
						<xsl:call-template name="substringLastChar">
							<xsl:with-param name="nbChar" select="8" />
							<xsl:with-param name="input"
								select="concat('00000000', $facture/n:RefTitre/n:NumTitre/@V)" />
						</xsl:call-template>
						<xsl:call-template name="substringLastChar">
							<xsl:with-param name="nbChar" select="6" />
							<xsl:with-param name="input"
								select="concat('000000', $facture/n:RefTitre/n:NumLigne/@V)" />
						</xsl:call-template>
						<xsl:call-template name="substringLastChar">
							<xsl:with-param name="nbChar" select="2" />
							<xsl:with-param name="input"
								select="$facture/n:RefTitre/n:Exercice/@V" />
						</xsl:call-template>
						T
					</td>
				</tr>
				<tr>
					<td>
						n°émetteur =
						<xsl:if test="$facture/n:ModeRecouvrement/@V = 1">
							<xsl:if
								test="string-length($pesFacture/n:Emetteur/n:InfoSuppl/n:RefBdf/@V) &gt; 0">
								<xsl:value-of select="$pesFacture/n:Emetteur/n:InfoSuppl/n:RefBdf/@V" />
							</xsl:if>
						</xsl:if>
					</td>
				</tr>

				<tr>
					<td>
						<table style="margin-left:0px;">
							<tr>
								<td>Lignes optiques :</td>
								<td>
									<xsl:if
										test="string-length($facture/n:TalonOptique2Lignes/n:Ligne1/@V) &gt; 0">
										<xsl:value-of select="$facture/n:TalonOptique2Lignes/n:Ligne1/@V" />
									</xsl:if>
								</td>
							</tr>
							<tr>
								<td>&nbsp;
								</td>
								<td>
									<xsl:if
										test="string-length($facture/n:TalonOptique2Lignes/n:Ligne2/@V) &gt; 0">
										<xsl:value-of select="$facture/n:TalonOptique2Lignes/n:Ligne2/@V" />
									</xsl:if>
								</td>
							</tr>
						</table>
					</td>
				</tr>



			</table>
		</fieldset>
	</xsl:template>


	<xsl:template name="substringLastChar">
		<xsl:param name="nbChar" />
		<xsl:param name="input" />
		<xsl:choose>
			<xsl:when test="string-length($input) &gt; $nbChar">
				<xsl:value-of
					select="substring($input, string-length($input) - $nbChar + 1)" />
			</xsl:when>
			<xsl:when test="string-length($input) = $nbChar">
				<xsl:value-of
					select="substring($input, string-length($input) - $nbChar + 1)" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$input" />
			</xsl:otherwise>
		</xsl:choose>

	</xsl:template>



	<xsl:template name="CadreCentreFinancesPublique">
		<xsl:param name="destinataire" />
		<xsl:param name="entetePes" />
		<xsl:param name="debiteur" />


		<p class="thick">AVIS DES SOMMES A PAYER</p>
		<br />
		<table class="noborder">
			<tr>
				<td>Centre des Finances Publiques</td>
			</tr>
			<xsl:if test="string-length($destinataire/n:InfoTiers/n:Nom/@V) &gt; 0">
				<tr>
					<td>
						<xsl:value-of select="$destinataire/n:InfoTiers/n:Nom/@V" />
					</td>
				</tr>
			</xsl:if>
			<xsl:if
				test="string-length($destinataire/n:InfoTiers/n:ComplNom/@V) &gt; 0">
				<tr>
					<td>
						<xsl:value-of select="$destinataire/n:InfoTiers/n:ComplNom/@V" />
					</td>
				</tr>
			</xsl:if>
			<xsl:if test="string-length($destinataire/n:Adresse/n:Adr1/@V) &gt; 0">
				<tr>
					<td>
						<xsl:value-of select="$destinataire/n:Adresse/n:Adr1/@V" />
					</td>
				</tr>
			</xsl:if>
			<xsl:if test="string-length($destinataire/n:Adresse/n:Adr2/@V) &gt; 0">
				<tr>
					<td>
						<xsl:value-of select="$destinataire/n:Adresse/n:Adr2/@V" />
					</td>
				</tr>
			</xsl:if>
			<xsl:if test="string-length($destinataire/n:Adresse/n:Adr3/@V) &gt; 0">
				<tr>
					<td>
						<xsl:value-of select="$destinataire/n:Adresse/n:Adr3/@V" />
					</td>
				</tr>
			</xsl:if>
			<tr>
				<td>
					<xsl:if test="string-length($destinataire/n:Adresse/n:CP/@V) &gt; 0">
						<xsl:value-of select="$destinataire/n:Adresse/n:CP/@V" /> &nbsp;
					</xsl:if>
					<xsl:if test="string-length($destinataire/n:Adresse/n:Ville/@V) &gt; 0">
						<xsl:value-of select="$destinataire/n:Adresse/n:Ville/@V" />
					</xsl:if>
				</td>
			</tr>

		</table>
		<br />
		<table class="noborder">

			<tr>
				<td>
					<xsl:if test="string-length($debiteur/n:InfoTiers/n:Civilite/@V) &gt; 0">
						<xsl:value-of select="$debiteur/n:InfoTiers/n:Civilite/@V" /> &nbsp;
					</xsl:if>
					<xsl:if test="string-length($debiteur/n:InfoTiers/n:Nom/@V) &gt; 0">
						<xsl:value-of select="$debiteur/n:InfoTiers/n:Nom/@V" /> &nbsp;
					</xsl:if>
					<xsl:if test="string-length($debiteur/n:InfoTiers/n:Prenom/@V) &gt; 0">
						<xsl:value-of select="$debiteur/n:InfoTiers/n:Prenom/@V" />
					</xsl:if>
				</td>
			</tr>

			<xsl:if test="string-length($debiteur/n:InfoTiers/n:ComplNom/@V) &gt; 0">
				<tr>
					<td>
						<xsl:value-of select="$debiteur/n:InfoTiers/n:ComplNom/@V" />

					</td>
				</tr>
			</xsl:if>
			<xsl:if test="string-length($debiteur/n:Adresse/n:Adr1/@V) &gt; 0">
				<tr>
					<td>
						<xsl:value-of select="$debiteur/n:Adresse/n:Adr1/@V" />
					</td>
				</tr>
			</xsl:if>
			<xsl:if test="string-length($debiteur/n:Adresse/n:Adr2/@V) &gt; 0">
				<tr>
					<td>
						<xsl:value-of select="$debiteur/n:Adresse/n:Adr2/@V" />
					</td>
				</tr>
			</xsl:if>
			<xsl:if test="string-length($debiteur/n:Adresse/n:Adr3/@V) &gt; 0">
				<tr>
					<td>
						<xsl:value-of select="$debiteur/n:Adresse/n:Adr3/@V" />
					</td>
				</tr>
			</xsl:if>
			<tr>
				<td>
					<xsl:if test="string-length($debiteur/n:Adresse/n:CP/@V) &gt; 0">
						<xsl:value-of select="$debiteur/n:Adresse/n:CP/@V" /> &nbsp;
					</xsl:if>
					<xsl:if test="string-length($debiteur/n:Adresse/n:Ville/@V) &gt; 0">
						<xsl:value-of select="$debiteur/n:Adresse/n:Ville/@V" />
					</xsl:if>
				</td>
			</tr>
		</table>

	</xsl:template>




	<xsl:template name="FAC_CadreFacture">
		<xsl:param name="facture" />
		<table class="facture" cellpadding="0" cellspacing="0">
			<tr>
				<td class="facturecolonne1" style="border-bottom: solid 1px #000000;"
					align="center">Objet</td>
				<td class="facturecolonne2" style="border-bottom: solid 1px #000000;"
					align="center">Prix unitaire</td>
				<td class="facturecolonne3" style="border-bottom: solid 1px #000000;"
					align="center">Qté. 1</td>
				<td class="facturecolonne4" style="border-bottom: solid 1px #000000;"
					align="center">Qté. 2</td>
				<td class="facturecolonne5" style="border-bottom: solid 1px #000000;"
					align="center">Montant total HT</td>
				<td class="facturecolonne6" style="border-bottom: solid 1px #000000;"
					align="center">TVA</td>
				<td class="facturecolonne7" style="border-bottom: solid 1px #000000;"
					align="center">Montant TTC</td>
			</tr>

			<!-- LIGNES DE FACTURE DE CETTE FACTURE -->
			<xsl:for-each select="$facture/n:LigneFacture">
				<tr>
					<td class="facturecolonne1">
						<xsl:value-of select="n:Libelle/@V" />
						-
						<xsl:value-of select="n:DateDebut/@V" />
						-
						<xsl:value-of select="n:DateFin/@V" />
					</td>
					<td class="facturecolonne2" align="right">
						<xsl:if test="n:MtUnitaire/@V">
							<xsl:value-of
								select="format-number(n:MtUnitaire/@V,'# ##0,00','decformat')" />
						</xsl:if>
					</td>
					<td class="facturecolonne3" align="right">
						<xsl:if test="n:Quantite/@V">
							<xsl:value-of select="n:Quantite/@V" />
						</xsl:if>
						<xsl:if test="n:Unite1/@V">
							<xsl:value-of select="n:Unite1/@V" />
						</xsl:if>
					</td>
					<td class="facturecolonne4" align="right">
						<xsl:if test="n:Quantite2/@V">
							<xsl:value-of select="./n:Quantite2/@V" />
						</xsl:if>
						<xsl:if test="n:Unite2/@V">
							<xsl:value-of select="n:Unite2/@V" />
						</xsl:if>
					</td>
					<td class="facturecolonne5" align="right">
						<xsl:if test="n:MtHT/@V">
							<xsl:value-of select="format-number(n:MtHT/@V,'# ##0,00','decformat')" />
						</xsl:if>
					</td>
					<td class="facturecolonne6" align="right">
						<xsl:if test="n:MtTVA/@V">
							<xsl:value-of select="format-number(n:MtTVA/@V,'# ##0,00','decformat')" />
						</xsl:if>
					</td>
					<td class="facturecolonne7" align="right">
						<xsl:if test="n:MtTTC/@V">
							<xsl:value-of select="format-number(n:MtTTC/@V,'# ##0,00','decformat')" />
						</xsl:if>
					</td>
				</tr>
			</xsl:for-each>
			<!-- REFERENCES DIVERS -->

		</table>
		<!-- TOTAUX -->
		<!-- <xsl:variable name="lignes_5_5" select="n:LigneFacture[n:TauxTVA/@V='5.5']"/>
			<xsl:variable name="lignes_19_6" select="n:LigneFacture[n:TauxTVA/@V='19.6']"/>
			<xsl:if test="$lignes_5_5!='' or $lignes_19_6!=''"> <div class="total"> <table
			style="text-align: left;" class="tableau3"> <tbody> <tr> <td class="cell_titre_col"
			colspan="2">Totaux T.V.A par Taux</td> </tr> <xsl:if test="$lignes_5_5!=''">
			<tr> <td class="cell_tableau1">5.5&nbsp;%</td> <td class="cell_numeric_g">
			<xsl:apply-templates select="$lignes_5_5"/> <xsl:value-of select="format-number(sum($lignes_5_5/n:MtTVA/@V),'#
			##0,00','decformat')"/> </td> </tr> </xsl:if> <xsl:if test="$lignes_19_6!=''">
			<tr> <td class="cell_tableau1">19.6&nbsp;%</td> <td class="cell_numeric_g">
			<xsl:apply-templates select="$lignes_19_6"/> <xsl:value-of select="format-number(sum($lignes_19_6/n:MtTVA/@V),'#
			##0,00','decformat')"/> </td> </tr> </xsl:if> </tbody> </table> </div> </xsl:if> -->

		<!--  <div class="total"> -->
			<table style="text-align: left;" class="tableau3" align="right">
				<tbody>
					<tr>
						<td class="cell_titre_col" colspan="2">Total</td>
					</tr>
					<tr>
						<td class="cell_tableau1">Montant HT</td>
						<td class="cell_numeric_g">
							<xsl:value-of
								select="format-number(n:TotalAPayer/n:MtTotalHT/@V,'# ##0,00','decformat')" />
						</td>
					</tr>
					<tr>
						<td class="cell_tableau1">Montant TVA</td>
						<td class="cell_numeric_g">
							<xsl:value-of
								select="format-number(n:TotalAPayer/n:MtTotalTVA/@V,'# ##0,00','decformat')" />
						</td>
					</tr>
					<tr>
						<td class="cell_tableau1">Montant TTC</td>
						<td class="cell_numeric_g">
							<xsl:value-of
								select="format-number(n:TotalAPayer/n:MtTotalTTC/@V,'# ##0,00','decformat')" />
						</td>
					</tr>
				</tbody>
			</table>
		<!--</div> -->

	</xsl:template>

	<xsl:template name="CadreVerso1">
		<xsl:param name="encaissement"/>
		<xsl:param name="facture"/>
		<xsl:param name="comptable"/>

		<p>
			Comment régler votre dette auprès de l'organisme public : <br/>
			<ul>
				<li>Si l'organisme public offre la possibilité de la payer par internet, au moyen d'une carte bancaire, vous êtes invité(e) à vous connecter à l'adresse électronique mentionnée dans le cadre concerné au recto ;</li>
				<li>Sinon, il vous est recommandé de payer par titre interbancaire de paiement (TIP), en détachant le talon en bas du recto du présent avis, en le datant et le signant dans l'encadré indiqué. Si vos coordonnées bancaires ne sont pas mentionnées en haut à gauche de ce TIP, joignez le relevé d'identité bancaire du compte sur lequel sera prélevée cette créance. Le tout est à envoyer à l'adresse mentionnée sur le TIP ;</li>
				<li>Si vous réglez par chèque, libellez-le à l'ordre du trésor public et joignez-le TIP non signé et non agrafé, sans aucun autre document. Le tout est à envoyer à l'adresse mentionnée sur le TIP ;</li>
				<li>Si vous réglez par virement bancaire, faites le vers le compte bancaire du comptable public (BIC/IBAN : <xsl:value-of select="$comptable/CpteBancaire/BIC/@V"/>&nbsp;<xsl:value-of select="$comptable/CpteBancaire/IBAN/@V"/>) en indiquant, en zone objet / libellé les références à rappeler mentionnées au recto ;</li>
				<li>Si vous réglez en espèces (dans la limite de 300€) auprès du guichet du comptable public indiqué au recto ou auprès d'un autre Centre des Finances Publiques, munissez-vous du présent avis ;</li>
				<li>Si vous souhaitez que vos dettes futures soient prélevées automatiquement sur votre compte bancaire, et si la collectivité offre cette possibilité, la démarche est la suivante : <xsl:value-of select="$facture/ModalitesReglement/@V"/>.</li>
			</ul>
		</p>
	</xsl:template>

	<xsl:template name="CadreVerso2">
		<xsl:param name="encaissement"/>
		<xsl:param name="facture"/>

		<p>
			Comment contester ou vous renseigner sur votre dette envers l'organisme public : <br/>
			<ul>
				<li>Pour tout renseignement complémentaire sur la créance dont le paiement vous est réclamé, vous devez contacter le service émetteur de la créance indiqué au recto du présent avis ;</li>
				<li><xsl:value-of select="$facture/ModalitesContestation/@V"/></li>
				<li>Toute somme non acquittée dans le délai de 30 jours de la réception du présent avis fera l'objet de poursuites engagées par le comptable public indiqué au recto (seul celui-ci peut accorder un délai de paiement dans des cas exceptionnels dûment justifiés par vous). Pour contester ces poursuites, vous devez déposer un recours devant le juge de l'exécution mentionnés aux articles L. 213-5 et L. 213.6 du code de l'organisation judiciaire dans un délai de deux mois suivant la notification de l'acte contesté (cf 2° de l'article L.1617-5 du code général des collectivités locales).</li>
			</ul>
		</p>
	</xsl:template>

	<!-- début du document -->
	<xsl:template match="/n:PES_Aller">
		<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
			<head>
				<style type="text/css">
					@page {
					size: 29.7cm 21cm;
					margins: 0.5 0.5 0.5 0.5;
					}
					body {
					width: 29cm;
					font-family: verdana, sans-serif;
					font-size:
					10px;
					background-color: #FFFFFF;
					}
					div.divpage {
					width: 29cm;
					margin: 0
					0 0 0;
					}


					fieldset{
					border:2px solid;
					-moz-border-radius:8px;
					-webkit-border-radius:8px;
					border-radius:8px;
					}

					table.noborder tr td {
					border : none;
					}

					table.withborder tr td {
					border : solid 1px;
					margin:0px;
					}


					div.seul{

					display:block;
					width:90%;
					}

					div.demi{
					display:inline-block;
					width:45%;
					}

					p.thick {
					font-weight: bold;
					}

					span.thick {
					font-weight: bold;
					}

					table.entete {
					margin: 0;
					border-style: none;
					border-width:
					0px;
					border-color:
					black;
					width:
					100%;
					font-size: 14px;
					}
					anomalie {
					background-color:
					#FFFF66; }
					tr {
					margin:
					0; }
					td {
					margin: 0;
					border-style: inset;
					border-width: 1px;
					border-color: black;
					vertical-align: top;
					line-height: 13px;
					}
					td.entetecolonne1 {
					width:
					1.5cm;
					border-style:
					none;
					}
					td.entetecolonne2 {
					width: 1.5cm;
					border-style: none;
					}
					td.entetecolonne3 {
					width: 4cm;
					border-style:
					none;
					}
					td.entetecolonne4
					{
					width: 1cm;
					border-style: none;
					}
					td.entetecolonne5 {
					width: 3.5cm;
					border-style: none;
					}
					td.entetecolonne6 {
					width: 7.5cm;
					border-style:
					none;
					}

					table.fournisseur {
					margin: 0;
					border-style: solid;;
					border-width:
					1px;
					border-color: black;
					width: 8.5cm;
					font-size: 12px;
					align: left;
					}
					td.fournisseurcolonne1 {
					width: 3cm;
					border-style: none;
					text-align:
					right;
					}
					td.fournisseurcolonne2 {
					width: 5.5cm;
					border-style: none;
					text-align: left;
					}

					table.facture {
					margin: 0;
					border-style: solid;
					border-width: 1px;
					border-color: #000000;
					width:
					100%;
					font-size:
					12px;
					align: left;
					}
					td.facturecolonneTitre1 {
					width:
					4cm;
					border-top:
					none;
					border-left: none;
					border-bottom: solid 1px
					#000000;
					border-right:
					none;
					}
					td.facturecolonneTitre2 {
					width: 1.5cm;
					border-top: none;
					border-left: solid 1px #000000;
					border-bottom:
					solid 1px #000000;
					border-right: none;
					}
					td.facturecolonne1 {
					width:
					4cm;
					border-top: none;
					border-left: none;
					border-bottom: none;
					border-right: none;
					}
					td.facturecolonne2 {
					width: 1.5cm;
					border-top:
					none;
					border-left: solid
					1px #000000;
					border-bottom: none;
					border-right: none;
					}
					td.facturecolonne3 {
					width: 1.25cm;
					border-top:
					none;
					border-left:
					solid 1px #000000;;
					border-bottom: none;
					border-right: none;
					}
					td.facturecolonne4 {
					width: 1.25cm;
					border-top:
					none;
					border-left:
					solid
					1px #000000;
					border-bottom: none;
					border-right: none;
					}
					td.facturecolonne5 {
					width: 1.25cm;
					border-top:
					none;
					border-left:
					solid
					1px #000000;
					border-bottom: none;
					border-right: none;
					}
					td.facturecolonne6 {
					width: 1.25cm;
					border-top:
					none;
					border-left:
					solid 1px #000000;
					border-bottom: none;
					border-right: none;
					}
					td.facturecolonne7 {
					width: 1.25cm;
					border-top:
					none;
					border-left:
					solid 1px #000000;
					border-bottom: none;
					border-right: none;
					}
					td.facturecolonne8 {
					width: 1.25cm;
					border-top:
					none;
					border-left:
					solid 1px #000000;
					border-bottom: none;
					border-right: none;
					}
					td.facturecolonne9 {
					width: 1.25cm;
					border-top:
					none;
					border-left:
					solid 1px #000000;
					border-bottom: none;
					border-right: none;
					}
					td.facturecolonne10 {
					width: 1.25cm;
					border-top:
					none;
					border-left:
					solid 1px #000000;
					border-bottom: none;
					border-right: solid 1px
					#000000;
					}
					td.facturecolonne11 {
					width: 1.25cm;
					border-style: none;
					}

					.bordHaut { border-top: solid 1px #000000; }
					.bordGauche {
					border-left: solid 1px #000000; }
					.bordBas {
					border-bottom: solid
					1px
					#000000; }
					.bordDroit { border-right: solid
					1px #000000; }

					noupper {
					border-top-style: none; }
					:link { color:
					#00c; background:
					transparent }
					:visited { color: #609; background:
					transparent }
					a:active { color: #c00; background: transparent }
					a:link img,
					a:visited img { border-style: none }
					.gras { font-weight:
					bold; }
					.gauche { text-align: left; }
					.droite { text-align: right; }
					.nomBanque {border: solid 1px #000000; text-align: center;
					font-size: 12px;
					font-weight: bold;}

					.total {
					position: relative;
					left: 20,3cm;
					width: 6cm;
					align: right;
					}

					.tableau3 { border-style: none;
					border-width: 0px;
					font-size: 12px;
					font-family: Verdana;
					font-style:
					normal;
					}
					.cell_titre_col {border-top-style: solid;
					border-bottom-style: solid;
					border-top-width: 1px;
					border-bottom-width: 1px;
					font-weight: bold;
					text-align: center;
					background-color: rgb(202, 202, 202);

					}
					.cell_numeric_g
					{border-top-style: solid;
					border-bottom-style: solid;
					border-top-width: 1px;
					border-bottom-width: 1px;
					text-align: right;
					font-weight: bold;
					width: 3.5cm;
					}

					.cell_tableau1 {
					border-top-style:
					solid;
					border-bottom-style: solid;
					border-top-width: 1px;
					border-bottom-width: 1px;
					width: 2.5cm;
					}

				</style>
			</head>
			<body>
				<div class="divpage">



					<xsl:for-each
						select="if (not($numeroFacture)) then n:PES_Facture/n:FactureIndiv else n:PES_Facture/n:FactureIndiv[n:NumeroFacture/@V = $numeroFacture]">
						<div class="demi">
							<!-- FOURNISSEUR -->
							<xsl:call-template name="CadreEmetteurCreance">
								<xsl:with-param name="emetteur" select="../n:Emetteur" />
							</xsl:call-template>

							<xsl:call-template name="CadreDestinatairePaiement">
								<xsl:with-param name="destinataire"
									select="../n:Tiers[n:InfoTiers/n:TypTiers/@V=26]" />
								<xsl:with-param name="entetePes" select="../../n:EnTetePES" />
							</xsl:call-template>

						</div>
						<div class="demi">
							<xsl:call-template name="CadreCentreFinancesPublique">
								<xsl:with-param name="destinataire"
									select="../n:Tiers[n:InfoTiers/n:TypTiers/@V=26]" />
								<xsl:with-param name="entetePes" select="../../n:EnTetePES" />
								<xsl:with-param name="debiteur" select="n:Debiteur" />
							</xsl:call-template>

						</div>
						<div class="seul">
							Madame, Monsieur,
							<br />
							En application des
							articles L.252 A
							du livre des procédures
							fiscales et L.1617-5 du
							code général des collectivités
							territoriales, j'ai émis et rendu
							exécutoire un titre
							de recette
							pour recouvrer la créance dont les
							<br />
							caractéristiques sont
							les
							suivantes :

						</div>
					<!--  <div class="seul">-->
							<div class="demi">
								<xsl:call-template name="CadreReferenceARappeler">
									<xsl:with-param name="factureIndiv" select="." />
									<xsl:with-param name="entetePes" select="../../n:EnTetePES" />
								</xsl:call-template>
							</div>
							<div class="demi">
								<xsl:call-template name="CadrePaiement">
									<xsl:with-param name="pesFacture" select=".." />
									<xsl:with-param name="refTitre" select="n:RefTitre" />
								</xsl:call-template>
							</div>
						<!-- </div>-->

						<!-- FACTURE -->
						<div class="seul">
							<xsl:call-template name="FAC_CadreFacture">
								<xsl:with-param name="facture" select="." />
							</xsl:call-template>
						</div>

						<div class="seul">
							A compter du présent avis, vous disposez d'un délai de :
							<br />
							- trente jours pour payer cette somme au comptable public selon
							les modalités détaillées au verso ;
							<br />
							- deux mois pour éventuellement contester ce titre de recette,
							selon les modalités détaillées au verso.
							<br />
							Mes services se tiennent à votre disposition pour tout
							renseignement supplémentaire.
							<br />
							Je vous prie de croire, Madame,
							Monsieur, à l'assurance de ma
							considération distinguée.
						</div>

						<br />
						<div class="seul">
							<xsl:call-template name="CadreSignataire">
								<xsl:with-param name="signataire" select="../n:Emetteur/n:Signataire" />
							</xsl:call-template>

						</div>

						<div class="seul">
							<xsl:call-template name="CadreTalonPaiement">
								<xsl:with-param name="encaissement"
									select="../n:Tiers[n:InfoTiers/n:TypTiers/@V=25]" />
								<xsl:with-param name="entetePes" select="../../n:EnTetePES" />
								<xsl:with-param name="facture" select="." />
								<xsl:with-param name="pesFacture" select=".." />
							</xsl:call-template>
						</div>

						<div class="seul">
							<xsl:call-template name="CadreVerso1">
								<xsl:with-param name="encaissement" select="../Tiers[InfoTiers/TypTiers/@V = 25]"/>
								<xsl:with-param name="facture" select="."/>
								<xsl:with-param name="comptable" select="../Tiers[InfoTiers/TypTiers/@V = 26]"/>
							</xsl:call-template>
						</div>

						<div class="seul">
							<xsl:call-template name="CadreVerso2">
								<xsl:with-param name="encaissement" select="../Tiers[InfoTiers/TypTiers/@V = 25]"/>
								<xsl:with-param name="facture" select="."/>
							</xsl:call-template>
						</div>

					</xsl:for-each>
				</div>
			</body>

		</html>
	</xsl:template>

	<!-- pour le moyen de paiement -->
	<xsl:template name="moyenPaiement">
		<xsl:param name="typeMoyen" />
		<xsl:value-of
			select="$data/data:data/data:moyenPaiement/data:m[@type=$typeMoyen]/text()" />
	</xsl:template>

	<xsl:function name="fn:iso-date">
		<xsl:param name="datebrute" />
		<xsl:choose>
			<xsl:when
				test="string-length($datebrute)>0 and not(contains($datebrute,'..'))">
				<xsl:value-of select="substring($datebrute, 9, 2)" />
				<xsl:text>/</xsl:text>
				<xsl:value-of select="substring($datebrute,6,2)" />
				<xsl:text>/</xsl:text>
				<xsl:value-of select="substring($datebrute, 1, 4)" />
			</xsl:when>
			<xsl:when test="string-length($datebrute)>0 and contains($datebrute,'..')">
				<xsl:value-of select="$datebrute" />
			</xsl:when>
			<xsl:otherwise>
				-
			</xsl:otherwise>
		</xsl:choose>
	</xsl:function>
</xsl:stylesheet>
