/*
 * PES-Viewer
 * Copyright (C) 2019-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.pesviewer.service;

import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.File;
import java.io.IOException;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
@ExtendWith(SpringExtension.class)
public class HtmlAdapterServiceTest {


    private @Autowired HtmlAdapterService htmlAdapterService;


    private String readFile(String path) throws IOException {
        return FileUtils.readFileToString(new File(path), UTF_8);
    }


    @Test
    void badHtml_shouldTransformToDocumentWithBodyContent() {
        String str = "coucou";
        Document document = htmlAdapterService.prepareHtml(str);
        assertEquals(str, document.body().text());
    }


    @Test
    void stringHtml_shouldTransformToDocument() throws IOException {
        Document document = htmlAdapterService.prepareHtml(readFile("src/test/resources/minimal.html"));
        assertNotNull(document);
    }


    @Test
    void htmlWithXmlTag_shouldDeleteTheTag() throws IOException {
        Document document = htmlAdapterService.prepareHtml(readFile("src/test/resources/withxmltag.html"));
        // We have only one child => <html> tag and not the <?xml> one
        assertEquals(1, document.children().size());
    }


    @Test
    void htmlInlineStyle_shouldDeleteTheAttribute() throws IOException {
        String htmlFile = readFile("src/test/resources/withinlinestyle.html");

        Document beforeTransform = Jsoup.parse(htmlFile);
        assertEquals(1, beforeTransform.getElementsByAttribute("style").size());

        Document afterTransform = htmlAdapterService.prepareHtml(htmlFile);
        assertEquals(0, afterTransform.getElementsByAttribute("style").size());
    }


    @Test
    void styleInHeadTag_shouldBeDeleted() throws IOException {
        String htmlFile = readFile("src/test/resources/withheadstyle.html");

        Document beforeTransform = Jsoup.parse(htmlFile);
        assertEquals(1, beforeTransform.getElementsByTag("style").size());

        Document afterTransform = htmlAdapterService.prepareHtml(htmlFile);
        assertEquals(0, afterTransform.getElementsByTag("style").size());
    }


    @Test
    void linksInTags_shouldBeReplaced() throws IOException {
        String htmlFile = readFile("src/test/resources/withlinks.html");

        Document document = htmlAdapterService.prepareHtml(htmlFile);

        Elements links = document.select("a");
        links.forEach(link -> assertTrue(link.attr("href").contains("xemelios:/")));

        htmlAdapterService.replaceLinks(document, "ignoreThat");
        links = document.select("a");
        links.forEach(link -> assertFalse(link.attr("href").contains("xemelios:/")));
    }


}
