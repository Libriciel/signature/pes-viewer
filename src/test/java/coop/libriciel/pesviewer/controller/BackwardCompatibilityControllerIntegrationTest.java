/*
 * PES-Viewer
 * Copyright (C) 2019-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.pesviewer.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class BackwardCompatibilityControllerIntegrationTest {


    @Autowired private MockMvc mockMvc;


    @Test
    void whenPing_getPong() throws Exception {
        mockMvc.perform(get("/ping"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string("pong"));
    }


    @Test
    void whenConf_getStaticConf() throws Exception {
        // Here the value for 'sharedPath' must match the one of retro.shared.path in application.yml
        // In the context of Unit test, it is set to 'src.test.resources' for simplicity,
        // but it s typically set to '/var/tmp/bl-xemwebviewer/xwv-shared' in production environments
        mockMvc.perform(get("/conf"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.xemelios").exists())
                .andExpect(jsonPath("$.xemelios.['xemeliosControl.codCol']").value("146"))
                .andExpect(jsonPath("$.xemelios.['xemeliosControl.idPost']").value("058090"))
                .andExpect(jsonPath("$.xemelios.['xemeliosControl.nomencl']").value("M14"))
                .andExpect(jsonPath("$.xemelios.['xemeliosControl.coltva']").value("true"))
                .andExpect(jsonPath("$.xemelios.['xemeliosControl.codBud']").value("00"))
                .andExpect(jsonPath("$.xemelios.['xemeliosControl.dureedgp']").value("30"))
                .andExpect(jsonPath("$.xemelios.['xemeliosControl.idColl']").value("20000240000014"))
                .andExpect(jsonPath("$.xemelios.['xemeliosControl.votop']").value("false"))
                .andExpect(jsonPath("$.xemelios.['xemeliosControl.servADM']").value(""))
                .andExpect(jsonPath("$.xemelios.['xemeliosControl.typnomencl']").value("Nature"))
                .andExpect(jsonPath("$.xemelios.['xemeliosControl.finJur']").value(""))
                .andExpect(jsonPath("$.xemelios.['xemeliosControl.codProd']").value("69;70;71;72;73;74;75;76;77;78;79;80;81;82;83;84;85;86;87;88;89;90;91;92;93;94;95;96;97;98;99;100;101;102;103;104;105;106;107;248;300;302;305;306;307;308"))
                .andExpect(jsonPath("$.xemelios.['xemeliosControl.servTVA']").value(""))
                .andExpect(jsonPath("$.context").exists())
                .andExpect(jsonPath("$.context.['session.tokenLifetime']").value("60000"))
                .andExpect(jsonPath("$.context.['extractPath']").value("/var/tmp/bl-xemwebviewer/xwv-extract"))
                .andExpect(jsonPath("$.context.['firewall.public.trusted']").value("[{'address':'127.0.0.1','mask':'255.255.255.255'}]"))
                .andExpect(jsonPath("$.context.['realClientIpHeader']").value("x-real-ip"))
                .andExpect(jsonPath("$.context.['firewall.application.trusted']").value("[{'address':'127.0.0.1','mask':'255.255.255.255'}]"))
                .andExpect(jsonPath("$.context.['cachePath']").value("/var/tmp/bl-xemwebviewer/xwv-cache"))
                .andExpect(jsonPath("$.context.['sharedPath']").value("src/test/resources/"))
                .andExpect(jsonPath("$.context.['session.sessionLifetime']").value("600000"))
                .andExpect(jsonPath("$.context.['log4j.properties']").value("/opt/visionneuse-Xemelios/conf/Catalina/localhost/bl-xemwebviewer.log4j.properties"))
                .andExpect(jsonPath("$.context.['forceProtocol']").value("https"))
                .andExpect(jsonPath("$.context.['forceHost']").value("host"))
                .andExpect(jsonPath("$.context.['version']").value("1.6.4.0"));
    }


    @Test
    void whenPrepare_getJsonRedirectUrl() throws Exception {
        mockMvc.perform(get("/prepare").param("propertiesDocumentPath", "peswithoutpj.properties"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.redirectUrl").value(containsString("view")));
    }


    @Test
    void whenView_getFinalRedirection() throws Exception {
        mockMvc.perform(get("/view").param("filepath", "src/test/resources/peswithoutpj.xml"))
                .andDo(print())
                .andExpect(status().is(302));
    }


}
