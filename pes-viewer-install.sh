#!/bin/bash

#
# PES-Viewer
# Copyright (C) 2019-2023 Libriciel-SCOP
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

# go to pes-viewer directory
pushd /opt/pes-viewer

# Create specific user
id -u pes-viewer || useradd pes-viewer -s /sbin/nologin
# Add service to systemd
cp /opt/pes-viewer/pes-viewer.service /etc/systemd/system/
# Set user owner of dir
chown -R pes-viewer: /opt/pes-viewer
# Enable service on boot
systemctl enable pes-viewer.service
# Set jar executable
chmod +x /opt/pes-viewer/pes-viewer-*.jar
# Remove and define symlink
rm pes-viewer.jar
ln -s pes-viewer-*.jar pes-viewer.jar
# Start service
systemctl restart pes-viewer

# quit directory
popd
