/*
 * PES-Viewer
 * Copyright (C) 2019-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fr.gouv.finances.dgfip.utils.xml;

import coop.libriciel.pesviewer.util.DefineHelperFunction;
import fr.gouv.finances.dgfip.utils.Base64;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import net.sf.saxon.Configuration;
import net.sf.saxon.expr.XPathContext;
import net.sf.saxon.lib.ExtensionFunctionCall;
import net.sf.saxon.om.Item;
import net.sf.saxon.om.Sequence;
import net.sf.saxon.om.SequenceTool;
import net.sf.saxon.trans.XPathException;
import net.sf.saxon.tree.iter.SingletonIterator;
import net.sf.saxon.value.EmptySequence;
import net.sf.saxon.value.StringValue;
import org.apache.commons.lang.SerializationException;

import java.io.ByteArrayInputStream;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringTokenizer;


// This is a "xemelios" class we had to modify because of the saxon-he 9.9 update
@Log4j2
@SuppressWarnings("unused")
public class XemeliosFunctionsAdapter {

    public static final String NS_URI = "http://xemelios.org/extensions/xml/functions";
    public static final String NS_PREFIX = "xem";

    public static String extractCN(String base64) {
        String certifCommonName = "";

        try {
            byte[] buffer = Base64.decode(base64);
            CertificateFactory factory = CertificateFactory.getInstance("X.509");
            ByteArrayInputStream bais = new ByteArrayInputStream(buffer);
            X509Certificate cert = (X509Certificate)factory.generateCertificate(bais);
            String dn = cert.getSubjectX500Principal().getName();
            StringTokenizer tokenizer = new StringTokenizer(dn, ",");

            while(tokenizer.hasMoreTokens()) {
                String tok = tokenizer.nextToken();
                if (tok.startsWith("CN=")) {
                    certifCommonName = tok.substring(3);
                }
            }
        } catch (Exception var9) {
            log.error("unable to decode " + base64);
        }

        return certifCommonName;
    }

    public static String extractEMail(String base64) {
        String certifEmail = "";

        try {
            byte[] buffer = Base64.decode(base64);
            CertificateFactory factory = CertificateFactory.getInstance("X.509");
            ByteArrayInputStream bais = new ByteArrayInputStream(buffer);
            X509Certificate cert = (X509Certificate)factory.generateCertificate(bais);
            String dn = cert.getSubjectX500Principal().toString();
            StringTokenizer tokenizer = new StringTokenizer(dn, ",");

            while(tokenizer.hasMoreTokens()) {
                String tok = tokenizer.nextToken();
                if (tok.startsWith("EMAILADDRESS=")) {
                    certifEmail = tok.substring("EMAILADDRESS=".length());
                }
            }
        } catch (Exception var9) {
            log.error("unable to decode " + base64);
        }

        return certifEmail;
    }

    public static String getDate(String fmt) {
        Date today = new Date();
        DateFormat dateFormat = new SimpleDateFormat(fmt);
        return dateFormat.format(today);
    }


    public void register(Configuration registry) {
        registry.registerExtensionFunction(DefineHelperFunction.of("extractCN", new CallHelperFunctionExtractCN()));
        registry.registerExtensionFunction(DefineHelperFunction.of("extractEMail", new CallHelperFunctionExtractEMail()));
        registry.registerExtensionFunction(DefineHelperFunction.of("getDate", new CallHelperFunctionGetDate()));
    }


    private String getParamString(Sequence siq) throws XPathException {
        Item item = siq.iterate().next();
        return item != null ? item.getStringValue() : null;
    }


    private Sequence createResult() {
        return EmptySequence.getInstance();
    }


    private Sequence createResult(String item) throws XPathException {
        return SequenceTool.toLazySequence(SingletonIterator.makeIterator(new StringValue(item)));
    }


    @NoArgsConstructor
    public class CallHelperFunctionExtractCN extends ExtensionFunctionCall {

        @Override
        public Sequence call(XPathContext xPathContext, Sequence[] sequences) {
            try {
                String arg0 = getParamString(sequences[0]);
                String result = extractCN(arg0);
                return createResult(result);
            } catch (Exception var5) {
                log.error(var5.getMessage());
                return createResult();
            }
        }
    }


    @NoArgsConstructor
    public class CallHelperFunctionExtractEMail extends ExtensionFunctionCall {

        @Override
        public Sequence call(XPathContext xPathContext, Sequence[] sequences) {
            try {
                String arg0 = getParamString(sequences[0]);
                String result = extractEMail(arg0);
                return createResult(result);
            } catch (Exception var5) {
                log.error(var5.getMessage());
                return createResult();
            }
        }
    }


    @NoArgsConstructor
    public class CallHelperFunctionGetDate extends ExtensionFunctionCall {

        @Override
        public Sequence call(XPathContext xPathContext, Sequence[] sequences) {
            try {
                String arg0 = getParamString(sequences[0]);
                if (arg0 != null) {
                    String result = getDate(arg0);
                    return createResult(result);
                } else {
                    throw new SerializationException("arg is null");
                }
            } catch (Exception var5) {
                log.error(var5.getMessage());
                return createResult();
            }
        }
    }


}
