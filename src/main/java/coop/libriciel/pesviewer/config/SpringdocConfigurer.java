/*
 * PES-Viewer
 * Copyright (C) 2019-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.pesviewer.config;

import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * Cheers to this example : {@see https://keithtmiller.com/2020/03/18/OpenApi3-Header-setup}
 */
@Slf4j
@Configuration
@ConditionalOnProperty(name = "springdoc.api-docs.enabled", havingValue = "true")
public class SpringdocConfigurer {


    private @Value("${info.application.name}") String applicationName;
    private @Value("${info.application.author.name}") String authorName;
    private @Value("${application.version?:DEVELOP}") String version;


    @Bean
    public OpenAPI apiInfo() {
        return new OpenAPI()
                .info(generateLocalInfo())
                .externalDocs(new ExternalDocumentation().url("https://www.libriciel.fr/i-parapheur/"));
    }


    private Info generateLocalInfo() {
        return new Info()
                .title(applicationName)
                .description("Replacement of the old Xemwebviewer.")
                .version(version)
                .license(new License().name("Affero GPL 3.0").url("https://www.gnu.org/licenses/agpl-3.0.en.html"))
                .contact(new Contact().name(authorName).url("https://libriciel.fr").email("iparapheur@libriciel.coop"));
    }


}
