Change Log
==========

Tous les changements notables sur ce projet seront documentés dans ce fichier.

Ce format est basé sur [Keep a Changelog](http://keepachangelog.com/)
et ce projet adhère au [Semantic Versioning](http://semver.org/).


## [1.5.1] - 2022-06-15
[1.5.1]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-core/-/tags/1.5.1
### Corrections
- Réparation de l'api legacy, cassée suite la montée de version springboot


## [1.5.0] - 2022-03-08
[1.5.0]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-core/-/tags/1.5.0
### Corrections
- Mise à jour de toutes les dépendances Log4j
- Montée de version de Springboot importante, et consécutivement de toutes les dépendances


## [1.4.0] - 2021-01-07
[1.4.0]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-core/-/tags/1.4.0
### Ajout
- Support de la lecture des pièces-jointes de type facture ASAP.
### Corrections
- Fix du rendu incorrect de certains montants pour les résumés de bordereaux


## [1.3.2] - 2021-01-08
[1.3.2]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-core/-/tags/1.3.2
### Corrections
- Désactivation de swagger par défaut


## [1.3.1] - 2020-10-02
[1.3.1]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-core/-/tags/1.3.1
### Ajout
 - Ajout d'un script de récupération des dernières version des ressources XSLT xemelios
### Ameliorations
 - Mise à jour des ressources XSLT xemelios, version 5.2.30 (dans xemelios-1.1.jar)


## [1.3.0] - 2020-09-03
[1.3.0]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-core/-/tags/1.3.0
### Corrections
- Récupération encore plus stricte du nom de fichier et du Mimetype des PJ, s'appuyant sur les champs présents dans le PES.
- Suppression du nom dans le lien de téléchargement des PJ, seul l'ID est désormais passé en paramètre.
### Améliorations
- Passage de Maven à Gradle
- Mise à jour des librairies


## [1.2.11] - 2020-07-27
[1.2.11]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-core/-/tags/1.2.11
### Corrections
- Changement de la mécanique de récupération des noms et extensions des fichiers pièces jointes, au moment du téléchargement. 


## [1.2.9] - 2020-04-23
[1.2.9]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-core/-/tags/1.2.9
### Améliorations
- Package de l'application
### Corrections
- Le script d'installation du service est maintenant idempotent


## [1.2.8] - 2020-03-30
[1.2.8]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-core/-/tags/1.2.8
### Corrections
- Prise en charge des PJ incorrectement encodées


## [1.2.7] - 2020-02-21
[1.2.7]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-core/-/tags/1.2.7
### Améliorations
- Mise à jour du plugin `auto-update`


## [1.2.6] - 2019-12-30
[1.2.6]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-core/-/tags/1.2.6
### Corrections
- Problème d'affichage des icônes de présence des PJ plusieures fois référencées dans les flux
- Problème d'affichage du "Total général au précédent bordereau" des PES Recette
- Les PJ n'étant pas au format ZIP ne bloquent plus la visualisation
- Les PJ sans extensions sont maintenant gérées en tant que fichier PDF
- Certaines erreurs remontaient à tort dans Sentry 


## [1.2.5] - 2019-11-26
[1.2.5]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-core/-/tags/1.2.5
### Corrections
- Ajout d'un token pour accéder a la page de maintenance


## [1.2.4] - 2019-10-02
[1.2.4]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-core/-/tags/1.2.4
### Corrections
- Problème d'affichage de certaines PJ des PES Dépense


## [1.2.3] - 2019-10-02
[1.2.3]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-core/-/tags/1.2.3
### Corrections
- Problème d'affichage des PJ des PES Recette


## [1.2.2] - 2019-10-01
[1.2.2]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-core/-/tags/1.2.2
### Corrections
- Problème d'affichage des PES Recette


## [1.2.1] - 2019-09-30
[1.2.1]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-core/-/tags/1.2.1
### Corrections
- Le modèle de PJ ne s'affiche maintenant que si des PJ sont référencées 


## [1.2.0] - 2019-09-27
[1.2.0]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-core/-/tags/1.2.0
### Améliorations
- Affichage de la présence des pièces jointes dans le flux ou dans le stockage
- Ajout d'API de maintenance, utilisée via l'administration i-Parapheur
- Possibilité d'activer ou désactiver la remontée d'informations
- Possibilité d'activer ou désactiver la mise à jour automatique
- Redirection automatique vers le bordereau lorsque celui-ci est le seul dans le flux


## [1.1.3] - 2019-08-20
[1.1.3]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-core/-/tags/1.1.3
### Corrections
- Problème d'affichage de fonts


## [1.1.0] - 2019-08-16
[1.1.0]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-core/-/tags/1.1.0
### Améliorations
- Le plugin `auto-update` a été mis en place


## [1.0.1] - 2019-07-10
[1.0.1]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-core/-/tags/1.0.1
### Corrections
- La redirection HTTPS ne fonctionnait pas sans des propriétés spécifiques définies dans la configuration
### Améliorations
- Suppression d'étapes inutiles dans la chaine d'intégration continue


## [1.0.0] - 2019-07-10
[1.0.0]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-core/-/tags/1.0.0
### Améliorations
- Page d'erreur spécifique pour les PJ inaccessibles
- Page d'erreur spécifique pour les sessions expirées
- Les exceptions attendues ne sont plus envoyées sur Sentry


## [0.10.4] - 2019-07-09
[0.10.4]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-core/-/tags/0.10.4
### Corrections
- Problème sur les tags XML `Fichier` contenant uniquement des espaces et sauts de ligne 


## [0.10.3] - 2019-07-08
[0.10.3]: https://gitlab.libriciel.fr/libriciel/pole-signature/i-Parapheur-v5/ip-core/-/tags/0.10.3
### Améliorations
- Définition d'erreurs spécifiques en sortie d'appel REST
- Léger refactoring 


## [0.10.2] - 2019-07-04
### Corrections
- Meilleur log pour isolation d'un bug


## [0.10.1] - 2019-07-04
### Corrections
- Gestion des erreurs lors d'une déconnexion trop rapide d'un client


## [0.10.0] - 2019-07-01
### Ajout
- Intégration Sentry


## [0.9.1] - 2019-06-12
### Corrections
- Les Id uniques des PJ ne sont pas uniques, un nouvel identifiant utilisant le champ IdColl est donc généré


## [0.9.0] - 2019-05-13
### Ajout
- Création du projet
- ISO fonctionnel par rapport à la visionneuse "bl-xemwebviewer"
- Les pièces jointes sont conservées 30 jours (par défaut) dans un répertoire temporaire
