/*
 * PES-Viewer
 * Copyright (C) 2019-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.pesviewer.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.http.MediaType.*;


public class MimeTypeUtils {


    public static final String IMAGE_TIFF_VALUE = "image/tiff";
    public static final String APPLICATION_ZIP_VALUE = "application/zip";

    public static final String APPLICATION_MSWORD_VALUE = "application/msword";
    public static final String APPLICATION_VND_MS_EXCEL_VALUE = "application/vnd.ms-excel";
    public static final String APPLICATION_VND_MS_POWERPOINT_VALUE = "application/vnd.ms-powerpoint";

    public static final String APPLICATION_VND_OASIS_OPENDOCUMENT_TEXT_VALUE = "application/vnd.oasis.opendocument.text";
    public static final String APPLICATION_VND_OASIS_OPENDOCUMENT_PRESENTATION_VALUE = "application/vnd.oasis.opendocument.presentation";
    public static final String APPLICATION_VND_OASIS_OPENDOCUMENT_SPREADSHEET_VALUE = "application/vnd.oasis.opendocument.spreadsheet";

    public static final String APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSINGML_DOCUMENT_VALUE = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
    public static final String APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEETML_SHEET_VALUE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    public static final String APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATIONML_PRESENTATION_VALUE = "application/vnd.openxmlformats-officedocument.presentationml.presentation";


    private static final Map<String, String> _mimeTypeToSingleExtensionMap;


    static {
        _mimeTypeToSingleExtensionMap = new HashMap<>();
        _mimeTypeToSingleExtensionMap.put(TEXT_XML_VALUE, "xml");
        _mimeTypeToSingleExtensionMap.put(APPLICATION_XML_VALUE, "xml");
        _mimeTypeToSingleExtensionMap.put(TEXT_HTML_VALUE, "html");
        _mimeTypeToSingleExtensionMap.put(APPLICATION_XHTML_XML_VALUE, "xhtml");

        _mimeTypeToSingleExtensionMap.put(APPLICATION_PDF_VALUE, "pdf");
        _mimeTypeToSingleExtensionMap.put(APPLICATION_ZIP_VALUE, "zip");

        _mimeTypeToSingleExtensionMap.put(APPLICATION_MSWORD_VALUE, "doc");
        _mimeTypeToSingleExtensionMap.put(APPLICATION_VND_MS_EXCEL_VALUE, "xls");
        _mimeTypeToSingleExtensionMap.put(APPLICATION_VND_MS_POWERPOINT_VALUE, "ppt");

        _mimeTypeToSingleExtensionMap.put(APPLICATION_VND_OASIS_OPENDOCUMENT_TEXT_VALUE, "odt");
        _mimeTypeToSingleExtensionMap.put(APPLICATION_VND_OASIS_OPENDOCUMENT_PRESENTATION_VALUE, "odp");
        _mimeTypeToSingleExtensionMap.put(APPLICATION_VND_OASIS_OPENDOCUMENT_SPREADSHEET_VALUE, "ods");

        _mimeTypeToSingleExtensionMap.put(APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSINGML_DOCUMENT_VALUE, "docx");
        _mimeTypeToSingleExtensionMap.put(APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEETML_SHEET_VALUE, "xlsx");
        _mimeTypeToSingleExtensionMap.put(APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATIONML_PRESENTATION_VALUE, "pptx");

        _mimeTypeToSingleExtensionMap.put(IMAGE_JPEG_VALUE, "jpg");
        _mimeTypeToSingleExtensionMap.put(IMAGE_PNG_VALUE, "png");
        _mimeTypeToSingleExtensionMap.put(IMAGE_GIF_VALUE, "gif");
        _mimeTypeToSingleExtensionMap.put(IMAGE_TIFF_VALUE, "tif");
    }


    private static final Map<String, String> _extensionToMimeTypeMap;


    static {
        _extensionToMimeTypeMap = new HashMap<>();
        _extensionToMimeTypeMap.put("xml", APPLICATION_XML_VALUE);
        _extensionToMimeTypeMap.put("html", TEXT_HTML_VALUE);
        _extensionToMimeTypeMap.put("xhtml", APPLICATION_XHTML_XML_VALUE);

        _extensionToMimeTypeMap.put("pdf", APPLICATION_PDF_VALUE);
        _extensionToMimeTypeMap.put("zip", APPLICATION_ZIP_VALUE);

        _extensionToMimeTypeMap.put("doc", APPLICATION_MSWORD_VALUE);
        _extensionToMimeTypeMap.put("xls", APPLICATION_VND_MS_EXCEL_VALUE);
        _extensionToMimeTypeMap.put("ppt", APPLICATION_VND_MS_POWERPOINT_VALUE);

        _extensionToMimeTypeMap.put("odt", APPLICATION_VND_OASIS_OPENDOCUMENT_TEXT_VALUE);
        _extensionToMimeTypeMap.put("odp", APPLICATION_VND_OASIS_OPENDOCUMENT_PRESENTATION_VALUE);
        _extensionToMimeTypeMap.put("ods", APPLICATION_VND_OASIS_OPENDOCUMENT_SPREADSHEET_VALUE);

        _extensionToMimeTypeMap.put("docx", APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSINGML_DOCUMENT_VALUE);
        _extensionToMimeTypeMap.put("xlsx", APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEETML_SHEET_VALUE);
        _extensionToMimeTypeMap.put("pptx", APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATIONML_PRESENTATION_VALUE);

        _extensionToMimeTypeMap.put("jpg", IMAGE_JPEG_VALUE);
        _extensionToMimeTypeMap.put("jpeg", IMAGE_JPEG_VALUE);
        _extensionToMimeTypeMap.put("png", IMAGE_PNG_VALUE);
        _extensionToMimeTypeMap.put("gif", IMAGE_GIF_VALUE);
        _extensionToMimeTypeMap.put("tif", IMAGE_TIFF_VALUE);
    }


    private MimeTypeUtils() {
        throw new IllegalStateException("Utility class");
    }


    /**
     * Retrieve the extension to use for the passed mime type.
     * <p>
     * Return null if this mmime-type is not handled yet.
     *
     * @param mimetype the file mime-type, low-case.
     * @return the extension to use for this mime type, or null.
     */
    public static String getExtensionFromMimeType(String mimetype) {
        return _mimeTypeToSingleExtensionMap.get(mimetype);
    }


    /**
     * Retrieve the mime type corresponding to the passed extension.
     * <p>
     * Return null if this extension is not handled yet.
     *
     * @param extension the file extension, low-case.
     * @return the corresponding mimeType, or null.
     */
    public static String getMimeTypeFromExtension(String extension) {
        return _extensionToMimeTypeMap.get(extension);
    }


    public static String probeMimeTypeFromExtension(String fileName) {
        try {
            return Files.probeContentType(new File(fileName).toPath());
        } catch (IOException e) {
            return APPLICATION_PDF_VALUE;
        }
    }


}
