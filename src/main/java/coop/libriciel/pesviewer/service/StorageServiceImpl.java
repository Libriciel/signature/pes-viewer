/*
 * PES-Viewer
 * Copyright (C) 2019-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package coop.libriciel.pesviewer.service;

import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.AgeFileFilter;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Clock;
import java.time.Instant;
import java.util.Date;

import static java.time.temporal.ChronoUnit.DAYS;


@Log4j2
@Service
@EnableScheduling
public class StorageServiceImpl implements StorageService {


    // <editor-fold desc="Beans">


    @Getter
    private final StorageServiceProperties properties;


    @Autowired
    public StorageServiceImpl(StorageServiceProperties properties) {
        this.properties = properties;
    }


    @PostConstruct
    public void onStart() throws IOException {
        createStoreIfNotExists();
    }


    // </editor-fold desc="Beans">


    @Scheduled(cron = "0 0 * * * *")
    public void cleanupPJDirectory() throws IOException {
        log.info("Cleanup " + properties.getPath() + " directory ...");

        // Get time minus 30 days
        Clock clock = Clock.systemUTC();
        Instant earlier = Instant.now(clock).minus(properties.getKeepingTime(), DAYS);

        Date threshold = Date.from(earlier);
        AgeFileFilter filter = new AgeFileFilter(threshold);

        File path = new File(properties.getPath());
        File[] oldFolders = FileFilterUtils.filter(filter, path.listFiles());

        for (File folder : oldFolders) {
            Files.delete(folder.toPath());
            log.info(folder.getAbsolutePath() + " deleted");
        }
    }


    private void createStoreIfNotExists() throws IOException {
        if (!Paths.get(properties.getPath()).toFile().exists()) {
            Files.createDirectory(Paths.get(properties.getPath()));
        }
    }


    @Override
    public void store(String id, InputStream file) throws IOException {
        if (!fileExists(id)) {
            // We eventually create the store
            this.createStoreIfNotExists();
            // And create the file in the store
            Path path = Paths.get(properties.getPath(), id);
            Files.copy(file, path);
        } else {
            file.close();
        }
    }


    /**
     * Get the file content if it exists
     *
     * @param id the attachment id
     * @return a nullable {@link Path}
     */
    public @Nullable Path get(String id) {
        Path result = Paths.get(properties.getPath(), id);
        return result.toFile().exists() ? result : null;
    }


    @Override
    public boolean fileExists(String id) {
        return Paths.get(properties.getPath(), id).toFile().exists();
    }


    @Override
    public long getStoreSize() {
        return FileUtils.sizeOfDirectory(new File(properties.getPath()));
    }


}
