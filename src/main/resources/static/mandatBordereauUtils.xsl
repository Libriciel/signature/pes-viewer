<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE xsl:stylesheet [
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:n="http://www.minefi.gouv.fr/cp/helios/pes_v2/Rev0/aller"
	xmlns:added="http://projets.admisource.gouv.fr/xemelios/namespaces#added"
	xmlns:ano="http://projets.admisource.gouv.fr/xemelios/namespaces#anomally"
	xmlns:ds="http://www.w3.org/2000/09/xmldsig#" xmlns:xad="http://uri.etsi.org/01903/v1.1.1#"
	xmlns:data="data.uri" xmlns:xem="http://xemelios.org/extensions/xml/functions"
	xmlns:dyn="http://exslt.org/dynamic" extension-element-prefixes="dyn"
	version="2.0">

	<xsl:template name="showPjTemplate">
		<xsl:param name="supportLocation"/>
		<xsl:variable name="nbPJPieceElectro">
			<xsl:value-of
					select="count($supportLocation)"/>
		</xsl:variable>
		<xsl:if test="$nbPJPieceElectro > 0">
			<br/>
			Justifi&#233; par
			<xsl:value-of
					select="$nbPJPieceElectro"/> documents :
			<ul class="pj">
				<xsl:for-each
						select="$supportLocation">
					<li class="pj">
						<xsl:variable name="idpj" select="./n:IdUnique/@V"/>
						<xsl:variable name="isPresent" select="count(/n:PES_Aller/n:PES_PJ/n:PJ/n:IdUnique[@V=$idpj]) = 1"/>
						<xsl:choose>
							<xsl:when test="$isPresent">
								<span class="badge badge-success">Pr&#233;sent</span>&nbsp;
								<a target="_blank">
									<xsl:attribute name="title">
										Identifiant unique : <xsl:call-template
											name="afficher.balise">
										<xsl:with-param name="element.nom"
														select="'IdUnique'"/>
										<xsl:with-param name="element"
														select="./n:IdUnique"/>
										<xsl:with-param name="parent.nodeid"
														select="./@ano:node-id"/>
									</xsl:call-template>
									</xsl:attribute>
									<xsl:choose>
										<xsl:when test="/n:PES_Aller/n:PES_PJ/n:PJ[n:IdUnique/@V=$idpj]/n:TypePJ/@V='006'">
											<xsl:attribute name="href">xemelios:/asapAttachment?pjId=<xsl:value-of
													select="./n:IdUnique/@V"/>&amp;collectivite=<xsl:value-of
													select="/n:PES_Aller/n:EnTetePES/n:IdColl/@V"/>
											</xsl:attribute>
										</xsl:when>
										<xsl:otherwise>
											<xsl:attribute name="href">xemelios:/attachment?state=present&amp;pjId=<xsl:value-of
													select="./n:IdUnique/@V"/>&amp;collectivite=<xsl:value-of
													select="/n:PES_Aller/n:EnTetePES/n:IdColl/@V"/>
											</xsl:attribute>
										</xsl:otherwise>
									</xsl:choose>
									<xsl:choose>
										<xsl:when
												test="string-length(./n:NomPJ/@V) &gt; 0">
											<xsl:call-template
													name="afficher.balise">
												<xsl:with-param
														name="element.nom"
														select="'NomPJ'"/>
												<xsl:with-param
														name="element"
														select="./n:NomPJ"/>
												<xsl:with-param
														name="parent.nodeid"
														select="./@ano:node-id"/>
											</xsl:call-template>
										</xsl:when>
										<xsl:otherwise>
											<xsl:call-template
													name="afficher.balise">
												<xsl:with-param name="element.nom"
																select="'IdUnique'"/>
												<xsl:with-param name="element"
																select="./n:IdUnique"/>
												<xsl:with-param name="parent.nodeid"
																select="./@ano:node-id"/>
											</xsl:call-template>
										</xsl:otherwise>
									</xsl:choose>
								</a>
							</xsl:when>
							<xsl:otherwise>

								<div style="display: none;">
									<xsl:attribute name="class">stock-<xsl:value-of select="$idpj"/></xsl:attribute>
									<span class="badge badge-info">Stock&#233;</span>&nbsp;&nbsp;
									<a target="_blank">
										<xsl:attribute name="title">
											Identifiant unique : <xsl:call-template
												name="afficher.balise">
											<xsl:with-param name="element.nom"
															select="'IdUnique'"/>
											<xsl:with-param name="element"
															select="./n:IdUnique"/>
											<xsl:with-param name="parent.nodeid"
															select="./@ano:node-id"/>
										</xsl:call-template>
										</xsl:attribute>
										<xsl:attribute name="href">
                                            xemelios:/attachment?pjId=<xsl:value-of
												select="./n:IdUnique/@V"/>&amp;collectivite=<xsl:value-of
												select="/n:PES_Aller/n:EnTetePES/n:IdColl/@V"/>
										</xsl:attribute>


										<xsl:choose>
											<xsl:when
													test="string-length(./n:NomPJ/@V) &gt; 0">
												<xsl:call-template
														name="afficher.balise">
													<xsl:with-param
															name="element.nom"
															select="'NomPJ'"/>
													<xsl:with-param
															name="element"
															select="./n:NomPJ"/>
													<xsl:with-param
															name="parent.nodeid"
															select="./@ano:node-id"/>
												</xsl:call-template>
											</xsl:when>
											<xsl:otherwise>
												<xsl:call-template
														name="afficher.balise">
													<xsl:with-param name="element.nom"
																	select="'IdUnique'"/>
													<xsl:with-param name="element"
																	select="./n:IdUnique"/>
													<xsl:with-param name="parent.nodeid"
																	select="./@ano:node-id"/>
												</xsl:call-template>
											</xsl:otherwise>
										</xsl:choose>
									</a>
								</div>
								<div>
									<xsl:attribute name="class">miss-<xsl:value-of select="$idpj"/></xsl:attribute>
									<span class="badge badge-danger">Absent</span>
									<span>
										<xsl:attribute name="title">
											Identifiant unique : <xsl:call-template
												name="afficher.balise">
											<xsl:with-param name="element.nom"
															select="'IdUnique'"/>
											<xsl:with-param name="element"
															select="./n:IdUnique"/>
											<xsl:with-param name="parent.nodeid"
															select="./@ano:node-id"/>
										</xsl:call-template>
										</xsl:attribute>
										&nbsp;
										<xsl:choose>
											<xsl:when
													test="string-length(./n:NomPJ/@V) &gt; 0">
												<xsl:call-template
														name="afficher.balise">
													<xsl:with-param
															name="element.nom"
															select="'NomPJ'"/>
													<xsl:with-param name="element"
																	select="./n:NomPJ"/>
													<xsl:with-param
															name="parent.nodeid"
															select="./@ano:node-id"/>
												</xsl:call-template>

											</xsl:when>
											<xsl:otherwise>
												<xsl:call-template name="afficher.balise">
													<xsl:with-param name="element.nom"
																	select="'IdUnique'"/>
													<xsl:with-param name="element"
																	select="./n:IdUnique"/>
													<xsl:with-param name="parent.nodeid"
																	select="./@ano:node-id"/>
												</xsl:call-template>
											</xsl:otherwise>
										</xsl:choose>
									</span>
								</div>
								<script>
									checkForPjExistence("<xsl:value-of select="$idpj"/>", function(id_res, result) {
									var stockEl = document.getElementsByClassName("stock-" + id_res);
									var missEl = document.getElementsByClassName("miss-" + id_res);

									[].forEach.call(stockEl, function(el) {
									el.style.display = result ? 'block' : 'none';
									});
									[].forEach.call(missEl, function(el) {
									el.style.display = result ? 'none' : 'block';
									});
									});
								</script>
							</xsl:otherwise>
						</xsl:choose>
					</li>
				</xsl:for-each>
			</ul>
		</xsl:if>
	</xsl:template>

	<xsl:template name="afficher.balise">

		<!-- PARAMETRES -->
		<!-- Chemin de l'�l�ment � afficher -->
		<xsl:param name="element" />
		<xsl:param name="element.nom" select="''" />
		<!-- Pour certaines balises on affiche non pas la valeur mais un libell� correspondant que l'on passe ici en param�tre -->
		<xsl:param name="libelle" select="''" />
		<!-- Pour savoir ou l'�l�ment se situe dans le flux... -->
		<xsl:param name="parent.nodeid" />


		<!-- VARIABLES -->
		<!-- El�ment � afficher -->
		<xsl:variable name="element.node.id" select="$element/@ano:node-id" />


		<!-- Noeuds en erreur port�s par l'anomalie sur laquelle on clique -->
		<!-- cas standard : le noeud de l'ano correspond au nodeId de l'�l�ment -->
		<xsl:variable name="noeud.a.surligner.id"
			select="distinct-values($tags-ano//node[@id = $element.node.id]/@id)" />
		<!-- cas de l'ano tagg�e : le noeud de l'ano correspond au nodeId du bloc parent -->
		<xsl:variable name="noeud.a.surligner.id.parent"
			select="distinct-values($tags-ano//node[@id = $parent.nodeid]/@id)" />

		<!-- Tags en erreur port� par l'anomalie sur laquelle on clique (cas d'une
			balise absente du flux) -->
		<xsl:variable name="noeud.a.surligner.tag" select="distinct-values($tags-ano//node[@tag = $element.nom]/@tag)" />

		<!-- S�v�rit� de l'anomalie sur laquelle on clique -->
		<xsl:variable name="anomalie.severite" select="$tags-ano//node/@severity" />

		<!-- Couleur d'affichage de la balise en fonction de la s�v�rit� de l'anomalie -->
		<xsl:choose>
			<!-- anomalie standard : balise pr�sente et node-id present -->
			<xsl:when
				test="string-length($noeud.a.surligner.id) &gt; 0 and string-length($noeud.a.surligner.tag) eq 0">

				<xsl:call-template name="valeur.surlignee">
					<xsl:with-param name="severity" select="$anomalie.severite" />
					<xsl:with-param name="element.a.afficher" select="$element" />
					<xsl:with-param name="libelle.a.afficher" select="$libelle" />
				</xsl:call-template>
			</xsl:when>
			<!-- anomalie tag�e : balise non pr�sente et tag present -->
			<xsl:when
				test="string-length($noeud.a.surligner.id.parent) &gt; 0 and string-length($noeud.a.surligner.tag) &gt; 0">
				<xsl:call-template name="valeur.surlignee">
					<xsl:with-param name="severity" select="$anomalie.severite" />
					<xsl:with-param name="element.a.afficher" select="$element" />
					<xsl:with-param name="libelle.a.afficher" select="$libelle" />
				</xsl:call-template>
			</xsl:when>
			<!-- la balise n'est pas en anomalie, on veut simplement afficher sa valeur,
				on ne lui passe donc pas de s�v�rit� en param�tre -->
			<xsl:otherwise>
				<xsl:call-template name="valeur.surlignee">
					<xsl:with-param name="severity" select="''" />
					<xsl:with-param name="element.a.afficher" select="$element" />
					<xsl:with-param name="libelle.a.afficher" select="$libelle" />
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>

	</xsl:template>



	<xsl:template name="valeur.surlignee">
		<xsl:param name="severity" />
		<xsl:param name="element.a.afficher" />
		<xsl:param name="libelle.a.afficher" />

		<!-- On r�cup�re la couleur en fonction de la s�v�rit� de l'anomalie sur
			laquelle on clique -->
		<xsl:variable name="severity.color"
			select="$severity.colors//severity[@Code= $severity]/@Couleur" />

		<xsl:choose>
			<!-- Si on est dans le cas d'une anomalie, on recherche la couleur avec
				laquelle on va surligner la valeur puis on affiche la valeur -->
			<xsl:when test="string-length($severity.color) &gt; 0">
				<xsl:element name="span">
					<xsl:attribute name="style">background-color: <xsl:value-of
						select="$severity.color" />;</xsl:attribute>
					<!-- affichage de la valeur de la balise -->
					<xsl:choose>
						<xsl:when test="string-length($element.a.afficher/@V) &gt; 0 or string-length($libelle.a.afficher) &gt; 0">
							<xsl:choose>
								<xsl:when test="string-length($libelle.a.afficher) &gt; 0">
									<xsl:value-of select="$libelle.a.afficher"/>
								</xsl:when>
								<!-- Dans le cas d'une liste de valeurs, on passe dans le otherwise et il faut ajouter un s�parateur (saut de ligne) -->
								<xsl:otherwise>
									<xsl:value-of select="$element.a.afficher/@V" separator="&lt;br/&gt;" disable-output-escaping="yes"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<xsl:otherwise>
		            	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						</xsl:otherwise>
					</xsl:choose>
				</xsl:element>
			</xsl:when>

			<!-- Si on n'est pas dans le cas d'une anomalie, on affiche simplement
				la valeur -->
			<xsl:otherwise>
				<xsl:choose>
					<xsl:when test="string-length($libelle.a.afficher) &gt; 0">
						<xsl:value-of select="$libelle.a.afficher"/>
					</xsl:when>
					<!-- Dans le cas d'une liste de valeurs, on passe dans le otherwise et il faut ajouter un s�parateur (saut de ligne) -->
					<xsl:otherwise>
						<xsl:value-of select="$element.a.afficher/@V" separator="&lt;br/&gt;" disable-output-escaping="yes"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>

	</xsl:template>

</xsl:stylesheet>
