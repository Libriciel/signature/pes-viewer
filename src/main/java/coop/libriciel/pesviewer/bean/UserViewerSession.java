/*
 * PES-Viewer
 * Copyright (C) 2019-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.pesviewer.bean;

import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;
import org.w3c.dom.Document;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


@Getter
@Log4j2
@Component
@SessionScope
public class UserViewerSession implements DisposableBean {


    Map<String, Document> richPesFiles;


    public UserViewerSession() {
        this.richPesFiles = new ConcurrentHashMap<>();
    }


    @Override
    public void destroy() {
        this.richPesFiles.forEach((key, value) -> log.info("Destroying session " + key));
    }


}
