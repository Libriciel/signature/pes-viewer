#!/usr/bin/env python

#  PES-Viewer
#  Copyright (C) 2019-2024 Libriciel-SCOP
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as
#  published by the Free Software Foundation, either version 3 of the
#  License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.

import gzip
import io
import os
import sys
from optparse import OptionParser
from xml.dom.minidom import parseString, Element

import requests

VERSION = "0.0.2"


def get_update_file_xml_content(updateUrl: str) -> str:
    """
    Download and return the content of the update file, located at updateUrl, as text.
    :return: the content of the update file
    :rtype: str
    """
    print("getUpdateFile, updateUrl : " + updateUrl)
    updateFileUrl = os.path.join(updateUrl, "update.xml")
    print("updateFileUrl : " + updateFileUrl)

    r = requests.get(updateFileUrl)
    print("r.status_code : " + str(r.status_code))
    print("r.headers['content-type'] : " + str(r.headers['content-type']))
    print("r.encoding : " + str(r.encoding))
    return r.text


def select_proper_version_element(xmlRoot: Element, options) -> Element:
    """

    :param xmlRoot: root Element of the update file
    :type xmlRoot: :rtype: minidom.Element
    :param options: options for this script
    :type options: object
    :return:the DOM element corresponding to the target version
    :rtype: minidom.Element
    """
    t = options.targetVersion
    print('select_proper_version_element - target version : ' + t)
    versionElemList = xmlRoot.getElementsByTagName('version')
    for versionElem in versionElemList:
        if versionElem.getAttribute('version') == t:
            return versionElem

    return None


def browse_and_download_files(versionElement: Element, options):
    """
    Creates target dir, then browse the 'file' elements, and call download_file on each

    :param versionElement: the element containing all
    :type versionElement: minidom.Element
    :param options: options for this script
    :type options: object
    :return: -
    :rtype: void
    """
    print('browse_and_download_files')
    filesElemList = versionElement.getElementsByTagName('files')
    filesElem = filesElemList[0]
    rootUrl = filesElem.getAttribute('root')
    dstDir = options.dstDir if options.dstDir else 'dst'

    if not os.path.isdir(dstDir):
        os.mkdir(dstDir)

    print('browse_and_download_files - rootUrl : ' + rootUrl)
    print('browse_and_download_files - dst : ' + dstDir)

    fileList = filesElem.getElementsByTagName('file')
    print("fileList size : " + str(fileList.length))
    for file in fileList:
        download_file(file, rootUrl, dstDir)


def download_file(fileElement: Element, rootUrl: str, dstRoot: str):
    """

    :param fileElement:
    :type fileElement:
    :param rootUrl: root url for every files of this version
    :type rootUrl: str
    :param dstRoot: destination folder on the local filesystem
    :type dstRoot: str
    :return: -
    :rtype: void
    """
    print('download_file')
    name = fileElement.getAttribute('name')
    src = fileElement.getAttribute('source')
    dst = fileElement.getAttribute('dest')
    print('download_file - read dest : ' + dst)

    dstDir = str(dst).replace('${xemelios.root}', dstRoot) if dst else dstRoot
    os.makedirs(dstDir, exist_ok=True)
    dstPath = os.path.join(dstDir, name)
    print('download_file - dstPath : ' + dstPath)

    print('src : ' + src)
    if not src:
        return

    targetUrl = os.path.join(rootUrl, src)
    print('download_file - targetUrl : ' + targetUrl)

    r = requests.get(targetUrl)
    print("r.status_code : " + str(r.status_code))

    stream = io.BytesIO()
    stream.write(r.content)
    stream.seek(0)  # This is crucial
    gzf = gzip.GzipFile(fileobj=stream)
    uncompressed_content = gzf.read()

    with open(dstPath, 'bw') as fi:
        fi.write(uncompressed_content)


def load_update_xml_content(xmlContent: str, options):
    """
    Load the content of the update into a DOM tree, and start browsing the files of the selected version
    """
    xml = parseString(xmlContent)
    root = xml.documentElement
    print("loadUpdateXmlContent - root : " + str(root))
    versionElem = select_proper_version_element(root, options)
    print('selected versionElem : ' + str(versionElem))
    browse_and_download_files(versionElem, options)


if __name__ == '__main__':
    versionStr = "%prog {0}".format(VERSION)
    usage = "usage: %prog [options] "
    parser = OptionParser(usage=usage, version=versionStr)
    parser.add_option("-u", "--updateUrl", dest="updateUrl",
                      help="base update url (default : http://xemelios.org/updatesV5/pes-aller/PRODUCTION)",
                      default="http://xemelios.org/updatesV5/pes-aller/PRODUCTION")
    parser.add_option("-t", "--targetVersion", dest="targetVersion",
                      help="target version of xemelios - mandatory. latest at time of writing : 5.2.30.0")
    parser.add_option("-d", "--dstDir", dest="dstDir", help="directory where the files will be downloaded")

    cmd_options, args = parser.parse_args()

    if (not cmd_options.targetVersion):
        print('providing a target version with -t is mandatory. Example version : 5.2.30.0')
        sys.exit(1)

    updateContent = get_update_file_xml_content(cmd_options.updateUrl)
    load_update_xml_content(updateContent, cmd_options)
