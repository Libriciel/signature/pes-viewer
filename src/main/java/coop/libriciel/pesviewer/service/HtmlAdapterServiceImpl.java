/*
 * PES-Viewer
 * Copyright (C) 2019-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.pesviewer.service;

import lombok.extern.log4j.Log4j2;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import static coop.libriciel.pesviewer.util.XemeliosConstants.SESSION_KEY;
import static java.nio.charset.StandardCharsets.UTF_8;


@Log4j2
@Service
public class HtmlAdapterServiceImpl implements HtmlAdapterService {

    private static final String XEMELIOS_QUERY_LINK = "xemelios:/query";
    private static final String XEMELIOS_ATTACHMENT_LINK = "xemelios:/attachment";
    private static final String XEMELIOS_ASAP_ATTACHMENT_LINK = "xemelios:/asapAttachment";
    private static final String HTML_STYLE_ATTR = "style";


    @Value("${server.servlet.context-path:/bl-xemwebviewer}")
    String contextPath;

    @Value("${retro.style.default:true}")
    boolean useDefaultStyle;


    @Override
    public Document prepareHtml(String htmlStr) {

        htmlStr = htmlStr.replace("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>", "");
        Document document = Jsoup.parse(htmlStr);

        this.handleNewStyle(document);

        document.updateMetaCharsetElement(true);
        document.charset(UTF_8);

        return document;
    }


    private void handleNewStyle(Document document) {
        if (!useDefaultStyle) {
            Elements withStyle = document.getElementsByAttribute(HTML_STYLE_ATTR);
            withStyle.forEach(attrElement -> attrElement.removeAttr(HTML_STYLE_ATTR));

            document.getElementsByAttribute("border").forEach(attrElement -> attrElement.removeAttr("border"));
            document.getElementsByAttribute("align").forEach(attrElement -> attrElement.removeAttr("align"));
            document.getElementsByAttribute("valign").forEach(attrElement -> attrElement.removeAttr("valign"));
            document.getElementsByAttribute("width").forEach(attrElement -> attrElement.removeAttr("width"));
            document.getElementsByAttribute("href").forEach(attrElement -> attrElement.attr("href", attrElement.attr("href")
                    .replace(" ", "")
                    .replace("%0A", "")));

            document.select(HTML_STYLE_ATTR).remove();

            document.head().append("<link rel=\"stylesheet\" type=\"text/css\" href=\"css/font-awesome.min.css\">");
            document.head().append("<link rel=\"stylesheet\" type=\"text/css\" href=\"css/bootstrap.min.css\">");
            document.head().append("<link rel=\"stylesheet\" type=\"text/css\" href=\"css/custom-xemelios.css\">");

            document.head().append("<script src=\"ls-elements.js\"/>");

            document.select("table").forEach(table -> table.addClass("table").addClass("table-striped"));

            document.body().append("<ls-back-top-top></ls-back-top-top>");
        }
    }


    @Override
    public Document replaceLinks(Document document, String sessionKey) {
        Elements links = document.select("a");
        for (Element link : links) {
            String newRef = "";
            if (link.attr("href").contains(XEMELIOS_QUERY_LINK)) {
                String currentRef = link.attr("href").replace(XEMELIOS_QUERY_LINK, "");
                newRef = String.format("%s/browse%s&%s=%s", contextPath, currentRef, SESSION_KEY, sessionKey);
            } else if (link.attr("href").contains(XEMELIOS_ATTACHMENT_LINK)) {
                String currentRef = link.attr("href").replace(XEMELIOS_ATTACHMENT_LINK, "");
                newRef = String.format("%s/attachment%s&%s=%s", contextPath, currentRef, SESSION_KEY, sessionKey);
            } else if (link.attr("href").contains(XEMELIOS_ASAP_ATTACHMENT_LINK)) {
                String currentRef = link.attr("href").replace(XEMELIOS_ASAP_ATTACHMENT_LINK, "");
                newRef = String.format("%s/browse-asap-content%s&%s=%s", contextPath, currentRef, SESSION_KEY, sessionKey);
            }

            link.attr("href", newRef);
        }

        return document;
    }


}
