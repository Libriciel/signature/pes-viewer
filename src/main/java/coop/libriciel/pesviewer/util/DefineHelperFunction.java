/*
 * PES-Viewer
 * Copyright (C) 2019-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.pesviewer.util;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import net.sf.saxon.lib.ExtensionFunctionCall;
import net.sf.saxon.lib.ExtensionFunctionDefinition;
import net.sf.saxon.om.StructuredQName;
import net.sf.saxon.value.SequenceType;

import static fr.gouv.finances.dgfip.utils.xml.XemeliosFunctionsAdapter.NS_PREFIX;
import static fr.gouv.finances.dgfip.utils.xml.XemeliosFunctionsAdapter.NS_URI;


@RequiredArgsConstructor(staticName = "of")
public class DefineHelperFunction extends ExtensionFunctionDefinition {


    public @NonNull String functionName;
    public @NonNull ExtensionFunctionCall extensionFunctionCall;


    @Override
    public StructuredQName getFunctionQName() {
        return new StructuredQName(NS_PREFIX, NS_URI, functionName);
    }


    @Override
    public SequenceType[] getArgumentTypes() {
        return new SequenceType[]{SequenceType.SINGLE_STRING};
    }


    @Override
    public SequenceType getResultType(SequenceType[] sequenceTypes) {
        return SequenceType.SINGLE_STRING;
    }


    @Override
    public ExtensionFunctionCall makeCallExpression() {
        return extensionFunctionCall;
    }


}
