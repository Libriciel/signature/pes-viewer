/*
 * PES-Viewer
 * Copyright (C) 2019-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package coop.libriciel.pesviewer.config;

import coop.libriciel.pesviewer.exception.PJNotFoundException;
import coop.libriciel.pesviewer.exception.PesSessionExpiredException;
import coop.libriciel.pesviewer.exception.PjUnreadableException;
import org.apache.catalina.connector.ClientAbortException;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.annotation.Priority;
import javax.servlet.http.HttpServletRequest;


@ControllerAdvice
@Priority(1)
@Order(1) // Run this first to catch "required" exceptions before sending to sentry
public class PesViewerExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(PJNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ModelAndView pjNotFoundHandler(PJNotFoundException e, HttpServletRequest req) {
        logger.warn("Request: " + req.getRequestURL() + " raised " + e);

        ModelAndView mav = new ModelAndView();
        mav.addObject("message", e.getMessage());
        mav.setViewName("pjnotfound");
        return mav;
    }


    @ExceptionHandler(PjUnreadableException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ModelAndView pjUnreadableHandler(PjUnreadableException e, HttpServletRequest req) {
        logger.warn("Request: " + req.getRequestURL() + " raised " + e);

        ModelAndView mav = new ModelAndView();
        mav.addObject("message", e.getMessage());
        mav.setViewName("pjunreadable");
        return mav;
    }


    @ExceptionHandler(ClientAbortException.class)
    @ResponseStatus(HttpStatus.GONE)
    public void abortHandler(ClientAbortException e, HttpServletRequest req) {
        logger.warn("Client abort exception catched on :" + req.getRequestURL());
    }


    @ExceptionHandler(PesSessionExpiredException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public ModelAndView sessionExpiredHandler(PesSessionExpiredException e, HttpServletRequest req) {
        logger.warn("Request: " + req.getRequestURL() + " raised " + e);

        ModelAndView mav = new ModelAndView();
        mav.setViewName("sessionExpired");
        return mav;
    }

}
