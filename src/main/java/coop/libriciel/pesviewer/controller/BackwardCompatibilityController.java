/*
 * PES-Viewer
 * Copyright (C) 2019-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.pesviewer.controller;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.TEXT_PLAIN_VALUE;


/**
 * This is a controller for backward compatibility with the old "bl-xemwebviewer"
 */
@Log4j2
@RestController
public class BackwardCompatibilityController {


    private static final String SHARED_PATH_PLACEHOLDER = "%%SHARED_FILES_PATH%%";
    private static final String PING_RESPONSE = "pong";
    private static final String CONF_RESPONSE_TEMPLATE = "{\"xemelios\":{\"xemeliosControl.codCol\":\"146\",\"xemeliosControl.idPost\":\"058090\",\"xemeliosControl.nomencl\":\"M14\",\"xemeliosControl.coltva\":\"true\",\"xemeliosControl.codBud\":\"00\",\"xemeliosControl.dureedgp\":\"30\",\"xemeliosControl.idColl\":\"20000240000014\",\"xemeliosControl.votop\":\"false\",\"xemeliosControl.servADM\":\"\",\"xemeliosControl.typnomencl\":\"Nature\",\"xemeliosControl.finJur\":\"\",\"xemeliosControl.codProd\":\"69;70;71;72;73;74;75;76;77;78;79;80;81;82;83;84;85;86;87;88;89;90;91;92;93;94;95;96;97;98;99;100;101;102;103;104;105;106;107;248;300;302;305;306;307;308\",\"xemeliosControl.servTVA\":\"\"},\"context\":{\"session.tokenLifetime\":\"60000\",\"extractPath\":\"/var/tmp/bl-xemwebviewer/xwv-extract\",\"firewall.public.trusted\":\"[{'address':'127.0.0.1','mask':'255.255.255.255'}]\",\"realClientIpHeader\":\"x-real-ip\",\"firewall.application.trusted\":\"[{'address':'127.0.0.1','mask':'255.255.255.255'}]\",\"cachePath\":\"/var/tmp/bl-xemwebviewer/xwv-cache\",\"sharedPath\":\"" + SHARED_PATH_PLACEHOLDER + "\",\"session.sessionLifetime\":\"600000\",\"log4j.properties\":\"/opt/visionneuse-Xemelios/conf/Catalina/localhost/bl-xemwebviewer.log4j.properties\",\"forceProtocol\":\"https\",\"forceHost\":\"host\",\"version\":\"1.6.4.0\"}}";
    private static final String PREPARE_RESPONSE = "{\"redirectUrl\":\"%s%s\",\"exception\":null}";


    private final PesViewerController navigate;
    @Value("${server.servlet.context-path:/bl-xemwebviewer}")
    private String contextPath;
    @Value("${retro.shared.path:/var/tmp/bl-xemwebviewer/xwv-shared/}")
    private String sharedPath;


    @Autowired
    public BackwardCompatibilityController(PesViewerController navigate) {
        this.navigate = navigate;
    }


    @GetMapping(path = "/ping")
    public String ping() {
        return PING_RESPONSE;
    }


    @GetMapping(path = "/conf", produces = APPLICATION_JSON_VALUE)
    public String conf() {
        return CONF_RESPONSE_TEMPLATE.replace(SHARED_PATH_PLACEHOLDER, sharedPath);
    }


    // We have to do "prepare" then "view", because "prepare" is called by the server and cannot handle Session Scoped Bean
    @GetMapping(path = "/prepare", produces = APPLICATION_JSON_VALUE)
    public String doPrepare(@RequestParam String propertiesDocumentPath) throws IOException {
        // Read properties file and get pes xml file name
        Properties properties = new Properties();
        try (InputStream propertiesInputStream = new FileInputStream(sharedPath + propertiesDocumentPath)) {
            properties.load(propertiesInputStream);
        }

        log.info("Preparing from old API document " + sharedPath + properties.getProperty("pesDocumentPath"));

        String redirection = "/view?filepath=" + sharedPath + properties.getProperty("pesDocumentPath");

        return String.format(PREPARE_RESPONSE, contextPath, redirection);
    }


    // View is called by the client, so we can "prepare" from the real Controller
    @GetMapping(path = "/view")
    public ModelAndView doView(@RequestParam String filepath) throws IOException {
        try (InputStream fileInputStream = new FileInputStream(filepath)) {
            MultipartFile multipartFile = new MockMultipartFile("test.xml", "test.xml", TEXT_PLAIN_VALUE, fileInputStream);
            return new ModelAndView(this.navigate.doPrepare(multipartFile));
        }
    }


}
