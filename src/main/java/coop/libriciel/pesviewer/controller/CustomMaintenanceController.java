/*
 * PES-Viewer
 * Copyright (C) 2019-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.pesviewer.controller;

import coop.libriciel.pesviewer.model.MaintenanceModel;
import coop.libriciel.pesviewer.service.StorageService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;


@Log4j2
@RestController
@RequestMapping(path = "/maintenance/custom")
public class CustomMaintenanceController {


    private final StorageService storageService;


    @Autowired
    public CustomMaintenanceController(StorageService storageService) {
        this.storageService = storageService;
    }


    @GetMapping
    public MaintenanceModel getMaintenanceInformations() {
        MaintenanceModel model = new MaintenanceModel();

        model.setConservation(storageService.getProperties().getKeepingTime());
        model.setDiskUsage(storageService.getStoreSize());

        return model;
    }


    @PostMapping(path = "/cleanup")
    public void cleanUpStore() throws IOException {
        storageService.cleanupPJDirectory();
    }


}
