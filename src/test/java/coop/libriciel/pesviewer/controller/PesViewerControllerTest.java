/*
 * PES-Viewer
 * Copyright (C) 2019-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.pesviewer.controller;

import coop.libriciel.pesviewer.service.XemeliosService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.w3c.dom.Document;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;

import static coop.libriciel.pesviewer.util.XemeliosConstants.PJ_ID;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;


@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class PesViewerControllerTest {


    @Autowired XemeliosService xemeliosService;


    @Test
    void createRichPesFileAndExtractPJ() throws IOException {

        Path resourceDirectory = Paths.get("src", "test", "resources", "peswithpj.xml");
        InputStream inputStream = new FileInputStream(resourceDirectory.toFile());
        assertNotNull(inputStream);

        MockMultipartFile multipartFile = new MockMultipartFile("peswithpj.xml", inputStream);
        Document document = xemeliosService.createRichPesFileAndExtractPj(multipartFile.getInputStream());
        assertNotNull(document);

        HashMap<String, String> map = new HashMap<>();
        map.put(PJ_ID, "thisisunique");
        Path attachmentPath = xemeliosService.getAttachmentInRichFile(document, map);
        assertNotNull(attachmentPath);
        try (InputStream attachmentInputStream = Files.newInputStream(attachmentPath)) {
            assertNotNull(attachmentInputStream.readAllBytes());
        }
    }


}
