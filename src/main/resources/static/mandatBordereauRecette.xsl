<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE xsl:stylesheet [
        <!ENTITY nbsp "&#160;">
        ]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:n="http://www.minefi.gouv.fr/cp/helios/pes_v2/Rev0/aller"
                xmlns:added="http://projets.admisource.gouv.fr/xemelios/namespaces#added"
                xmlns:ano="http://projets.admisource.gouv.fr/xemelios/namespaces#anomally"
                xmlns:ds="http://www.w3.org/2000/09/xmldsig#"
                xmlns:xad="http://uri.etsi.org/01903/v1.1.1#"
                xmlns:data="data.uri"
                xmlns:xem="http://xemelios.org/extensions/xml/functions"
                version="2.0">
    <xsl:param name="elementId"/> <!-- Bordereau pour afficher le bordereau, vide sinon -->
    <xsl:param name="primaryKey"/>    <!-- utilise dans le cas d'un document multi-bordereau -->
    <xsl:param
            name="mandatId"/>  <!-- dans le cas de l'affichage d'un titre, l'id du titre (./n:PES_RecetteAller/n:Bordereau/n:Piece/n:BlocPiece/n:IdPce/@V ) -->
    <xsl:param name="browser-destination"/>
    <xsl:param name="anoId"/>    <!-- id de l'anomalie a mettre en surbrillance -->
    <xsl:output encoding="ISO-8859-1" method="xhtml" exclude-result-prefixes="n added ano data xem"
                doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" include-content-type="no" indent="yes"/>
    <xsl:decimal-format name="decformat"
                        decimal-separator=","
                        grouping-separator=" "
                        digit="#"
                        pattern-separator=";"
                        NaN="NaN"
                        minus-sign="-"/>
    <xsl:variable name="tags-ano">
        <xsl:for-each select="//ano:Anomalie[@ano:anoId = $anoId]/ano:node">
            <xsl:element name="node">
                <xsl:attribute name="id" select="./@ano:id"/>
                <xsl:attribute name="tag" select="./@ano:tag"/>
                <xsl:attribute name="severity" select="parent::node()/@ano:severity"/>
            </xsl:element>
        </xsl:for-each>
    </xsl:variable>
    <xsl:variable name="severity.colors">
        <severitys>
            <severity Code="BLOQUANT" Couleur="#FF4949"/>
            <severity Code="NON BLOQUANT" Couleur="#FFC975"/>
        </severitys>
    </xsl:variable>
    <xsl:variable name="bordereau"
                  select="/n:PES_Aller/n:PES_RecetteAller/n:Bordereau[(string-length($primaryKey) &gt; 1 and @added:primary-key=$primaryKey) or (string-length($primaryKey) = 0 and position()=1) ][1]"></xsl:variable>
    <xsl:variable name="NomFic" select="/n:PES_Aller/n:PES_PJ/n:PJ/n:NomPJ/@V"/>
    <xsl:variable name="collId" select="/n:PES_Aller/n:EnTetePES/n:IdColl/@V"/>

    <!-- Donn&#233;es pour les &#233;quivalences des codes -->
    <xsl:variable name="data">
        <data:data>
            <data:typeBordereau>
                <data:t type="01">Ordinaire</data:t>
                <data:t type="02">Annulation/r&#233;duction</data:t>
                <data:t type="03">Ordres de recette</data:t>
                <data:t type="04">de titre &#233;mis suite &#224; d&#233;cision juridictionnelle</data:t>
                <data:t type="05">Ent&#234;te P503</data:t>
            </data:typeBordereau>
            <data:typePiece>
                <data:t type="01">Titre ordinaire</data:t>
                <data:t type="02">Titre correctif</data:t>
                <data:t type="03">Titre d&apos;ordre budg&#233;taire</data:t>
                <data:t type="04">Titre d&apos;ordre mixte</data:t>
                <data:t type="05">Titre &#233;mis apr&#232;s encaissement</data:t>
                <data:t type="06">Titre r&#233;capitulatif avec r&#244;le</data:t>
                <data:t type="07">Titre r&#233;capitulatif sans r&#244;le</data:t>
                <data:t type="08">R&#244;le titre</data:t>
                <data:t type="09">Titre de majoration</data:t>
                <data:t type="10">Titre en plusieurs ann&#233;es</data:t>
                <data:t type="11">Titre de rattachement</data:t>
                <data:t type="12">Ordre de recette ordonnateur</data:t>
                <data:t type="13">Demande &#233;mission de titre (P503)</data:t>
                <data:t type="14">Produits constat&#233;s d?avance</data:t>
            </data:typePiece>
            <data:naturePiece>
                <data:n type="01">Fonctionnement</data:n>
                <data:n type="02">Investissement</data:n>
                <data:n type="03">Inventaire</data:n>
                <data:n type="04">Emprunt</data:n>
                <data:n type="05">R&#233;gie</data:n>
                <data:n type="06">Annulation-R&#233;duction</data:n>
                <data:n type="07">Compl&#233;mentaire</data:n>
                <data:n type="08">R&#233;&#233;mis</data:n>
                <data:n type="09">Annulant un mandat</data:n>
                <data:n type="10">Annulation du mandat de rattachement</data:n>
                <data:n type="11">March&#233;</data:n>
                <data:n type="12">Autres</data:n>
                <data:n type="18">Cession</data:n>
            </data:naturePiece>
            <data:typePieceOrigine>
                <data:t type="01">Titre</data:t>
                <data:t type="02">Mandat</data:t>
                <data:t type="03">Pi&#232;ce &#233;margement</data:t>
                <data:t type="04">Liste non valeur</data:t>
                <data:t type="05">Liste de majoration</data:t>
            </data:typePieceOrigine>
            <data:typeTiersNational>
                <data:t type="01">Siret</data:t>
                <data:t type="02">Siren</data:t>
                <data:t type="03">Finess</data:t>
                <data:t type="04">Nir</data:t>
                <data:t type="05">TVAIntraCo</data:t>
                <data:t type="06">Hors UE</data:t>
                <data:t type="07">TAHITI</data:t>
                <data:t type="08">RIDET</data:t>
                <data:t type="09">EnCoursImm</data:t>
                <data:t type="10">FRWF</data:t>
                <data:t type="11">IREP</data:t>
                <data:t type="12">NFP</data:t>
            </data:typeTiersNational>
            <data:categorieTiers>
                <data:c V="01">Personnes physiques</data:c>
                <data:c V="20">&#233;tat et &#233;tablissements publics nationaux</data:c>
                <data:c V="21">R&#233;gions</data:c>
                <data:c V="22">D&#233;partements</data:c>
                <data:c V="23">Communes</data:c>
                <data:c V="24">Groupements de collectivit&#233;s</data:c>
                <data:c V="25">Caisses des &#233;coles</data:c>
                <data:c V="26">CCAS</data:c>
                <data:c V="27">Etablissements publics de sant&#233;</data:c>
                <data:c V="28">Ecole nationale de la sant&#233; publique</data:c>
                <data:c V="29">Autres &#233;tablissements publics et organismes internationaux</data:c>
                <data:c V="50">Personnes morales de droit priv&#233; autres qu'organismes sociaux</data:c>
                <data:c V="60">Caisses de s&#233;curit&#233; sociale r&#233;gime g&#233;n&#233;ral</data:c>
                <data:c V="61">Caisses de s&#233;curit&#233; sociale r&#233;gime agricole</data:c>
                <data:c V="62">S&#233;curit&#233; sociale des travailleurs non salari&#233;s et professions non
                    agricoles
                </data:c>
                <data:c V="63">Autres r&#233;gimes obligatoires de s&#233;curit&#233; sociale</data:c>
                <data:c V="64">Mutuelles et organismes d'assurance</data:c>
                <data:c V="65">Divers autres tiers payants</data:c>
                <data:c V="70">CNRACL</data:c>
                <data:c V="71">IRCANTEC</data:c>
                <data:c V="72">ASSEDIC</data:c>
                <data:c V="73">Caisses mutualistes de retraite compl&#233;mentaires</data:c>
                <data:c V="74">Autres organismes sociaux</data:c>
            </data:categorieTiers>
            <data:natureJuridiqueTiers>
                <data:t V="00">Inconnue</data:t>
                <data:t V="01">Particuliers</data:t>
                <data:t V="02">Artisan / Commer�ant / Agriculteur</data:t>
                <data:t V="03">Soci&#233;t&#233;</data:t>
                <data:t V="04">CAM ou caisse appliquant les m&#234;mes r&#232;gles</data:t>
                <data:t V="05">Caisse compl&#233;mentaire</data:t>
                <data:t V="06">Association</data:t>
                <data:t V="07">Etat ou organisme d&apos;Etat</data:t>
                <data:t V="08">Etablissement public national</data:t>
                <data:t V="09">Collectivit&#233; territoriale / EPL / EPS</data:t>
                <data:t V="10">Etat &#233;tranger / ambassade</data:t>
                <data:t V="11">CAF</data:t>
            </data:natureJuridiqueTiers>
            <data:codePaysINSEE>
                <data:p V="99125">ALBANIE</data:p>
                <data:p V="99109">ALLEMAGNE</data:p>
                <data:p V="99130">ANDORRE</data:p>
                <data:p V="99110">AUTRICHE</data:p>
                <data:p V="99131">BELGIQUE</data:p>
                <data:p V="99148">BIELORUSSIE</data:p>
                <data:p V="99118">BOSNIE-HERZEGOVINE</data:p>
                <data:p V="99103">BOUVET (ILE)</data:p>
                <data:p V="99111">BULGARIE</data:p>
                <data:p V="99119">CROATIE</data:p>
                <data:p V="99101">DANEMARK</data:p>
                <data:p V="99134">ESPAGNE</data:p>
                <data:p V="99106">ESTONIE</data:p>
                <data:p V="99156">EX-REPUBLIQUE YOUGOSLAVE DE MACEDOINE</data:p>
                <data:p V="99101">FEROE (ILES)</data:p>
                <data:p V="99105">FINLANDE</data:p>
                <data:p V="99133">GIBRALTAR</data:p>
                <data:p V="99126">GRECE</data:p>
                <data:p V="99112">HONGRIE</data:p>
                <data:p V="99136">IRLANDE, ou EIRE</data:p>
                <data:p V="99102">ISLANDE</data:p>
                <data:p V="99127">ITALIE</data:p>
                <data:p V="99107">LETTONIE</data:p>
                <data:p V="99113">LIECHTENSTEIN</data:p>
                <data:p V="99108">LITUANIE</data:p>
                <data:p V="99137">LUXEMBOURG</data:p>
                <data:p V="99144">MALTE</data:p>
                <data:p V="99151">MOLDAVIE</data:p>
                <data:p V="99138">MONACO</data:p>
                <data:p V="99120">MONTENEGRO</data:p>
                <data:p V="99103">NORVEGE</data:p>
                <data:p V="99135">PAYS-BAS</data:p>
                <data:p V="99122">POLOGNE</data:p>
                <data:p V="99139">PORTUGAL</data:p>
                <data:p V="99141">REPUBLIQUE DEMOCRATIQUE ALLEMANDE</data:p>
                <data:p V="99142">REPUBLIQUE FEDERALE D'ALLEMAGNE</data:p>
                <data:p V="99114">ROUMANIE</data:p>
                <data:p V="99132">ROYAUME-UNI</data:p>
                <data:p V="99123">RUSSIE</data:p>
                <data:p V="99128">SAINT-MARIN</data:p>
                <data:p V="99121">SERBIE-ET-MONT&#233;N&#233;GRO</data:p>
                <data:p V="99117">SLOVAQUIE</data:p>
                <data:p V="99145">SLOVENIE</data:p>
                <data:p V="99104">SUEDE</data:p>
                <data:p V="99140">SUISSE</data:p>
                <data:p V="99103">SVALBARD et ILE JAN MAYEN</data:p>
                <data:p V="99115">TCHECOSLOVAQUIE</data:p>
                <data:p V="99116">TCHEQUE (REPUBLIQUE)</data:p>
                <data:p V="99124">TURQUIE D'EUROPE</data:p>
                <data:p V="99155">UKRAINE</data:p>
                <data:p V="99129">VATICAN, ou SAINT-SIEGE</data:p>
                <data:p V="99212">AFGHANISTAN</data:p>
                <data:p V="99201">ARABIE SAOUDITE</data:p>
                <data:p V="99252">ARMENIE</data:p>
                <data:p V="99253">AZERBAIDJAN</data:p>
                <data:p V="99249">BAHREIN</data:p>
                <data:p V="99246">BANGLADESH</data:p>
                <data:p V="99214">BHOUTAN</data:p>
                <data:p V="99224">BIRMANIE</data:p>
                <data:p V="99225">BRUNEI</data:p>
                <data:p V="99234">CAMBODGE</data:p>
                <data:p V="99216">CHINE</data:p>
                <data:p V="99254">CHYPRE</data:p>
                <data:p V="99237">COREE</data:p>
                <data:p V="99239">COREE (REPUBLIQUE DE)</data:p>
                <data:p V="99238">COREE (REPUBLIQUE POPULAIRE DEMOCRATIQUE DE)</data:p>
                <data:p V="99247">EMIRATS ARABES UNIS</data:p>
                <data:p V="99228">ETATS MALAIS NON FEDERES</data:p>
                <data:p V="99255">GEORGIE</data:p>
                <data:p V="99223">GOA</data:p>
                <data:p V="99230">HONG-KONG</data:p>
                <data:p V="99223">INDE</data:p>
                <data:p V="99231">INDONESIE</data:p>
                <data:p V="99204">IRAN</data:p>
                <data:p V="99203">IRAQ</data:p>
                <data:p V="99207">ISRAEL</data:p>
                <data:p V="99217">JAPON</data:p>
                <data:p V="99222">JORDANIE</data:p>
                <data:p V="99211">KAMTCHATKA</data:p>
                <data:p V="99256">KAZAKHSTAN</data:p>
                <data:p V="99257">KIRGHIZISTAN</data:p>
                <data:p V="99240">KOWEIT</data:p>
                <data:p V="99241">LAOS</data:p>
                <data:p V="99205">LIBAN</data:p>
                <data:p V="99232">MACAO</data:p>
                <data:p V="99227">MALAISIE</data:p>
                <data:p V="99229">MALDIVES</data:p>
                <data:p V="99218">MANDCHOURIE</data:p>
                <data:p V="99242">MONGOLIE</data:p>
                <data:p V="99215">NEPAL</data:p>
                <data:p V="99250">OMAN</data:p>
                <data:p V="99258">OUZBEKISTAN</data:p>
                <data:p V="99213">PAKISTAN</data:p>
                <data:p V="99261">PALESTINE</data:p>
                <data:p V="99220">PHILIPPINES</data:p>
                <data:p V="99221">POSSESSIONS BRITANNIQUES AU PROCHE-ORIENT</data:p>
                <data:p V="99248">QATAR</data:p>
                <data:p V="99209">SIBERIE</data:p>
                <data:p V="99226">SINGAPOUR</data:p>
                <data:p V="99235">SRI LANKA</data:p>
                <data:p V="99206">SYRIE</data:p>
                <data:p V="99259">TADJIKISTAN</data:p>
                <data:p V="99236">TAIWAN</data:p>
                <data:p V="99219">THAILANDE</data:p>
                <data:p V="99262">TIMOR ORIENTAL</data:p>
                <data:p V="99210">TURKESTAN RUSSE</data:p>
                <data:p V="99260">TURKMENISTAN</data:p>
                <data:p V="99208">TURQUIE</data:p>
                <data:p V="99243">VIET NAM</data:p>
                <data:p V="99244">VIET NAM DU NORD</data:p>
                <data:p V="99245">VIET NAM DU SUD</data:p>
                <data:p V="99251">YEMEN</data:p>
                <data:p V="99233">YEMEN DEMOCRATIQUE</data:p>
                <data:p V="99202">YEMEN (REPUBLIQUE ARABE DU)</data:p>
                <data:p V="99319">ACORES, MADERE</data:p>
                <data:p V="99303">AFRIQUE DU SUD</data:p>
                <data:p V="99352">ALGERIE</data:p>
                <data:p V="99395">ANGOLA</data:p>
                <data:p V="99327">BENIN</data:p>
                <data:p V="99347">BOTSWANA</data:p>
                <data:p V="99331">BURKINA</data:p>
                <data:p V="99321">BURUNDI</data:p>
                <data:p V="99322">CAMEROUN</data:p>
                <data:p V="99305">CAMEROUN ET TOGO</data:p>
                <data:p V="99313">CANARIES (ILES)</data:p>
                <data:p V="99396">CAP-VERT</data:p>
                <data:p V="99323">CENTRAFRICAINE (REPUBLIQUE)</data:p>
                <data:p V="99397">COMORES</data:p>
                <data:p V="99324">CONGO</data:p>
                <data:p V="99312">CONGO (REPUBLIQUE DEMOCRATIQUE)</data:p>
                <data:p V="99326">COTE D'IVOIRE</data:p>
                <data:p V="99399">DJIBOUTI</data:p>
                <data:p V="99301">EGYPTE</data:p>
                <data:p V="99317">ERYTHREE</data:p>
                <data:p V="99315">ETHIOPIE</data:p>
                <data:p V="99328">GABON</data:p>
                <data:p V="99304">GAMBIE</data:p>
                <data:p V="99329">GHANA</data:p>
                <data:p V="99330">GUINEE</data:p>
                <data:p V="99314">GUINEE EQUATORIALE</data:p>
                <data:p V="99392">GUINEE-BISSAU</data:p>
                <data:p V="99320">ILES PORTUGAISES DE L'OCEAN INDIEN</data:p>
                <data:p V="99332">KENYA</data:p>
                <data:p V="99348">LESOTHO</data:p>
                <data:p V="99302">LIBERIA</data:p>
                <data:p V="99316">LIBYE</data:p>
                <data:p V="99333">MADAGASCAR</data:p>
                <data:p V="99334">MALAWI</data:p>
                <data:p V="99335">MALI</data:p>
                <data:p V="99350">MAROC</data:p>
                <data:p V="99390">MAURICE</data:p>
                <data:p V="99336">MAURITANIE</data:p>
                <data:p V="99393">MOZAMBIQUE</data:p>
                <data:p V="99311">NAMIBIE</data:p>
                <data:p V="99337">NIGER</data:p>
                <data:p V="99338">NIGERIA</data:p>
                <data:p V="99308">OCEAN INDIEN (TERRITOIRE BRITANNIQUE DE L')</data:p>
                <data:p V="99339">OUGANDA</data:p>
                <data:p V="99313">PRESIDES</data:p>
                <data:p V="99313">PROVINCES ESPAGNOLES D'AFRIQUE</data:p>
                <data:p V="99340">RWANDA</data:p>
                <data:p V="99389">SAHARA OCCIDENTAL</data:p>
                <data:p V="99306">SAINTE-HELENE</data:p>
                <data:p V="99394">SAO TOME-ET-PRINCIPE</data:p>
                <data:p V="99341">SENEGAL</data:p>
                <data:p V="99398">SEYCHELLES</data:p>
                <data:p V="99342">SIERRA LEONE</data:p>
                <data:p V="99318">SOMALIE</data:p>
                <data:p V="99343">SOUDAN</data:p>
                <data:p V="99307">SOUDAN ANGLO-EGYPTIEN, KENYA, OUGANDA</data:p>
                <data:p V="99391">SWAZILAND</data:p>
                <data:p V="99325">TANGER</data:p>
                <data:p V="99309">TANZANIE</data:p>
                <data:p V="99344">TCHAD</data:p>
                <data:p V="99345">TOGO</data:p>
                <data:p V="99351">TUNISIE</data:p>
                <data:p V="99346">ZAMBIE</data:p>
                <data:p V="99308">ZANZIBAR</data:p>
                <data:p V="99310">ZIMBABWE</data:p>
                <data:p V="99404">ALASKA</data:p>
                <data:p V="99425">ANGUILLA</data:p>
                <data:p V="99441">ANTIGUA-ET-BARBUDA</data:p>
                <data:p V="99431">ANTILLES NEERLANDAISES</data:p>
                <data:p V="99415">ARGENTINE</data:p>
                <data:p V="99431">ARUBA</data:p>
                <data:p V="99436">BAHAMAS</data:p>
                <data:p V="99434">BARBADE</data:p>
                <data:p V="99429">BELIZE</data:p>
                <data:p V="99425">BERMUDES</data:p>
                <data:p V="99418">BOLIVIE</data:p>
                <data:p V="99416">BRESIL</data:p>
                <data:p V="99425">CAIMAN (ILES)</data:p>
                <data:p V="99401">CANADA</data:p>
                <data:p V="99417">CHILI</data:p>
                <data:p V="99419">COLOMBIE</data:p>
                <data:p V="99406">COSTA RICA</data:p>
                <data:p V="99407">CUBA</data:p>
                <data:p V="99408">DOMINICAINE (REPUBLIQUE)</data:p>
                <data:p V="99438">DOMINIQUE</data:p>
                <data:p V="99414">EL SALVADOR</data:p>
                <data:p V="99420">EQUATEUR</data:p>
                <data:p V="99404">ETATS-UNIS</data:p>
                <data:p V="99427">GEORGIE DU SUD ET LES ILES SANDWICH DU SUD</data:p>
                <data:p V="99435">GRENADE</data:p>
                <data:p V="99430">GROENLAND</data:p>
                <data:p V="99409">GUATEMALA</data:p>
                <data:p V="99428">GUYANA</data:p>
                <data:p V="99410">HAITI</data:p>
                <data:p V="99411">HONDURAS</data:p>
                <data:p V="99426">JAMAIQUE</data:p>
                <data:p V="99403">LABRADOR</data:p>
                <data:p V="99427">MALOUINES, OU FALKLAND (ILES)</data:p>
                <data:p V="99405">MEXIQUE</data:p>
                <data:p V="99425">MONTSERRAT</data:p>
                <data:p V="99412">NICARAGUA</data:p>
                <data:p V="99413">PANAMA</data:p>
                <data:p V="99421">PARAGUAY</data:p>
                <data:p V="99422">PEROU</data:p>
                <data:p V="99432">PORTO RICO</data:p>
                <data:p V="99442">SAINT-CHRISTOPHE-ET-NIEVES</data:p>
                <data:p V="99439">SAINTE-LUCIE</data:p>
                <data:p V="99440">SAINT-VINCENT-ET-LES GRENADINES</data:p>
                <data:p V="99437">SURINAME</data:p>
                <data:p V="99432">TERR. DES ETATS-UNIS D'AMERIQUE EN AMERIQUE</data:p>
                <data:p V="99427">TERR. DU ROYAUME-UNI DANS L'ATLANTIQUE SUD</data:p>
                <data:p V="99402">TERRE-NEUVE</data:p>
                <data:p V="99431">TERRITOIRE DES PAYS-BAS</data:p>
                <data:p V="99425">TERRITOIRES DU ROYAUME-UNI AUX ANTILLES</data:p>
                <data:p V="99433">TRINITE-ET-TOBAGO</data:p>
                <data:p V="99425">TURKS ET CAIQUES (ILES)</data:p>
                <data:p V="99423">URUGUAY</data:p>
                <data:p V="99424">VENEZUELA</data:p>
                <data:p V="99425">VIERGES BRITANNIQUES (ILES)</data:p>
                <data:p V="99432">VIERGES DES ETATS-UNIS (ILES)</data:p>
                <data:p V="99501">AUSTRALIE</data:p>
                <data:p V="99501">CHRISTMAS (ILE)</data:p>
                <data:p V="99501">COCOS ou KEELING (ILES)</data:p>
                <data:p V="99502">COOK (ILES)</data:p>
                <data:p V="99508">FIDJI</data:p>
                <data:p V="99505">GUAM</data:p>
                <data:p V="99504">HAWAII (ILES)</data:p>
                <data:p V="99501">HEARD ET MACDONALD (ILES)</data:p>
                <data:p V="99513">KIRIBATI</data:p>
                <data:p V="99505">MARIANNES DU NORD (ILES)</data:p>
                <data:p V="99515">MARSHALL (ILES)</data:p>
                <data:p V="99516">MICRONESIE (ETATS FEDERES DE)</data:p>
                <data:p V="99507">NAURU</data:p>
                <data:p V="99502">NIUE</data:p>
                <data:p V="99501">NORFOLK (ILE)</data:p>
                <data:p V="99502">NOUVELLE-ZELANDE</data:p>
                <data:p V="99517">PALAOS (ILES)</data:p>
                <data:p V="99510">PAPOUASIE-NOUVELLE-GUINEE</data:p>
                <data:p V="99503">PITCAIRN (ILE)</data:p>
                <data:p V="99512">SALOMON (ILES)</data:p>
                <data:p V="99505">SAMOA AMERICAINES</data:p>
                <data:p V="99506">SAMOA OCCIDENTALES</data:p>
                <data:p V="99505">TERR. DES ETATS-UNIS D'AMERIQUE EN OCEANIE</data:p>
                <data:p V="99502">TOKELAU</data:p>
                <data:p V="99509">TONGA</data:p>
                <data:p V="99511">TUVALU</data:p>
                <data:p V="99514">VANUATU</data:p>
            </data:codePaysINSEE>
            <data:typeAdresse>
                <data:a V="0"></data:a>
                <data:a V="1">principal</data:a>
                <data:a V="2">secondaire</data:a>
            </data:typeAdresse>
            <data:moyenPaiement>
                <data:m type="01">Num&#233;raire</data:m>
                <data:m type="02">Ch&#232;que</data:m>
                <data:m type="03">Virement</data:m>
                <data:m type="04">Virement appli externe</data:m>
                <data:m type="05">Virement gros montant</data:m>
                <data:m type="06">Virement &#224; l&apos;&#233;tranger</data:m>
                <data:m type="07">Op&#233;ration budget rattach&#233;</data:m>
                <data:m type="08">Op&#233;ration d&apos;ordre</data:m>
                <data:m type="09">Autres</data:m>
                <data:m type="10">Pr&#233;l&#232;vement</data:m>
                <data:m type="11">Virement interne</data:m>
            </data:moyenPaiement>
        </data:data>
    </xsl:variable>

    <xsl:include href="./mandatBordereauUtils.xsl"/>

    <!-- template pour convertir le code du Type de bordereau en libell&#233; -->
    <xsl:template name="typeBordereauLibelle">
        <xsl:param name="typeBord"/>
        <xsl:value-of select="$data/data:data/data:typeBordereau/data:t[@type=$typeBord]/text()"/>
    </xsl:template>
    <!-- pour convertir le code du Type de pi&#232;ce en libell&#233; -->
    <xsl:template name="typePieceLibelle">
        <xsl:param name="typePiece"/>
        <xsl:value-of select="$data/data:data/data:typePiece/data:t[@type=$typePiece]/text()"/>
    </xsl:template>
    <!-- pour convertir le code de la nature de pi&#232;ce en libell&#233; -->
    <xsl:template name="naturePieceLibelle">
        <xsl:param name="naturePiece"/>
        <xsl:value-of select="$data/data:data/data:naturePiece/data:n[@type=$naturePiece]/text()"/>
    </xsl:template>
    <!-- pour convertir le type de pi&#232;ce d'origine en libell&#233; -->
    <xsl:template name="typePieceOrigine">
        <xsl:param name="typePiece"/>
        <xsl:value-of select="$data/data:data/data:typePieceOrigine/data:t[@type=$typePiece]/text()"/>
    </xsl:template>
    <!-- pour le type d'identifiant du tiers au niveau national -->
    <xsl:template name="typeTiersNational">
        <xsl:param name="typeTiers"/>
        <xsl:value-of select="$data/data:data/data:typeTiersNational/data:t[@type=$typeTiers]/text()"/>
    </xsl:template>
    <!-- cat&#233;gorie du tiers -->
    <xsl:template name="categorieTiers">
        <xsl:param name="catTiers"/>
        <xsl:value-of select="$data/data:data/data:categorieTiers/data:c[@V=$catTiers]/text()"/>
    </xsl:template>
    <!-- Nature juridique du tiers -->
    <xsl:template name="natureJuridiqueTiers">
        <xsl:param name="natJur"/>
        <xsl:value-of select="$data/data:data/data:natureJuridiqueTiers/data:t[@V=$natJur]/text()"/>
    </xsl:template>
    <!-- Correspondance entre le code de pays INSEE et son nom -->
    <xsl:template xmlns:data="data.uri" name="codePaysINSEE">
        <xsl:param name="codePays"/>
        <xsl:value-of select="$data/data:data/data:codePaysINSEE/data:p[@V=$codePays]/text()"/>
    </xsl:template>
    <!-- Type d'adresse -->
    <xsl:template name="typeAdresse">
        <xsl:param name="codeType"/>
        <xsl:value-of select="$data/data:data/data:typeAdresse/data:a[@V=$codeType]/text()"/>
    </xsl:template>
    <!-- pour le moyen de paiement -->
    <xsl:template name="moyenPaiement">
        <xsl:param name="typeMoyen"/>
        <xsl:value-of select="$data/data:data/data:moyenPaiement/data:m[@type=$typeMoyen]/text()"/>
    </xsl:template>

    <xsl:template name="slash-date">
        <xsl:param name="datebrute"/>
        <!--date xmlns:data="data.uri" xsl:exclude-result-prefixes="data"-->
        <xsl:if test="string-length($datebrute) &gt; 0">
            <xsl:value-of select="substring($datebrute, 9, 2)"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="substring($datebrute, 6, 2)"/>
            <xsl:text>/</xsl:text>
            <xsl:value-of select="substring($datebrute, 1, 4)"/>
        </xsl:if>
        <!--/date-->
    </xsl:template>
    <xsl:template name="number">
        <xsl:param name="num"/>
        <xsl:choose>
            <xsl:when test="count($num) &gt; 1">
                <xsl:value-of select="format-number(sum($num),'# ##0,00','decformat')"/>
            </xsl:when>
            <xsl:when test="string-length(string($num)) = 0">0,00</xsl:when>
            <xsl:when test="number($num) = 0">0,00</xsl:when>
            <xsl:when test="string(number($num)) = 'NaN'"/>
            <xsl:otherwise>
                <xsl:value-of select="format-number($num,'# ##0,00;-# ##0,00','decformat')"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- d&#233;but du document -->
    <xsl:template match="/n:PES_Aller">
        <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
            <head>
                <title>
                    <xsl:choose>
                        <xsl:when test="$elementId='Bordereau'">Bordereau</xsl:when>
                        <xsl:otherwise>Titre</xsl:otherwise>
                    </xsl:choose>
                    de recette
                </title>
                <script>
                    var checkForPjExistence = function(id, success) {
                    var xhr = new XMLHttpRequest();
                    var req = document.location.origin + document.location.pathname.replace('browse', 'exists');
                    var session = document.location.search.split('xwvSession=')[1];
                    xhr.open('GET', req + '?pjId=' + id + '<xsl:text disable-output-escaping="yes"><![CDATA[&]]></xsl:text>collId=<xsl:value-of select="$collId"/><xsl:text disable-output-escaping="yes"><![CDATA[&]]></xsl:text>xwvSession=' + session);
                    xhr.onreadystatechange = function () {
                    var DONE = 4; // readyState 4 means the request is done.
                    var OK = 200; // status 200 is a successful return.
                    if (xhr.readyState === DONE) {
                    if (xhr.status === OK) {
                    success(id, xhr.responseText === "true");
                    } else {
                    console.log('Error: ' + xhr.status); // An error occurred during the request.
                    }
                    }
                    };
                    xhr.send(null);
                    }
                </script>
            </head>
            <body>

                <h1 class="h3">
                    <xsl:call-template name="afficher.balise">
                        <xsl:with-param name="element.nom" select="'IdPost'"/>
                        <xsl:with-param name="element" select="./n:EnTetePES/n:IdPost"/>
                        <xsl:with-param name="parent.nodeid" select="./n:EnTetePES/@ano:node-id"/>
                    </xsl:call-template>&nbsp;<xsl:call-template name="afficher.balise">
                    <xsl:with-param name="element.nom" select="'LibellePoste'"/>
                    <xsl:with-param name="element" select="./n:EnTetePES/n:LibellePoste"/>
                    <xsl:with-param name="parent.nodeid" select="./n:EnTetePES/@ano:node-id"/>
                </xsl:call-template>
                </h1>

                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="xemelios:/query?docId=pes-aller&amp;etatId=general_view">Accueil</a>
                        </li>
                        <li>
                            <xsl:choose>
                                <xsl:when test="not($mandatId)">
                                    <xsl:attribute name="class">breadcrumb-item active</xsl:attribute>Bordereau
                                    <xsl:call-template name="afficher.balise">
                                        <xsl:with-param name="element.nom" select="'IdBord'"/>
                                        <xsl:with-param name="element" select="$bordereau/n:BlocBordereau/n:IdBord"/>
                                        <xsl:with-param name="parent.nodeid" select="$bordereau/@ano:node-id"/>
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:attribute name="class">breadcrumb-item</xsl:attribute>
                                    <a>
                                        <xsl:attribute name="href">xemelios:/query?docId=pes-aller&amp;etatId=PES_RecetteAller&amp;elementId=BordereauRecette&amp;collectivite=<xsl:value-of
                                                select="/n:PES_Aller/n:EnTetePES/n:IdColl/@V"/>&amp;budget=<xsl:value-of
                                                select="/n:PES_Aller/n:EnTetePES/n:CodBud/@V"/>&amp;path=[@added:primary-key='<xsl:value-of
                                                select="$bordereau/@added:primary-key"/>']&amp;xsl:param=(elementId,Bordereau)
                                        </xsl:attribute>Bordereau
                                        <xsl:call-template name="afficher.balise">
                                            <xsl:with-param name="element.nom" select="'IdBord'"/>
                                            <xsl:with-param name="element"
                                                            select="$bordereau/n:BlocBordereau/n:IdBord"/>
                                            <xsl:with-param name="parent.nodeid" select="$bordereau/@ano:node-id"/>
                                        </xsl:call-template>
                                    </a>
                                </xsl:otherwise>
                            </xsl:choose>
                        </li>
                        <xsl:if test="$mandatId != ''">
                            <li class="breadcrumb-item active">Titre
                                <xsl:value-of select="$mandatId"/>
                            </li>
                        </xsl:if>
                    </ol>
                </nav>

                <h2 class="h4">
                    Exercice
                    <xsl:call-template name="afficher.balise">
                        <xsl:with-param name="element.nom" select="'Exer'"/>
                        <xsl:with-param name="element" select="$bordereau/n:BlocBordereau/n:Exer"/>
                        <xsl:with-param name="parent.nodeid" select="$bordereau/@ano:node-id"/>
                    </xsl:call-template>
                </h2>

                <xsl:choose>

                    <!-- affichage du bordereau -->
                    <xsl:when test="$elementId='Bordereau'">

                        <!-- table pour le titre du bordereau -->
                        <table class="entete" cellspacing="0">
                            <tr>
                                <td class="colonne1">
                                    <xsl:element name="codeCol">
                                        <xsl:call-template name="afficher.balise">
                                            <xsl:with-param name="element.nom" select="'CodCol'"/>
                                            <xsl:with-param name="element" select="./n:EnTetePES/n:CodCol"/>
                                            <xsl:with-param name="parent.nodeid" select="./n:EnTetePES/@ano:node-id"/>
                                        </xsl:call-template>
                                    </xsl:element>
                                    <xsl:element name="codeBudget">
                                        <xsl:call-template name="afficher.balise">
                                            <xsl:with-param name="element.nom" select="'CodBud'"/>
                                            <xsl:with-param name="element" select="./n:EnTetePES/n:CodBud"/>
                                            <xsl:with-param name="parent.nodeid" select="./n:EnTetePES/@ano:node-id"/>
                                        </xsl:call-template>
                                    </xsl:element>
                                    &nbsp;
                                    <xsl:call-template name="afficher.balise">
                                        <xsl:with-param name="element.nom" select="'LibelleColBud'"/>
                                        <xsl:with-param name="element" select="./n:EnTetePES/n:LibelleColBud"/>
                                        <xsl:with-param name="parent.nodeid" select="./n:EnTetePES/@ano:node-id"/>
                                    </xsl:call-template>
                                    <br/>
                                    <xsl:element name="siret">
                                        <xsl:call-template name="afficher.balise">
                                            <xsl:with-param name="element.nom" select="'IdColl'"/>
                                            <xsl:with-param name="element" select="./n:EnTetePES/n:IdColl"/>
                                            <xsl:with-param name="parent.nodeid" select="./n:EnTetePES/@ano:node-id"/>
                                        </xsl:call-template>
                                    </xsl:element>
                                    <br/>
                                    <xsl:element name="finJur">
                                        <xsl:call-template name="afficher.balise">
                                            <xsl:with-param name="element.nom" select="'FinJur'"/>
                                            <xsl:with-param name="element" select="./n:EnTetePES/n:FinJur"/>
                                            <xsl:with-param name="parent.nodeid" select="./n:EnTetePES/@ano:node-id"/>
                                        </xsl:call-template>
                                    </xsl:element>
                                </td>

                                <td class="colonne2">Bordereau
                                    <xsl:variable name="libelleTypBord">
                                        <xsl:call-template name="typeBordereauLibelle">
                                            <xsl:with-param name="typeBord"
                                                            select="$bordereau/n:BlocBordereau/n:TypBord/@V"/>
                                        </xsl:call-template>
                                    </xsl:variable>
                                    <xsl:call-template name="afficher.balise">
                                        <xsl:with-param name="element.nom" select="'CodBud'"/>
                                        <xsl:with-param name="element" select="./n:EnTetePES/n:CodBud"/>
                                        <xsl:with-param name="libelle" select="$libelleTypBord"/>
                                        <xsl:with-param name="parent.nodeid" select="./n:EnTetePES/@ano:node-id"/>
                                    </xsl:call-template>
                                    <br/>

                                    <xsl:call-template name="afficher.balise">
                                        <xsl:with-param name="element.nom" select="'NbrPce'"/>
                                        <xsl:with-param name="element" select="$bordereau/n:BlocBordereau/n:NbrPce"/>
                                        <xsl:with-param name="parent.nodeid" select="$bordereau/@ano:node-id"/>
                                    </xsl:call-template>
                                    &nbsp;
                                    <xsl:choose>
                                        <xsl:when test="$bordereau/n:BlocBordereau/n:NbrPce/@V = 1">Titre</xsl:when>
                                        <xsl:otherwise>Titres</xsl:otherwise>
                                    </xsl:choose>
                                </td>
                                <td class="noborder">Date d'&#233;mission</td>
                                <td class="noborder">
                                    <xsl:variable name="libelleDteBordEm">
                                        <xsl:call-template name="slash-date">
                                            <xsl:with-param name="datebrute"
                                                            select="$bordereau/n:BlocBordereau/n:DteBordEm/@V"></xsl:with-param>
                                        </xsl:call-template>
                                    </xsl:variable>
                                    <xsl:call-template name="afficher.balise">
                                        <xsl:with-param name="element.nom" select="'DteBordEm'"/>
                                        <xsl:with-param name="element" select="$bordereau/n:BlocBordereau/n:DteBordEm"/>
                                        <xsl:with-param name="libelle" select="$libelleDteBordEm"/>
                                        <xsl:with-param name="parent.nodeid" select="$bordereau/@ano:node-id"/>
                                    </xsl:call-template>
                                </td>
                            </tr>
                        </table>

                        <!-- entete de la table principale -->
                        <table class="enteteTablePrincipale" cellspacing="0">
                            <thead>
                                <tr>
                                    <th rowspan="2">Nom et adresse du d&#233;biteur</th>
                                    <th rowspan="2">Titre</th>
                                    <th rowspan="2">Objet titre</th>
                                    <th colspan="3">Imputation</th>
                                    <th colspan="3" class="text-right">Somme</th>
                                </tr>
                                <tr>
                                    <th>Nature</th>
                                    <th>Fonction</th>
                                    <th>Op&#233;ration</th>
                                    <th class="text-right">HT</th>
                                    <th class="text-right">TVA</th>
                                    <th class="text-right">TTC</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- contenu de la table principale -->
                                <xsl:for-each select="$bordereau/n:Piece">
                                    <xsl:element name="tr">
                                        <td class="principaleCol1">
                                            <xsl:choose>
                                                <xsl:when
                                                        test="count(distinct-values(n:LigneDePiece/n:Tiers[1]/n:InfoTiers/@added:key)) = 1">
                                                    <xsl:if test="./n:LigneDePiece[1]/n:Tiers/n:InfoTiers/n:NatIdTiers/@V">
                                                        <xsl:variable name="piece.libelleNatIdTiers">
                                                            <xsl:call-template name="typeTiersNational">
                                                                <xsl:with-param name="typeTiers"
                                                                                select="./n:LigneDePiece[1]/n:Tiers/n:InfoTiers/n:NatIdTiers/@V"></xsl:with-param>
                                                            </xsl:call-template>
                                                        </xsl:variable>
                                                        <xsl:call-template name="afficher.balise">
                                                            <xsl:with-param name="element.nom" select="'NatIdTiers'"/>
                                                            <xsl:with-param name="element"
                                                                            select="./n:LigneDePiece[1]/n:Tiers/n:InfoTiers/n:NatIdTiers"/>
                                                            <xsl:with-param name="libelle"
                                                                            select="$piece.libelleNatIdTiers"/>
                                                            <xsl:with-param name="parent.nodeid"
                                                                            select="./n:LigneDePiece[1]/n:Tiers[1]/@ano:node-id"/>
                                                        </xsl:call-template>
                                                        &nbsp;:&nbsp;
                                                    </xsl:if>
                                                    <xsl:call-template name="afficher.balise">
                                                        <xsl:with-param name="element.nom" select="'IdTiers'"/>
                                                        <xsl:with-param name="element"
                                                                        select="./n:LigneDePiece[1]/n:Tiers/n:InfoTiers/n:IdTiers"/>
                                                        <xsl:with-param name="parent.nodeid"
                                                                        select="./n:LigneDePiece[1]/n:Tiers[1]/@ano:node-id"/>
                                                    </xsl:call-template>
                                                    <br/>

                                                    <xsl:variable name="piece.libelleCatTiers">
                                                        <xsl:call-template name="categorieTiers">
                                                            <xsl:with-param name="catTiers"
                                                                            select="./n:LigneDePiece[1]/n:Tiers/n:InfoTiers/n:CatTiers/@V"></xsl:with-param>
                                                        </xsl:call-template>
                                                    </xsl:variable>
                                                    <xsl:call-template name="afficher.balise">
                                                        <xsl:with-param name="element.nom" select="'CatTiers'"/>
                                                        <xsl:with-param name="element"
                                                                        select="./n:LigneDePiece[1]/n:Tiers/n:InfoTiers/n:CatTiers"/>
                                                        <xsl:with-param name="libelle" select="$piece.libelleCatTiers"/>
                                                        <xsl:with-param name="parent.nodeid"
                                                                        select="./n:LigneDePiece[1]/n:Tiers[1]/@ano:node-id"/>
                                                    </xsl:call-template>
                                                    <br/>

                                                    <xsl:variable name="piece.libelleNatJur">
                                                        <xsl:call-template name="natureJuridiqueTiers">
                                                            <xsl:with-param name="natJur"
                                                                            select="./n:LigneDePiece[1]/n:Tiers/n:InfoTiers/n:NatJur/@V"></xsl:with-param>
                                                        </xsl:call-template>
                                                    </xsl:variable>
                                                    <xsl:call-template name="afficher.balise">
                                                        <xsl:with-param name="element.nom" select="'NatJur'"/>
                                                        <xsl:with-param name="element"
                                                                        select="./n:LigneDePiece[1]/n:Tiers/n:InfoTiers/n:NatJur"/>
                                                        <xsl:with-param name="libelle" select="$piece.libelleNatJur"/>
                                                        <xsl:with-param name="parent.nodeid"
                                                                        select="./n:LigneDePiece[1]/n:Tiers[1]/@ano:node-id"/>
                                                    </xsl:call-template>
                                                    <br/>

                                                    <xsl:call-template name="afficher.balise">
                                                        <xsl:with-param name="element.nom" select="'Nom'"/>
                                                        <xsl:with-param name="element"
                                                                        select="./n:LigneDePiece[1]/n:Tiers[1]/n:InfoTiers/n:Nom"/>
                                                        <xsl:with-param name="parent.nodeid"
                                                                        select="./n:LigneDePiece[1]/n:Tiers[1]/@ano:node-id"/>
                                                    </xsl:call-template>
                                                    <xsl:if test="string-length(./n:LigneDePiece[1]/n:Tiers[1]/n:InfoTiers/n:Nom/@V) &gt; 0">
                                                        &nbsp;
                                                    </xsl:if>
                                                    <xsl:call-template name="afficher.balise">
                                                        <xsl:with-param name="element.nom" select="'Prenom'"/>
                                                        <xsl:with-param name="element"
                                                                        select="./n:LigneDePiece[1]/n:Tiers[1]/n:InfoTiers/n:Prenom"/>
                                                        <xsl:with-param name="parent.nodeid"
                                                                        select="./n:LigneDePiece[1]/n:Tiers[1]/@ano:node-id"/>
                                                    </xsl:call-template>
                                                    <xsl:if test="string-length(./n:LigneDePiece[1]/n:Tiers[1]/n:InfoTiers/n:Prenom/@V) &gt; 0">
                                                        &nbsp;
                                                    </xsl:if>
                                                    <xsl:call-template name="afficher.balise">
                                                        <xsl:with-param name="element.nom" select="'ComplNom'"/>
                                                        <xsl:with-param name="element"
                                                                        select="./n:LigneDePiece[1]/n:Tiers[1]/n:InfoTiers/n:ComplNom"/>
                                                        <xsl:with-param name="parent.nodeid"
                                                                        select="./n:LigneDePiece[1]/n:Tiers[1]/@ano:node-id"/>
                                                    </xsl:call-template>
                                                    <xsl:if test="string-length(./n:LigneDePiece[1]/n:Tiers[1]/n:InfoTiers/n:ComplNom/@V) &gt; 0">
                                                        &nbsp;
                                                    </xsl:if>
                                                    <xsl:if test="(string-length(./n:LigneDePiece[1]/n:Tiers[1]/n:InfoTiers/n:Nom/@V)+string-length(./n:LigneDePiece[1]/n:Tiers[1]/n:InfoTiers/n:Prenom/@V)+string-length(./n:LigneDePiece[1]/n:Tiers[1]/n:InfoTiers/n:ComplNom/@V)) &gt; 0">
                                                        <br/>
                                                    </xsl:if>
                                                    <xsl:call-template name="afficher.balise">
                                                        <xsl:with-param name="element.nom" select="'Adr1'"/>
                                                        <xsl:with-param name="element"
                                                                        select="./n:LigneDePiece[1]/n:Tiers[1]/n:Adresse/n:Adr1"/>
                                                        <xsl:with-param name="parent.nodeid"
                                                                        select="./n:LigneDePiece[1]/n:Tiers[1]/@ano:node-id"/>
                                                    </xsl:call-template>
                                                    <xsl:if test="string-length(./n:LigneDePiece[1]/n:Tiers[1]/n:Adresse/n:Adr1/@V) &gt; 0">
                                                        <br/>
                                                    </xsl:if>
                                                    <xsl:call-template name="afficher.balise">
                                                        <xsl:with-param name="element.nom" select="'Adr2'"/>
                                                        <xsl:with-param name="element"
                                                                        select="./n:LigneDePiece[1]/n:Tiers[1]/n:Adresse/n:Adr2"/>
                                                        <xsl:with-param name="parent.nodeid"
                                                                        select="./n:LigneDePiece[1]/n:Tiers[1]/@ano:node-id"/>
                                                    </xsl:call-template>
                                                    <xsl:if test="string-length(./n:LigneDePiece[1]/n:Tiers[1]/n:Adresse/n:Adr2/@V) &gt; 0">
                                                        <br/>
                                                    </xsl:if>

                                                    <xsl:call-template name="afficher.balise">
                                                        <xsl:with-param name="element.nom" select="'CP'"/>
                                                        <xsl:with-param name="element"
                                                                        select="./n:LigneDePiece[1]/n:Tiers[1]/n:Adresse/n:CP"/>
                                                        <xsl:with-param name="parent.nodeid"
                                                                        select="./n:LigneDePiece[1]/n:Tiers[1]/@ano:node-id"/>
                                                    </xsl:call-template>
                                                    &nbsp;
                                                    <xsl:call-template name="afficher.balise">
                                                        <xsl:with-param name="element.nom" select="'Ville'"/>
                                                        <xsl:with-param name="element"
                                                                        select="./n:LigneDePiece[1]/n:Tiers[1]/n:Adresse/n:Ville"/>
                                                        <xsl:with-param name="parent.nodeid"
                                                                        select="./n:LigneDePiece[1]/n:Tiers[1]/@ano:node-id"/>
                                                    </xsl:call-template>

                                                    <xsl:if test="string-length(./n:LigneDePiece[1]/n:Tiers[1]/n:Adresse/n:CodPays) &gt; 0">
                                                        &nbsp;-&nbsp;
                                                    </xsl:if>
                                                    <xsl:variable name="libelleCodePays">
                                                        <xsl:call-template name="codePaysINSEE">
                                                            <xsl:with-param name="codePays"
                                                                            select="./n:LigneDePiece[1]/n:Tiers[1]/n:Adresse/n:CodPays/@V"></xsl:with-param>
                                                        </xsl:call-template>
                                                    </xsl:variable>
                                                    <xsl:call-template name="afficher.balise">
                                                        <xsl:with-param name="element.nom" select="'CodPays'"/>
                                                        <xsl:with-param name="element"
                                                                        select="./n:LigneDePiece[1]/n:Tiers[1]/n:Adresse/n:CodPays"/>
                                                        <xsl:with-param name="libelle" select="$libelleCodePays"/>
                                                        <xsl:with-param name="parent.nodeid"
                                                                        select="./n:LigneDePiece[1]/n:Tiers[1]/@ano:node-id"/>
                                                    </xsl:call-template>

                                                </xsl:when>
                                                <xsl:otherwise>DIVERS DEBITEURS</xsl:otherwise>
                                            </xsl:choose>
                                        </td>
                                        <xsl:element name="td">
                                            <xsl:attribute name="class">principaleCol2</xsl:attribute>
                                            <xsl:if test="$browser-destination = 'internal'">
                                                <xsl:element name="a">
                                                    <xsl:attribute name="href">xemelios:/query?docId=pes-aller&amp;etatId=PES_RecetteAller&amp;elementId=BordereauRecette&amp;collectivite=<xsl:value-of
                                                            select="../../../n:EnTetePES/n:IdColl/@V"/>&amp;budget=<xsl:value-of
                                                            select="../../../n:EnTetePES/n:CodBud/@V"/>&amp;path=[@added:primary-key='<xsl:value-of
                                                            select="../@added:primary-key"/>']&amp;xsl:param=(mandatId,<xsl:value-of
                                                            select="n:BlocPiece/n:IdPce/@V"/>)
                                                    </xsl:attribute>
                                                    <xsl:call-template name="afficher.balise">
                                                        <xsl:with-param name="element.nom" select="'IdPce'"/>
                                                        <xsl:with-param name="element" select="./n:BlocPiece/n:IdPce"/>
                                                        <xsl:with-param name="parent.nodeid" select="./@ano:node-id"/>
                                                    </xsl:call-template>
                                                </xsl:element>
                                            </xsl:if>
                                            <xsl:if test="$browser-destination = 'external'">
                                                <xsl:call-template name="afficher.balise">
                                                    <xsl:with-param name="element.nom" select="'IdPce'"/>
                                                    <xsl:with-param name="element" select="./n:BlocPiece/n:IdPce"/>
                                                    <xsl:with-param name="parent.nodeid" select="./@ano:node-id"/>
                                                </xsl:call-template>
                                            </xsl:if>
                                        </xsl:element>
                                        <td class="principaleCol3">
                                            <xsl:call-template name="afficher.balise">
                                                <xsl:with-param name="element.nom" select="'ObjPce'"/>
                                                <xsl:with-param name="element" select="./n:BlocPiece/n:ObjPce"/>
                                                <xsl:with-param name="parent.nodeid" select="./@ano:node-id"/>
                                            </xsl:call-template>
                                            <br/>
                                            <xsl:variable name="libelleTypPce">
                                                <xsl:call-template name="typePieceLibelle">
                                                    <xsl:with-param name="typePiece"
                                                                    select="./n:BlocPiece/n:TypPce/@V"></xsl:with-param>
                                                </xsl:call-template>
                                            </xsl:variable>
                                            <xsl:call-template name="afficher.balise">
                                                <xsl:with-param name="element.nom" select="'TypPce'"/>
                                                <xsl:with-param name="element" select="./n:BlocPiece/n:TypPce"/>
                                                <xsl:with-param name="libelle" select="$libelleTypPce"/>
                                                <xsl:with-param name="parent.nodeid" select="./@ano:node-id"/>
                                            </xsl:call-template>
                                            <br/>
                                            <xsl:variable name="libelleNatPce">
                                                <xsl:call-template name="naturePieceLibelle">
                                                    <xsl:with-param name="naturePiece"
                                                                    select="./n:BlocPiece/n:NatPce/@V"></xsl:with-param>
                                                </xsl:call-template>
                                            </xsl:variable>
                                            <xsl:call-template name="afficher.balise">
                                                <xsl:with-param name="element.nom" select="'NatPce'"/>
                                                <xsl:with-param name="element" select="./n:BlocPiece/n:NatPce"/>
                                                <xsl:with-param name="libelle" select="$libelleNatPce"/>
                                                <xsl:with-param name="parent.nodeid" select="./@ano:node-id"/>
                                            </xsl:call-template>

                                            <xsl:call-template name="showPjTemplate">
                                                <xsl:with-param name="supportLocation" select="./n:BlocPiece/n:PJRef[n:Support/@V=01]"/>
                                            </xsl:call-template>

                                            <br/>
                                        </td>

                                        <!-- boucler dans les colonnes suivantes quand il y a plusieurs cr&#233;anciers -->
                                        <td class="principaleCol4">
                                            <xsl:for-each-group select="./n:LigneDePiece/n:BlocLignePiece/n:InfoLignePiece"
                                                                group-by="@added:imputation">
                                                <xsl:call-template name="afficher.balise">
                                                    <xsl:with-param name="element.nom" select="'Nature'"/>
                                                    <xsl:with-param name="element" select="current-group()/n:Nature"/>
                                                    <xsl:with-param name="parent.nodeid"
                                                                    select="./n:LigneDePiece/@ano:node-id"/>
                                                </xsl:call-template>
                                                <br/>
                                            </xsl:for-each-group>
                                        </td>
                                        <td class="principaleCol5">
                                            <xsl:for-each-group select="./n:LigneDePiece/n:BlocLignePiece/n:InfoLignePiece"
                                                                group-by="@added:imputation">
                                                <xsl:call-template name="afficher.balise">
                                                    <xsl:with-param name="element.nom" select="'Fonction'"/>
                                                    <xsl:with-param name="element" select="current-group()/n:Fonction"/>
                                                    <xsl:with-param name="parent.nodeid"
                                                                    select="./n:LigneDePiece/@ano:node-id"/>
                                                </xsl:call-template>
                                                <br/>
                                            </xsl:for-each-group>
                                        </td>
                                        <td class="principaleCol6">
                                            <xsl:for-each-group select="./n:LigneDePiece/n:BlocLignePiece/n:InfoLignePiece"
                                                                group-by="@added:imputation">
                                                <xsl:call-template name="afficher.balise">
                                                    <xsl:with-param name="element.nom" select="'Operation'"/>
                                                    <xsl:with-param name="element" select="current-group()/n:Operation"/>
                                                    <xsl:with-param name="parent.nodeid"
                                                                    select="./n:LigneDePiece/@ano:node-id"/>
                                                </xsl:call-template>
                                                <br/>
                                            </xsl:for-each-group>
                                        </td>
                                        <td class="text-right">
                                            <xsl:for-each-group select="./n:LigneDePiece/n:BlocLignePiece/n:InfoLignePiece"
                                                                group-by="@added:imputation">
                                                <xsl:variable name="libelleMtHT">
                                                    <xsl:call-template name="number">
                                                        <xsl:with-param name="num"
                                                                        select="current-group()/n:MtHT/@V"></xsl:with-param>
                                                    </xsl:call-template>
                                                </xsl:variable>
                                                <xsl:call-template name="afficher.balise">
                                                    <xsl:with-param name="element.nom" select="'MtHT'"/>
                                                    <xsl:with-param name="element" select="current-group()/n:MtHT"/>
                                                    <xsl:with-param name="libelle" select="$libelleMtHT"/>
                                                    <xsl:with-param name="parent.nodeid"
                                                                    select="./n:LigneDePiece/@ano:node-id"/>
                                                </xsl:call-template>
                                                <br/>
                                            </xsl:for-each-group>
                                        </td>
                                        <td class="text-right">
                                            <xsl:for-each-group select="./n:LigneDePiece/n:BlocLignePiece/n:InfoLignePiece"
                                                                group-by="@added:imputation">
                                                <xsl:variable name="libelleMtTVA">
                                                    <xsl:call-template name="number">
                                                        <xsl:with-param name="num"
                                                                        select="current-group()/n:MtTVA/@V"></xsl:with-param>
                                                    </xsl:call-template>
                                                </xsl:variable>
                                                <xsl:call-template name="afficher.balise">
                                                    <xsl:with-param name="element.nom" select="'MtTVA'"/>
                                                    <xsl:with-param name="element" select="current-group()/n:MtTVA"/>
                                                    <xsl:with-param name="libelle" select="$libelleMtTVA"/>
                                                    <xsl:with-param name="parent.nodeid"
                                                                    select="./n:LigneDePiece/@ano:node-id"/>
                                                </xsl:call-template>
                                                <br/>
                                            </xsl:for-each-group>
                                        </td>
                                        <td class="text-right">
                                            <xsl:for-each-group select="./n:LigneDePiece/n:BlocLignePiece/n:InfoLignePiece"
                                                                group-by="@added:imputation">
                                                <xsl:call-template name="number">
                                                    <xsl:with-param name="num"
                                                                    select="sum(current-group()/n:MtHT/@V)+sum(current-group()/n:MtTVA/@V)"/>
                                                </xsl:call-template>
                                                <br/>
                                            </xsl:for-each-group>
                                        </td>
                                    </xsl:element>
                                </xsl:for-each>
                            </tbody>
                        </table>
                        <p>
                            <br/>
                        </p>
                        <!-- table pour les totaux -->
                        <xsl:variable name="totalTVABordereau">
                            <xsl:choose>
                                <xsl:when test="$bordereau/n:BlocBordereau/n:MtBordTVA/@V">
                                    <xsl:value-of select="$bordereau/n:BlocBordereau/n:MtBordTVA/@V"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="0"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:variable>
                        <xsl:variable name="totalTTCBordereau"
                                      select="$bordereau/n:BlocBordereau/n:MtBordHt/@V + $totalTVABordereau"/>

                        <xsl:variable name="minMandat">
                            <xsl:for-each select="$bordereau/n:Piece/n:BlocPiece/n:IdPce/@V">
                                <xsl:sort data-type="number" order="ascending"/>
                                <xsl:if test="position()=1">
                                    <xsl:value-of select="."/>
                                </xsl:if>
                            </xsl:for-each>
                        </xsl:variable>
                        <xsl:variable name="maxMandat">
                            <xsl:for-each select="$bordereau/n:Piece/n:BlocPiece/n:IdPce/@V">
                                <xsl:sort data-type="number" order="descending"/>
                                <xsl:if test="position()=1">
                                    <xsl:value-of select="."/>
                                </xsl:if>
                            </xsl:for-each>
                        </xsl:variable>
                        <table class="totaux" cellspacing="0">
                            <tr>
                                <td class="noborder" rowspan="5">Arr&#234;t&#233; le pr&#233;sent bordereau &#224; la
                                    somme de
                                    <xsl:call-template name="number">
                                        <xsl:with-param name="num" select="$totalTTCBordereau"></xsl:with-param>
                                    </xsl:call-template>
                                    euros.
                                    <br/>
                                    <xsl:choose>
                                        <xsl:when test="$minMandat = $maxMandat">Comprenant le titre n&#176;<xsl:value-of
                                                select="$minMandat"/>
                                        </xsl:when>
                                        <xsl:otherwise>Comprenant les titres n&#176;<xsl:value-of select="$minMandat"/>
                                            &#224;
                                            <xsl:value-of select="$maxMandat"/>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                    <xsl:variable name="isSigned" select="exists($bordereau/ds:Signature)"/>
                                    <xsl:variable name="isSignedGlobal" select="exists(PES/ds:Signature)"/>
                                    <xsl:if test="count($bordereau/ds:Signature/ds:KeyInfo/ds:X509Data/ds:X509Certificate) &gt; 1">
                                        <xsl:message>Un seul certificat est acc�pt�</xsl:message>
                                    </xsl:if>
                                    <xsl:variable name="signatureProductionPlace"
                                                  select="$bordereau/ds:Signature/ds:Object/*[local-name() eq 'QualifyingProperties']/*[local-name() eq 'SignedProperties']/*[local-name() eq 'SignedSignatureProperties']/*[local-name() eq 'SignatureProductionPlace']"/>
                                    <xsl:variable name="signatureProductionTime"
                                                  select="$bordereau/ds:Signature/ds:Object/*[local-name() eq 'QualifyingProperties']/*[local-name() eq 'SignedProperties']/*[local-name() eq 'SignedSignatureProperties']/*[local-name() eq 'SigningTime']"/>

                                    <xsl:variable name="signature"
                                                  select="$bordereau/ds:Signature/ds:KeyInfo/ds:X509Data"/><!-- ds:X509Certificate/text()-->

                                    <xsl:variable name="nomSignataire">
                                        <xsl:choose>
                                            <xsl:when test="$isSigned">
                                                <xsl:for-each select="$signature/ds:X509Certificate">
                                                    <xsl:value-of select="xem:extractCN(./text())"/>
                                                    <xsl:if test="not(position() = last())">/</xsl:if>
                                                </xsl:for-each>
                                            </xsl:when>
                                            <xsl:otherwise>[NomSignataire]</xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:variable>
                                    <xsl:variable name="role">
                                        <xsl:choose>
                                            <xsl:when
                                                    test="$isSigned and $bordereau/ds:Signature/ds:Object/*[local-name()='QualifyingProperties']/*[local-name()='SignedProperties']/*[local-name()='SignedSignatureProperties']/*[local-name()='SignerRole']">
                                                <xsl:value-of
                                                        select="$bordereau/ds:Signature/ds:Object/*[local-name()='QualifyingProperties']/*[local-name()='SignedProperties']/*[local-name()='SignedSignatureProperties']/*[local-name()='SignerRole']/*[local-name()='ClaimedRoles']/*[local-name()='ClaimedRole'][1]/text()"></xsl:value-of>
                                            </xsl:when>
                                            <xsl:otherwise>[Role]</xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:variable>
                                    <xsl:variable name="email">
                                        <xsl:choose>
                                            <xsl:when test="$isSigned">
                                                <xsl:for-each select="$signature/ds:X509Certificate">
                                                    <xsl:value-of select="xem:extractEMail(./text())"/>
                                                    <xsl:if test="not(position() = last())">/</xsl:if>
                                                </xsl:for-each>
                                            </xsl:when>
                                            <xsl:otherwise>[AdresseEMail]</xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:variable>

                                    <xsl:variable name="libelleSignature">Ce bordereau est sign&#233;. Ces &#233;l&#233;ments
                                        sont d&#233;duits du flux avec pr&#233;sence de signature &#233;lectronique.
                                    </xsl:variable>
                                    <xsl:choose>
                                        <xsl:when test="$isSigned">
                                            <br/>
                                            <br/>
                                            <span class="text-danger">
                                                <xsl:call-template name="afficher.balise">
                                                    <xsl:with-param name="element.nom" select="'Signature'"/>
                                                    <xsl:with-param name="element" select="$bordereau/ds:Signature"/>
                                                    <xsl:with-param name="libelle" select="$libelleSignature"/>
                                                    <xsl:with-param name="parent.nodeid"
                                                                    select="$bordereau/ds:Signature/@ano:node-id"/>
                                                </xsl:call-template>
                                            </span>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <br/>
                                            <emp>
                                                <xsl:value-of select="$bordereau/comment()"/>
                                            </emp>
                                            <span class="text-danger">Ces &#233;l&#233;ments sont d&#233;duits du flux mais
                                                absence de signature &#233;lectronique.
                                            </span>
                                        </xsl:otherwise>
                                    </xsl:choose>

                                    <br/>
                                    <br/>L'ordonnateur
                                    <xsl:value-of select="$nomSignataire"/>
                                    <br/>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<xsl:value-of
                                            select="$role"/>
                                    <br/>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<xsl:value-of
                                            select="$signatureProductionPlace"/>
                                    <br/>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<xsl:value-of
                                            select="$email"/>
                                    <br/>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;le
                                    <xsl:call-template name="slash-date">
                                        <xsl:with-param name="datebrute" select="$signatureProductionTime"/>
                                    </xsl:call-template>
                                </td>
                                <td class="totalLibelle" style="border-style: solid none none solid;">Total HT</td>
                                <td class="text-right" style="border-style: solid solid none none;">
                                    <xsl:variable name="libelleMtBordHt">
                                        <xsl:call-template name="number">
                                            <xsl:with-param name="num"
                                                            select="$bordereau/n:BlocBordereau/n:MtBordHt/@V"></xsl:with-param>
                                        </xsl:call-template>
                                    </xsl:variable>
                                    <xsl:call-template name="afficher.balise">
                                        <xsl:with-param name="element.nom" select="'MtBordHt'"/>
                                        <xsl:with-param name="element" select="$bordereau/n:BlocBordereau/n:MtBordHt"/>
                                        <xsl:with-param name="libelle" select="$libelleMtBordHt"/>
                                        <xsl:with-param name="parent.nodeid" select="$bordereau/@ano:node-id"/>
                                    </xsl:call-template>&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td class="totalLibelle" style="border-style: none none none solid;">Total&nbsp;du&nbsp;pr&#233;sent&nbsp;bordereau</td>
                                <td class="text-right" style="border-style: none solid none none;">
                                    <xsl:call-template name="number">
                                        <xsl:with-param name="num" select="$totalTTCBordereau"/>
                                    </xsl:call-template>&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td class="totalLibelle" style="border-style: none none none solid;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dont
                                    TVA
                                </td>
                                <td class="text-right" style="border-style: none solid none none;">
                                    <xsl:variable name="libelleMtBordTVA">
                                        <xsl:call-template name="number">
                                            <xsl:with-param name="num"
                                                            select="$bordereau/n:BlocBordereau/n:MtBordTVA/@V"></xsl:with-param>
                                        </xsl:call-template>
                                    </xsl:variable>
                                    <xsl:call-template name="afficher.balise">
                                        <xsl:with-param name="element.nom" select="'MtBordTVA'"/>
                                        <xsl:with-param name="element" select="$bordereau/n:BlocBordereau/n:MtBordTVA"/>
                                        <xsl:with-param name="libelle" select="$libelleMtBordTVA"/>
                                        <xsl:with-param name="parent.nodeid" select="$bordereau/@ano:node-id"/>
                                    </xsl:call-template>&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td class="totalLibelle" style="border-style: none none none solid;">Total&nbsp;g&#233;n&#233;ral&nbsp;au&nbsp;pr&#233;c&#233;dent&nbsp;bordereau</td>
                                <td class="text-right" style="border-style: none solid none none;">
                                    <xsl:call-template name="number">
                                        <xsl:with-param name="num"
                                                        select="$bordereau/n:BlocBordereau/n:MtCumulAnnuel/@V - $bordereau/n:BlocBordereau/n:MtBordHt/@V"/>
                                    </xsl:call-template>&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td class="totalLibelle" style="border-style: none none solid solid;">
                                    Cumul&nbsp;annuel
                                </td>
                                <td class="text-right" style="border-style: none solid solid none;">
                                    <xsl:variable name="libelleMtCumulAnnuel">
                                        <xsl:call-template name="number">
                                            <xsl:with-param name="num"
                                                            select="$bordereau/n:BlocBordereau/n:MtCumulAnnuel/@V"></xsl:with-param>
                                        </xsl:call-template>
                                    </xsl:variable>
                                    <xsl:call-template name="afficher.balise">
                                        <xsl:with-param name="element.nom" select="'MtCumulAnnuel'"/>
                                        <xsl:with-param name="element"
                                                        select="$bordereau/n:BlocBordereau/n:MtCumulAnnuel"/>
                                        <xsl:with-param name="libelle" select="$libelleMtCumulAnnuel"/>
                                        <xsl:with-param name="parent.nodeid" select="$bordereau/@ano:node-id"/>
                                    </xsl:call-template>&nbsp;
                                </td>
                            </tr>
                        </table>

                        <!-- Liste des anomalies sur le bordereau -->
                        <p>
                            <br/>
                        </p>
                        <xsl:if test="$bordereau/ano:Anomalie">
                            <hr/>
                            <xsl:for-each select="$bordereau/ano:Anomalie">
                                <xsl:variable name="severity.color"
                                              select="$severity.colors//severity[@Code= $tags-ano//node/@severity]/@Couleur"/>
                                <xsl:element name="div">
                                    <xsl:if test="$anoId = ./@ano:anoId">
                                        <xsl:attribute name="style">background-color: <xsl:value-of
                                                select="$severity.color"/>;
                                        </xsl:attribute>
                                    </xsl:if>
                                    <h3>
                                        <xsl:element name="a">
                                            <xsl:attribute name="name">AnoId_<xsl:value-of select="./@ano:anoId"/>
                                            </xsl:attribute>
                                            <xsl:attribute name="href">xemelios:/query?docId=pes-aller&amp;etatId=PES_RecetteAller&amp;elementId=BordereauRecette&amp;collectivite=<xsl:value-of
                                                    select="../../../n:EnTetePES/n:IdColl/@V"/>&amp;budget=<xsl:value-of
                                                    select="../../../n:EnTetePES/n:CodBud/@V"/>&amp;path=[@added:primary-key='<xsl:value-of
                                                    select="../@added:primary-key"/>']&amp;xsl:param=(elementId,Bordereau)&amp;xsl:param=(anoId,<xsl:value-of
                                                    select="./@ano:anoId"/>)
                                            </xsl:attribute>
                                            Anomalie <!--xsl:value-of select="./@ano:anoId"/-->
                                        </xsl:element>
                                    </h3>
                                    <p>
                                        <span class="gras">Contr&#244;le :</span>
                                        <xsl:value-of select="./@ano:ctrlLibelle"/>&nbsp;(<xsl:value-of
                                            select="./@ano:ctrlId"/>)
                                    </p>
                                    <!--p><span class="gras">R&#232;gle fonctionnelle : </span><xsl:value-of select="./ano:ctrlRegleFonct/text()" disable-output-escaping="yes"/></p-->
                                    <p>
                                        <span class="gras">Message :</span>
                                        <xsl:value-of select="./ano:message/text()" disable-output-escaping="yes"/>
                                    </p>
                                    <!--p><span class="gras">Noeuds : </span><xsl:for-each select="ano:node"><xsl:value-of select="./@ano:id"/>, </xsl:for-each></p-->
                                </xsl:element>
                            </xsl:for-each>
                            <hr/>
                        </xsl:if>
                    </xsl:when>


                    <!-- affichage de la pi�ce -->
                    <xsl:otherwise>
                        <!-- Cas d'un titre -->
                        <xsl:for-each select="$bordereau/n:Piece[number(n:BlocPiece/n:IdPce/@V) = number($mandatId)]">

                            <!-- on calcule les totaux HT et TTC -->
                            <xsl:variable name="totalHT">
                                <xsl:value-of
                                        select="format-number(sum(./n:LigneDePiece/n:BlocLignePiece/n:InfoLignePiece/n:MtHT/@V),'# ##0,00','decformat')"/>
                            </xsl:variable>
                            <xsl:variable name="totalTTC">
                                <xsl:value-of
                                        select="sum(./n:LigneDePiece/n:BlocLignePiece/n:InfoLignePiece/n:MtTVA/@V) + sum(./n:LigneDePiece/n:BlocLignePiece/n:InfoLignePiece/n:MtHT/@V)"/>
                            </xsl:variable>


                            <!-- table pour le titre du mandat -->
                            <table class="entete" cellspacing="0">
                                <tr>
                                    <td class="colonne1">
                                        <xsl:call-template name="afficher.balise">
                                            <xsl:with-param name="element.nom" select="'CodCol'"/>
                                            <xsl:with-param name="element" select="../../../n:EnTetePES/n:CodCol"/>
                                            <xsl:with-param name="parent.nodeid"
                                                            select="../../../n:EnTetePES/@ano:node-id"/>
                                        </xsl:call-template>
                                        &nbsp;
                                        <xsl:call-template name="afficher.balise">
                                            <xsl:with-param name="element.nom" select="'CodBud'"/>
                                            <xsl:with-param name="element" select="../../../n:EnTetePES/n:CodBud"/>
                                            <xsl:with-param name="parent.nodeid"
                                                            select="../../../n:EnTetePES/@ano:node-id"/>
                                        </xsl:call-template>
                                        &nbsp;
                                        <xsl:call-template name="afficher.balise">
                                            <xsl:with-param name="element.nom" select="'LibelleColBud'"/>
                                            <xsl:with-param name="element"
                                                            select="../../../n:EnTetePES/n:LibelleColBud"/>
                                            <xsl:with-param name="parent.nodeid"
                                                            select="../../../n:EnTetePES/@ano:node-id"/>
                                        </xsl:call-template>
                                        <br/>
                                        <xsl:call-template name="afficher.balise">
                                            <xsl:with-param name="element.nom" select="'IdColl'"/>
                                            <xsl:with-param name="element" select="../../../n:EnTetePES/n:IdColl"/>
                                            <xsl:with-param name="parent.nodeid"
                                                            select="../../../n:EnTetePES/@ano:node-id"/>
                                        </xsl:call-template>
                                        <br/>
                                        <xsl:call-template name="afficher.balise">
                                            <xsl:with-param name="element.nom" select="'FinJur'"/>
                                            <xsl:with-param name="element" select="../../../n:EnTetePES/n:FinJur"/>
                                            <xsl:with-param name="parent.nodeid"
                                                            select="../../../n:EnTetePES/@ano:node-id"/>
                                        </xsl:call-template>
                                        <br/>
                                        <xsl:call-template name="afficher.balise">
                                            <xsl:with-param name="element.nom" select="'CodEtGeo'"/>
                                            <xsl:with-param name="element"
                                                            select="./n:LigneDePiece[1]/n:BlocLignePiece/n:InfoLignePiece/n:CodEtGeo"/>
                                            <xsl:with-param name="parent.nodeid"
                                                            select="./n:LigneDePiece[1]/@ano:node-id"/>
                                        </xsl:call-template>
                                        <br/>
                                        <xsl:call-template name="afficher.balise">
                                            <xsl:with-param name="element.nom" select="'FinGeo'"/>
                                            <xsl:with-param name="element"
                                                            select="./n:LigneDePiece[1]/n:BlocLignePiece/n:InfoLignePiece/n:FinGeo"/>
                                            <xsl:with-param name="parent.nodeid"
                                                            select="./n:LigneDePiece[1]/@ano:node-id"/>
                                        </xsl:call-template>

                                    </td>
                                    <td class="colonne2" style="font-weight:bold">
                                        <xsl:variable name="piece.libelleTypPce">
                                            <xsl:call-template name="typePieceLibelle">
                                                <xsl:with-param name="typePiece" select="./n:BlocPiece/n:TypPce/@V"/>
                                            </xsl:call-template>
                                        </xsl:variable>
                                        <xsl:call-template name="afficher.balise">
                                            <xsl:with-param name="element.nom" select="'TypPce'"/>
                                            <xsl:with-param name="element" select="./n:BlocPiece/n:TypPce"/>
                                            <xsl:with-param name="libelle" select="$piece.libelleTypPce"/>
                                            <xsl:with-param name="parent.nodeid" select="./@ano:node-id"/>
                                        </xsl:call-template>
                                        <br/>
                                        <xsl:variable name="piece.libelleNatPce">
                                            <xsl:call-template name="naturePieceLibelle">
                                                <xsl:with-param name="naturePiece" select="./n:BlocPiece/n:NatPce/@V"/>
                                            </xsl:call-template>
                                        </xsl:variable>
                                        <xsl:call-template name="afficher.balise">
                                            <xsl:with-param name="element.nom" select="'NatPce'"/>
                                            <xsl:with-param name="element" select="./n:BlocPiece/n:NatPce"/>
                                            <xsl:with-param name="libelle" select="$piece.libelleNatPce"/>
                                            <xsl:with-param name="parent.nodeid" select="./@ano:node-id"/>
                                        </xsl:call-template>
                                        <br/>

                                        <xsl:call-template name="afficher.balise">
                                            <xsl:with-param name="element.nom" select="'ObjPce'"/>
                                            <xsl:with-param name="element" select="./n:BlocPiece/n:ObjPce"/>
                                            <xsl:with-param name="parent.nodeid" select="./@ano:node-id"/>
                                        </xsl:call-template>
                                        <br/>

                                        <div style="font-size: 10px;text-align: left; margin-left: 5cm;">
                                            <xsl:call-template name="showPjTemplate">
                                                <xsl:with-param name="supportLocation" select="./n:BlocPiece/n:PJRef[n:Support/@V=01]"/>
                                            </xsl:call-template>
                                        </div>
                                    </td>
                                    <td class="colonne3">Titre<br/>R&#244;le<br/>Bordereau
                                    </td>
                                    <td class="colonne4">
                                        <xsl:call-template name="afficher.balise">
                                            <xsl:with-param name="element.nom" select="'IdPce'"/>
                                            <xsl:with-param name="element" select="./n:BlocPiece/n:IdPce"/>
                                            <xsl:with-param name="parent.nodeid" select="./@ano:node-id"/>
                                        </xsl:call-template>
                                        <br/>
                                        <xsl:call-template name="afficher.balise">
                                            <xsl:with-param name="element.nom" select="'IdRol'"/>
                                            <xsl:with-param name="element" select="./n:BlocPiece/n:IdRol"/>
                                            <xsl:with-param name="parent.nodeid" select="./@ano:node-id"/>
                                        </xsl:call-template>
                                        <br/>
                                        <xsl:choose>
                                            <xsl:when test="$browser-destination = 'internal'">
                                                <xsl:element name="a">
                                                    <xsl:attribute name="href">xemelios:/query?docId=pes-aller&amp;etatId=PES_RecetteAller&amp;elementId=BordereauRecette&amp;collectivite=<xsl:value-of
                                                            select="/n:PES_Aller/n:EnTetePES/n:IdColl/@V"/>&amp;budget=<xsl:value-of
                                                            select="/n:PES_Aller/n:EnTetePES/n:CodBud/@V"/>&amp;path=[@added:primary-key='<xsl:value-of
                                                            select="../@added:primary-key"/>']&amp;xsl:param=(elementId,Bordereau)
                                                    </xsl:attribute>
                                                    <xsl:call-template name="afficher.balise">
                                                        <xsl:with-param name="element.nom" select="'IdBord'"/>
                                                        <xsl:with-param name="element"
                                                                        select="../n:BlocBordereau/n:IdBord"/>
                                                        <xsl:with-param name="parent.nodeid" select="../@ano:node-id"/>
                                                    </xsl:call-template>
                                                </xsl:element>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:call-template name="afficher.balise">
                                                    <xsl:with-param name="element.nom" select="'IdBord'"/>
                                                    <xsl:with-param name="element"
                                                                    select="../n:BlocBordereau/n:IdBord"/>
                                                    <xsl:with-param name="parent.nodeid" select="../@ano:node-id"/>
                                                </xsl:call-template>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="noborder">&nbsp;</td>
                                    <td class="noborder">Date d'&#233;mission<br/>Date d'envoi de l'ASAP ou de d�p�t dans l'ENSU
                                    </td>
                                    <td class="noborder">
                                        <xsl:variable name="piece.libelleDteBordEm">
                                            <xsl:call-template name="slash-date">
                                                <xsl:with-param name="datebrute"
                                                                select="../n:BlocBordereau/n:DteBordEm/@V"></xsl:with-param>
                                            </xsl:call-template>
                                        </xsl:variable>
                                        <xsl:call-template name="afficher.balise">
                                            <xsl:with-param name="element.nom" select="'DteBordEm'"/>
                                            <xsl:with-param name="element" select="../n:BlocBordereau/n:DteBordEm"/>
                                            <xsl:with-param name="libelle" select="$piece.libelleDteBordEm"/>
                                            <xsl:with-param name="parent.nodeid" select="../@ano:node-id"/>
                                        </xsl:call-template>
                                        <br/>
                                        <br/>
                                        <xsl:variable name="piece.libelleDteAsp">
                                            <xsl:call-template name="slash-date">
                                                <xsl:with-param name="datebrute"
                                                                select="./n:BlocPiece/n:DteAsp/@V"></xsl:with-param>
                                            </xsl:call-template>
                                        </xsl:variable>
                                        <xsl:call-template name="afficher.balise">
                                            <xsl:with-param name="element.nom" select="'DteAsp'"/>
                                            <xsl:with-param name="element" select="./n:BlocPiece/n:DteAsp"/>
                                            <xsl:with-param name="libelle" select="$piece.libelleDteAsp"/>
                                            <xsl:with-param name="parent.nodeid" select="./@ano:node-id"/>
                                        </xsl:call-template>
                                    </td>
                                </tr>
                            </table>
                            <p style="text-align: center;">Titre ex&#233;cutoire en application de l'article L252 A du
                                Livre des proc&#233;dures fiscales, pris, &#233;mis et rendu ex&#233;cutoire<br/>conform&#233;ment
                                aux dispositions des articles R.2342-4 et D.3342-11 du code g&#233;n&#233;ral des
                                collectivit&#233;s locales
                                <br/>
                            </p>
                            <!-- entete de la table principale -->
                            <table class="enteteTablePrincipale" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th rowspan="2">Cr&#233;ancier</th>
                                        <th rowspan="2">Ligne</th>
                                        <th rowspan="2">Pi&#232;ce(s) de rattachement</th>
                                        <th rowspan="2">Description</th>
                                        <th colspan="3">Imputation</th>
                                        <th rowspan="2">Montant/D&#233;lais</th>
                                    </tr>
                                    <tr>
                                        <th>Nature</th>
                                        <th>Fonction</th>
                                        <th>Op&#233;ration</th>
                                    </tr>
                                </thead>
                                <!-- contenu de la table principale -->
                                <xsl:for-each select="./n:LigneDePiece">
                                    <xsl:sort select="n:BlocLignePiece/n:InfoLignePiece/n:IdLigne/@V"
                                              data-type="number"></xsl:sort>

                                    <xsl:element name="tr">

                                        <td class="mandatCol1">

                                            <xsl:for-each select="./n:Tiers">

                                                <xsl:if test="./n:InfoTiers/n:NatIdTiers/@V">
                                                    <xsl:variable name="piece.libelleNatIdTiers">
                                                        <xsl:call-template name="typeTiersNational">
                                                            <xsl:with-param name="typeTiers"
                                                                            select="./n:InfoTiers/n:NatIdTiers/@V"></xsl:with-param>
                                                        </xsl:call-template>
                                                    </xsl:variable>
                                                    <xsl:call-template name="afficher.balise">
                                                        <xsl:with-param name="element.nom" select="'NatIdTiers'"/>
                                                        <xsl:with-param name="element"
                                                                        select="./n:InfoTiers/n:NatIdTiers"/>
                                                        <xsl:with-param name="libelle"
                                                                        select="$piece.libelleNatIdTiers"/>
                                                        <xsl:with-param name="parent.nodeid" select="./@ano:node-id"/>
                                                    </xsl:call-template>
                                                    &nbsp;:&nbsp;
                                                </xsl:if>
                                                <xsl:call-template name="afficher.balise">
                                                    <xsl:with-param name="element.nom" select="'IdTiers'"/>
                                                    <xsl:with-param name="element" select="./n:InfoTiers/n:IdTiers"/>
                                                    <xsl:with-param name="parent.nodeid" select="./@ano:node-id"/>
                                                </xsl:call-template>
                                                <br/>

                                                <xsl:variable name="piece.libelleCatTiers">
                                                    <xsl:call-template name="categorieTiers">
                                                        <xsl:with-param name="catTiers"
                                                                        select="./n:InfoTiers/n:CatTiers/@V"></xsl:with-param>
                                                    </xsl:call-template>
                                                </xsl:variable>
                                                <xsl:call-template name="afficher.balise">
                                                    <xsl:with-param name="element.nom" select="'CatTiers'"/>
                                                    <xsl:with-param name="element" select="./n:InfoTiers/n:CatTiers"/>
                                                    <xsl:with-param name="libelle" select="$piece.libelleCatTiers"/>
                                                    <xsl:with-param name="parent.nodeid" select="./@ano:node-id"/>
                                                </xsl:call-template>
                                                <br/>

                                                <xsl:variable name="piece.libelleNatJur">
                                                    <xsl:call-template name="natureJuridiqueTiers">
                                                        <xsl:with-param name="natJur"
                                                                        select="./n:InfoTiers/n:NatJur/@V"></xsl:with-param>
                                                    </xsl:call-template>
                                                </xsl:variable>
                                                <xsl:call-template name="afficher.balise">
                                                    <xsl:with-param name="element.nom" select="'NatJur'"/>
                                                    <xsl:with-param name="element" select="./n:InfoTiers/n:NatJur"/>
                                                    <xsl:with-param name="libelle" select="$piece.libelleNatJur"/>
                                                    <xsl:with-param name="parent.nodeid" select="./@ano:node-id"/>
                                                </xsl:call-template>
                                                <br/>


                                                <xsl:call-template name="afficher.balise">
                                                    <xsl:with-param name="element.nom" select="'Civilite'"/>
                                                    <xsl:with-param name="element" select="./n:InfoTiers/n:Civilite"/>
                                                    <xsl:with-param name="parent.nodeid" select="./@ano:node-id"/>
                                                </xsl:call-template>
                                                <xsl:if test="string-length(./n:InfoTiers/n:Civilite/@V) &gt; 0">
                                                    &nbsp;
                                                </xsl:if>
                                                <xsl:call-template name="afficher.balise">
                                                    <xsl:with-param name="element.nom" select="'Nom'"/>
                                                    <xsl:with-param name="element" select="./n:InfoTiers/n:Nom"/>
                                                    <xsl:with-param name="parent.nodeid" select="./@ano:node-id"/>
                                                </xsl:call-template>
                                                <xsl:if test="string-length(./n:InfoTiers/n:Nom/@V) &gt; 0">
                                                    &nbsp;
                                                </xsl:if>
                                                <xsl:call-template name="afficher.balise">
                                                    <xsl:with-param name="element.nom" select="'Prenom'"/>
                                                    <xsl:with-param name="element" select="./n:InfoTiers/n:Prenom"/>
                                                    <xsl:with-param name="parent.nodeid" select="./@ano:node-id"/>
                                                </xsl:call-template>
                                                <xsl:if test="string-length(./n:InfoTiers/n:Prenom/@V) &gt; 0">
                                                    &nbsp;
                                                </xsl:if>
                                                <xsl:call-template name="afficher.balise">
                                                    <xsl:with-param name="element.nom" select="'ComplNom'"/>
                                                    <xsl:with-param name="element" select="./n:InfoTiers/n:ComplNom"/>
                                                    <xsl:with-param name="parent.nodeid" select="./@ano:node-id"/>
                                                </xsl:call-template>
                                                <xsl:if test="string-length(./n:InfoTiers/n:ComplNom/@V) &gt; 0">
                                                    &nbsp;
                                                </xsl:if>
                                                <xsl:if test="(string-length(./n:InfoTiers/n:Civilite/@V)+string-length(./n:InfoTiers/n:Nom/@V)+string-length(./n:InfoTiers/n:Prenom/@V)+string-length(./n:InfoTiers/n:ComplNom/@V)) &gt; 0">
                                                    <br/>
                                                </xsl:if>

                                                <xsl:call-template name="afficher.balise">
                                                    <xsl:with-param name="element.nom" select="'Adr1'"/>
                                                    <xsl:with-param name="element" select="./n:Adresse/n:Adr1"/>
                                                    <xsl:with-param name="parent.nodeid" select="./@ano:node-id"/>
                                                </xsl:call-template>
                                                <xsl:if test="string-length(./n:Adresse/n:Adr1/@V) &gt; 0">
                                                    <br/>
                                                </xsl:if>
                                                <xsl:call-template name="afficher.balise">
                                                    <xsl:with-param name="element.nom" select="'Adr2'"/>
                                                    <xsl:with-param name="element" select="./n:Adresse/n:Adr2"/>
                                                    <xsl:with-param name="parent.nodeid" select="./@ano:node-id"/>
                                                </xsl:call-template>
                                                <xsl:if test="string-length(./n:Adresse/n:Adr2/@V) &gt; 0">
                                                    <br/>
                                                </xsl:if>

                                                <xsl:call-template name="afficher.balise">
                                                    <xsl:with-param name="element.nom" select="'CP'"/>
                                                    <xsl:with-param name="element" select="./n:Adresse/n:CP"/>
                                                    <xsl:with-param name="parent.nodeid" select="./@ano:node-id"/>
                                                </xsl:call-template>
                                                &nbsp;
                                                <xsl:call-template name="afficher.balise">
                                                    <xsl:with-param name="element.nom" select="'Ville'"/>
                                                    <xsl:with-param name="element" select="./n:Adresse/n:Ville"/>
                                                    <xsl:with-param name="parent.nodeid" select="./@ano:node-id"/>
                                                </xsl:call-template>
                                                <xsl:if test="string-length(./n:Adresse/n:Ville/@V) &gt; 0">
                                                    &nbsp;
                                                </xsl:if>

                                                <xsl:if test="boolean(./n:Adresse/n:CodPays/@V)">
                                                    -&nbsp;
                                                </xsl:if>
                                                <xsl:variable name="piece.libelleCodPays">
                                                    <xsl:call-template name="codePaysINSEE">
                                                        <xsl:with-param name="codePays"
                                                                        select="./n:Adresse/n:CodPays/@V"></xsl:with-param>
                                                    </xsl:call-template>
                                                </xsl:variable>
                                                <xsl:call-template name="afficher.balise">
                                                    <xsl:with-param name="element.nom" select="'CodPays'"/>
                                                    <xsl:with-param name="element" select="./n:Adresse/n:CodPays"/>
                                                    <xsl:with-param name="libelle" select="$piece.libelleCodPays"/>
                                                    <xsl:with-param name="parent.nodeid" select="./@ano:node-id"/>
                                                </xsl:call-template>

                                                <br/>
                                                <br/>

                                                <!-- CpteBancaire -->
                                                <xsl:call-template name="afficher.balise">
                                                    <xsl:with-param name="element.nom" select="'LibVir1'"/>
                                                    <xsl:with-param name="element"
                                                                    select="../n:BlocLignePiece/n:InfoLignePiece/n:LibVir1"/>
                                                    <xsl:with-param name="parent.nodeid" select="../@ano:node-id"/>
                                                </xsl:call-template>
                                                <br/>

                                                <xsl:call-template name="afficher.balise">
                                                    <xsl:with-param name="element.nom" select="'CodeEtab'"/>
                                                    <xsl:with-param name="element"
                                                                    select="./n:CpteBancaire/n:CodeEtab"/>
                                                    <xsl:with-param name="parent.nodeid" select="./@ano:node-id"/>
                                                </xsl:call-template>
                                                <xsl:call-template name="afficher.balise">
                                                    <xsl:with-param name="element.nom" select="'CodeGuic'"/>
                                                    <xsl:with-param name="element"
                                                                    select="./n:CpteBancaire/n:CodeGuic"/>
                                                    <xsl:with-param name="parent.nodeid" select="./@ano:node-id"/>
                                                </xsl:call-template>
                                                <xsl:call-template name="afficher.balise">
                                                    <xsl:with-param name="element.nom" select="'IdCpte'"/>
                                                    <xsl:with-param name="element" select="./n:CpteBancaire/n:IdCpte"/>
                                                    <xsl:with-param name="parent.nodeid" select="./@ano:node-id"/>
                                                </xsl:call-template>
                                                <xsl:call-template name="afficher.balise">
                                                    <xsl:with-param name="element.nom" select="'CleRib'"/>
                                                    <xsl:with-param name="element" select="./n:CpteBancaire/n:CleRib"/>
                                                    <xsl:with-param name="parent.nodeid" select="./@ano:node-id"/>
                                                </xsl:call-template>
                                                <br/>
                                                <xsl:call-template name="afficher.balise">
                                                    <xsl:with-param name="element.nom" select="'IBAN'"/>
                                                    <xsl:with-param name="element" select="./n:CpteBancaire/n:IBAN"/>
                                                    <xsl:with-param name="parent.nodeid" select="./@ano:node-id"/>
                                                </xsl:call-template>
                                                <br/>
                                                <xsl:call-template name="afficher.balise">
                                                    <xsl:with-param name="element.nom" select="'LibBanc'"/>
                                                    <xsl:with-param name="element" select="./n:CpteBancaire/n:LibBanc"/>
                                                    <xsl:with-param name="parent.nodeid" select="./@ano:node-id"/>
                                                </xsl:call-template>
                                                <br/>
                                                <xsl:call-template name="afficher.balise">
                                                    <xsl:with-param name="element.nom" select="'IdBancInt'"/>
                                                    <xsl:with-param name="element"
                                                                    select="./n:CpteBancaire/n:IdBancInt"/>
                                                    <xsl:with-param name="parent.nodeid" select="./@ano:node-id"/>
                                                </xsl:call-template>

                                            </xsl:for-each>
                                        </td>
                                        <td class="mandatCol2">
                                            <xsl:call-template name="afficher.balise">
                                                <xsl:with-param name="element.nom" select="'IdLigne'"/>
                                                <xsl:with-param name="element"
                                                                select="n:BlocLignePiece/n:InfoLignePiece/n:IdLigne"/>
                                                <xsl:with-param name="parent.nodeid" select="./@ano:node-id"/>
                                            </xsl:call-template>
                                        </td>
                                        <td class="mandatCol3">
                                            <!-- On calcule le nombre de PJ pour chaque type de support -->
                                            <xsl:variable name="nbPJLigneElectro">
                                                <xsl:value-of
                                                        select="count(n:BlocLignePiece/n:InfoLignePiece/n:PJRef/n:Support[@V=01])"/>
                                            </xsl:variable>
                                            <xsl:variable name="nbPJLignePapier">
                                                <xsl:value-of
                                                        select="count(n:BlocLignePiece/n:InfoLignePiece/n:PJRef/n:Support[@V=02])"/>
                                            </xsl:variable>
                                            <xsl:variable name="nbPJLigneCDROM">
                                                <xsl:value-of
                                                        select="count(n:BlocLignePiece/n:InfoLignePiece/n:PJRef/n:Support[@V=03])"/>
                                            </xsl:variable>

                                            <ul>
                                                <xsl:if test="count(n:BlocLignePiece/n:RattachPiece/n:NatPceOrig/@V) > 0">
                                                    <li>
                                                        <xsl:variable name="piece.libelleNatPceOrig">
                                                            <xsl:call-template name="typePieceOrigine">
                                                                <xsl:with-param name="typePiece"
                                                                                select="n:BlocLignePiece/n:RattachPiece/n:NatPceOrig/@V"></xsl:with-param>
                                                            </xsl:call-template>
                                                        </xsl:variable>
                                                        <xsl:call-template name="afficher.balise">
                                                            <xsl:with-param name="element.nom"
                                                                            select="'NatPceOrig'"/>
                                                            <xsl:with-param name="element"
                                                                            select="n:BlocLignePiece/n:RattachPiece/n:NatPceOrig"/>
                                                            <xsl:with-param name="libelle"
                                                                            select="$piece.libelleNatPceOrig"/>
                                                            <xsl:with-param name="parent.nodeid"
                                                                            select="./@ano:node-id"/>
                                                        </xsl:call-template>
                                                    </li>
                                                </xsl:if>
                                                <xsl:if test="count(./n:BlocLignePiece/n:RattachPiece/n:ExerRat) > 0">
                                                    <li>
                                                        Exercice :
                                                        <xsl:call-template name="afficher.balise">
                                                            <xsl:with-param name="element.nom" select="'ExerRat'"/>
                                                            <xsl:with-param name="element"
                                                                            select="./n:BlocLignePiece/n:RattachPiece/n:ExerRat"/>
                                                            <xsl:with-param name="parent.nodeid"
                                                                            select="./@ano:node-id"/>
                                                        </xsl:call-template>
                                                    </li>
                                                </xsl:if>
                                                <xsl:if test="count(./n:BlocLignePiece/n:RattachPiece/n:IdPceOrig) > 0">
                                                    <li>
                                                        Identifiant pi&#232;ce :
                                                        <xsl:call-template name="afficher.balise">
                                                            <xsl:with-param name="element.nom"
                                                                            select="'IdPceOrig'"/>
                                                            <xsl:with-param name="element"
                                                                            select="./n:BlocLignePiece/n:RattachPiece/n:IdPceOrig"/>
                                                            <xsl:with-param name="parent.nodeid"
                                                                            select="./@ano:node-id"/>
                                                        </xsl:call-template>
                                                    </li>
                                                </xsl:if>
                                                <xsl:if test="count(./n:BlocLignePiece/n:RattachPiece/n:IdLigneOrig) > 0">
                                                    <li>
                                                        Ligne pi&#232;ce :
                                                        <xsl:call-template name="afficher.balise">
                                                            <xsl:with-param name="element.nom"
                                                                            select="'IdLigneOrig'"/>
                                                            <xsl:with-param name="element"
                                                                            select="./n:BlocLignePiece/n:RattachPiece/n:IdLigneOrig"/>
                                                            <xsl:with-param name="parent.nodeid"
                                                                            select="./@ano:node-id"/>
                                                        </xsl:call-template>
                                                    </li>
                                                </xsl:if>
                                                <xsl:if test="($nbPJLigneElectro + $nbPJLignePapier + $nbPJLigneCDROM) > 0">
                                                    <li>
                                                        Justifi&#233;e par
                                                        <xsl:value-of
                                                                select="$nbPJLigneElectro + $nbPJLignePapier + $nbPJLigneCDROM"/>
                                                        PJ
                                                        <xsl:if test="$nbPJLigneElectro + $nbPJLignePapier + $nbPJLigneCDROM &gt; 0">
                                                            dont :
                                                        </xsl:if>
                                                        <ul style="margin-top: 0px; margin-left: 0.5cm;">
                                                            <xsl:if test="$nbPJLigneElectro &gt; 0">
                                                                <li>
                                                                    <xsl:value-of select="$nbPJLigneElectro"/> en
                                                                    ligne
                                                                    <ul style="margin-top: 0; margin-left: 0.5cm; list-style-type: none;">
                                                                        <xsl:for-each
                                                                                select="./n:BlocLignePiece/n:InfoLignePce/n:PJRef[n:Support/@V=01]">
                                                                            <li>
                                                                                <xsl:choose>
                                                                                    <xsl:when test="true()">
                                                                                        <a>
                                                                                            <xsl:attribute
                                                                                                    name="href">
                                                                                                xemelios:/attachment?pjId=<xsl:value-of
                                                                                                    select="./n:IdUnique/@V"/>&amp;collectivite=<xsl:value-of
                                                                                                    select="/n:PES_Aller/n:EnTetePES/n:IdColl/@V"/>
                                                                                            </xsl:attribute>
                                                                                            <xsl:choose>
                                                                                                <xsl:when
                                                                                                        test="string-length(./n:Libelle/@V) &gt; 0">
                                                                                                    <xsl:call-template
                                                                                                            name="afficher.balise">
                                                                                                        <xsl:with-param
                                                                                                                name="element.nom"
                                                                                                                select="'Libelle'"/>
                                                                                                        <xsl:with-param
                                                                                                                name="element"
                                                                                                                select="./n:Libelle"/>
                                                                                                        <xsl:with-param
                                                                                                                name="parent.nodeid"
                                                                                                                select="./@ano:node-id"/>
                                                                                                    </xsl:call-template>
                                                                                                </xsl:when>
                                                                                                <xsl:otherwise>
                                                                                                    <xsl:call-template
                                                                                                            name="afficher.balise">
                                                                                                        <xsl:with-param
                                                                                                                name="element.nom"
                                                                                                                select="'IdUnique'"/>
                                                                                                        <xsl:with-param
                                                                                                                name="element"
                                                                                                                select="./n:IdUnique"/>
                                                                                                        <xsl:with-param
                                                                                                                name="parent.nodeid"
                                                                                                                select="./@ano:node-id"/>
                                                                                                    </xsl:call-template>
                                                                                                </xsl:otherwise>
                                                                                            </xsl:choose>
                                                                                        </a>
                                                                                    </xsl:when>
                                                                                    <xsl:otherwise>
                                                                                        <xsl:choose>
                                                                                            <xsl:when
                                                                                                    test="string-length(./n:Libelle/@V) &gt; 0">
                                                                                                <xsl:call-template
                                                                                                        name="afficher.balise">
                                                                                                    <xsl:with-param
                                                                                                            name="element.nom"
                                                                                                            select="'Libelle'"/>
                                                                                                    <xsl:with-param
                                                                                                            name="element"
                                                                                                            select="./n:Libelle"/>
                                                                                                    <xsl:with-param
                                                                                                            name="parent.nodeid"
                                                                                                            select="./@ano:node-id"/>
                                                                                                </xsl:call-template>
                                                                                            </xsl:when>
                                                                                            <xsl:otherwise>
                                                                                                <xsl:call-template
                                                                                                        name="afficher.balise">
                                                                                                    <xsl:with-param
                                                                                                            name="element.nom"
                                                                                                            select="'IdUnique'"/>
                                                                                                    <xsl:with-param
                                                                                                            name="element"
                                                                                                            select="./n:IdUnique"/>
                                                                                                    <xsl:with-param
                                                                                                            name="parent.nodeid"
                                                                                                            select="./@ano:node-id"/>
                                                                                                </xsl:call-template>
                                                                                            </xsl:otherwise>
                                                                                        </xsl:choose>
                                                                                    </xsl:otherwise>
                                                                                </xsl:choose>
                                                                            </li>
                                                                        </xsl:for-each>
                                                                    </ul>
                                                                </li>
                                                            </xsl:if>
                                                            <xsl:if test="$nbPJLignePapier &gt; 0">
                                                                <li>
                                                                    <xsl:value-of select="$nbPJLignePapier"/> papier
                                                                    <ul style="margin-top: 0; margin-left: 0.5cm; list-style-type: none;">
                                                                        <xsl:for-each
                                                                                select="./n:BlocLignePiece/n:InfoLignePce/n:PJRef[n:Support/@V=02]">
                                                                            <li>
                                                                                <xsl:choose>
                                                                                    <xsl:when
                                                                                            test="string-length(./n:Libelle/@V) &gt; 0">
                                                                                        <xsl:call-template
                                                                                                name="afficher.balise">
                                                                                            <xsl:with-param
                                                                                                    name="element.nom"
                                                                                                    select="'Libelle'"/>
                                                                                            <xsl:with-param
                                                                                                    name="element"
                                                                                                    select="./n:Libelle"/>
                                                                                            <xsl:with-param
                                                                                                    name="parent.nodeid"
                                                                                                    select="./@ano:node-id"/>
                                                                                        </xsl:call-template>
                                                                                    </xsl:when>
                                                                                    <xsl:otherwise>
                                                                                        <xsl:call-template
                                                                                                name="afficher.balise">
                                                                                            <xsl:with-param
                                                                                                    name="element.nom"
                                                                                                    select="'IdUnique'"/>
                                                                                            <xsl:with-param
                                                                                                    name="element"
                                                                                                    select="./n:IdUnique"/>
                                                                                            <xsl:with-param
                                                                                                    name="parent.nodeid"
                                                                                                    select="./@ano:node-id"/>
                                                                                        </xsl:call-template>
                                                                                    </xsl:otherwise>
                                                                                </xsl:choose>
                                                                            </li>
                                                                        </xsl:for-each>
                                                                    </ul>
                                                                </li>
                                                            </xsl:if>
                                                            <xsl:if test="$nbPJLigneCDROM &gt; 0">
                                                                <li>
                                                                    <xsl:value-of select="$nbPJLigneCDROM"/> sur
                                                                    CDROM
                                                                    <ul style="margin-top: 0; margin-left: 0.5cm; list-style-type: none;">
                                                                        <xsl:for-each
                                                                                select="./n:BlocLignePiece/n:InfoLignePce/n:PJRef[n:Support/@V=03]">
                                                                            <li>
                                                                                <xsl:choose>
                                                                                    <xsl:when
                                                                                            test="string-length(./n:Libelle/@V) &gt; 0">
                                                                                        <xsl:call-template
                                                                                                name="afficher.balise">
                                                                                            <xsl:with-param
                                                                                                    name="element.nom"
                                                                                                    select="'Libelle'"/>
                                                                                            <xsl:with-param
                                                                                                    name="element"
                                                                                                    select="./n:Libelle"/>
                                                                                            <xsl:with-param
                                                                                                    name="parent.nodeid"
                                                                                                    select="./@ano:node-id"/>
                                                                                        </xsl:call-template>
                                                                                    </xsl:when>
                                                                                    <xsl:otherwise>
                                                                                        <xsl:call-template
                                                                                                name="afficher.balise">
                                                                                            <xsl:with-param
                                                                                                    name="element.nom"
                                                                                                    select="'IdUnique'"/>
                                                                                            <xsl:with-param
                                                                                                    name="element"
                                                                                                    select="./n:IdUnique"/>
                                                                                            <xsl:with-param
                                                                                                    name="parent.nodeid"
                                                                                                    select="./@ano:node-id"/>
                                                                                        </xsl:call-template>
                                                                                    </xsl:otherwise>
                                                                                </xsl:choose>
                                                                            </li>
                                                                        </xsl:for-each>
                                                                    </ul>
                                                                </li>
                                                            </xsl:if>
                                                        </ul>
                                                    </li>
                                                </xsl:if>
                                            </ul>
                                        </td>
                                        <td class="mandatCol4">
                                            <ul>
                                                <xsl:if test="count(./n:BlocLignePiece/n:InfoLignePiece/n:CodProdLoc) > 0">
                                                    <li>
                                                        Code produit :
                                                        <xsl:call-template name="afficher.balise">
                                                            <xsl:with-param name="element.nom" select="'CodProdLoc'"/>
                                                            <xsl:with-param name="element"
                                                                            select="./n:BlocLignePiece/n:InfoLignePiece/n:CodProdLoc"/>
                                                            <xsl:with-param name="parent.nodeid"
                                                                            select="./@ano:node-id"/>
                                                        </xsl:call-template>
                                                    </li>
                                                </xsl:if>

                                                <xsl:if test="count(../n:BlocPiece/n:CodServ) > 0">
                                                    <li>
                                                        Code service :
                                                        <xsl:call-template name="afficher.balise">
                                                            <xsl:with-param name="element.nom" select="'CodServ'"/>
                                                            <xsl:with-param name="element"
                                                                            select="../n:BlocPiece/n:CodServ"/>
                                                            <xsl:with-param name="parent.nodeid"
                                                                            select="../@ano:node-id"/>
                                                        </xsl:call-template>
                                                    </li>
                                                </xsl:if>

                                                <xsl:if test="count(./n:BlocLignePiece/n:InfoLignePiece/n:ObjLignePce) > 0">
                                                    <li>
                                                        Objet :
                                                        <xsl:call-template name="afficher.balise">
                                                            <xsl:with-param name="element.nom" select="'ObjLignePce'"/>
                                                            <xsl:with-param name="element"
                                                                            select="./n:BlocLignePiece/n:InfoLignePiece/n:ObjLignePce"/>
                                                            <xsl:with-param name="parent.nodeid"
                                                                            select="./@ano:node-id"/>
                                                        </xsl:call-template>
                                                    </li>
                                                </xsl:if>

                                                <xsl:if test="count(./n:BlocLignePiece/n:LiensIdent/n:IdEncaissement) > 0">
                                                    <li>
                                                        Encaissement :
                                                        <xsl:call-template name="afficher.balise">
                                                            <xsl:with-param name="element.nom"
                                                                            select="'IdEncaissement'"/>
                                                            <xsl:with-param name="element"
                                                                            select="./n:BlocLignePiece/n:LiensIdent/n:IdEncaissement"/>
                                                            <xsl:with-param name="parent.nodeid"
                                                                            select="./@ano:node-id"/>
                                                        </xsl:call-template>
                                                    </li>
                                                </xsl:if>

                                                <xsl:if test="count(./n:BlocLignePiece/n:LiensIdent/n:IdActif) > 0">
                                                    <li>
                                                        Fiche inventaire :
                                                        <xsl:call-template name="afficher.balise">
                                                            <xsl:with-param name="element.nom" select="'IdActif'"/>
                                                            <xsl:with-param name="element"
                                                                            select="./n:BlocLignePiece/n:LiensIdent/n:IdActif"/>
                                                            <xsl:with-param name="parent.nodeid"
                                                                            select="./@ano:node-id"/>
                                                        </xsl:call-template>
                                                    </li>
                                                </xsl:if>


                                                <xsl:if test="count(./n:BlocLignePiece/n:LiensIdent/n:IdEmpruntOrdo) > 0">
                                                    <li>
                                                        Fiche emprunt :
                                                        <xsl:call-template name="afficher.balise">
                                                            <xsl:with-param name="element.nom"
                                                                            select="'IdEmpruntOrdo'"/>
                                                            <xsl:with-param name="element"
                                                                            select="./n:BlocLignePiece/n:LiensIdent/n:IdEmpruntOrdo"/>
                                                            <xsl:with-param name="parent.nodeid"
                                                                            select="./@ano:node-id"/>
                                                        </xsl:call-template>
                                                    </li>
                                                </xsl:if>


                                                <xsl:if test="count(./n:BlocLignePiece/n:LiensIdent/n:IdRegie) > 0">
                                                    <li>
                                                        Num&#233;ro de r&#233;gie :
                                                        <xsl:call-template name="afficher.balise">
                                                            <xsl:with-param name="element.nom" select="'IdRegie'"/>
                                                            <xsl:with-param name="element"
                                                                            select="./n:BlocLignePiece/n:LiensIdent/n:IdRegie"/>
                                                            <xsl:with-param name="parent.nodeid"
                                                                            select="./@ano:node-id"/>
                                                        </xsl:call-template>
                                                    </li>
                                                </xsl:if>

                                                <xsl:if test="count(./n:BlocLignePiece/n:LiensIdent/n:IdMarche) > 0">
                                                    <li>
                                                        Num&#233;ro de marche :
                                                        <xsl:call-template name="afficher.balise">
                                                            <xsl:with-param name="element.nom" select="'IdMarche'"/>
                                                            <xsl:with-param name="element"
                                                                            select="./n:BlocLignePiece/n:LiensIdent/n:IdMarche"/>
                                                            <xsl:with-param name="parent.nodeid"
                                                                            select="./@ano:node-id"/>
                                                        </xsl:call-template>
                                                    </li>
                                                </xsl:if>

                                                <xsl:if test="count(./n:BlocLignePiece/n:LiensIdent/n:IdConv) > 0">
                                                    <li>
                                                        Fiche de convention :
                                                        <xsl:call-template name="afficher.balise">
                                                            <xsl:with-param name="element.nom" select="'IdConv'"/>
                                                            <xsl:with-param name="element"
                                                                            select="./n:BlocLignePiece/n:LiensIdent/n:IdConv"/>
                                                            <xsl:with-param name="parent.nodeid"
                                                                            select="./@ano:node-id"/>
                                                        </xsl:call-template>
                                                    </li>
                                                </xsl:if>

                                                <xsl:if test="count(../n:BlocPiece/n:NumeroFacture) > 0">
                                                    <li>
                                                        Num�ro de facture :
                                                        <xsl:call-template name="afficher.balise">
                                                            <xsl:with-param name="element.nom"
                                                                            select="'NumeroFacture'"/>
                                                            <xsl:with-param name="element"
                                                                            select="../n:BlocPiece/n:NumeroFacture"/>
                                                            <xsl:with-param name="parent.nodeid"
                                                                            select="../@ano:node-id"/>
                                                        </xsl:call-template>
                                                    </li>
                                                </xsl:if>

                                                <xsl:if test="count(../n:BlocPiece/n:NumeroMarche) > 0">
                                                    <li>
                                                        Num�ro de March� Fact. :
                                                        <xsl:call-template name="afficher.balise">
                                                            <xsl:with-param name="element.nom" select="'NumeroMarche'"/>
                                                            <xsl:with-param name="element"
                                                                            select="../n:BlocPiece/n:NumeroMarche"/>
                                                            <xsl:with-param name="parent.nodeid"
                                                                            select="../@ano:node-id"/>
                                                        </xsl:call-template>
                                                    </li>
                                                </xsl:if>

                                                <xsl:if test="count(../n:BlocPiece/n:NumeroEngagement) > 0">
                                                    <li>
                                                        Num�ro d'engagement fact. :
                                                        <xsl:call-template name="afficher.balise">
                                                            <xsl:with-param name="element.nom"
                                                                            select="'NumeroEngagement'"/>
                                                            <xsl:with-param name="element"
                                                                            select="../n:BlocPiece/n:NumeroEngagement"/>
                                                            <xsl:with-param name="parent.nodeid"
                                                                            select="../@ano:node-id"/>
                                                        </xsl:call-template>
                                                    </li>
                                                </xsl:if>

                                                <xsl:if test="count(../n:BlocPiece/n:CodeService) > 0">
                                                    <li>
                                                        Code service Fact. :
                                                        <xsl:call-template name="afficher.balise">
                                                            <xsl:with-param name="element.nom" select="'CodeService'"/>
                                                            <xsl:with-param name="element"
                                                                            select="../n:BlocPiece/n:CodeService"/>
                                                            <xsl:with-param name="parent.nodeid"
                                                                            select="../@ano:node-id"/>
                                                        </xsl:call-template>
                                                    </li>
                                                </xsl:if>
                                            </ul>
                                        </td>

                                        <td class="mandatCol5">
                                            <xsl:call-template name="afficher.balise">
                                                <xsl:with-param name="element.nom" select="'Nature'"/>
                                                <xsl:with-param name="element"
                                                                select="./n:BlocLignePiece/n:InfoLignePiece/n:Nature"/>
                                                <xsl:with-param name="parent.nodeid" select="./@ano:node-id"/>
                                            </xsl:call-template>
                                            &nbsp;
                                        </td>
                                        <td class="mandatCol6">
                                            <xsl:call-template name="afficher.balise">
                                                <xsl:with-param name="element.nom" select="'Fonction'"/>
                                                <xsl:with-param name="element"
                                                                select="./n:BlocLignePiece/n:InfoLignePiece/n:Fonction"/>
                                                <xsl:with-param name="parent.nodeid" select="./@ano:node-id"/>
                                            </xsl:call-template>
                                            &nbsp;
                                        </td>
                                        <td class="mandatCol7">
                                            <xsl:call-template name="afficher.balise">
                                                <xsl:with-param name="element.nom" select="'Operation'"/>
                                                <xsl:with-param name="element"
                                                                select="./n:BlocLignePiece/n:InfoLignePiece/n:Operation"/>
                                                <xsl:with-param name="parent.nodeid" select="./@ano:node-id"/>
                                            </xsl:call-template>
                                            &nbsp;
                                        </td>
                                        <td class="mandatCol8">
                                            <xsl:variable name="ligneHT">
                                                <xsl:choose>
                                                    <xsl:when test="./n:BlocLignePiece/n:InfoLignePiece/n:MtHT/@V">
                                                        <xsl:value-of
                                                                select="./n:BlocLignePiece/n:InfoLignePiece/n:MtHT/@V"/>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:value-of select="0"/>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </xsl:variable>
                                            <xsl:variable name="ligneTVA">
                                                <xsl:choose>
                                                    <xsl:when test="./n:BlocLignePiece/n:InfoLignePiece/n:MtTVA/@V">
                                                        <xsl:value-of
                                                                select="./n:BlocLignePiece/n:InfoLignePiece/n:MtTVA/@V"/>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:value-of select="0"/>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </xsl:variable>
                                            <xsl:variable name="ligneTTC">
                                                <xsl:value-of select="$ligneHT + $ligneTVA"/>
                                            </xsl:variable>
                                            <table class="inner" width="100%" cellspacing="0">
                                                <tr>
                                                    <td class="no-wrap">Montant HT</td>
                                                    <td class="text-right">
                                                        <xsl:variable name="piece.libelleMtHT">
                                                            <xsl:call-template name="number">
                                                                <xsl:with-param name="num"
                                                                                select="$ligneHT"></xsl:with-param>
                                                            </xsl:call-template>
                                                        </xsl:variable>
                                                        <xsl:call-template name="afficher.balise">
                                                            <xsl:with-param name="element.nom" select="'MtHT'"/>
                                                            <xsl:with-param name="element"
                                                                            select="./n:BlocLignePiece/n:InfoLignePiece/n:MtHT"/>
                                                            <xsl:with-param name="libelle" select="$piece.libelleMtHT"/>
                                                            <xsl:with-param name="parent.nodeid"
                                                                            select="./@ano:node-id"/>
                                                        </xsl:call-template>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="no-wrap">Montant TVA</td>
                                                    <td class="text-right">
                                                        <xsl:variable name="piece.libelleMtHT">
                                                            <xsl:call-template name="number">
                                                                <xsl:with-param name="num"
                                                                                select="$ligneTVA"></xsl:with-param>
                                                            </xsl:call-template>
                                                        </xsl:variable>
                                                        <xsl:call-template name="afficher.balise">
                                                            <xsl:with-param name="element.nom" select="'MtTVA'"/>
                                                            <xsl:with-param name="element"
                                                                            select="./n:BlocLignePiece/n:InfoLignePiece/n:MtTVA"/>
                                                            <xsl:with-param name="libelle" select="$piece.libelleMtHT"/>
                                                            <xsl:with-param name="parent.nodeid"
                                                                            select="./@ano:node-id"/>
                                                        </xsl:call-template>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="no-wrap">Montant TTC</td>
                                                    <td class="text-right">
                                                        <xsl:call-template name="number">
                                                            <xsl:with-param name="num" select="$ligneTTC"/>
                                                        </xsl:call-template>
                                                    </td>
                                                </tr>
                                                <xsl:if test="count(../n:BlocPiece/n:DebFact) > 0">
                                                    <tr>
                                                        <td class="no-wrap">D�but de facturation</td>
                                                        <td class="text-right">
                                                            <xsl:variable name="piece.libelleDebFact">
                                                                <xsl:call-template name="slash-date">
                                                                    <xsl:with-param name="datebrute"
                                                                                    select="../n:BlocPiece/n:DebFact/@V"></xsl:with-param>
                                                                </xsl:call-template>
                                                            </xsl:variable>
                                                            <xsl:call-template name="afficher.balise">
                                                                <xsl:with-param name="element.nom" select="'DebFact'"/>
                                                                <xsl:with-param name="element"
                                                                                select="../n:BlocPiece/n:DebFact"/>
                                                                <xsl:with-param name="libelle"
                                                                                select="$piece.libelleDebFact"/>
                                                                <xsl:with-param name="parent.nodeid"
                                                                                select="../@ano:node-id"/>
                                                            </xsl:call-template>
                                                        </td>
                                                    </tr>
                                                </xsl:if>
                                                <xsl:if test="count(../n:BlocPiece/n:FinFact) > 0">
                                                    <tr>
                                                        <td class="no-wrap">Fin de facturation</td>
                                                        <td class="text-right">
                                                            <xsl:variable name="piece.libelleFinFact">
                                                                <xsl:call-template name="slash-date">
                                                                    <xsl:with-param name="datebrute"
                                                                                    select="../n:BlocPiece/n:FinFact/@V"></xsl:with-param>
                                                                </xsl:call-template>
                                                            </xsl:variable>
                                                            <xsl:call-template name="afficher.balise">
                                                                <xsl:with-param name="element.nom" select="'FinFact'"/>
                                                                <xsl:with-param name="element"
                                                                                select="../n:BlocPiece/n:FinFact"/>
                                                                <xsl:with-param name="libelle"
                                                                                select="$piece.libelleFinFact"/>
                                                                <xsl:with-param name="parent.nodeid"
                                                                                select="../@ano:node-id"/>
                                                            </xsl:call-template>
                                                        </td>
                                                    </tr>
                                                </xsl:if>
                                                <xsl:if test="count(./n:BlocLignePiece/n:InfoLignePiece/n:DteMajo) > 0">
                                                    <tr>
                                                        <td class="no-wrap">Date de majoration</td>
                                                        <td class="text-right">
                                                            <xsl:variable name="ligne.libelleDteMajo">
                                                                <xsl:call-template name="slash-date">
                                                                    <xsl:with-param name="datebrute"
                                                                                    select="./n:BlocLignePiece/n:InfoLignePiece/n:DteMajo/@V"></xsl:with-param>
                                                                </xsl:call-template>
                                                            </xsl:variable>
                                                            <xsl:call-template name="afficher.balise">
                                                                <xsl:with-param name="element.nom" select="'DteMajo'"/>
                                                                <xsl:with-param name="element"
                                                                                select="./n:BlocLignePiece/n:InfoLignePiece/n:DteMajo"/>
                                                                <xsl:with-param name="libelle"
                                                                                select="$ligne.libelleDteMajo"/>
                                                                <xsl:with-param name="parent.nodeid"
                                                                                select="./@ano:node-id"/>
                                                            </xsl:call-template>
                                                        </td>
                                                    </tr>
                                                </xsl:if>
                                                <xsl:if test="count(./n:BlocLignePiece/n:InfoLignePiece/n:TxMajo) > 0">
                                                    <tr>
                                                        <td class="no-wrap">Taux de majoration</td>
                                                        <td class="text-right">
                                                            <xsl:call-template name="afficher.balise">
                                                                <xsl:with-param name="element.nom" select="'TxMajo'"/>
                                                                <xsl:with-param name="element"
                                                                                select="./n:BlocLignePiece/n:InfoLignePiece/n:TxMajo"/>
                                                                <xsl:with-param name="parent.nodeid"
                                                                                select="./@ano:node-id"/>
                                                            </xsl:call-template>
                                                        </td>
                                                    </tr>
                                                </xsl:if>
                                            </table>
                                        </td>

                                    </xsl:element>
                                </xsl:for-each>
                                <!-- pour les totaux -->
                                <tr style="text-align: left">
                                    <td colspan="7">&nbsp;</td>
                                    <td>
                                        <table class="inner" width="100%" cellspacing="0">
                                            <tr>
                                                <td class="no-wrap">TOTAL HT</td>
                                                <td class="text-right">
                                                    <xsl:call-template name="number">
                                                        <xsl:with-param name="num" select="$totalHT"/>
                                                    </xsl:call-template>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr style="text-align: left">
                                    <td colspan="7">&nbsp;</td>
                                    <td>
                                        <table class="inner" width="100%" cellspacing="0">
                                            <tr>
                                                <td class="no-wrap">TOTAL TTC</td>
                                                <td class="text-right">
                                                    <xsl:call-template name="number">
                                                        <xsl:with-param name="num" select="$totalTTC"/>
                                                    </xsl:call-template>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <p/>
                            <xsl:if test="./ano:Anomalie">
                                <hr/>
                                <xsl:for-each select="./ano:Anomalie">
                                    <xsl:variable name="severity.color"
                                                  select="$severity.colors//severity[@Code= $tags-ano//node/@severity]/@Couleur"/>
                                    <xsl:element name="div">
                                        <xsl:if test="$anoId = ./@ano:anoId">
                                            <xsl:attribute name="style">background-color: <xsl:value-of
                                                    select="$severity.color"/>;
                                            </xsl:attribute>
                                        </xsl:if>
                                        <xsl:variable name="idBordereau">
                                            <xsl:value-of select="../../@added:primary-key"/>
                                        </xsl:variable>
                                        <xsl:variable name="idPiece">
                                            <xsl:value-of select="../n:BlocPiece/n:IdPce/@V"/>
                                        </xsl:variable>
                                        <h3>
                                            <xsl:element name="a">
                                                <xsl:attribute name="name">AnoId_<xsl:value-of select="./@ano:anoId"/>
                                                </xsl:attribute>
                                                <xsl:attribute name="href">xemelios:/query?docId=pes-aller&amp;etatId=PES_RecetteAller&amp;elementId=BordereauRecette&amp;collectivite=<xsl:value-of
                                                        select="/n:PES_Aller/n:EnTetePES/n:IdColl/@V"/>&amp;budget=<xsl:value-of
                                                        select="/n:PES_Aller/n:EnTetePES/n:CodBud/@V"/>&amp;path=[@added:primary-key='<xsl:value-of
                                                        select="$idBordereau"/>']&amp;xsl:param=(mandatId,<xsl:value-of
                                                        select="$idPiece"/>)&amp;xsl:param=(anoId,<xsl:value-of
                                                        select="./@ano:anoId"/>)
                                                </xsl:attribute>
                                                Anomalie <!--xsl:value-of select="./@ano:anoId"/-->
                                            </xsl:element>
                                        </h3>
                                        <p>
                                            <span class="gras">Contr&#244;le :</span>
                                            <xsl:value-of select="./@ano:ctrlLibelle"/>&nbsp;(<xsl:value-of
                                                select="./@ano:ctrlId"/>)
                                        </p>
                                        <!--p><span class="gras">R&#232;gle fonctionnelle : </span><xsl:value-of select="./ano:ctrlRegleFonct/text()" disable-output-escaping="yes"/></p-->
                                        <p>
                                            <span class="gras">Message :</span>
                                            <xsl:value-of select="./ano:message/text()" disable-output-escaping="yes"/>
                                        </p>
                                        <!--p><span class="gras">Noeuds : </span><xsl:for-each select="ano:node"><xsl:value-of select="./@ano:id"/>, </xsl:for-each></p-->
                                    </xsl:element>
                                </xsl:for-each>
                                <hr/>
                            </xsl:if>
                        </xsl:for-each>
                    </xsl:otherwise>

                </xsl:choose>

                <!-- une table pour le pied de page, le m&#234;me pour le bordereau et le mandat -->
                <!--table class="footer" cellspacing="0">
                    <tr>
                        <td class="colonne1" style="border-style:none">Edition du&nbsp;
                        </td>
                        <td class="colonne2" style="border-style:none">&nbsp;</td>
                        <td class="colonne3" style="border-style:none">&nbsp;</td>
                        <td class="colonne4" style="border-style:none">&nbsp;</td>
                    </tr>
                </table-->

                <!--/div-->
            </body>

        </html>
    </xsl:template>

</xsl:stylesheet>
