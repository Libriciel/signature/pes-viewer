/*
 * PES-Viewer
 * Copyright (C) 2019-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.pesviewer.util;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


class FileUtilsTest {


    @Test
    void cleanFileSystemForbiddenCharacters() {
        assertEquals("test_test", FileUtils.cleanFileSystemForbiddenCharacters("test?test"));
        assertEquals("test_test", FileUtils.cleanFileSystemForbiddenCharacters("test|test"));
        assertEquals("test_test", FileUtils.cleanFileSystemForbiddenCharacters("test\\test"));
        assertEquals("test_test", FileUtils.cleanFileSystemForbiddenCharacters("test*test"));
        assertEquals("test_test", FileUtils.cleanFileSystemForbiddenCharacters("test/test"));
        assertEquals("test_test", FileUtils.cleanFileSystemForbiddenCharacters("test:test"));
        assertEquals("_test_.pdf", FileUtils.cleanFileSystemForbiddenCharacters("<test>.pdf"));
    }


}
