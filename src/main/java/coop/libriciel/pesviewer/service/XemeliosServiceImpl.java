/*
 * PES-Viewer
 * Copyright (C) 2019-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.pesviewer.service;

import coop.libriciel.pesviewer.exception.BrowseInPesException;
import coop.libriciel.pesviewer.exception.PJNotFoundException;
import coop.libriciel.pesviewer.exception.UnableToCreateRichPesException;
import coop.libriciel.pesviewer.util.XmlPjExtractor;
import fr.gouv.finances.cp.xemelios.ui.HtmlViewer;
import fr.gouv.finances.dgfip.utils.IoUtils;
import fr.gouv.finances.dgfip.utils.NavigationContext;
import fr.gouv.finances.dgfip.utils.xml.FactoryProvider;
import fr.gouv.finances.dgfip.utils.xml.NamespaceContextImpl;
import fr.gouv.finances.dgfip.utils.xml.transform.DefaultUriResolver;
import fr.gouv.finances.dgfip.xemelios.common.config.DocumentModel;
import fr.gouv.finances.dgfip.xemelios.common.config.DocumentsModel;
import fr.gouv.finances.dgfip.xemelios.common.config.ElementModel;
import fr.gouv.finances.dgfip.xemelios.common.config.EtatModel;
import lombok.extern.log4j.Log4j2;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.*;
import java.io.*;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static coop.libriciel.pesviewer.util.PesViewerRequestConstants.STATE_ID_KEY;
import static coop.libriciel.pesviewer.util.XemeliosConstants.DOC_ID;
import static coop.libriciel.pesviewer.util.XemeliosConstants.PJ_ID;
import static javax.xml.xpath.XPathConstants.NODESET;
import static javax.xml.xpath.XPathConstants.STRING;


@Service
@Log4j2
public class XemeliosServiceImpl implements XemeliosService {

    private static final String ASAP_PJ_MARKER = "ASAP_PJ_REF_";
    private final DocumentsModel documentsModel;
    private final StorageService storageService;
    private final XPathFactory xPathFactory;
    private final NamespaceContextImpl namespaceContext;

    private final TransformerFactory tFactory;
    private final DocumentBuilderFactory dbf;


    @Autowired
    public XemeliosServiceImpl(DocumentsModel documentsModel, StorageService storageService) throws TransformerConfigurationException, ParserConfigurationException {
        this.documentsModel = documentsModel;
        this.storageService = storageService;

        xPathFactory = new net.sf.saxon.xpath.XPathFactoryImpl();
        namespaceContext = new NamespaceContextImpl();
        namespaceContext.addMapping("n", "http://www.minefi.gouv.fr/cp/helios/pes_v2/Rev0/aller");

        tFactory = TransformerFactory.newInstance("net.sf.saxon.TransformerFactoryImpl", null);
        tFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);

        dbf = DocumentBuilderFactory.newInstance();
        dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
        dbf.setNamespaceAware(true);

    }


    @Override
    public Document createRichPesFileAndExtractPj(InputStream fileIo) {
        try (ByteArrayOutputStream pesWithoutPj = this.extractPjAndGetNewXml(fileIo);
             InputStream pesWithoutPjInputStream = new ByteArrayInputStream(pesWithoutPj.toByteArray());
             ByteArrayOutputStream richPesFile = this.createRichPesXml(pesWithoutPjInputStream);
             InputStream richPesFileInputStream = new ByteArrayInputStream(richPesFile.toByteArray())) {

            Document domDoc = dbf.newDocumentBuilder().parse(richPesFileInputStream);
            this.referenceAsapPj(domDoc);
            return domDoc;

        } catch (IOException | TransformerException | ParserConfigurationException | SAXException | XMLStreamException e) {
            throw new UnableToCreateRichPesException("Cannot create rich PES file", e);
        }
    }


    private ByteArrayOutputStream extractPjAndGetNewXml(InputStream fileIo) throws XMLStreamException, IOException {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();

        XmlPjExtractor extractor = new XmlPjExtractor(fileIo, bout);

        extractor.getPjFiles().forEach((idUnique, inputStream) -> {
            try {
                storageService.store(composedPjId(extractor.getIdColl(), idUnique), inputStream);
            } catch (IOException e) {
                log.error("Cannot store PJ with id " + idUnique, e);
            }
        });

        return bout;
    }


    @Override
    public @NotNull Path getAttachmentInRichFile(Document doc, Map<String, String> allRequestParams) {
        String idUnique = allRequestParams.get(PJ_ID);

        XPath xp = xPathFactory.newXPath();
        xp.setNamespaceContext(namespaceContext);
        try {
            Element idColl = (Element) xp.evaluate("//n:IdColl", doc, XPathConstants.NODE);
            String idCollStr = idColl.getAttribute("V");
            String fullPjId = this.composedPjId(idCollStr, idUnique);

            return Optional.ofNullable(storageService.get(fullPjId))
                    .orElseThrow(() -> new PJNotFoundException(idUnique));
        } catch (XPathExpressionException e) {
            log.error("Unhandled exception thrown : " + e.getMessage());
            throw new PJNotFoundException(e.getMessage(), e);
        }
    }


    @Override
    public ByteArrayOutputStream browseInRichFile(Document doc, Map<String, String> allRequestParams) {
        Map<String, Object> params = new HashMap<>();
        NavigationContext navigationContext = this.prepareNavigationContext(allRequestParams, params);

        ByteArrayOutputStream outArray = new ByteArrayOutputStream();

        EtatModel em = documentsModel.getDocumentById(navigationContext.getDocId()).getEtatById(navigationContext.getEtatId());

        ElementModel element = (
                navigationContext.getElementId() != null ?
                        em.getElementById(navigationContext.getElementId()) :
                        em.getBrowsableElement());


        File f = new File(em.getParent().getBaseDirectory(), element.getXslt());
        Map<String, Object> parameters = new HashMap<>(params);

        // on passe la nomenclature en parametre
        parameters.put("repository", doc);

        // on passe la config
        parameters.put("config", documentsModel.getSmallDOM());

        if (navigationContext.getCollectivite() != null)
            parameters.put("collectivite", navigationContext.getCollectivite());

        if (navigationContext.getBudget() != null)
            parameters.put("budget", navigationContext.getBudget());


        TransformerFactory tFactory = FactoryProvider.getTransformerFactory();

        try {
            InputStream is = IoUtils.getInputStream(f.getPath());

            DefaultUriResolver uriResolver = new DefaultUriResolver();
            DefaultUriResolver.initializationHelper(uriResolver, doc, tFactory.getURIResolver(), f.getParentFile().toURI().toURL().toExternalForm());

            tFactory.setURIResolver(uriResolver);

            Transformer transformer = tFactory.newTransformer(new StreamSource(is));

            // Copy all parameters in transformer
            parameters.forEach((key, value) -> {
                try {
                    transformer.setParameter(key, value);
                } catch (NullPointerException e) {
                    log.error("La clé " + key + " n'a pas de valeur", e.getMessage());
                    throw e;
                }
            });

            transformer.transform(new DOMSource(doc), new StreamResult(outArray));
        } catch (Exception e) {
            log.error("Error while browsing PES file", e);
            throw new BrowseInPesException(e.getMessage(), e);
        }


        return outArray;
    }


    @Override
    public String getMimeTypeForPj(Document doc, String pjId) {
        log.debug("getMimeTypeForPJ - id : {}", pjId);

        XPath xp = xPathFactory.newXPath();
        xp.setNamespaceContext(namespaceContext);

        Map<String, String> varMap = Map.of("pjId", pjId);
        XPathVariableResolver varResolver = variableName -> varMap.getOrDefault(variableName.getLocalPart(), "");
        xp.setXPathVariableResolver(varResolver);

        String xpQuery = "//n:PJ[n:IdUnique/@V=$pjId]/n:Contenu/n:Fichier/@MIMEType";
        String mimeType;
        try {
            mimeType = (String) xp.evaluate(xpQuery, doc, STRING);
        } catch (XPathExpressionException e) {
            throw new RuntimeException("Unexpected internal error, custom XPath query used seems invalid", e);
        }
        log.debug("getMimeTypeForPJ - found mimeType : {}", mimeType);

        return mimeType;

    }


    @Override
    public String getOriginalFileNameForPj(Document doc, String pjId) {
        log.debug("getOriginalFileNameForPJ - id:{}", pjId);

        XPath xp = xPathFactory.newXPath();
        xp.setNamespaceContext(namespaceContext);

        Map<String, String> varMap = Map.of("pjId", pjId);
        XPathVariableResolver varResolver = variableName -> varMap.getOrDefault(variableName.getLocalPart(), "");
        xp.setXPathVariableResolver(varResolver);

        String xpQuery = "//n:PJ[n:IdUnique/@V=$pjId]/n:NomPJ/@V";
        String filename;
        try {
            filename = (String) xp.evaluate(xpQuery, doc, STRING);
        } catch (XPathExpressionException e) {
            throw new RuntimeException("Unexpected internal error, custom XPath query used seems invalid", e);
        }

        log.debug("getOriginalFileNameForPJ - found filename : {}", filename);
        return filename;
    }


    @Override
    public String getPjName(Document doc, String pjId) {
        log.debug("getPJName - id:{}", pjId);

        XPath xp = xPathFactory.newXPath();
        xp.setNamespaceContext(namespaceContext);

        Map<String, String> varMap = Map.of("pjId", pjId);
        XPathVariableResolver varResolver = variableName -> varMap.getOrDefault(variableName.getLocalPart(), "");
        xp.setXPathVariableResolver(varResolver);

        String xpQuery = "//n:PJRef[n:IdUnique/@V=$pjId]/n:NomPJ/@V";
        String filename;
        try {
            filename = (String) xp.evaluate(xpQuery, doc, STRING);
        } catch (XPathExpressionException e) {
            throw new RuntimeException("Unexpected internal error, custom XPath query used seems invalid", e);
        }

        log.debug("getPJName - found PJ name : {}", filename);
        return filename;
    }


    @Override
    public boolean pjIsAsap(Document doc, String pjId) {

        XPath xp = xPathFactory.newXPath();
        xp.setNamespaceContext(namespaceContext);
        try {
            String collId = (String) xp.evaluate("//n:IdColl/@V", doc, STRING);
            String asapRef = this.composedASAPPjRef(collId, pjId);
            if (storageService.fileExists(asapRef)) {
                return true;
            }
        } catch (XPathExpressionException e) {
            log.error("Unhandled exception thrown while checking for ASAP pj: " + e.getMessage());
        }

        return false;
    }


    private void referenceAsapPj(Document domDoc) throws IOException {
        log.debug("referenceAsapPj");
        XPath xp = xPathFactory.newXPath();
        xp.setNamespaceContext(namespaceContext);

        String collIdXpXpQuery = "//n:EnTetePES/n:IdColl/@V";
        String asapXpQuery = "//n:PJ[n:TypePJ/@V='006']/n:IdUnique/@V";

        try {
            String collId = xp.evaluate(collIdXpXpQuery, domDoc);
            NodeList nodes = (NodeList) xp.evaluate(asapXpQuery, domDoc, NODESET);

            byte emptyB[] = new byte[0];
            for (int i = 0; i < nodes.getLength(); i++) {
                Node n = nodes.item(i);
                storageService.store(composedASAPPjRef(collId, n.getNodeValue()), new ByteArrayInputStream(emptyB));
            }

        } catch (XPathExpressionException e) {
            log.error("An errror occurred while referencing ASAP pj, some may not be readable in ASAP format afterwards");
        }
    }


    private String composedPjId(String collId, String pjId) {
        return collId + pjId;
    }


    private String composedASAPPjRef(String collId, String pjId) {
        return ASAP_PJ_MARKER + collId + pjId;
    }


    private NavigationContext prepareNavigationContext(Map<String, String> requestParams, Map<String, Object> params) {
        NavigationContext navigationContext = new NavigationContext();

        navigationContext.setDocId(DOC_ID);
        String stateId = requestParams.get(STATE_ID_KEY);
        if (stateId != null) {
            navigationContext.setEtatId(stateId);
        }

        params.put(HtmlViewer.PARAM_DESTINATION, HtmlViewer.VALUE_DESTINATION_INTERNAL);

        requestParams.forEach((key, value) -> {

            if ("docId".equals(key)) {
                // c'est soit "PES_Aller", soit "DocumentRapport"
                navigationContext.setDocId(value);
            } else if ("elementId".equals(key)) navigationContext.setElementId(value);
            else if ("collectivite".equals(key)) navigationContext.setCollectivite(value);
            else if ("budget".equals(key)) navigationContext.setBudget(value);
            else if ("sp1".equals(key)) navigationContext.setSp1(value);
            else if ("sp2".equals(key)) navigationContext.setSp2(value);
            else if ("path".equals(key)) {
                navigationContext.setPath(value);
                transformPath(value, params);
            } else {
                if (key.startsWith("xsl:param")) {
                    int virg = value.indexOf(',');
                    String pName = value.substring(1, virg);
                    String pValue = value.substring(virg + 1, value.length() - 1);
                    params.put(pName, pValue);
                }
            }
        });

        return navigationContext;
    }


    private void transformPath(String paramValue, Map<String, Object> xslParams) {
        String sTmp = paramValue.substring(1, paramValue.length() - 2);
        String[] vals = sTmp.split("=");
        if ("@added:primary-key".equals(vals[0])) {
            if (vals.length > 1) {
                String pk = vals[1].substring(1);
                xslParams.put("primaryKey", pk);
            }
            return;
        }
        log.error("error processing path=" + paramValue);
    }


    private ByteArrayOutputStream createRichPesXml(InputStream pesStream) throws IOException, TransformerException {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();

        DocumentModel pesModel = documentsModel.getDocumentById(DOC_ID);

        Transformer tr = tFactory.newTransformer(new StreamSource(new File(pesModel.getBaseDirectory(), pesModel.getGlobalImportXsltFile())));
        StreamResult result = new StreamResult(bout);

        tr.transform(new StreamSource(pesStream), result);
        result.getOutputStream().flush();
        result.getOutputStream().close();

        return bout;
    }


}
