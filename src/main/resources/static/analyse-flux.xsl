<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE xsl:stylesheet [
        ]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:p="http://www.minefi.gouv.fr/cp/helios/pes_v2/Rev0/aller"
                xmlns:added="http://projets.admisource.gouv.fr/xemelios/namespaces#added"
                xmlns:ano="http://projets.admisource.gouv.fr/xemelios/namespaces#anomally"
                xmlns:ds="http://www.w3.org/2000/09/xmldsig#" xmlns:xad="http://uri.etsi.org/01903/v1.1.1#"
                xmlns:data="data.uri"
                xmlns:xemhlp="http://xemelios.org/extensions/xml/functions"
                version="2.0">
    <!--   xmlns:xemhlp="fr.gouv.finances.cp.utils.xml.helper.HelperFunction"  xmlns:xem="fr.gouv.finances.cp.utils.xml.certs.Certificate509" -->
    <xsl:param name="browser-destination"/>
    <xsl:output encoding="ISO-8859-1" method="xhtml" exclude-result-prefixes="p added ano data"
                doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" include-content-type="no" indent="yes"/>
    <xsl:decimal-format name="decformat" decimal-separator="," grouping-separator=" " digit="#"
                        pattern-separator=";" NaN="NaN" minus-sign="-"/>
    <xsl:key name="distinct-exer" match="Exer" use="@V"/>
    <xsl:variable name="exercice">
        <xsl:value-of select="xemhlp:getDate('yyyy')"/>
        <!--xsl:value-of select="format-date(current(),'[Y]')"/-->
    </xsl:variable>
    <xsl:variable name="sousdomaines">
        <domaine code="depense">
            <triplet code="01-01-01" valeur="Ordinaire"/>
            <triplet code="01-01-02" valeur="Ordinaire"/>
            <triplet code="02-02-06" valeur="Correctif"/>
            <triplet code="01-02-09" valeur="Correctif"/>
            <triplet code="02-02-10" valeur="Correctif"/>
            <triplet code="01-05-01" valeur="EAP"/>
            <triplet code="01-05-02" valeur="EAP"/>
            <triplet code="01-05-03" valeur="EAP"/>
            <triplet code="01-05-04" valeur="EAP"/>
            <triplet code="01-01-05" valeur="R�gie"/>
            <triplet code="01-05-05" valeur="R�gie"/>
            <triplet code="01-09-01" valeur="March�"/>
            <triplet code="01-09-02" valeur="March�"/>
            <triplet code="01-09-12" valeur="March�"/>
            <triplet code="01-09-13" valeur="March�"/>
            <triplet code="01-01-03" valeur="Inventaire/Emprunt"/>
            <triplet code="01-01-04" valeur="Inventaire/Emprunt"/>
            <triplet code="01-10-01" valeur="Rattachement"/>
            <triplet code="01-10-02" valeur="Rattachement"/>
            <triplet code="01-10-03" valeur="Rattachement"/>
            <triplet code="01-10-04" valeur="Rattachement"/>
            <triplet code="01-10-05" valeur="Rattachement"/>
            <triplet code="01-10-11" valeur="Rattachement"/>
            <triplet code="01-03-01" valeur="Fin d'exercice"/>
            <triplet code="01-03-02" valeur="Fin d'exercice"/>
            <triplet code="01-03-03" valeur="Fin d'exercice"/>
            <triplet code="01-03-04" valeur="Fin d'exercice"/>
            <triplet code="01-03-18" valeur="Fin d'exercice"/>
            <triplet code="01-04-01" valeur="Fin d'exercice"/>
            <triplet code="01-04-02" valeur="Fin d'exercice"/>
            <triplet code="01-04-03" valeur="Fin d'exercice"/>
            <triplet code="01-04-04" valeur="Fin d'exercice"/>
            <triplet code="02-04-06" valeur="Fin d'exercice"/>
            <triplet code="01-13-01" valeur="Fin d'exercice"/>
            <triplet code="02-13-06" valeur="Fin d'exercice"/>
            <triplet code="01-08-01" valeur="Collectif"/>
            <triplet code="01-08-02" valeur="Collectif"/>
            <triplet code="01-01-11" valeur="Paie"/>
            <triplet code="01-08-11" valeur="Paie"/>
            <triplet code="01-07-01" valeur="Admission en non valeur"/>
            <triplet code="01-06-01" valeur="Global"/>
            <triplet code="01-06-04" valeur="Global"/>
            <triplet code="01-06-07" valeur="Global"/>
            <triplet code="01-02-07" valeur="Hors Scope"/>
            <triplet code="01-02-08" valeur="Hors Scope"/>
            <triplet code="03-11-01" valeur="Hors Scope"/>
            <triplet code="03-11-02" valeur="Hors Scope"/>
            <triplet code="03-11-14" valeur="Hors Scope"/>
        </domaine>
        <domaine code="recette">
            <triplet code="01-01-01" valeur="Ordinaire"/>
            <triplet code="01-01-02" valeur="Ordinaire"/>
            <triplet code="02-02-06" valeur="Correctif"/>
            <triplet code="01-02-09" valeur="Correctif"/>
            <triplet code="02-02-10" valeur="Correctif"/>
            <triplet code="01-05-01" valeur="Emis Apr�s Encaissement"/>
            <triplet code="01-05-02" valeur="Emis Apr�s Encaissement"/>
            <triplet code="01-05-03" valeur="Emis Apr�s Encaissement"/>
            <triplet code="01-05-04" valeur="Emis Apr�s Encaissement"/>
            <triplet code="01-01-05" valeur="R�gie"/>
            <triplet code="01-05-05" valeur="R�gie"/>
            <triplet code="01-01-03" valeur="Inventaire/Emprunt"/>
            <triplet code="01-01-04" valeur="Inventaire/Emprunt"/>
            <triplet code="01-11-01" valeur="Rattachement"/>
            <triplet code="01-11-02" valeur="Rattachement"/>
            <triplet code="01-11-04" valeur="Rattachement"/>
            <triplet code="01-01-18" valeur="Fin d'exercice"/>
            <triplet code="01-03-01" valeur="Fin d'exercice"/>
            <triplet code="01-03-02" valeur="Fin d'exercice"/>
            <triplet code="01-03-03" valeur="Fin d'exercice"/>
            <triplet code="01-03-04" valeur="Fin d'exercice"/>
            <triplet code="01-03-18" valeur="Fin d'exercice"/>
            <triplet code="01-04-01" valeur="Fin d'exercice"/>
            <triplet code="01-04-02" valeur="Fin d'exercice"/>
            <triplet code="01-04-03" valeur="Fin d'exercice"/>
            <triplet code="01-04-04" valeur="Fin d'exercice"/>
            <triplet code="02-04-06" valeur="Fin d'exercice"/>
            <triplet code="01-14-01" valeur="Fin d'exercice"/>
            <triplet code="02-14-06" valeur="Fin d'exercice"/>
            <triplet code="01-09-01" valeur="Majoration"/>
            <triplet code="01-06-01" valeur="R�capitulatif"/>
            <triplet code="02-06-06" valeur="R�capitulatif"/>
            <triplet code="01-07-01" valeur="R�capitulatif"/>
            <triplet code="01-02-07" valeur="Hors Scope"/>
            <triplet code="01-02-08" valeur="Hors Scope"/>
            <triplet code="01-10-01" valeur="Hors Scope"/>
            <triplet code="01-10-02" valeur="Hors Scope"/>
            <triplet code="03-12-01" valeur="Hors Scope"/>
            <triplet code="03-12-02" valeur="Hors Scope"/>
            <triplet code="03-12-11" valeur="Hors Scope"/>
        </domaine>
    </xsl:variable>
    <xsl:template match="*"/>
    <xsl:template match="/p:PES_Aller">
        <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
            <head>
                <title>ANALYSE DES FLUX de PES</title>
            </head>
            <body>
                <h1 class="h3">
                    <xsl:value-of select="./p:EnTetePES/p:IdPost/@V"/>&#160;<xsl:value-of select="./p:EnTetePES/p:LibellePoste/@V"/>
                </h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="xemelios:/query?docId=pes-aller&amp;etatId=general_view">Accueil</a>
                        </li>
                        <li class="breadcrumb-item active">
                            R&#233;sum&#233;
                        </li>
                    </ol>
                </nav>

                <h2 class="h4">R&#233;sum&#233; du flux</h2>

                <div align="center">
                    <table>
                        <thead>
                            <tr>
                                <th>Domaine</th>
                                <th>Nombre de bordereaux</th>
                                <th>Nombre de pi&#232;ces</th>
                                <th>Nombre de lignes</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Recette</td>
                                <td>
                                    <xsl:value-of select="count(./p:PES_RecetteAller/p:Bordereau/p:BlocBordereau)"/>
                                </td>
                                <td>
                                    <xsl:value-of select="count(./p:PES_RecetteAller/p:Bordereau/p:Piece/p:BlocPiece)"/>
                                </td>
                                <td>
                                    <xsl:value-of
                                            select="count(./p:PES_RecetteAller/p:Bordereau/p:Piece/p:LigneDePiece/p:BlocLignePiece)"/>
                                </td>
                            </tr>
                            <tr>
                                <td>D&#233;pense</td>
                                <td>
                                    <xsl:value-of select="count(./p:PES_DepenseAller/p:Bordereau/p:BlocBordereau)"/>
                                </td>
                                <td>
                                    <xsl:value-of
                                            select="count(./p:PES_DepenseAller/p:Bordereau/p:Piece/p:BlocPiece/p:InfoPce)"/>
                                </td>
                                <td>
                                    <xsl:value-of
                                            select="count(./p:PES_DepenseAller/p:Bordereau/p:Piece/p:LigneDePiece/p:BlocLignePiece)"/>
                                </td>
                            </tr>
                            <tr>
                                <td>PJ</td>
                                <td colspan="3">
                                    <xsl:value-of select="count(.//p:PES_PJ/p:PJ)"/>
                                </td>
                            </tr>
                            <tr>
                                <td>Exercice</td>
                                <td colspan="3">
                                    <xsl:for-each
                                            select="//p:Bordereau/p:BlocBordereau/p:Exer[generate-id() = generate-id(key('distinct-exer',string(@V))[1])]">
                                        <xsl:value-of select="@V"/>&#160;
                                    </xsl:for-each>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <h3 class="h5">Description du flux par sous domaine</h3>

                <table>
                    <thead>
                        <tr>
                            <th>Domaine</th>
                            <th>Sous domaine</th>
                            <th>Num&#233;ro de bordereaux</th>
                            <th>Nbre total</th>
                            <th>Triplet correspondant</th>
                            <th>R&#233;gie</th>
                            <th>Inventaire</th>
                            <th>Convention</th>
                            <th>Emprunt</th>
                            <th>Encaiss. \ Paiement</th>
                            <th>Pi&#232;ce d'origine N</th>
                            <th>Pi&#232;ce d'origine N-1</th>
                        </tr>
                    </thead>
                    <tbody>
                        <xsl:call-template name="add-description">
                            <xsl:with-param name="domaine">Recette</xsl:with-param>
                            <xsl:with-param name="sousdomaine">Ordinaires</xsl:with-param>
                            <xsl:with-param name="list-bord"
                                            select="./p:PES_RecetteAller/p:Bordereau[p:Piece/p:BlocPiece/p:TypPce/@V='01' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='02')  ]"/>
                            <xsl:with-param name="nbr"
                                            select="count(./p:PES_RecetteAller/p:Bordereau/p:BlocBordereau/p:IdBord/@V[../../../p:Piece/p:BlocPiece/p:TypPce/@V='01' and (../../../p:Piece/p:BlocPiece/p:NatPce/@V='01' or ../../../p:Piece/p:BlocPiece/p:NatPce/@V='02')  ])"/>
                            <xsl:with-param name="triplets"
                                            select="('Ordinaire \ Fonctionnement', 'Ordinaire \ Investissement')"/>
                            <xsl:with-param name="regie"
                                            select="./p:PES_RecetteAller/p:Bordereau[(p:Piece/p:BlocPiece/p:TypPce/@V='01' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='02')) and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdRegie ]"/>
                            <xsl:with-param name="inventaire"
                                            select="./p:PES_RecetteAller/p:Bordereau[(p:Piece/p:BlocPiece/p:TypPce/@V='01' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='02')) and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdActif ]"/>
                            <xsl:with-param name="convention"
                                            select="./p:PES_RecetteAller/p:Bordereau[(p:Piece/p:BlocPiece/p:TypPce/@V='01' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='02')) and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdConv ]"/>
                            <xsl:with-param name="emprunt"
                                            select="./p:PES_RecetteAller/p:Bordereau[(p:Piece/p:BlocPiece/p:TypPce/@V='01' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='02')) and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdEmpruntOrdo ]"/>
                            <xsl:with-param name="encaiss"
                                            select="./p:PES_RecetteAller/p:Bordereau[(p:Piece/p:BlocPiece/p:TypPce/@V='01' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='02')) and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdEncaissement ]"/>
                            <xsl:with-param name="piecen"
                                            select="./p:PES_RecetteAller/p:Bordereau[(p:Piece/p:BlocPiece/p:TypPce/@V='01' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='02')) and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:ExerRat/@V=$exercice ]"/>
                            <xsl:with-param name="piecen-1"
                                            select="./p:PES_RecetteAller/p:Bordereau[(p:Piece/p:BlocPiece/p:TypPce/@V='01' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='02')) and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:ExerRat/@V=$exercice -1 ]"/>
                        </xsl:call-template>
                        <xsl:call-template name="add-description">
                            <xsl:with-param name="domaine">Recette</xsl:with-param>
                            <xsl:with-param name="sousdomaine">Correction \Annulation</xsl:with-param>
                            <xsl:with-param name="list-bord"
                                            select="./p:PES_RecetteAller/p:Bordereau[p:Piece/p:BlocPiece/p:TypPce/@V='02' and (p:Piece/p:BlocPiece/p:NatPce/@V='06' or p:Piece/p:BlocPiece/p:NatPce/@V='09' or p:Piece/p:BlocPiece/p:NatPce/@V='10')  ]"/>
                            <xsl:with-param name="nbr"
                                            select="count(./p:PES_RecetteAller/p:Bordereau/p:BlocBordereau/p:IdBord/@V[../../../p:Piece/p:BlocPiece/p:TypPce/@V='02' and (../../../p:Piece/p:BlocPiece/p:NatPce/@V='06' or ../../../p:Piece/p:BlocPiece/p:NatPce/@V='09' or ../../../p:Piece/p:BlocPiece/p:NatPce/@V='10')  ])"/>
                            <xsl:with-param name="triplets"
                                            select="('Correctif \ Annulation\R&#233;duction','Correctif \ Annulant un mandat','Correctif \ Annulation un titre de rattachement')"/>
                            <xsl:with-param name="regie"
                                            select="./p:PES_RecetteAller/p:Bordereau[(p:Piece/p:BlocPiece/p:TypPce/@V='02' and (p:Piece/p:BlocPiece/p:NatPce/@V='06' or p:Piece/p:BlocPiece/p:NatPce/@V='09' or p:Piece/p:BlocPiece/p:NatPce/@V='10')) and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdRegie   ]"/>
                            <xsl:with-param name="inventaire"
                                            select="./p:PES_RecetteAller/p:Bordereau[(p:Piece/p:BlocPiece/p:TypPce/@V='02' and (p:Piece/p:BlocPiece/p:NatPce/@V='06' or p:Piece/p:BlocPiece/p:NatPce/@V='09' or p:Piece/p:BlocPiece/p:NatPce/@V='10')) and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdActif   ]"/>
                            <xsl:with-param name="convention"
                                            select="./p:PES_RecetteAller/p:Bordereau[(p:Piece/p:BlocPiece/p:TypPce/@V='02' and (p:Piece/p:BlocPiece/p:NatPce/@V='06' or p:Piece/p:BlocPiece/p:NatPce/@V='09' or p:Piece/p:BlocPiece/p:NatPce/@V='10')) and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdConv   ]"/>
                            <xsl:with-param name="emprunt"
                                            select="./p:PES_RecetteAller/p:Bordereau[(p:Piece/p:BlocPiece/p:TypPce/@V='02' and (p:Piece/p:BlocPiece/p:NatPce/@V='06' or p:Piece/p:BlocPiece/p:NatPce/@V='09' or p:Piece/p:BlocPiece/p:NatPce/@V='10')) and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdEmpruntOrdo ]"/>
                            <xsl:with-param name="encaiss"
                                            select="./p:PES_RecetteAller/p:Bordereau[(p:Piece/p:BlocPiece/p:TypPce/@V='02' and (p:Piece/p:BlocPiece/p:NatPce/@V='06' or p:Piece/p:BlocPiece/p:NatPce/@V='09' or p:Piece/p:BlocPiece/p:NatPce/@V='10')) and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdEncaissement ]"/>
                            <xsl:with-param name="piecen"
                                            select="./p:PES_RecetteAller/p:Bordereau[(p:Piece/p:BlocPiece/p:TypPce/@V='02' and (p:Piece/p:BlocPiece/p:NatPce/@V='06' or p:Piece/p:BlocPiece/p:NatPce/@V='09' or p:Piece/p:BlocPiece/p:NatPce/@V='10')) and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:ExerRat/@V=$exercice  ]"/>
                            <xsl:with-param name="piecen-1"
                                            select="./p:PES_RecetteAller/p:Bordereau[(p:Piece/p:BlocPiece/p:TypPce/@V='02' and (p:Piece/p:BlocPiece/p:NatPce/@V='06' or p:Piece/p:BlocPiece/p:NatPce/@V='09' or p:Piece/p:BlocPiece/p:NatPce/@V='10')) and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:ExerRat/@V=$exercice -1  ]"/>
                        </xsl:call-template>
                        <xsl:call-template name="add-description">
                            <xsl:with-param name="domaine">Recette</xsl:with-param>
                            <xsl:with-param name="sousdomaine">EAE</xsl:with-param>
                            <xsl:with-param name="list-bord"
                                            select="./p:PES_RecetteAller/p:Bordereau[p:Piece/p:BlocPiece/p:TypPce/@V='05' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='02'  or p:Piece/p:BlocPiece/p:NatPce/@V='03' or p:Piece/p:BlocPiece/p:NatPce/@V='04')  ]"/>
                            <xsl:with-param name="nbr"
                                            select="count(./p:PES_RecetteAller/p:Bordereau/p:BlocBordereau/p:IdBord/@V[../../../p:Piece/p:BlocPiece/p:TypPce/@V='05' and (../../../p:Piece/p:BlocPiece/p:NatPce/@V='01' or ../../../p:Piece/p:BlocPiece/p:NatPce/@V='02'  or ../../../p:Piece/p:BlocPiece/p:NatPce/@V='03' or ../../../p:Piece/p:BlocPiece/p:NatPce/@V='04')  ])"/>
                            <xsl:with-param name="triplets"
                                            select="('Emis apr&#232;s encaissement de toutes natures sauf R&#233;gie')"/>
                            <xsl:with-param name="regie"
                                            select="./p:PES_RecetteAller/p:Bordereau[(p:Piece/p:BlocPiece/p:TypPce/@V='05' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='02'  or p:Piece/p:BlocPiece/p:NatPce/@V='03' or p:Piece/p:BlocPiece/p:NatPce/@V='04') ) and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdRegie  ]"/>
                            <xsl:with-param name="inventaire"
                                            select="./p:PES_RecetteAller/p:Bordereau[(p:Piece/p:BlocPiece/p:TypPce/@V='05' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='02'  or p:Piece/p:BlocPiece/p:NatPce/@V='03' or p:Piece/p:BlocPiece/p:NatPce/@V='04') ) and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdActif  ]"/>
                            <xsl:with-param name="convention"
                                            select="./p:PES_RecetteAller/p:Bordereau[(p:Piece/p:BlocPiece/p:TypPce/@V='05' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='02'  or p:Piece/p:BlocPiece/p:NatPce/@V='03' or p:Piece/p:BlocPiece/p:NatPce/@V='04') ) and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdConv  ]"/>
                            <xsl:with-param name="emprunt"
                                            select="./p:PES_RecetteAller/p:Bordereau[(p:Piece/p:BlocPiece/p:TypPce/@V='05' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='02'  or p:Piece/p:BlocPiece/p:NatPce/@V='03' or p:Piece/p:BlocPiece/p:NatPce/@V='04') ) and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdEmpruntOrdo  ]"/>
                            <xsl:with-param name="encaiss"
                                            select="./p:PES_RecetteAller/p:Bordereau[(p:Piece/p:BlocPiece/p:TypPce/@V='05' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='02'  or p:Piece/p:BlocPiece/p:NatPce/@V='03' or p:Piece/p:BlocPiece/p:NatPce/@V='04') ) and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdEncaissement  ]"/>
                            <xsl:with-param name="piecen"
                                            select="./p:PES_RecetteAller/p:Bordereau[(p:Piece/p:BlocPiece/p:TypPce/@V='05' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='02'  or p:Piece/p:BlocPiece/p:NatPce/@V='03' or p:Piece/p:BlocPiece/p:NatPce/@V='04') ) and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:ExerRat/@V=$exercice  ]"/>
                            <xsl:with-param name="piecen-1"
                                            select="./p:PES_RecetteAller/p:Bordereau[(p:Piece/p:BlocPiece/p:TypPce/@V='05' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='02'  or p:Piece/p:BlocPiece/p:NatPce/@V='03' or p:Piece/p:BlocPiece/p:NatPce/@V='04') ) and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:ExerRat/@V=$exercice -1 ]"/>
                        </xsl:call-template>
                        <xsl:call-template name="add-description">
                            <xsl:with-param name="domaine">Recette</xsl:with-param>
                            <xsl:with-param name="sousdomaine">R&#233;gie</xsl:with-param>
                            <xsl:with-param name="list-bord"
                                            select="./p:PES_RecetteAller/p:Bordereau[(p:Piece/p:BlocPiece/p:TypPce/@V='01' or p:Piece/p:BlocPiece/p:TypPce/@V='05') and p:Piece/p:BlocPiece/p:NatPce/@V='05'  ]"/>
                            <xsl:with-param name="nbr"
                                            select="count(./p:PES_RecetteAller/p:Bordereau/p:BlocBordereau/p:IdBord/@V[(../../../p:Piece/p:BlocPiece/p:TypPce/@V='01' or ../../../p:Piece/p:BlocPiece/p:TypPce/@V='05') and ../../../p:Piece/p:BlocPiece/p:NatPce/@V='05'  ])"/>
                            <xsl:with-param name="triplets" select="('Ordinaire ou EAE \ R&#233;gie')"/>
                            <xsl:with-param name="regie"
                                            select="./p:PES_RecetteAller/p:Bordereau[((p:Piece/p:BlocPiece/p:TypPce/@V='01' or p:Piece/p:BlocPiece/p:TypPce/@V='05') and p:Piece/p:BlocPiece/p:NatPce/@V='05' )  and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdRegie ]"/>
                            <xsl:with-param name="inventaire"
                                            select="./p:PES_RecetteAller/p:Bordereau[((p:Piece/p:BlocPiece/p:TypPce/@V='01' or p:Piece/p:BlocPiece/p:TypPce/@V='05') and p:Piece/p:BlocPiece/p:NatPce/@V='05' )  and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdActif ]"/>
                            <xsl:with-param name="convention"
                                            select="./p:PES_RecetteAller/p:Bordereau[((p:Piece/p:BlocPiece/p:TypPce/@V='01' or p:Piece/p:BlocPiece/p:TypPce/@V='05') and p:Piece/p:BlocPiece/p:NatPce/@V='05' )  and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdConv ]"/>
                            <xsl:with-param name="emprunt"
                                            select="./p:PES_RecetteAller/p:Bordereau[((p:Piece/p:BlocPiece/p:TypPce/@V='01' or p:Piece/p:BlocPiece/p:TypPce/@V='05') and p:Piece/p:BlocPiece/p:NatPce/@V='05' )  and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdEmpruntOrdo ]"/>
                            <xsl:with-param name="encaiss"
                                            select="./p:PES_RecetteAller/p:Bordereau[((p:Piece/p:BlocPiece/p:TypPce/@V='01' or p:Piece/p:BlocPiece/p:TypPce/@V='05') and p:Piece/p:BlocPiece/p:NatPce/@V='05' )  and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdEncaissement ]"/>
                            <xsl:with-param name="piecen"
                                            select="./p:PES_RecetteAller/p:Bordereau[((p:Piece/p:BlocPiece/p:TypPce/@V='01' or p:Piece/p:BlocPiece/p:TypPce/@V='05') and p:Piece/p:BlocPiece/p:NatPce/@V='05' )  and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:ExerRat/@V=$exercice ]"/>
                            <xsl:with-param name="piecen-1"
                                            select="./p:PES_RecetteAller/p:Bordereau[((p:Piece/p:BlocPiece/p:TypPce/@V='01' or p:Piece/p:BlocPiece/p:TypPce/@V='05') and p:Piece/p:BlocPiece/p:NatPce/@V='05' )  and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:ExerRat/@V=$exercice -1 ]"/>
                        </xsl:call-template>
                        <xsl:call-template name="add-description">
                            <xsl:with-param name="domaine">Recette</xsl:with-param>
                            <xsl:with-param name="sousdomaine">March&#233;</xsl:with-param>
                            <xsl:with-param name="list-bord"
                                            select="./p:PES_RecetteAller/p:Bordereau[ p:Piece/p:BlocPiece/p:NatPce/@V='11' ]"/>
                            <xsl:with-param name="nbr"
                                            select="count(./p:PES_RecetteAller/p:Bordereau/p:BlocBordereau/p:IdBord/@V[ ../../../p:Piece/p:BlocPiece/p:NatPce/@V='11' ])"/>
                            <xsl:with-param name="triplets" select="('March&#233;')"/>
                            <xsl:with-param name="regie"
                                            select="./p:PES_RecetteAller/p:Bordereau[ p:Piece/p:BlocPiece/p:NatPce/@V='11'  and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdRegie ]"/>
                            <xsl:with-param name="inventaire"
                                            select="./p:PES_RecetteAller/p:Bordereau[ p:Piece/p:BlocPiece/p:NatPce/@V='11'  and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdActif ]"/>
                            <xsl:with-param name="convention"
                                            select="./p:PES_RecetteAller/p:Bordereau[ p:Piece/p:BlocPiece/p:NatPce/@V='11'  and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdConv ]"/>
                            <xsl:with-param name="emprunt"
                                            select="./p:PES_RecetteAller/p:Bordereau[ p:Piece/p:BlocPiece/p:NatPce/@V='11'  and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdEmpruntOrdo ]"/>
                            <xsl:with-param name="encaiss"
                                            select="./p:PES_RecetteAller/p:Bordereau[ p:Piece/p:BlocPiece/p:NatPce/@V='11'  and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdEncaissement ]"/>
                            <xsl:with-param name="piecen"
                                            select="./p:PES_RecetteAller/p:Bordereau[ p:Piece/p:BlocPiece/p:NatPce/@V='11'  and p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:ExerRat/@V=$exercice  ]"/>
                            <xsl:with-param name="piecen-1"
                                            select="./p:PES_RecetteAller/p:Bordereau[ p:Piece/p:BlocPiece/p:NatPce/@V='11'  and p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:ExerRat/@V=$exercice -1  ]"/>
                        </xsl:call-template>
                        <xsl:call-template name="add-description">
                            <xsl:with-param name="domaine">Recette</xsl:with-param>
                            <xsl:with-param name="sousdomaine">Inventaire \Emprunt</xsl:with-param>
                            <xsl:with-param name="list-bord"
                                            select="./p:PES_RecetteAller/p:Bordereau[p:Piece/p:BlocPiece/p:TypPce/@V='01' and (p:Piece/p:BlocPiece/p:NatPce/@V='03' or p:Piece/p:BlocPiece/p:NatPce/@V='04')  ]"/>
                            <xsl:with-param name="nbr"
                                            select="count(./p:PES_RecetteAller/p:Bordereau/p:BlocBordereau/p:IdBord/@V[../../../p:Piece/p:BlocPiece/p:TypPce/@V='01' and (../../../p:Piece/p:BlocPiece/p:NatPce/@V='03' or ../../../p:Piece/p:BlocPiece/p:NatPce/@V='04')  ])"/>
                            <xsl:with-param name="triplets" select="('Ordinaire \ Inventaire','Ordinaire \ Emprunt')"/>
                            <xsl:with-param name="regie"
                                            select="./p:PES_RecetteAller/p:Bordereau[(p:Piece/p:BlocPiece/p:TypPce/@V='01' and (p:Piece/p:BlocPiece/p:NatPce/@V='03' or p:Piece/p:BlocPiece/p:NatPce/@V='04') )  and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdRegie  ]"/>
                            <xsl:with-param name="inventaire"
                                            select="./p:PES_RecetteAller/p:Bordereau[(p:Piece/p:BlocPiece/p:TypPce/@V='01' and (p:Piece/p:BlocPiece/p:NatPce/@V='03' or p:Piece/p:BlocPiece/p:NatPce/@V='04') )  and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdActif  ]"/>
                            <xsl:with-param name="convention"
                                            select="./p:PES_RecetteAller/p:Bordereau[(p:Piece/p:BlocPiece/p:TypPce/@V='01' and (p:Piece/p:BlocPiece/p:NatPce/@V='03' or p:Piece/p:BlocPiece/p:NatPce/@V='04') )  and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdConv  ]"/>
                            <xsl:with-param name="emprunt"
                                            select="./p:PES_RecetteAller/p:Bordereau[(p:Piece/p:BlocPiece/p:TypPce/@V='01' and (p:Piece/p:BlocPiece/p:NatPce/@V='03' or p:Piece/p:BlocPiece/p:NatPce/@V='04') )  and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdEmpruntOrdo  ]"/>
                            <xsl:with-param name="encaiss"
                                            select="./p:PES_RecetteAller/p:Bordereau[(p:Piece/p:BlocPiece/p:TypPce/@V='01' and (p:Piece/p:BlocPiece/p:NatPce/@V='03' or p:Piece/p:BlocPiece/p:NatPce/@V='04') )  and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdEncaissement  ]"/>
                            <xsl:with-param name="piecen"
                                            select="./p:PES_RecetteAller/p:Bordereau[(p:Piece/p:BlocPiece/p:TypPce/@V='01' and (p:Piece/p:BlocPiece/p:NatPce/@V='03' or p:Piece/p:BlocPiece/p:NatPce/@V='04') )  and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:ExerRat/@V=$exercice  ]"/>
                            <xsl:with-param name="piecen-1"
                                            select="./p:PES_RecetteAller/p:Bordereau[(p:Piece/p:BlocPiece/p:TypPce/@V='01' and (p:Piece/p:BlocPiece/p:NatPce/@V='03' or p:Piece/p:BlocPiece/p:NatPce/@V='04') )  and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:ExerRat/@V=$exercice -1 ]"/>
                        </xsl:call-template>
                        <xsl:call-template name="add-description">
                            <xsl:with-param name="domaine">Recette</xsl:with-param>
                            <xsl:with-param name="sousdomaine">R&#233;capitulatif</xsl:with-param>
                            <xsl:with-param name="list-bord"
                                            select="./p:PES_RecetteAller/p:Bordereau[( (p:Piece/p:BlocPiece/p:TypPce/@V='06' or p:Piece/p:BlocPiece/p:TypPce/@V='07')  and p:Piece/p:BlocPiece/p:NatPce/@V='01' ) or (p:Piece/p:BlocPiece/p:TypPce/@V='06'  and p:Piece/p:BlocPiece/p:NatPce/@V='06' ) ]"/>
                            <xsl:with-param name="nbr"
                                            select="count(./p:PES_RecetteAller/p:Bordereau/p:BlocBordereau/p:IdBord/@V[( (../../../p:Piece/p:BlocPiece/p:TypPce/@V='06' or ../../../p:Piece/p:BlocPiece/p:TypPce/@V='07')  and ../../../p:Piece/p:BlocPiece/p:NatPce/@V='01' ) or (../../../p:Piece/p:BlocPiece/p:TypPce/@V='06'  and ../../../p:Piece/p:BlocPiece/p:NatPce/@V='06' ) ])"/>
                            <xsl:with-param name="triplets" select="('R&#233;capitulatif avec ou sans R&#244;le')"/>
                            <xsl:with-param name="regie"
                                            select="./p:PES_RecetteAller/p:Bordereau[(( (p:Piece/p:BlocPiece/p:TypPce/@V='06' or p:Piece/p:BlocPiece/p:TypPce/@V='07')  and p:Piece/p:BlocPiece/p:NatPce/@V='01' ) or (p:Piece/p:BlocPiece/p:TypPce/@V='06'  and p:Piece/p:BlocPiece/p:NatPce/@V='06' ) )  and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdRegie ]"/>
                            <xsl:with-param name="inventaire"
                                            select="./p:PES_RecetteAller/p:Bordereau[(( (p:Piece/p:BlocPiece/p:TypPce/@V='06' or p:Piece/p:BlocPiece/p:TypPce/@V='07')  and p:Piece/p:BlocPiece/p:NatPce/@V='01' ) or (p:Piece/p:BlocPiece/p:TypPce/@V='06'  and p:Piece/p:BlocPiece/p:NatPce/@V='06' ) )  and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdActif ]"/>
                            <xsl:with-param name="convention"
                                            select="./p:PES_RecetteAller/p:Bordereau[(( (p:Piece/p:BlocPiece/p:TypPce/@V='06' or p:Piece/p:BlocPiece/p:TypPce/@V='07')  and p:Piece/p:BlocPiece/p:NatPce/@V='01' ) or (p:Piece/p:BlocPiece/p:TypPce/@V='06'  and p:Piece/p:BlocPiece/p:NatPce/@V='06' ) )  and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdConv ]"/>
                            <xsl:with-param name="emprunt"
                                            select="./p:PES_RecetteAller/p:Bordereau[(( (p:Piece/p:BlocPiece/p:TypPce/@V='06' or p:Piece/p:BlocPiece/p:TypPce/@V='07')  and p:Piece/p:BlocPiece/p:NatPce/@V='01' ) or (p:Piece/p:BlocPiece/p:TypPce/@V='06'  and p:Piece/p:BlocPiece/p:NatPce/@V='06' ) )  and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdEmpruntOrdo ]"/>
                            <xsl:with-param name="encaiss"
                                            select="./p:PES_RecetteAller/p:Bordereau[(( (p:Piece/p:BlocPiece/p:TypPce/@V='06' or p:Piece/p:BlocPiece/p:TypPce/@V='07')  and p:Piece/p:BlocPiece/p:NatPce/@V='01' ) or (p:Piece/p:BlocPiece/p:TypPce/@V='06'  and p:Piece/p:BlocPiece/p:NatPce/@V='06' ) )  and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdEncaissement ]"/>
                            <xsl:with-param name="piecen"
                                            select="./p:PES_RecetteAller/p:Bordereau[(( (p:Piece/p:BlocPiece/p:TypPce/@V='06' or p:Piece/p:BlocPiece/p:TypPce/@V='07')  and p:Piece/p:BlocPiece/p:NatPce/@V='01' ) or (p:Piece/p:BlocPiece/p:TypPce/@V='06'  and p:Piece/p:BlocPiece/p:NatPce/@V='06' ) )  and p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:ExerRat/@V=$exercice ]"/>
                            <xsl:with-param name="piecen-1"
                                            select="./p:PES_RecetteAller/p:Bordereau[(( (p:Piece/p:BlocPiece/p:TypPce/@V='06' or p:Piece/p:BlocPiece/p:TypPce/@V='07')  and p:Piece/p:BlocPiece/p:NatPce/@V='01' ) or (p:Piece/p:BlocPiece/p:TypPce/@V='06'  and p:Piece/p:BlocPiece/p:NatPce/@V='06' ) )  and p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:ExerRat/@V=$exercice -1 ]"/>
                        </xsl:call-template>
                        <xsl:call-template name="add-description">
                            <xsl:with-param name="domaine">Recette</xsl:with-param>
                            <xsl:with-param name="sousdomaine">Majoration</xsl:with-param>
                            <xsl:with-param name="list-bord"
                                            select="./p:PES_RecetteAller/p:Bordereau[p:Piece/p:BlocPiece/p:TypPce/@V='09' and (p:Piece/p:BlocPiece/p:NatPce/@V='01')  ]"/>
                            <xsl:with-param name="nbr"
                                            select="count(./p:PES_RecetteAller/p:Bordereau/p:BlocBordereau/p:IdBord/@V[../../../p:Piece/p:BlocPiece/p:TypPce/@V='09' and (../../../p:Piece/p:BlocPiece/p:NatPce/@V='01')  ])"/>
                            <xsl:with-param name="triplets" select="('Majoration \ Fonctionnement')"/>
                            <xsl:with-param name="regie"
                                            select="./p:PES_RecetteAller/p:Bordereau[(p:Piece/p:BlocPiece/p:TypPce/@V='09' and (p:Piece/p:BlocPiece/p:NatPce/@V='01')  )  and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdRegie ]"/>
                            <xsl:with-param name="inventaire"
                                            select="./p:PES_RecetteAller/p:Bordereau[(p:Piece/p:BlocPiece/p:TypPce/@V='09' and (p:Piece/p:BlocPiece/p:NatPce/@V='01')  )  and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdActif ]"/>
                            <xsl:with-param name="convention"
                                            select="./p:PES_RecetteAller/p:Bordereau[(p:Piece/p:BlocPiece/p:TypPce/@V='09' and (p:Piece/p:BlocPiece/p:NatPce/@V='01')  )  and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdConv ]"/>
                            <xsl:with-param name="emprunt"
                                            select="./p:PES_RecetteAller/p:Bordereau[(p:Piece/p:BlocPiece/p:TypPce/@V='09' and (p:Piece/p:BlocPiece/p:NatPce/@V='01')  )  and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdEmpruntOrdo ]"/>
                            <xsl:with-param name="encaiss"
                                            select="./p:PES_RecetteAller/p:Bordereau[(p:Piece/p:BlocPiece/p:TypPce/@V='09' and (p:Piece/p:BlocPiece/p:NatPce/@V='01')  )  and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdEncaissement ]"/>
                            <xsl:with-param name="piecen"
                                            select="./p:PES_RecetteAller/p:Bordereau[(p:Piece/p:BlocPiece/p:TypPce/@V='09' and (p:Piece/p:BlocPiece/p:NatPce/@V='01')  )  and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:ExerRat/@V=$exercice  ]"/>
                            <xsl:with-param name="piecen-1"
                                            select="./p:PES_RecetteAller/p:Bordereau[(p:Piece/p:BlocPiece/p:TypPce/@V='09' and (p:Piece/p:BlocPiece/p:NatPce/@V='01')  )  and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:ExerRat/@V=$exercice -1  ]"/>
                        </xsl:call-template>
                        <xsl:call-template name="add-description">
                            <xsl:with-param name="domaine">Recette</xsl:with-param>
                            <xsl:with-param name="sousdomaine">Rattachement</xsl:with-param>
                            <xsl:with-param name="list-bord"
                                            select="./p:PES_RecetteAller/p:Bordereau[p:Piece/p:BlocPiece/p:TypPce/@V='11' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:NatPce/@V='04')  ]"/>
                            <xsl:with-param name="nbr"
                                            select="count(./p:PES_RecetteAller/p:Bordereau/p:BlocBordereau/p:IdBord/@V[../../../p:Piece/p:BlocPiece/p:TypPce/@V='11' and (../../../p:Piece/p:BlocPiece/p:NatPce/@V='01' or ../../../p:Piece/p:BlocPiece/p:NatPce/@V='02' or ../../../p:Piece/p:BlocPiece/p:NatPce/@V='04')  ])"/>
                            <xsl:with-param name="triplets" select="('Rattachement de toutes natures')"/>
                            <xsl:with-param name="regie"
                                            select="./p:PES_RecetteAller/p:Bordereau[(p:Piece/p:BlocPiece/p:TypPce/@V='11' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:NatPce/@V='04')) and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdRegie ]"/>
                            <xsl:with-param name="inventaire"
                                            select="./p:PES_RecetteAller/p:Bordereau[(p:Piece/p:BlocPiece/p:TypPce/@V='11' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:NatPce/@V='04')) and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdActif ]"/>
                            <xsl:with-param name="convention"
                                            select="./p:PES_RecetteAller/p:Bordereau[(p:Piece/p:BlocPiece/p:TypPce/@V='11' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:NatPce/@V='04')) and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdConv ]"/>
                            <xsl:with-param name="emprunt"
                                            select="./p:PES_RecetteAller/p:Bordereau[(p:Piece/p:BlocPiece/p:TypPce/@V='11' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:NatPce/@V='04')) and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdEmpruntOrdo ]"/>
                            <xsl:with-param name="encaiss"
                                            select="./p:PES_RecetteAller/p:Bordereau[(p:Piece/p:BlocPiece/p:TypPce/@V='11' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:NatPce/@V='04')) and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdEncaissement ]"/>
                            <xsl:with-param name="piecen"
                                            select="./p:PES_RecetteAller/p:Bordereau[(p:Piece/p:BlocPiece/p:TypPce/@V='11' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:NatPce/@V='04')) and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:ExerRat/@V=$exercice  ]"/>
                            <xsl:with-param name="piecen-1"
                                            select="./p:PES_RecetteAller/p:Bordereau[(p:Piece/p:BlocPiece/p:TypPce/@V='11' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:NatPce/@V='04')) and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:ExerRat/@V=$exercice  - 1]"/>
                        </xsl:call-template>
                        <xsl:call-template name="add-description">
                            <xsl:with-param name="domaine">Recette</xsl:with-param>
                            <xsl:with-param name="sousdomaine">Fin d'exercice</xsl:with-param>
                            <xsl:with-param name="list-bord"
                                            select="./p:PES_RecetteAller/p:Bordereau[ (p:Piece/p:BlocPiece/p:TypPce/@V='03' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:NatPce/@V='03' or p:Piece/p:BlocPiece/p:NatPce/@V='04' or p:Piece/p:BlocPiece/p:NatPce/@V='18')) or (p:Piece/p:BlocPiece/p:TypPce/@V='04' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:NatPce/@V='03' or p:Piece/p:BlocPiece/p:NatPce/@V='04' or p:Piece/p:BlocPiece/p:NatPce/@V='06')) or (p:Piece/p:BlocPiece/p:TypPce/@V='14' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='06' ))  or (p:Piece/p:BlocPiece/p:TypPce/@V='01' and (p:Piece/p:BlocPiece/p:NatPce/@V='18' )) ]"/>
                            <xsl:with-param name="nbr"
                                            select="count(./p:PES_RecetteAller/p:Bordereau/p:BlocBordereau/p:IdBord/@V[ (../../../p:Piece/p:BlocPiece/p:TypPce/@V='03' and (../../../p:Piece/p:BlocPiece/p:NatPce/@V='01' or ../../../p:Piece/p:BlocPiece/p:NatPce/@V='02' or ../../../p:Piece/p:BlocPiece/p:NatPce/@V='03' or ../../../p:Piece/p:BlocPiece/p:NatPce/@V='04' or ../../../p:Piece/p:BlocPiece/p:NatPce/@V='18')) or (../../../p:Piece/p:BlocPiece/p:TypPce/@V='04' and (../../../p:Piece/p:BlocPiece/p:NatPce/@V='01' or ../../../p:Piece/p:BlocPiece/p:NatPce/@V='02' or ../../../p:Piece/p:BlocPiece/p:NatPce/@V='03' or ../../../p:Piece/p:BlocPiece/p:NatPce/@V='04' or ../../../p:Piece/p:BlocPiece/p:NatPce/@V='06')) or (../../../p:Piece/p:BlocPiece/p:TypPce/@V='14' and (../../../p:Piece/p:BlocPiece/p:NatPce/@V='01' or ../../../p:Piece/p:BlocPiece/p:NatPce/@V='06' ))  or (../../../p:Piece/p:BlocPiece/p:TypPce/@V='01' and (../../../p:Piece/p:BlocPiece/p:NatPce/@V='18' )) ])"/>
                            <xsl:with-param name="triplets" select="()"/>
                            <xsl:with-param name="regie"
                                            select="./p:PES_RecetteAller/p:Bordereau[( (p:Piece/p:BlocPiece/p:TypPce/@V='03' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:NatPce/@V='03' or p:Piece/p:BlocPiece/p:NatPce/@V='04' or p:Piece/p:BlocPiece/p:NatPce/@V='18')) or (p:Piece/p:BlocPiece/p:TypPce/@V='04' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:NatPce/@V='03' or p:Piece/p:BlocPiece/p:NatPce/@V='04' or p:Piece/p:BlocPiece/p:NatPce/@V='06')) or (p:Piece/p:BlocPiece/p:TypPce/@V='14' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='06' ))  or (p:Piece/p:BlocPiece/p:TypPce/@V='01' and (p:Piece/p:BlocPiece/p:NatPce/@V='18' )))  and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdRegie  ]"/>
                            <xsl:with-param name="inventaire"
                                            select="./p:PES_RecetteAller/p:Bordereau[( (p:Piece/p:BlocPiece/p:TypPce/@V='03' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:NatPce/@V='03' or p:Piece/p:BlocPiece/p:NatPce/@V='04' or p:Piece/p:BlocPiece/p:NatPce/@V='18')) or (p:Piece/p:BlocPiece/p:TypPce/@V='04' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:NatPce/@V='03' or p:Piece/p:BlocPiece/p:NatPce/@V='04' or p:Piece/p:BlocPiece/p:NatPce/@V='06')) or (p:Piece/p:BlocPiece/p:TypPce/@V='14' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='06' ))  or (p:Piece/p:BlocPiece/p:TypPce/@V='01' and (p:Piece/p:BlocPiece/p:NatPce/@V='18' )))  and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdActif  ]"/>
                            <xsl:with-param name="convention"
                                            select="./p:PES_RecetteAller/p:Bordereau[( (p:Piece/p:BlocPiece/p:TypPce/@V='03' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:NatPce/@V='03' or p:Piece/p:BlocPiece/p:NatPce/@V='04' or p:Piece/p:BlocPiece/p:NatPce/@V='18')) or (p:Piece/p:BlocPiece/p:TypPce/@V='04' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:NatPce/@V='03' or p:Piece/p:BlocPiece/p:NatPce/@V='04' or p:Piece/p:BlocPiece/p:NatPce/@V='06')) or (p:Piece/p:BlocPiece/p:TypPce/@V='14' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='06' ))  or (p:Piece/p:BlocPiece/p:TypPce/@V='01' and (p:Piece/p:BlocPiece/p:NatPce/@V='18' )))  and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdConv ]"/>
                            <xsl:with-param name="emprunt"
                                            select="./p:PES_RecetteAller/p:Bordereau[( (p:Piece/p:BlocPiece/p:TypPce/@V='03' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:NatPce/@V='03' or p:Piece/p:BlocPiece/p:NatPce/@V='04' or p:Piece/p:BlocPiece/p:NatPce/@V='18')) or (p:Piece/p:BlocPiece/p:TypPce/@V='04' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:NatPce/@V='03' or p:Piece/p:BlocPiece/p:NatPce/@V='04' or p:Piece/p:BlocPiece/p:NatPce/@V='06')) or (p:Piece/p:BlocPiece/p:TypPce/@V='14' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='06' ))  or (p:Piece/p:BlocPiece/p:TypPce/@V='01' and (p:Piece/p:BlocPiece/p:NatPce/@V='18' )))  and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdEmpruntOrdo ]"/>
                            <xsl:with-param name="encaiss"
                                            select="./p:PES_RecetteAller/p:Bordereau[( (p:Piece/p:BlocPiece/p:TypPce/@V='03' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:NatPce/@V='03' or p:Piece/p:BlocPiece/p:NatPce/@V='04' or p:Piece/p:BlocPiece/p:NatPce/@V='18')) or (p:Piece/p:BlocPiece/p:TypPce/@V='04' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:NatPce/@V='03' or p:Piece/p:BlocPiece/p:NatPce/@V='04' or p:Piece/p:BlocPiece/p:NatPce/@V='06')) or (p:Piece/p:BlocPiece/p:TypPce/@V='14' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='06' ))  or (p:Piece/p:BlocPiece/p:TypPce/@V='01' and (p:Piece/p:BlocPiece/p:NatPce/@V='18' )))  and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdEncaissement ]"/>
                            <xsl:with-param name="piecen"
                                            select="./p:PES_RecetteAller/p:Bordereau[( (p:Piece/p:BlocPiece/p:TypPce/@V='03' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:NatPce/@V='03' or p:Piece/p:BlocPiece/p:NatPce/@V='04' or p:Piece/p:BlocPiece/p:NatPce/@V='18')) or (p:Piece/p:BlocPiece/p:TypPce/@V='04' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:NatPce/@V='03' or p:Piece/p:BlocPiece/p:NatPce/@V='04' or p:Piece/p:BlocPiece/p:NatPce/@V='06')) or (p:Piece/p:BlocPiece/p:TypPce/@V='14' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='06' ))  or (p:Piece/p:BlocPiece/p:TypPce/@V='01' and (p:Piece/p:BlocPiece/p:NatPce/@V='18' ))) and p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:ExerRat/@V=$exercice ]"/>
                            <xsl:with-param name="piecen-1"
                                            select="./p:PES_RecetteAller/p:Bordereau[( (p:Piece/p:BlocPiece/p:TypPce/@V='03' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:NatPce/@V='03' or p:Piece/p:BlocPiece/p:NatPce/@V='04' or p:Piece/p:BlocPiece/p:NatPce/@V='18')) or (p:Piece/p:BlocPiece/p:TypPce/@V='04' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:NatPce/@V='03' or p:Piece/p:BlocPiece/p:NatPce/@V='04' or p:Piece/p:BlocPiece/p:NatPce/@V='06')) or (p:Piece/p:BlocPiece/p:TypPce/@V='14' and (p:Piece/p:BlocPiece/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:NatPce/@V='06' ))  or (p:Piece/p:BlocPiece/p:TypPce/@V='01' and (p:Piece/p:BlocPiece/p:NatPce/@V='18' ))) and p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:ExerRat/@V=$exercice -1]"/>
                        </xsl:call-template>
                        <xsl:call-template name="add-description">
                            <xsl:with-param name="domaine">D&#233;pense</xsl:with-param>
                            <xsl:with-param name="sousdomaine">Ordinaire</xsl:with-param>
                            <xsl:with-param name="list-bord"
                                            select="./p:PES_DepenseAller/p:Bordereau[p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='01' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02')  ]"/>
                            <xsl:with-param name="nbr"
                                            select="count(./p:PES_DepenseAller/p:Bordereau/p:BlocBordereau/p:IdBord/@V[../../../p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='01' and (../../../p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or ../../../p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02') ])"/>
                            <xsl:with-param name="triplets" select="()"/>
                            <xsl:with-param name="regie"
                                            select="./p:PES_DepenseAller/p:Bordereau[p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='01' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02') and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdRegie  ]"/>
                            <xsl:with-param name="inventaire"
                                            select="./p:PES_DepenseAller/p:Bordereau[p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='01' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02') and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdActif  ]"/>
                            <xsl:with-param name="convention"
                                            select="./p:PES_DepenseAller/p:Bordereau[p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='01' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02') and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdConv  ]"/>
                            <xsl:with-param name="emprunt"
                                            select="./p:PES_DepenseAller/p:Bordereau[p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='01' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02') and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdEmpruntOrdo  ]"/>
                            <xsl:with-param name="encaiss"
                                            select="./p:PES_DepenseAller/p:Bordereau[p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='01' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02') and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:NatPceOrig/@V ='03'  ]"/>
                            <xsl:with-param name="piecen"
                                            select="./p:PES_DepenseAller/p:Bordereau[p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='01' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02') and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:ExerRat/@V=$exercice   ]"/>
                            <xsl:with-param name="piecen-1"
                                            select="./p:PES_DepenseAller/p:Bordereau[p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='01' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02') and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:ExerRat/@V=$exercice  - 1 ]"/>
                        </xsl:call-template>
                        <xsl:call-template name="add-description">
                            <xsl:with-param name="domaine">D&#233;pense</xsl:with-param>
                            <xsl:with-param name="sousdomaine">Correction \Annulation</xsl:with-param>
                            <xsl:with-param name="list-bord"
                                            select="./p:PES_DepenseAller/p:Bordereau[p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='02' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='06'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='09'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='10')  ]"/>
                            <xsl:with-param name="nbr"
                                            select="count(./p:PES_DepenseAller/p:Bordereau/p:BlocBordereau/p:IdBord/@V[../../../p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='02' and (../../../p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02' or ../../../p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='06'  or ../../../p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='09'  or ../../../p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='10')] )"/>
                            <xsl:with-param name="triplets"
                                            select="('Correctif \ Annulation\R&#233;duction annulant un titre, Annulation du mandat de rattachement')"/>
                            <xsl:with-param name="regie"
                                            select="./p:PES_DepenseAller/p:Bordereau[p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='02' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='06'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='09'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='10') and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdRegie  ]"/>
                            <xsl:with-param name="inventaire"
                                            select="./p:PES_DepenseAller/p:Bordereau[p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='02' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='06'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='09'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='10') and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdActif  ]"/>
                            <xsl:with-param name="convention"
                                            select="./p:PES_DepenseAller/p:Bordereau[p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='02' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='06'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='09'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='10') and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdConv  ]"/>
                            <xsl:with-param name="emprunt"
                                            select="./p:PES_DepenseAller/p:Bordereau[p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='02' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='06'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='09'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='10') and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdEmpruntOrdo  ]"/>
                            <xsl:with-param name="encaiss"
                                            select="./p:PES_DepenseAller/p:Bordereau[p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='02' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='06'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='09'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='10') and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:NatPceOrig/@V ='03'  ]"/>
                            <xsl:with-param name="piecen"
                                            select="./p:PES_DepenseAller/p:Bordereau[p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='02' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='06'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='09'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='10') and    p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:ExerRat/@V=$exercice  ]"/>
                            <xsl:with-param name="piecen-1"
                                            select="./p:PES_DepenseAller/p:Bordereau[p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='02' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='06'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='09'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='10') and    p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:ExerRat/@V=$exercice -1 ]"/>
                        </xsl:call-template>
                        <xsl:call-template name="add-description">
                            <xsl:with-param name="domaine">D&#233;pense</xsl:with-param>
                            <xsl:with-param name="sousdomaine">EAP</xsl:with-param>
                            <xsl:with-param name="list-bord"
                                            select="./p:PES_DepenseAller/p:Bordereau[p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='05' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='03'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04')  ]"/>
                            <xsl:with-param name="nbr"
                                            select="count(./p:PES_DepenseAller/p:Bordereau/p:BlocBordereau/p:IdBord/@V[../../../p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='05' and (../../../p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or ../../../p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02'  or ../../../p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='03'  or ../../../p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04')  ])"/>
                            <xsl:with-param name="triplets"
                                            select="('Emis apr&#232;s paiement de toutes natures sauf R&#233;gie')"/>
                            <xsl:with-param name="regie"
                                            select="./p:PES_DepenseAller/p:Bordereau[p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='05' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='03'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04') and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdRegie  ]"/>
                            <xsl:with-param name="inventaire"
                                            select="./p:PES_DepenseAller/p:Bordereau[p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='05' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='03'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04') and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdActif  ]"/>
                            <xsl:with-param name="convention"
                                            select="./p:PES_DepenseAller/p:Bordereau[p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='05' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='03'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04') and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdConv  ]"/>
                            <xsl:with-param name="emprunt"
                                            select="./p:PES_DepenseAller/p:Bordereau[p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='05' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='03'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04') and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdEmpruntOrdo  ]"/>
                            <xsl:with-param name="encaiss"
                                            select="./p:PES_DepenseAller/p:Bordereau[p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='05' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='03'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04') and    p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:NatPceOrig/@V ='03'    ]"/>
                            <xsl:with-param name="piecen"
                                            select="./p:PES_DepenseAller/p:Bordereau[p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='05' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='03'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04') and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:ExerRat/@V=$exercice     ]"/>
                            <xsl:with-param name="piecen-1"
                                            select="./p:PES_DepenseAller/p:Bordereau[p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='05' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='03'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04') and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:ExerRat/@V=$exercice  -1   ]"/>
                        </xsl:call-template>
                        <xsl:call-template name="add-description">
                            <xsl:with-param name="domaine">D&#233;pense</xsl:with-param>
                            <xsl:with-param name="sousdomaine">Fin d'exercice</xsl:with-param>
                            <xsl:with-param name="list-bord"
                                            select="./p:PES_DepenseAller/p:Bordereau[ ( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='03' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='03'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04'   or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='18') ) or  ( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='04' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='03'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04'   or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='06') )  or ( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='13' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='06') )]"/>
                            <xsl:with-param name="nbr"
                                            select="count(./p:PES_DepenseAller/p:Bordereau/p:BlocBordereau/p:IdBord/@V[ ( ../../../p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='03' and (../../../p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or ../../../p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02'  or ../../../p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='03'  or ../../../p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04'   or ../../../p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='18') ) or  ( ../../../p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='04' and (../../../p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or ../../../p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02'  or ../../../p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='03'  or ../../../p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04'   or ../../../p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='06') )  or ( ../../../p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='13' and (../../../p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or ../../../p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='06') )])"/>
                            <xsl:with-param name="triplets"
                                            select="('Ordre budg&#233;taire, Ordre mixte, Charge constat&#233;e d&#39;&#39;avance de toutes natures')"/>
                            <xsl:with-param name="regie"
                                            select="./p:PES_DepenseAller/p:Bordereau[ ( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='03' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='03'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04'   or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='18') ) or  ( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='04' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='03'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04'   or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='06') )  or ( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='13' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='06') ) and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdRegie ]"/>
                            <xsl:with-param name="inventaire"
                                            select="./p:PES_DepenseAller/p:Bordereau[ ( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='03' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='03'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04'   or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='18') ) or  ( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='04' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='03'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04'   or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='06') )  or ( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='13' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='06') ) and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdActif ]"/>
                            <xsl:with-param name="convention"
                                            select="./p:PES_DepenseAller/p:Bordereau[ ( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='03' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='03'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04'   or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='18') ) or  ( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='04' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='03'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04'   or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='06') )  or ( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='13' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='06') ) and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdConv ]"/>
                            <xsl:with-param name="emprunt"
                                            select="./p:PES_DepenseAller/p:Bordereau[ ( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='03' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='03'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04'   or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='18') ) or  ( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='04' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='03'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04'   or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='06') )  or ( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='13' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='06') ) and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdEmpruntOrdo ]"/>
                            <xsl:with-param name="encaiss"
                                            select="./p:PES_DepenseAller/p:Bordereau[ ( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='03' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='03'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04'   or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='18') ) or  ( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='04' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='03'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04'   or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='06') )  or ( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='13' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='06') ) and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:NatPceOrig/@V ='03'  ]"/>
                            <xsl:with-param name="piecen"
                                            select="./p:PES_DepenseAller/p:Bordereau[ ( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='03' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='03'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04'   or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='18') ) or  ( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='04' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='03'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04'   or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='06') )  or ( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='13' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='06') )  and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:ExerRat/@V=$exercice  ]"/>
                            <xsl:with-param name="piecen-1"
                                            select="./p:PES_DepenseAller/p:Bordereau[ ( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='03' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='03'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04'   or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='18') ) or  ( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='04' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='03'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04'   or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='06') )  or ( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='13' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='06') )  and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:ExerRat/@V=$exercice -1 ]"/>
                        </xsl:call-template>
                        <xsl:call-template name="add-description">
                            <xsl:with-param name="domaine">D&#233;pense</xsl:with-param>
                            <xsl:with-param name="sousdomaine">Collectif</xsl:with-param>
                            <xsl:with-param name="list-bord"
                                            select="./p:PES_DepenseAller/p:Bordereau[p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='08' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02')  ]"/>
                            <xsl:with-param name="nbr"
                                            select="count(./p:PES_DepenseAller/p:Bordereau/p:BlocBordereau/p:IdBord/@V[../../../p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='08' and (../../../p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or ../../../p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02')  ])"/>
                            <xsl:with-param name="triplets" select="('Collectif de toutes natures sauf Paie')"/>
                            <xsl:with-param name="regie"
                                            select="./p:PES_DepenseAller/p:Bordereau[p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='08' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02')  and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdRegie  ]"/>
                            <xsl:with-param name="inventaire"
                                            select="./p:PES_DepenseAller/p:Bordereau[p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='08' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02')  and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdActif ]"/>
                            <xsl:with-param name="convention"
                                            select="./p:PES_DepenseAller/p:Bordereau[p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='08' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02')  and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdConv ]"/>
                            <xsl:with-param name="emprunt"
                                            select="./p:PES_DepenseAller/p:Bordereau[p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='08' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02')  and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdEmpruntOrdo ]"/>
                            <xsl:with-param name="encaiss"
                                            select="./p:PES_DepenseAller/p:Bordereau[p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='08' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02')  and    p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:NatPceOrig/@V ='03' ]"/>
                            <xsl:with-param name="piecen"
                                            select="./p:PES_DepenseAller/p:Bordereau[p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='08' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02')  and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:ExerRat/@V=$exercice ]"/>
                            <xsl:with-param name="piecen-1"
                                            select="./p:PES_DepenseAller/p:Bordereau[p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='08' and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02')  and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:ExerRat/@V=$exercice -1 ]"/>
                        </xsl:call-template>
                        <xsl:call-template name="add-description">
                            <xsl:with-param name="domaine">D&#233;pense</xsl:with-param>
                            <xsl:with-param name="sousdomaine">Paie</xsl:with-param>
                            <xsl:with-param name="list-bord"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='01'or p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='08') and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='11' )  ]"/>
                            <xsl:with-param name="nbr"
                                            select="count(./p:PES_DepenseAller/p:Bordereau/p:BlocBordereau/p:IdBord/@V[( ../../../p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='01'or ../../../p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='08') and (../../../p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='11' )  ])"/>
                            <xsl:with-param name="triplets" select="()"/>
                            <xsl:with-param name="regie"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='01'or p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='08') and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='11' )  and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdRegie ]"/>
                            <xsl:with-param name="inventaire"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='01'or p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='08') and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='11' )  and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdActif ]"/>
                            <xsl:with-param name="convention"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='01'or p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='08') and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='11' )  and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdConv ]"/>
                            <xsl:with-param name="emprunt"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='01'or p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='08') and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='11' )  and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdEmpruntOrdo]"/>
                            <xsl:with-param name="encaiss"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='01'or p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='08') and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='11' )  and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:NatPceOrig/@V ='03' ]"/>
                            <xsl:with-param name="piecen"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='01'or p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='08') and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='11' )  and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:ExerRat/@V=$exercice  ]"/>
                            <xsl:with-param name="piecen-1"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='01'or p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='08') and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='11' )  and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:ExerRat/@V=$exercice -1 ]"/>
                        </xsl:call-template>
                        <xsl:call-template name="add-description">
                            <xsl:with-param name="domaine">D&#233;pense</xsl:with-param>
                            <xsl:with-param name="sousdomaine">Inventaire \Emprunt</xsl:with-param>
                            <xsl:with-param name="list-bord"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='01' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='03'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04')  ]"/>
                            <xsl:with-param name="nbr"
                                            select="count(./p:PES_DepenseAller/p:Bordereau/p:BlocBordereau/p:IdBord/@V[( ../../../p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='01' ) and (../../../p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='03'  or ../../../p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04')  ])"/>
                            <xsl:with-param name="triplets" select="('Ordinaire \ Inventaire','Ordinaire \ emprunt')"/>
                            <xsl:with-param name="regie"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='01' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='03'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04')   and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdRegie  ]"/>
                            <xsl:with-param name="inventaire"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='01' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='03'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04')   and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdActif  ]"/>
                            <xsl:with-param name="convention"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='01' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='03'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04')   and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdConv  ]"/>
                            <xsl:with-param name="emprunt"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='01' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='03'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04')   and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdEmpruntOrdo  ]"/>
                            <xsl:with-param name="encaiss"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='01' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='03'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04')   and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:NatPceOrig/@V ='03'  ]"/>
                            <xsl:with-param name="piecen"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='01' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='03'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04')   and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:ExerRat/@V=$exercice   ]"/>
                            <xsl:with-param name="piecen-1"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='01' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='03'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04')   and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:ExerRat/@V=$exercice  -1  ]"/>
                        </xsl:call-template>
                        <xsl:call-template name="add-description">
                            <xsl:with-param name="domaine">D&#233;pense</xsl:with-param>
                            <xsl:with-param name="sousdomaine">R&#233;gie</xsl:with-param>
                            <xsl:with-param name="list-bord"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='05' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='05')  ]"/>
                            <xsl:with-param name="nbr"
                                            select="count(./p:PES_DepenseAller/p:Bordereau/p:BlocBordereau/p:IdBord/@V[( ../../../p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='01' or ../../../p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='05' ) and (../../../p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='05')  ])"/>
                            <xsl:with-param name="triplets" select="('Ordinaire \ R&#233;gie','EAP \ R&#233;gie')"/>
                            <xsl:with-param name="regie"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='05' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='05')  and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdRegie  ]"/>
                            <xsl:with-param name="inventaire"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='05' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='05')  and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdActif  ]"/>
                            <xsl:with-param name="convention"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='05' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='05')  and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdConv  ]"/>
                            <xsl:with-param name="emprunt"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='05' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='05')  and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdEmpruntOrdo ]"/>
                            <xsl:with-param name="encaiss"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='05' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='05')  and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:NatPceOrig/@V ='03' ]"/>
                            <xsl:with-param name="piecen"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='05' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='05')  and p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:ExerRat/@V=$exercice  ]"/>
                            <xsl:with-param name="piecen-1"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='05' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='05')  and p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:ExerRat/@V=$exercice -1 ]"/>
                        </xsl:call-template>
                        <xsl:call-template name="add-description">
                            <xsl:with-param name="domaine">D&#233;pense</xsl:with-param>
                            <xsl:with-param name="sousdomaine">March&#233;</xsl:with-param>
                            <xsl:with-param name="list-bord"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='09' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='12' )  ]"/>
                            <xsl:with-param name="nbr"
                                            select="count(./p:PES_DepenseAller/p:Bordereau/p:BlocBordereau/p:IdBord/@V[( ../../../p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='09' ) and (../../../p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or ../../../p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02' or ../../../p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='12' )  ])"/>
                            <xsl:with-param name="triplets" select="('Mandats de march&#233; de toutes natures')"/>
                            <xsl:with-param name="regie"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='09' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='12' )   and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdRegie  ]"/>
                            <xsl:with-param name="inventaire"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='09' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='12' )   and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdActif  ]"/>
                            <xsl:with-param name="convention"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='09' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='12' )   and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdConv ]"/>
                            <xsl:with-param name="emprunt"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='09' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='12' )   and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdEmpruntOrdo ]"/>
                            <xsl:with-param name="encaiss"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='09' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='12' )   and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:NatPceOrig/@V ='03']"/>
                            <xsl:with-param name="piecen"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='09' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='12' )   and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:ExerRat/@V=$exercice  ]"/>
                            <xsl:with-param name="piecen-1"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='09' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='12' )   and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:ExerRat/@V=$exercice -1 ]"/>
                        </xsl:call-template>
                        <xsl:call-template name="add-description">
                            <xsl:with-param name="domaine">D&#233;pense</xsl:with-param>
                            <xsl:with-param name="sousdomaine">Rattachement</xsl:with-param>
                            <xsl:with-param name="list-bord"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='10' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='03'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='05' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='11')  ]"/>
                            <xsl:with-param name="nbr"
                                            select="count(./p:PES_DepenseAller/p:Bordereau/p:BlocBordereau/p:IdBord/@V[( ../../../p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='10' ) and (../../../p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or ../../../p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02' or ../../../p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='03'  or ../../../p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04' or ../../../p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='05' or ../../../p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='11')  ])"/>
                            <xsl:with-param name="triplets" select="('Mandats de rattachement de toutes natures')"/>
                            <xsl:with-param name="regie"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='10' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='03'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='05' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='11')   and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdRegie  ]"/>
                            <xsl:with-param name="inventaire"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='10' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='03'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='05' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='11')   and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdActif ]"/>
                            <xsl:with-param name="convention"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='10' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='03'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='05' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='11')   and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdConv ]"/>
                            <xsl:with-param name="emprunt"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='10' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='03'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='05' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='11')   and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdEmpruntOrdo ]"/>
                            <xsl:with-param name="encaiss"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='10' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='03'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='05' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='11')   and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:NatPceOrig/@V ='03' ]"/>
                            <xsl:with-param name="piecen"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='10' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='03'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='05' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='11')   and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:ExerRat/@V=$exercice  ]"/>
                            <xsl:with-param name="piecen-1"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='10' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='02' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='03'  or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='05' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='11')   and  p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:ExerRat/@V=$exercice -1 ]"/>
                        </xsl:call-template>
                        <xsl:call-template name="add-description">
                            <xsl:with-param name="domaine">D&#233;pense</xsl:with-param>
                            <xsl:with-param name="sousdomaine">Admission en non valeur</xsl:with-param>
                            <xsl:with-param name="list-bord"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='07' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' )  ]"/>
                            <xsl:with-param name="nbr"
                                            select="count(./p:PES_DepenseAller/p:Bordereau/p:BlocBordereau/p:IdBord/@V[( ../../../p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='07' ) and (../../../p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' )])"/>
                            <xsl:with-param name="triplets"
                                            select="('Mandats d&#39;&#39;admission en non valeur de toutes nature')"/>
                            <xsl:with-param name="regie"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='07' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' )   and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdRegie  ]"/>
                            <xsl:with-param name="inventaire"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='07' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' )   and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdActif  ]"/>
                            <xsl:with-param name="convention"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='07' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' )   and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdConv  ]"/>
                            <xsl:with-param name="emprunt"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='07' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' )   and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdEmpruntOrdo ]"/>
                            <xsl:with-param name="encaiss"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='07' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' )   and    p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:NatPceOrig/@V ='03' ]"/>
                            <xsl:with-param name="piecen"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='07' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' )   and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:ExerRat/@V=$exercice ]"/>
                            <xsl:with-param name="piecen-1"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='07' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' )   and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:ExerRat/@V=$exercice -1]"/>
                        </xsl:call-template>
                        <xsl:call-template name="add-description">
                            <xsl:with-param name="domaine">D&#233;pense</xsl:with-param>
                            <xsl:with-param name="sousdomaine">Mandat global</xsl:with-param>
                            <xsl:with-param name="list-bord"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='06' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='07' )  ]"/>
                            <xsl:with-param name="nbr"
                                            select="count(./p:PES_DepenseAller/p:Bordereau/p:BlocBordereau/p:IdBord/@V[( ../../../p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='06' ) and (../../../p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or ../../../p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04' or ../../../p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='07' )  ])"/>
                            <xsl:with-param name="triplets" select="('Mandats globaux de toutes natures')"/>
                            <xsl:with-param name="regie"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='06' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='07' )  and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdRegie  ]"/>
                            <xsl:with-param name="inventaire"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='06' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='07' )  and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdActif  ]"/>
                            <xsl:with-param name="convention"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='06' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='07' )  and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdConv ]"/>
                            <xsl:with-param name="emprunt"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='06' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='07' )  and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:LiensIdent/p:IdEmpruntOrdo ]"/>
                            <xsl:with-param name="encaiss"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='06' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='07' )  and    p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:NatPceOrig/@V ='03' ]"/>
                            <xsl:with-param name="piecen"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='06' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='07' )  and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:ExerRat/@V=$exercice  ]"/>
                            <xsl:with-param name="piecen-1"
                                            select="./p:PES_DepenseAller/p:Bordereau[( p:Piece/p:BlocPiece/p:InfoPce/p:TypPce/@V='06' ) and (p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='01' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='04' or p:Piece/p:BlocPiece/p:InfoPce/p:NatPce/@V='07' )  and   p:Piece/p:LigneDePiece/p:BlocLignePiece/p:RattachPiece/p:ExerRat/@V=$exercice -1 ]"/>
                        </xsl:call-template>
                    </tbody>
                </table>

                <h3 class="h5">Liste des bordereaux du flux</h3>

                <table>
                    <thead>
                        <tr>
                            <th>Poste Comptable</th>
                            <th>Budget Collectivit&#233;</th>
                            <th>Domaine</th>
                            <th>N&#176; Bordereau</th>
                            <th>Triplet</th>
                            <th>Sous domaine</th>
                        </tr>
                    </thead>

                    <tbody>
                        <xsl:for-each select="./p:PES_RecetteAller/p:Bordereau">
                            <xsl:variable name="triplet">
                                <xsl:value-of
                                        select="concat(./p:BlocBordereau/p:TypBord/@V, '-', ./p:Piece[position()=1]/p:BlocPiece/p:TypPce/@V, '-', ./p:Piece[position()=1]/p:BlocPiece/p:NatPce/@V)"
                                />
                            </xsl:variable>
                            <tr>
                                <td width="102" valign="top"
                                    style="width:76.75pt;border-top:none;padding:0cm 3.5pt 0cm 3.5pt">
                                    <p class="Normal">
                                        <span class="Resultat">
                                            <!--PES_Aller\EnTetePES\IdPost-->
                                            <!--<xsl:if test="position()=1">-->
                                            <xsl:value-of select="../../p:EnTetePES/p:IdPost/@V"/>
                                            <!--</xsl:if>-->
                                        </span>
                                    </p>
                                </td>
                                <td width="102" valign="top"
                                    style="width:76.75pt;border-top:none;padding:0cm 3.5pt 0cm 3.5pt">
                                    <!--<xsl:if test="position()=1"> -->
                                    <p class="Normal">
                                        <span class="Resultat">
                                            <!--PES_Aller\EnTetePES\CodColl-->
                                            <!--PES_Aller\EnTetePES\CodBud-->
                                            <xsl:value-of select="../../p:EnTetePES/p:CodCol/@V"/>
                                            <xsl:value-of select="../../p:EnTetePES/p:CodBud/@V"/>
                                        </span>
                                    </p>
                                    <!--</xsl:if>-->
                                </td>
                                <td width="175" valign="top"
                                    style="width:131.05pt;border-top:none;border-left:
                                                    none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                                                    padding:0cm 3.5pt 0cm 3.5pt">
                                    <p class="Normal">
                                        <span class="Resultat"><!--Si PES_Aller\PES_RecetteAller\Bordereau
                                                               Alors afficher �&#160;Recette&#160;�-->
                                            <!--Si
                        PES_Aller\PES_DepenseAller\Bordereau Alors afficher �&#160;D�pense&#160;�-->
                                            &#160;Recette&#160;
                                        </span>
                                    </p>
                                </td>
                                <td width="226" valign="top"
                                    style="width:169.6pt;border-top:none;border-left:
                                                    none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                                                    padding:0cm 3.5pt 0cm 3.5pt">
                                    <p class="Normal">
                                        <span class="Resultat">
                                            <!--Pour
                                                               PES_Aller\PES_RecetteAller\Bordereau&#160;:
                                                               Valeur(BordRec\IdBord@V)-->
                                            <xsl:call-template name="linkBordereau">
                                                <xsl:with-param name="IdBord" select="./p:BlocBordereau/p:IdBord/@V"/>
                                                <xsl:with-param name="pk" select="./@added:primary-key"/>
                                                <xsl:with-param name="domaine">recette</xsl:with-param>
                                            </xsl:call-template>
                                        </span>
                                    </p>
                                </td>
                                <td width="215" valign="top"
                                    style="width:76.75pt;border-top:none;border-left:
                                                    none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                                                    padding:0cm 3.5pt 0cm 3.5pt">
                                    <p class="Normal">
                                        <span class="Resultat">
                                            <!--Prendre les informations de la
                        premi�re pi�ce du bordereau au format &quot;nn-nn-nn&quot; :
                        Pour
                        PES_Aller\PES_RecetteAller\Bordereau&#160;: Concat�nation(BordRec\TypBord,
                        PiecRec\TypPce et PiecRec\NatPce
                        Pour
                        PES_Aller\PES_DepenseAller\Bordereau&#160;:
                        Concat�nation(BordDep\TypBord,
                        PiecDep\TypPce et PiecDep\NatPce)
                    -->
                                            <xsl:value-of select="$triplet"/>
                                        </span>
                                    </p>
                                </td>
                                <td width="102" valign="top"
                                    style="width:161.1pt;border-top:none;border-left:
                                                    none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                                                    padding:0cm 3.5pt 0cm 3.5pt">
                                    <p class="Normal">
                                        <span class="Resultat">
                                            <!--Pour l�alimentation de ce champ,
                                                               consulter la r�gle de gestion 1.-->
                                            <xsl:choose>
                                                <!--xsl:when test="$triplet='01-01-01'">Ordinaire</xsl:when>
                        <xsl:when test="$triplet='01-01-02'">Ordinaire</xsl:when>
                        <xsl:when test="$triplet='02-02-06'">Correctif</xsl:when>
                        <xsl:when test="$triplet='02-02-09'">Correctif</xsl:when>
                        <xsl:when test="$triplet='02-02-10'">Correctif</xsl:when>
                        <xsl:when test="$triplet='01-05-01'">Emis Apr&#232;s Encaissement</xsl:when>
                        <xsl:when test="$triplet='01-05-02'">Emis Apr&#232;s Encaissement</xsl:when>
                        <xsl:when test="$triplet='01-05-03'">Emis Apr&#232;s Encaissement</xsl:when>
                        <xsl:when test="$triplet='01-05-04'">Emis Apr&#232;s Encaissement</xsl:when>
                        <xsl:when test="$triplet='01-01-05'">R&#233;gie</xsl:when>
                        <xsl:when test="$triplet='01-05-05'">R&#233;gie</xsl:when>
                        <xsl:when test="$triplet='01-01-03'">Inventaire/Emprunt</xsl:when>
                        <xsl:when test="$triplet='01-01-04'">Inventaire/Emprunt</xsl:when>
                        <xsl:when test="$triplet='01-11-01'">Rattachement</xsl:when>
                        <xsl:when test="$triplet='01-11-02'">Rattachement</xsl:when>
                        <xsl:when test="$triplet='01-11-04'">Rattachement</xsl:when>
                        <xsl:when test="$triplet='01-01-18'">Fin d'exercice</xsl:when>
                        <xsl:when test="$triplet='01-03-01'">Fin d'exercice</xsl:when>
                        <xsl:when test="$triplet='01-03-02'">Fin d'exercice</xsl:when>
                        <xsl:when test="$triplet='01-03-03'">Fin d'exercice</xsl:when>
                        <xsl:when test="$triplet='01-03-04'">Fin d'exercice</xsl:when>
                        <xsl:when test="$triplet='01-03-18'">Fin d'exercice</xsl:when>
                        <xsl:when test="$triplet='01-04-01'">Fin d'exercice</xsl:when>
                        <xsl:when test="$triplet='01-04-02'">Fin d'exercice</xsl:when>
                        <xsl:when test="$triplet='01-04-03'">Fin d'exercice</xsl:when>
                        <xsl:when test="$triplet='01-04-04'">Fin d'exercice</xsl:when>
                        <xsl:when test="$triplet='01-04-06'">Fin d'exercice</xsl:when>
                        <xsl:when test="$triplet='01-14-01'">Fin d'exercice</xsl:when>
                        <xsl:when test="$triplet='01-14-06'">Fin d'exercice</xsl:when>
                        <xsl:when test="$triplet='01-09-01'">Majoration</xsl:when>
                        <xsl:when test="$triplet='01-06-01'">R&#233;capitulatif</xsl:when>
                        <xsl:when test="$triplet='01-07-01'">R&#233;capitulatif</xsl:when>
                        <xsl:when test="$triplet='01-06-01'">R&#233;capitulatif</xsl:when>
                        <xsl:when test="$triplet='01-02-07'">Hors Scope</xsl:when>
                        <xsl:when test="$triplet='01-02-08'">Hors Scope</xsl:when>
                        <xsl:when test="$triplet='01-10-01'">Hors Scope</xsl:when>
                        <xsl:when test="$triplet='01-10-02'">Hors Scope</xsl:when>
                        <xsl:when test="$triplet='01-12-01'">Hors Scope</xsl:when>
                        <xsl:when test="$triplet='01-12-02'">Hors Scope</xsl:when>
                        <xsl:when test="$triplet='01-12-11'">Hors Scope</xsl:when-->
                                                <xsl:when
                                                        test="$sousdomaines/domaine[@code='recette']/triplet[@code=$triplet]">
                                                    <xsl:value-of
                                                            select="$sousdomaines/domaine[@code='recette']/triplet[@code=$triplet]/@valeur"/>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <span style="color: red;">Incorrect</span>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </span>
                                    </p>
                                </td>
                            </tr>
                        </xsl:for-each>
                        <xsl:for-each select="./p:PES_DepenseAller/p:Bordereau">
                            <xsl:variable name="triplet">
                                <xsl:value-of
                                        select="concat(./p:BlocBordereau/p:TypBord/@V, '-', ./p:Piece[position()=1]/p:BlocPiece/p:InfoPce/p:TypPce/@V, '-', ./p:Piece[position()=1]/p:BlocPiece/p:InfoPce/p:NatPce/@V)"
                                />
                            </xsl:variable>
                            <tr>
                                <td width="102" valign="top"
                                    style="width:76.75pt;border-top:none;padding:0cm 3.5pt 0cm 3.5pt">
                                    <p class="Normal">
                                        <span class="Resultat">
                                            <!--PES_Aller\EnTetePES\IdPost-->
                                            <!--<xsl:if test="position()=1"> -->
                                            <xsl:value-of select="../../p:EnTetePES/p:IdPost/@V"/>
                                            <!--</xsl:if> -->
                                        </span>
                                    </p>
                                </td>
                                <td width="102" valign="top"
                                    style="width:76.75pt;border-top:none;padding:0cm 3.5pt 0cm 3.5pt">
                                    <!--<xsl:if test="position()=1"> -->
                                    <p class="Normal">
                                        <span class="Resultat">
                                            <!--PES_Aller\EnTetePES\CodColl-->
                                            <!--PES_Aller\EnTetePES\CodBud-->
                                            <xsl:value-of select="../../p:EnTetePES/p:CodCol/@V"/>
                                            <xsl:value-of select="../../p:EnTetePES/p:CodBud/@V"/>
                                        </span>
                                    </p>
                                    <!--</xsl:if>-->
                                </td>
                                <td width="175" valign="top"
                                    style="width:131.05pt;border-top:none;border-left:
                                                    none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                                                    padding:0cm 3.5pt 0cm 3.5pt">
                                    <p class="Normal">
                                        <span class="Resultat"><!--Si PES_Aller\PES_RecetteAller\Bordereau
                                                               Alors afficher �&#160;Recette&#160;�-->
                                            <!--Si
                        PES_Aller\PES_DepenseAller\Bordereau Alors afficher �&#160;D�pense&#160;�-->
                                            &#160;D&#233;pense&#160;
                                        </span>
                                    </p>
                                </td>
                                <td width="226" valign="top"
                                    style="width:169.6pt;border-top:none;border-left:
                                                    none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                                                    padding:0cm 3.5pt 0cm 3.5pt">
                                    <p class="Normal">
                                        <span class="Resultat">
                                            <!--Pour
                                                               PES_Aller\PES_DepenseAller\Bordereau&#160;:Valeur(BordDep\IdBord@V)-->
                                            <xsl:call-template name="linkBordereau">
                                                <xsl:with-param name="IdBord" select="./p:BlocBordereau/p:IdBord/@V"/>
                                                <xsl:with-param name="pk" select="./@added:primary-key"/>
                                                <xsl:with-param name="domaine">depense</xsl:with-param>
                                            </xsl:call-template>
                                        </span>
                                    </p>
                                </td>
                                <td width="215" valign="top"
                                    style="width:76.75pt;border-top:none;border-left:
                                                    none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                                                    padding:0cm 3.5pt 0cm 3.5pt">
                                    <p class="Normal">
                                        <span class="Resultat">
                                            <!--
                        Prendre les informations de la
                        premi�re pi�ce du bordereau au format &quot;nn-nn-nn&quot; :</span></p>
                        <p class="Normal"><span class="Resultat">Pour
                        PES_Aller\PES_RecetteAller\Bordereau&#160;: Concat�nation(BordRec\TypBord,
                        PiecRec\TypPce et PiecRec\NatPce)</span></p>
                        <p class="Normal"><span class="Resultat">&#160;</span></p>
                        <p class="Normal"><span class="Resultat">Pour
                        PES_Aller\PES_DepenseAller\Bordereau&#160;:</span></p>
                        <p class="Normal"><span class="Resultat">Concat�nation(BordDep\TypBord,
                        PiecDep\TypPce et PiecDep\NatPce)-->
                                            <xsl:value-of select="$triplet"/>
                                        </span>
                                    </p>
                                </td>
                                <td width="102" valign="top"
                                    style="width:161.1pt;border-top:none;border-left:
                                                    none;border-bottom:solid black 1.0pt;border-right:solid black 1.0pt;
                                                    padding:0cm 3.5pt 0cm 3.5pt">
                                    <p class="Normal">
                                        <span class="Resultat">
                                            <!--Pour l�alimentation de ce champ,
                                                               consulter la r�gle de gestion 1.-->
                                            <!--xsl:choose>
                        <xsl:when test="$triplet='01-01-01'">Ordinaire</xsl:when>
                        <xsl:when test="$triplet='01-01-02'">Ordinaire</xsl:when>
                        <xsl:when test="$triplet='02-02-06'">Correctif</xsl:when>
                        <xsl:when test="$triplet='02-02-09'">Correctif</xsl:when>
                        <xsl:when test="$triplet='02-02-10'">Correctif</xsl:when>
                        <xsl:when test="$triplet='01-05-01'">EAP</xsl:when>
                        <xsl:when test="$triplet='01-05-02'">EAP</xsl:when>
                        <xsl:when test="$triplet='01-05-03'">EAP</xsl:when>
                        <xsl:when test="$triplet='01-05-04'">EAP</xsl:when>
                        <xsl:when test="$triplet='01-01-05'">R&#233;gie</xsl:when>
                        <xsl:when test="$triplet='01-05-05'">R&#233;gie</xsl:when>
                        <xsl:when test="$triplet='01-09-01'">March&#233;</xsl:when>
                        <xsl:when test="$triplet='01-09-02'">March&#233;</xsl:when>
                        <xsl:when test="$triplet='01-09-12'">March&#233;</xsl:when>
                        <xsl:when test="$triplet='01-09-13'">March&#233;</xsl:when>
                        <xsl:when test="$triplet='01-01-03'">Inventaire/Emprunt</xsl:when>
                        <xsl:when test="$triplet='01-01-04'">Inventaire/Emprunt</xsl:when>
                        <xsl:when test="$triplet='01-10-01'">Rattachement</xsl:when>
                        <xsl:when test="$triplet='01-10-02'">Rattachement</xsl:when>
                        <xsl:when test="$triplet='01-10-04'">Rattachement</xsl:when>
                        <xsl:when test="$triplet='01-10-05'">Rattachement</xsl:when>
                        <xsl:when test="$triplet='01-10-11'">Rattachement</xsl:when>
                        <xsl:when test="$triplet='01-03-01'">Fin d'exercice</xsl:when>
                        <xsl:when test="$triplet='01-03-02'">Fin d'exercice</xsl:when>
                        <xsl:when test="$triplet='01-03-03'">Fin d'exercice</xsl:when>
                        <xsl:when test="$triplet='01-03-04'">Fin d'exercice</xsl:when>
                        <xsl:when test="$triplet='01-03-18'">Fin d'exercice</xsl:when>
                        <xsl:when test="$triplet='01-04-01'">Fin d'exercice</xsl:when>
                        <xsl:when test="$triplet='01-04-02'">Fin d'exercice</xsl:when>
                        <xsl:when test="$triplet='01-04-03'">Fin d'exercice</xsl:when>
                        <xsl:when test="$triplet='01-04-04'">Fin d'exercice</xsl:when>
                        <xsl:when test="$triplet='01-04-06'">Fin d'exercice</xsl:when>
                        <xsl:when test="$triplet='01-13-01'">Fin d'exercice</xsl:when>
                        <xsl:when test="$triplet='01-12-06'">Fin d'exercice</xsl:when>
                        <xsl:when test="$triplet='01-08-01'">Collectif</xsl:when>
                        <xsl:when test="$triplet='01-08-02'">Collectif</xsl:when>
                        <xsl:when test="$triplet='01-01-11'">Paie</xsl:when>
                        <xsl:when test="$triplet='01-08-12'">Paie</xsl:when>
                        <xsl:when test="$triplet='01-07-01'">Admission en non valeur</xsl:when>
                        <xsl:when test="$triplet='01-06-01'">Global</xsl:when>
                        <xsl:when test="$triplet='01-06-04'">Global</xsl:when>
                        <xsl:when test="$triplet='01-06-07'">Global</xsl:when>
                        <xsl:when test="$triplet='01-02-07'">Hors Scope</xsl:when>
                        <xsl:when test="$triplet='01-02-08'">Hors Scope</xsl:when>
                        <xsl:when test="$triplet='01-11-01'">Hors Scope</xsl:when>
                        <xsl:when test="$triplet='01-11-02'">Hors Scope</xsl:when>
                        <xsl:when test="$triplet='01-11-04'">Hors Scope</xsl:when>
                        <xsl:otherwise>
                          <span style="color: red;">Incorrect</span>
                        </xsl:otherwise>
                        </xsl:choose-->
                                            <xsl:choose>
                                                <xsl:when
                                                        test="$sousdomaines/domaine[@code='depense']/triplet[@code=$triplet]">
                                                    <xsl:value-of
                                                            select="$sousdomaines/domaine[@code='depense']/triplet[@code=$triplet]/@valeur"/>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <span style="color: red;">Incorrect</span>
                                                </xsl:otherwise>
                                            </xsl:choose>

                                        </span>
                                    </p>
                                </td>
                            </tr>
                        </xsl:for-each>
                    </tbody>
                </table>

                <div style="padding-top: 3mm;">
                    <xsl:if test="exists(p:PES_DepenseAller/p:Bordereau/p:Piece)">
                        <h3 class="h5">Mandats</h3>
                        <table>
                            <thead>
                                <tr>
                                    <th>
                                        Id Bord
                                    </th>
                                    <th>
                                        Id mandat
                                    </th>
                                    <th>
                                        Triplet
                                    </th>
                                    <th>
                                        Objet
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <xsl:for-each select="p:PES_DepenseAller/p:Bordereau/p:Piece/p:BlocPiece/p:InfoPce">
                                    <tr>
                                        <td>
                                            <xsl:call-template name="linkBordereau">
                                                <xsl:with-param name="IdBord"
                                                                select="../../../p:BlocBordereau/p:IdBord/@V"/>
                                                <xsl:with-param name="pk" select="../../../@added:primary-key"/>
                                                <xsl:with-param name="domaine">depense</xsl:with-param>
                                            </xsl:call-template>
                                        </td>
                                        <td>
                                            <xsl:call-template name="linkMandat">
                                                <xsl:with-param name="pk" select="../../../@added:primary-key"/>
                                                <xsl:with-param name="domaine">depense</xsl:with-param>
                                                <xsl:with-param name="mandatId" select="p:IdPce/@V"/>
                                            </xsl:call-template>
                                        </td>
                                        <td><xsl:value-of select="../../../p:BlocBordereau/p:TypBord/@V"/>-<xsl:value-of
                                                select="p:TypPce/@V"/>-<xsl:value-of select="p:NatPce/@V"/>
                                        </td>
                                        <td>
                                            <xsl:value-of select="p:Obj/@V"/>
                                        </td>
                                    </tr>
                                </xsl:for-each>
                            </tbody>
                        </table>
                    </xsl:if>
                    <xsl:if test="exists(p:PES_RecetteAller/p:Bordereau/p:Piece)">
                        <h3 class="h5">Titres</h3>
                        <table>
                            <thead>
                                <tr>
                                    <td style="background:gray;padding:0cm 3.5pt 0cm 3.5pt; color: white; font-weight: bold;">
                                        Id Bord
                                    </td>
                                    <td style="background:gray;padding:0cm 3.5pt 0cm 3.5pt; color:white; font-weight: bold;">
                                        Id titre
                                    </td>
                                    <td style="background:gray;padding:0cm 3.5pt 0cm 3.5pt; color: white; font-weight: bold;">
                                        Triplet
                                    </td>
                                    <td style="background:gray;padding:0cm 3.5pt 0cm 3.5pt; color:white; font-weight: bold;">
                                        Objet
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <xsl:for-each select="p:PES_RecetteAller/p:Bordereau/p:Piece/p:BlocPiece">
                                    <tr>
                                        <td>
                                            <xsl:value-of select="../../p:BlocBordereau/p:IdBord/@V"/>
                                        </td>
                                        <td>
                                            <xsl:value-of select="p:IdPce/@V"/>
                                        </td>
                                        <td><xsl:value-of select="../../p:BlocBordereau/p:TypBord/@V"/>-<xsl:value-of
                                                select="p:TypPce/@V"/>-<xsl:value-of select="p:NatPce/@V"/>
                                        </td>
                                        <td>
                                            <xsl:value-of select="p:ObjPce/@V"/>
                                        </td>
                                    </tr>
                                </xsl:for-each>
                            </tbody>
                        </table>
                    </xsl:if>
                </div>
                <xsl:apply-templates/>
            </body>
        </html>
    </xsl:template>
    <xsl:template name="linkBordereau">
        <xsl:param name="IdBord"/>
        <xsl:param name="pk"/>
        <xsl:param name="domaine"/>
        <xsl:element name="a">
            <xsl:variable name="etat">
                <xsl:choose>
                    <xsl:when test="lower-case($domaine) = 'recette'">PES_RecetteAller</xsl:when>
                    <xsl:when test="lower-case($domaine) = 'depense' or lower-case($domaine) = 'd�pense'">PES_DepenseAller</xsl:when>
                    <xsl:otherwise>general_view</xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:attribute name="href">xemelios:/query?docId=pes-aller&amp;etatId=<xsl:value-of
                    select="$etat"/>&amp;path=[@added:primary-key='<xsl:value-of select="$pk"
            />']&amp;xsl:param=(elementId,Bordereau)</xsl:attribute>
            <xsl:value-of select="$IdBord"/>
        </xsl:element>
    </xsl:template>

    <xsl:template name="linkMandat">
        <xsl:param name="pk"/>
        <xsl:param name="mandatId"/>
        <xsl:param name="domaine"/>
        <xsl:element name="a">
            <xsl:variable name="etat">
                <xsl:choose>
                    <xsl:when test="$domaine = 'recette'">PES_RecetteAller</xsl:when>
                    <xsl:when test="$domaine = 'depense'">PES_DepenseAller</xsl:when>
                    <xsl:otherwise>general_view</xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:attribute name="href">xemelios:/query?docId=pes-aller&amp;etatId=<xsl:value-of
                    select="$etat"/>&amp;path=[@added:primary-key='<xsl:value-of select="$pk"
            />']&amp;xsl:param=(mandatId,<xsl:value-of select="$mandatId"/>)</xsl:attribute>
            <xsl:value-of select="$mandatId"/>
        </xsl:element>
    </xsl:template>

    <xsl:template name="add-description">
        <xsl:param name="domaine"/>
        <xsl:param name="sousdomaine"/>
        <xsl:param name="list-bord"/>
        <xsl:param name="nbr"/>
        <xsl:param name="triplets"/>
        <xsl:param name="regie"/>
        <xsl:param name="inventaire"/>
        <xsl:param name="convention"/>
        <xsl:param name="emprunt"/>
        <xsl:param name="encaiss"/>
        <xsl:param name="piecen"/>
        <xsl:param name="piecen-1"/>

        <xsl:element name="a">
            <xsl:if test="$nbr > 0">
                <tr>
                    <td>
                        <xsl:value-of select="$domaine"/>
                    </td>
                    <td>
                        <xsl:value-of select="$sousdomaine"/>
                    </td>
                    <td>
                        <ul>
                            <xsl:for-each
                                    select="$list-bord">
                                <li>
                                    <xsl:call-template name="linkBordereau">
                                        <xsl:with-param name="IdBord"
                                                        select="./p:BlocBordereau/p:IdBord/@V"/>
                                        <xsl:with-param name="pk" select="./@added:primary-key"/>
                                        <xsl:with-param name="domaine" select="$domaine"/>
                                    </xsl:call-template>
                                </li>
                            </xsl:for-each>
                        </ul>
                    </td>
                    <td>
                        <xsl:value-of select="$nbr"/>
                    </td>
                    <td>
                        <ul>
                            <xsl:for-each
                                    select="$triplets">
                                <li>
                                    <xsl:value-of select="."/>
                                </li>
                            </xsl:for-each>
                        </ul>
                    </td>
                    <td>
                        <ul>
                            <xsl:for-each
                                    select="$regie">
                                <li>
                                    <xsl:call-template name="linkBordereau">
                                        <xsl:with-param name="IdBord"
                                                        select="./p:BlocBordereau/p:IdBord/@V"/>
                                        <xsl:with-param name="pk" select="./@added:primary-key"/>
                                        <xsl:with-param name="domaine">recette</xsl:with-param>
                                    </xsl:call-template>
                                </li>
                            </xsl:for-each>
                        </ul>
                    </td>
                    <td>
                        <ul>
                            <xsl:for-each
                                    select="$inventaire">
                                <li>
                                    <xsl:call-template name="linkBordereau">
                                        <xsl:with-param name="IdBord"
                                                        select="./p:BlocBordereau/p:IdBord/@V"/>
                                        <xsl:with-param name="pk" select="./@added:primary-key"/>
                                        <xsl:with-param name="domaine">recette</xsl:with-param>
                                    </xsl:call-template>
                                </li>
                            </xsl:for-each>
                        </ul>
                    </td>
                    <td>
                        <ul>
                            <xsl:for-each
                                    select="$convention">
                                <li>
                                    <xsl:call-template name="linkBordereau">
                                        <xsl:with-param name="IdBord"
                                                        select="./p:BlocBordereau/p:IdBord/@V"/>
                                        <xsl:with-param name="pk" select="./@added:primary-key"/>
                                        <xsl:with-param name="domaine">recette</xsl:with-param>
                                    </xsl:call-template>
                                </li>
                            </xsl:for-each>
                        </ul>
                    </td>
                    <td>
                        <ul>
                            <xsl:for-each
                                    select="$emprunt">
                                <li>
                                    <xsl:call-template name="linkBordereau">
                                        <xsl:with-param name="IdBord"
                                                        select="./p:BlocBordereau/p:IdBord/@V"/>
                                        <xsl:with-param name="pk" select="./@added:primary-key"/>
                                        <xsl:with-param name="domaine">recette</xsl:with-param>
                                    </xsl:call-template>
                                </li>
                            </xsl:for-each>
                        </ul>
                    </td>
                    <td>
                        <ul>
                            <xsl:for-each
                                    select="$encaiss">
                                <li>
                                    <xsl:call-template name="linkBordereau">
                                        <xsl:with-param name="IdBord"
                                                        select="./p:BlocBordereau/p:IdBord/@V"/>
                                        <xsl:with-param name="pk" select="./@added:primary-key"/>
                                        <xsl:with-param name="domaine">recette</xsl:with-param>
                                    </xsl:call-template>
                                </li>
                            </xsl:for-each>
                        </ul>
                    </td>
                    <td>
                        <ul>
                            <xsl:for-each
                                    select="$piecen">
                                <li>
                                    <xsl:call-template name="linkBordereau">
                                        <xsl:with-param name="IdBord"
                                                        select="./p:BlocBordereau/p:IdBord/@V"/>
                                        <xsl:with-param name="pk" select="./@added:primary-key"/>
                                        <xsl:with-param name="domaine">recette</xsl:with-param>
                                    </xsl:call-template>
                                </li>
                            </xsl:for-each>
                        </ul>
                    </td>
                    <td>
                        <ul>
                            <xsl:for-each
                                    select="$piecen-1">
                                <li>
                                    <xsl:call-template name="linkBordereau">
                                        <xsl:with-param name="IdBord"
                                                        select="./p:BlocBordereau/p:IdBord/@V"/>
                                        <xsl:with-param name="pk" select="./@added:primary-key"/>
                                        <xsl:with-param name="domaine">recette</xsl:with-param>
                                    </xsl:call-template>
                                </li>
                            </xsl:for-each>
                        </ul>
                    </td>
                </tr>
            </xsl:if>
        </xsl:element>
    </xsl:template>
</xsl:stylesheet>
