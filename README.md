Pes-Viewer
==========

[![pipeline status](https://gitlab.libriciel.fr/i-parapheur/components/pes-viewer/badges/master/pipeline.svg)](https://gitlab.libriciel.fr/i-parapheur/components/pes-viewer/commits/master)
[![coverage report](https://gitlab.libriciel.fr/i-parapheur/components/pes-viewer/badges/master/coverage.svg)](https://gitlab.libriciel.fr/i-parapheur/components/pes-viewer/commits/master)

[![Alerte](https://sonarqube.libriciel.fr/api/project_badges/measure?project=coop.libriciel%3Apes-viewer&metric=alert_status)](https://sonarqube.libriciel.fr/dashboard?id=coop.libriciel%3Apes-viewer)
[![Maintenabilité](https://sonarqube.libriciel.fr/api/project_badges/measure?project=coop.libriciel%3Apes-viewer&metric=sqale_rating)](https://sonarqube.libriciel.fr/dashboard?id=coop.libriciel%3Apes-viewer)
[![Fiabilité](https://sonarqube.libriciel.fr/api/project_badges/measure?project=coop.libriciel%3Apes-viewer&metric=reliability_rating)](https://sonarqube.libriciel.fr/dashboard?id=coop.libriciel%3Apes-viewer)
[![Sécurité](https://sonarqube.libriciel.fr/api/project_badges/measure?project=coop.libriciel%3Apes-viewer&metric=security_rating)](https://sonarqube.libriciel.fr/dashboard?id=coop.libriciel%3Apes-viewer)

[![Bugs](https://sonarqube.libriciel.fr/api/project_badges/measure?project=coop.libriciel%3Apes-viewer&metric=bugs)](https://sonarqube.libriciel.fr/dashboard?id=coop.libriciel%3Apes-viewer)
[![Mauvaises pratiques](https://sonarqube.libriciel.fr/api/project_badges/measure?project=coop.libriciel%3Apes-viewer&metric=code_smells)](https://sonarqube.libriciel.fr/dashboard?id=coop.libriciel%3Apes-viewer)
[![Dette Technique](https://sonarqube.libriciel.fr/api/project_badges/measure?project=coop.libriciel%3Apes-viewer&metric=sqale_index)](https://sonarqube.libriciel.fr/dashboard?id=coop.libriciel%3Apes-viewer)

Service de visualisation de PES, visant à remplacer le service de "visionneuse Xemelios" utilisé depuis trop d'années...

## Paramètres et variables d'environnement

- `PES_ATTACHMENT_KEEPING_TIME` : Nombre de jours de préservation des PJ PES (défaut : `30`)
- `JAVA_OPTS` : Options supplémentaires à transmettre à la JVM

## Installation sur le i-Parapheur v4

L'installation est assez simple et les modifications minimes :

- Ajouter des droits d'accès au dossier temporaire partagé de l'ancienne visionneuse :
  ```bash
  $ chmod +x /var/tmp/bl-xemwebviewer && chmod +x /var/tmp/bl-xemwebviewer/xwv-shared
  ```
- Récupérer [Pes-Viewer packagé](https://gitlab.libriciel.fr/i-parapheur/components/pes-viewer/-/jobs/artifacts/1.0.1/download?job=packaging)
- Le déposer dans le dossier `/opt`
- Décompresser via la commande `unzip pes-viewer*.zip`
- Lancer le script d'installation via la commande `/opt/pes-viewer/pes-viewer-install.sh`
- Remplacer la location `bl-xemwebviewer` sur le fichier de configuration NginX `parapheur.conf` :
  ```nginx
      # Accès à la visionneuse
      location /bl-xemwebviewer {
          proxy_pass http://127.0.0.1:8888;
      }    
  ```
- Remplacer la location `bl-xemwebviewer` sur le fichier de configuration NginX `parapheur_ssl.conf` :
  ```nginx
      # Accès à la visionneuse
      location /bl-xemwebviewer {
          proxy_intercept_errors off;
          proxy_pass http://127.0.0.1:8888;
      }    
  ```
- Recharger le service NginX via la commande `systemctl reload nginx`

## Packaging

Pré-requis : Java 17  
Lancer la commande suivante à la racine du projet :

```bash
$ ./gradlew bootJar
```

Le Jar packagé sera disponible dans `build/libs/pes-viewer-{VERSION}.jar`.

### Live testing

Il est possible d'activer l'interface Swagger, permettant de tester l'API directement,
en ajoutant l'Active Profile `dev` : `-Dspring.profiles.active=dev`.  
Cette option est surchargeable via la variable d'environnement `JAVA_OPTS`, via Docker/Docker-Compose.

On peut ensuite accéder à cette interface par le chemin `<hostname>:8888/bl-xemwebviewer/swagger-ui.html`
