/*
 * PES-Viewer
 * Copyright (C) 2019-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package coop.libriciel.pesviewer.util;

import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.apache.xerces.impl.dv.util.Base64;

import javax.xml.stream.*;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipException;


@Log4j2
public class XmlPjExtractor {

    private final XMLEventReader reader;
    private final XMLEventWriter writer;
    private final XMLEventFactory eventFactory;

    private GZIPInputStream pjInputStream = null;
    private String idUnique = null;

    @Getter
    private String idColl = null;

    private final Map<String, InputStream> pjFiles = new HashMap<>();


    public XmlPjExtractor(InputStream xmlInput, OutputStream xmlOutput) throws XMLStreamException, IOException {
        XMLInputFactory xmlInFact = XMLInputFactory.newInstance();
        xmlInFact.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, false);

        XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newInstance();

        reader = xmlInFact.createXMLEventReader(xmlInput);
        writer = xmlOutputFactory.createXMLEventWriter(xmlOutput);

        eventFactory = XMLEventFactory.newInstance();

        this.readFile();
    }


    private void readFile() throws XMLStreamException, IOException {
        while (reader.hasNext()) {
            XMLEvent event = reader.nextEvent();
            if (event.getEventType() == XMLEvent.START_ELEMENT) {
                handleStartElement(event.asStartElement());
            } else if (event.getEventType() == XMLEvent.END_ELEMENT) {
                handleEndElement(event.asEndElement());
            } else {
                writer.add(event);
            }
        }
        writer.flush();
        writer.close();
    }


    private void handleStartElement(StartElement event) throws XMLStreamException, IOException {
        if (event.getName().getLocalPart().equalsIgnoreCase("IdColl")) {
            Iterator<Attribute> it = event.getAttributes();
            if (it.hasNext()) idColl = it.next().getValue();
        }

        if (event.getName().getLocalPart().equalsIgnoreCase("Fichier")) {
            handleFichierElement(event);
        } else {
            writer.add(event);
        }

        if (event.getName().getLocalPart().equalsIgnoreCase("IdUnique") && pjInputStream != null) {
            Iterator<Attribute> it = event.getAttributes();
            if (it.hasNext()) idUnique = it.next().getValue();
            if (idUnique != null) {
                pjFiles.put(idUnique, pjInputStream);
            }
        }
    }


    private void handleEndElement(EndElement event) throws XMLStreamException {
        if (!event.getName().getLocalPart().equalsIgnoreCase("Fichier")) {
            writer.add(event);
        }
    }


    private void handleFichierElement(StartElement elementEvent) throws XMLStreamException, IOException {
        pjInputStream = null;

        this.addEmptyElementCopyFromStartEvent(elementEvent);

        String elementText = reader.getElementText();
        if (!elementText.isEmpty()) {
            byte[] fileBytes = Base64.decode(elementText);
            if (fileBytes == null) {
                log.error("Bad base64 format for PJ");
            } else if (fileBytes.length != 0) {
                try {
                    pjInputStream = new GZIPInputStream(new ByteArrayInputStream(fileBytes));
                } catch (ZipException e) {
                    log.error("PJ is not in ZIP format");
                }
            }
        }
    }


    public Map<String, InputStream> getPjFiles() {
        return pjFiles;
    }


    private void addEmptyElementCopyFromStartEvent(StartElement srcEvent) throws XMLStreamException {
        XMLEvent startEvent = eventFactory.createStartElement(srcEvent.getName(), srcEvent.getAttributes(), srcEvent.getNamespaces());
        writer.add(startEvent);
        XMLEvent endEvent = eventFactory.createEndElement(srcEvent.getName(), srcEvent.getNamespaces());
        writer.add(endEvent);
    }

}
