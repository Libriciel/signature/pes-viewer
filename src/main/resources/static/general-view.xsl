<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:n="http://www.minefi.gouv.fr/cp/helios/pes_v2/Rev0/aller"
    xmlns:added="http://projets.admisource.gouv.fr/xemelios/namespaces#added"
    xmlns:ano="http://projets.admisource.gouv.fr/xemelios/namespaces#anomally"
    xmlns:ds="http://www.w3.org/2000/09/xmldsig#"
    xmlns:xad="http://uri.etsi.org/01903/v1.1.1#"
    xmlns:data="data.uri"
    xmlns:xem="fr.gouv.finances.cp.utils.xml.certs.Certificate509"
    version="2.0">
    <xsl:param name="browser-destination" />
    <xsl:output encoding="ISO-8859-1" method="xhtml" exclude-result-prefixes="n added ano data" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" include-content-type="no" indent="yes"/>
    <xsl:decimal-format name="decformat" 
        decimal-separator="," 
        grouping-separator=" " 
        digit="#" 
        pattern-separator=";" 
        NaN="NaN" 
        minus-sign="-"/>
    <xsl:template match="*"/>
    <xsl:template match="/n:PES_Aller">
        <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
            <head>
                <title>Etat de PES Aller</title>
            </head>
            <body>
                <h1 class="h3">
                    <xsl:value-of select="./n:EnTetePES/n:IdPost/@V"/>&#160;<xsl:value-of select="./n:EnTetePES/n:LibellePoste/@V"/>
                </h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">
                            Accueil
                        </li>
                    </ol>
                </nav>

                <a class="btn btn-info" href="xemelios:/query?docId=pes-aller&amp;etatId=analyse-flux">
                    <i class="fa fa-list-ul"/>
                    R&#233;sum&#233; du flux
                </a>
                <hr/>
                <xsl:apply-templates/>
                <script>
                    var links = document.getElementsByClassName("bordereauLink");
                    if(links.length === 1) {
                        links[0].click();
                    }

                </script>
            </body>
        </html>
    </xsl:template>
    
    <xsl:template match="//n:PES_DepenseAller">
        <h2 class="h4">Domaine D&#233;pense</h2>
        <p>
            <ul class="list-bordereaux">
                <xsl:for-each select="n:Bordereau">
                    <xsl:call-template name="BordereauDepense">
                        <xsl:with-param name="bId" select="."/>
                    </xsl:call-template>
                </xsl:for-each>
            </ul>
        </p>
    </xsl:template>
    <xsl:template name="BordereauDepense">
        <xsl:param name="bId"/>
        <li>
            <xsl:variable name="bUri">xemelios:/query?docId=pes-aller&amp;etatId=PES_DepenseAller&amp;elementId=BordereauDepense&amp;path=[@added:primary-key='<xsl:value-of select="$bId/@added:primary-key"/>']&amp;xsl:param=(elementId,Bordereau)</xsl:variable>
            <xsl:element name="a">
                <xsl:attribute name="href" select="$bUri"/>
                <xsl:attribute name="class">bordereauLink</xsl:attribute>
                Bordereau <xsl:value-of select="$bId/n:BlocBordereau/n:IdBord/@V"/>
            </xsl:element>
        </li>
    </xsl:template>
    
    <xsl:template match="//n:PES_RecetteAller">
        <h2 class="h4">Domaine Recette</h2>
        <p>
            <ul class="list-bordereaux">
                <xsl:for-each select="n:Bordereau">
                    <xsl:call-template name="BordereauRecette">
                        <xsl:with-param name="bId" select="."/>
                    </xsl:call-template>
                </xsl:for-each>
            </ul>
        </p>
    </xsl:template>
    <xsl:template name="BordereauRecette">
        <xsl:param name="bId"/>
        <li>
            <xsl:variable name="bUri">xemelios:/query?docId=pes-aller&amp;etatId=PES_RecetteAller&amp;elementId=BordereauRecette&amp;path=[@added:primary-key='<xsl:value-of select="$bId/@added:primary-key"/>']&amp;xsl:param=(elementId,Bordereau)</xsl:variable>
            <xsl:element name="a">
                <xsl:attribute name="href" select="$bUri"/>
                <xsl:attribute name="class">bordereauLink</xsl:attribute>
                Bordereau <xsl:value-of select="$bId/n:BlocBordereau/n:IdBord/@V"/>
            </xsl:element>
        </li>
    </xsl:template>
</xsl:stylesheet>
