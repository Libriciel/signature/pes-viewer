/*
 * PES-Viewer
 * Copyright (C) 2019-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.pesviewer.controller;

import org.apache.commons.io.IOUtils;
import org.apache.hc.core5.http.NameValuePair;
import org.apache.hc.core5.net.URIBuilder;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.CollectionUtils;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

import static coop.libriciel.pesviewer.util.MimeTypeUtils.APPLICATION_ZIP_VALUE;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toMap;
import static org.apache.tomcat.websocket.Constants.FOUND;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;
import static org.springframework.http.HttpHeaders.LOCATION;
import static org.springframework.http.MediaType.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class PesViewerControllerIntegrationTest {


    @Autowired private MockMvc mockMvc;
    private final ClassLoader classLoader = getClass().getClassLoader();


    // <editor-fold desc="Tests Utils">


    private static byte[] buildFileContent(@NotNull ClassLoader classLoader, @NotNull String fileName) throws IOException {
        InputStream fileInputStream = classLoader.getResourceAsStream(fileName);
        assertNotNull(fileInputStream);
        return IOUtils.toByteArray(fileInputStream);
    }


    private MvcResult preparePes(@NotNull String fileName) throws Exception {
        AtomicReference<MvcResult> prepareResult = new AtomicReference<>();
        mockMvc.perform(
                        multipart("/prepare")
                                .file("file", buildFileContent(classLoader, fileName))
                                .contentType(APPLICATION_OCTET_STREAM_VALUE)
                )
                .andExpect(status().is(FOUND))
                .andDo(prepareResult::set);

        MvcResult mvcResult = prepareResult.get();
        assertNotNull(mvcResult);
        return mvcResult;
    }


    private static Map<String, List<String>> parseRedirectionParameters(MvcResult mvcResult) throws URISyntaxException {
        String redirectionResponse = mvcResult.getResponse().getHeader(LOCATION);
        assertNotNull(redirectionResponse);
        return new URIBuilder(redirectionResponse)
                .getQueryParams()
                .stream()
                .collect(toMap(
                        NameValuePair::getName,
                        nameValuePair -> singletonList(nameValuePair.getValue())
                ));
    }


    private static String parseRedirectionPath(MvcResult mvcResult) throws URISyntaxException {
        String redirectionResponse = mvcResult.getResponse().getHeader(LOCATION);
        assertNotNull(redirectionResponse);
        String path = new URI(redirectionResponse).getPath();
        assertNotNull(path);
        return "/%s".formatted(path);
    }


    private static HttpSession parseRedirectionSession(MvcResult mvcResult) {
        HttpSession redirectionSession = mvcResult.getRequest().getSession();
        assertNotNull(redirectionSession);
        return redirectionSession;
    }


    // </editor-fold desc="Tests Utils">


    @Test
    void whenBrowse_doReturnHtml() throws Exception {
        MvcResult prepareResult = preparePes("peswithoutpj.xml");
        mockMvc.perform(
                        get(parseRedirectionPath(prepareResult))
                                .params(CollectionUtils.toMultiValueMap(parseRedirectionParameters(prepareResult)))
                                .session((MockHttpSession) parseRedirectionSession(prepareResult))
                )
                .andExpect(status().isOk())
                .andExpect(content().contentType(TEXT_HTML_VALUE));
    }


    @Test
    void whenBrowseBadSession_doReturnHtmlError() throws Exception {
        MvcResult prepareResult = preparePes("peswithoutpj.xml");
        Map<String, List<String>> brokenXxvSessionParams = parseRedirectionParameters(prepareResult);
        brokenXxvSessionParams.put("xwvSession", singletonList("coucoucoucouc"));
        mockMvc.perform(
                        get(parseRedirectionPath(prepareResult))
                                .params(CollectionUtils.toMultiValueMap(brokenXxvSessionParams))
                                .session((MockHttpSession) parseRedirectionSession(prepareResult))
                )
                .andExpect(status().isForbidden());
    }


    @Test
    void whenGetAttachment_doReturnFile() throws Exception {
        MvcResult prepareResult = preparePes("peswithpj.xml");
        Map<String, List<String>> params = parseRedirectionParameters(prepareResult);
        params.put("pjId", singletonList("thisisunique"));
        mockMvc.perform(
                        get("/attachment")
                                .params(CollectionUtils.toMultiValueMap(params))
                                .session((MockHttpSession) parseRedirectionSession(prepareResult))
                )
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_PDF_VALUE));
    }


    @Test
    void whenAttachment_doReturnPresence() throws Exception {
        MvcResult prepareResult = preparePes("peswithpj.xml");
        Map<String, List<String>> params = parseRedirectionParameters(prepareResult);
        params.put("pjId", singletonList("thisisunique"));
        params.put("collId", singletonList("1002"));
        mockMvc.perform(
                        get("/exists")
                                .params(CollectionUtils.toMultiValueMap(params))
                                .session((MockHttpSession) parseRedirectionSession(prepareResult))
                )
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_VALUE));
    }


    @Test
    void whenGetAttachment_doReturnCorrectTypeAndExtension() throws Exception {
        MvcResult prepareResult = preparePes("peswithMultiplepj.xml");
        Map<String, List<String>> params = parseRedirectionParameters(prepareResult);
        params.put("collId", singletonList("1002"));

        params.put("pjId", singletonList("PdfPjUniqueId"));
        mockMvc.perform(
                        get("/attachment")
                                .params(CollectionUtils.toMultiValueMap(params))
                                .session((MockHttpSession) parseRedirectionSession(prepareResult))
                )
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_PDF_VALUE))
                .andExpect(header().string(CONTENT_DISPOSITION, "form-data; name=\"attachment\"; filename=\"maPJ.pdf\""));

        params.put("pjId", singletonList("XmlPjUniqueId"));
        mockMvc.perform(
                        get("/attachment")
                                .params(CollectionUtils.toMultiValueMap(params))
                                .session((MockHttpSession) parseRedirectionSession(prepareResult))
                )
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_XHTML_XML_VALUE))
                .andExpect(header().string(CONTENT_DISPOSITION, "form-data; name=\"attachment\"; filename=\"maPJ.xml\""));

        params.put("pjId", singletonList("ZipPjUniqueId"));
        mockMvc.perform(
                        get("/attachment")
                                .params(CollectionUtils.toMultiValueMap(params))
                                .session((MockHttpSession) parseRedirectionSession(prepareResult))
                )
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_ZIP_VALUE))
                .andExpect(header().string(CONTENT_DISPOSITION, "form-data; name=\"attachment\"; filename=\"maPJ.zip\""));
    }


    @Test
    void whenGetAttachmentWithoutMimeType_doReturnCorrectTypeAndExtension() throws Exception {
        MvcResult prepareResult = preparePes("pesWithMultiplePjButNoMimetype.xml");
        Map<String, List<String>> params = parseRedirectionParameters(prepareResult);
        params.put("collId", singletonList("1002"));

        params.put("pjId", singletonList("PdfPjUniqueId"));
        mockMvc.perform(
                        get("/attachment")
                                .params(CollectionUtils.toMultiValueMap(params))
                                .session((MockHttpSession) parseRedirectionSession(prepareResult))
                )
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_PDF_VALUE))
                .andExpect(header().string(CONTENT_DISPOSITION, "form-data; name=\"attachment\"; filename=\"maPJ.pdf\""));

        params.put("pjId", singletonList("XmlPjUniqueId"));
        mockMvc.perform(
                        get("/attachment")
                                .params(CollectionUtils.toMultiValueMap(params))
                                .session((MockHttpSession) parseRedirectionSession(prepareResult))
                )
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_XML_VALUE))
                .andExpect(header().string(CONTENT_DISPOSITION, "form-data; name=\"attachment\"; filename=\"maPJ.xml\""));

        params.put("pjId", singletonList("ZipPjUniqueId"));
        mockMvc.perform(
                        get("/attachment")
                                .params(CollectionUtils.toMultiValueMap(params))
                                .session((MockHttpSession) parseRedirectionSession(prepareResult))
                )
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_ZIP_VALUE))
                .andExpect(header().string(CONTENT_DISPOSITION, "form-data; name=\"attachment\"; filename=\"maPJ.zip\""));
    }


    @Test
    void whenGetAttachmentWithoutFilename_doReturnCorrectTypeAndExtension() throws Exception {
        MvcResult prepareResult = preparePes("pesWithMultiplePjButNoFilename.xml");
        Map<String, List<String>> params = parseRedirectionParameters(prepareResult);
        params.put("collId", singletonList("1002"));

        params.put("pjId", singletonList("PdfPjUniqueId"));
        mockMvc.perform(
                        get("/attachment")
                                .params(CollectionUtils.toMultiValueMap(params))
                                .session((MockHttpSession) parseRedirectionSession(prepareResult))
                )
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_PDF_VALUE))
                .andExpect(header().string(CONTENT_DISPOSITION, "form-data; name=\"attachment\"; filename=\"maPJ.pdf\""));

        params.put("pjId", singletonList("XmlPjUniqueId"));
        mockMvc.perform(
                        get("/attachment")
                                .params(CollectionUtils.toMultiValueMap(params))
                                .session((MockHttpSession) parseRedirectionSession(prepareResult))
                )
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_XHTML_XML_VALUE))
                .andExpect(header().string(CONTENT_DISPOSITION, "form-data; name=\"attachment\"; filename=\"maPJ.xml\""));

        params.put("pjId", singletonList("ZipPjUniqueId"));
        mockMvc.perform(
                        get("/attachment")
                                .params(CollectionUtils.toMultiValueMap(params))
                                .session((MockHttpSession) parseRedirectionSession(prepareResult))
                )
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_ZIP_VALUE))
                .andExpect(header().string(CONTENT_DISPOSITION, "form-data; name=\"attachment\"; filename=\"maPJ.zip\""));
    }


    @Test
    void whenGetAttachmentWithNoTypeInfo_doReturnExpectedTypeAndExtension() throws Exception {
        MvcResult prepareResult = preparePes("pesWithPjMissingTypeInfo.xml");
        Map<String, List<String>> params = parseRedirectionParameters(prepareResult);
        params.put("collId", singletonList("1002"));

        // no extension on input filename, default to pdf
        params.put("pjId", singletonList("thisisunique"));
        mockMvc.perform(
                        get("/attachment")
                                .params(CollectionUtils.toMultiValueMap(params))
                                .session((MockHttpSession) parseRedirectionSession(prepareResult))
                )
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_PDF_VALUE))
                .andExpect(header().string(CONTENT_DISPOSITION, "form-data; name=\"attachment\"; filename=\"maPJ.pdf\""));
    }


    @Test
    void whenGetAttachmentWithNoTypeInfoAndNoFilename_doReturnExpectedTypeAndExtension() throws Exception {
        MvcResult prepareResult = preparePes("pesWithPjMissingTypeInfoAndFilename.xml");
        Map<String, List<String>> params = parseRedirectionParameters(prepareResult);
        params.put("collId", singletonList("1002"));
        params.put("pjId", singletonList("thisisunique"));
        mockMvc.perform(
                        get("/attachment")
                                .params(CollectionUtils.toMultiValueMap(params))
                                .session((MockHttpSession) parseRedirectionSession(prepareResult))
                )
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_PDF_VALUE))
                .andExpect(header().string(CONTENT_DISPOSITION, "form-data; name=\"attachment\"; filename=\"PJ.pdf\""));
    }


    @Test
    void whenGetBadAttachment_doReturnHtmlError() throws Exception {
        MvcResult prepareResult = preparePes("peswithpj.xml");
        Map<String, List<String>> params = parseRedirectionParameters(prepareResult);
        params.put("collId", singletonList("1002"));
        params.put("pjId", singletonList("thisisbad"));
        mockMvc.perform(
                        get("/attachment")
                                .params(CollectionUtils.toMultiValueMap(params))
                                .session((MockHttpSession) parseRedirectionSession(prepareResult))
                )
                .andExpect(status().isNotFound())
                .andExpect(content().contentTypeCompatibleWith(TEXT_HTML));
    }


    @Test
    void whenGetBadEncodedAttachment_doReturnHtmlError() throws Exception {
        MvcResult prepareResult = preparePes("peswithpjbadbase64.xml");
        Map<String, List<String>> params = parseRedirectionParameters(prepareResult);
        params.put("collId", singletonList("1002"));
        params.put("state", singletonList("present"));
        params.put("pjId", singletonList("thisisunique1"));
        mockMvc.perform(
                        get("/attachment")
                                .params(CollectionUtils.toMultiValueMap(params))
                                .session((MockHttpSession) parseRedirectionSession(prepareResult))
                )
                .andExpect(status().isNotFound())
                .andExpect(content().contentTypeCompatibleWith(TEXT_HTML));
    }


    @Test
    void whenPesWithAbsentAsapPJ_doReturnNormalAttachmentLinks() throws Exception {
        MvcResult prepareResult = preparePes("pesASAP-absentPJ.xml");
        Map<String, List<String>> params = parseRedirectionParameters(prepareResult);
        params.put("etatId", singletonList("PES_RecetteAller"));
        params.put("elementId", singletonList("BordereauRecette"));
        params.put("path", singletonList("[@added:primary-key='2019-01-7']"));
        params.put("xsl:param", singletonList("(elementId,Bordereau)"));
        mockMvc.perform(
                        get(parseRedirectionPath(prepareResult))
                                .params(CollectionUtils.toMultiValueMap(params))
                                .session((MockHttpSession) parseRedirectionSession(prepareResult))
                )
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(TEXT_HTML))
                .andExpect(content().string(not(containsString("browse-asap-content"))));
    }


    @Test
    void whenPesWithAsapPJ_doReturnAsapLinks() throws Exception {
        MvcResult prepareResult = preparePes("pesWithASAP-minimal.xml");
        Map<String, List<String>> params = parseRedirectionParameters(prepareResult);
        params.put("etatId", singletonList("PES_RecetteAller"));
        params.put("elementId", singletonList("BordereauRecette"));
        params.put("path", singletonList("[@added:primary-key='2019-01-7']"));
        params.put("xsl:param", singletonList("(elementId,Bordereau)"));

        final AtomicReference<MvcResult> requestResult = new AtomicReference<>();
        mockMvc.perform(
                        get(parseRedirectionPath(prepareResult))
                                .params(CollectionUtils.toMultiValueMap(params))
                                .session((MockHttpSession) parseRedirectionSession(prepareResult))
                )
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(TEXT_HTML))
                .andDo(requestResult::set);

        // The result is not a valid XML file, we cannot simply test the xPath like we could the jsonPath.
        // Extracting it to a string, and use the regular insertions is simpler than create an HarmCrest Matcher.
        String responseString = requestResult.get().getResponse().getContentAsString(UTF_8);
        assertNotNull(responseString);
        String biffedItemString = responseString.replaceAll("xwvSession=\\w{8}-\\w{4}-\\w{4}-\\w{4}-\\w{12}", "xwvSession=<sessionId>");
        String biffedExpectedString = """
                <html xmlns="http://www.w3.org/1999/xhtml" xmlns:added="http://projets.admisource.gouv.fr/xemelios/namespaces#added" xmlns:ano="http://projets.admisource.gouv.fr/xemelios/namespaces#anomally" xmlns:data="data.uri" xmlns:ds="http://www.w3.org/2000/09/xmldsig#" xmlns:n="http://www.minefi.gouv.fr/cp/helios/pes_v2/Rev0/aller" xmlns:xad="http://uri.etsi.org/01903/v1.1.1#" xmlns:xem="http://xemelios.org/extensions/xml/functions" xml:lang="en" lang="en">
                 <head>
                  <title>Bordereau
                                    de recette
                                </title>
                  <script>
                                    var checkForPjExistence = function(id, success) {
                                    var xhr = new XMLHttpRequest();
                                    var req = document.location.origin + document.location.pathname.replace('browse', 'exists');
                                    var session = document.location.search.split('xwvSession=')[1];
                                    xhr.open('GET', req + '?pjId=' + id + '&collId=123456789&xwvSession=' + session);
                                    xhr.onreadystatechange = function () {
                                    var DONE = 4; // readyState 4 means the request is done.
                                    var OK = 200; // status 200 is a successful return.
                                    if (xhr.readyState === DONE) {
                                    if (xhr.status === OK) {
                                    success(id, xhr.responseText === "true");
                                    } else {
                                    console.log('Error: ' + xhr.status); // An error occurred during the request.
                                    }
                                    }
                                    };
                                    xhr.send(null);
                                    }
                                </script>
                  <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
                  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
                  <link rel="stylesheet" type="text/css" href="css/custom-xemelios.css">
                  <script src="ls-elements.js"></script>
                  <meta charset="UTF-8">
                 </head>
                 <body>
                  <h1 class="h3">001014&nbsp;TRES. GEX</h1>
                  <nav aria-label="breadcrumb">
                   <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/bl-xemwebviewer/browse?docId=pes-aller&amp;etatId=general_view&amp;xwvSession=<sessionId>">Accueil</a></li>
                    <li class="breadcrumb-item active">Bordereau 7</li>
                   </ol>
                  </nav>
                  <h2 class="h4">Exercice 2019</h2>
                  <table class="entete table table-striped" cellspacing="0">
                   <tbody>
                    <tr>
                     <td class="colonne1"><codecol>
                       143
                      </codecol> <codebudget>
                       00
                      </codebudget> &nbsp; BUDGET random<br><siret>
                       123456789
                      </siret><br><finjur></finjur></td>
                     <td class="colonne2">Bordereau Ordinaire<br>
                      1 &nbsp; Titre</td>
                     <td class="noborder">Date d'émission</td>
                     <td class="noborder">10/09/2019</td>
                    </tr>
                   </tbody>
                  </table>
                  <table class="enteteTablePrincipale table table-striped" cellspacing="0">
                   <thead>
                    <tr>
                     <th rowspan="2">Nom et adresse du débiteur</th>
                     <th rowspan="2">Titre</th>
                     <th rowspan="2">Objet titre</th>
                     <th colspan="3">Imputation</th>
                     <th colspan="3" class="text-right">Somme</th>
                    </tr>
                    <tr>
                     <th>Nature</th>
                     <th>Fonction</th>
                     <th>Opération</th>
                     <th class="text-right">HT</th>
                     <th class="text-right">TVA</th>
                     <th class="text-right">TTC</th>
                    </tr>
                   </thead>
                   <tbody>
                    <tr>
                     <td class="principaleCol1">Siret &nbsp;:&nbsp; 9876<br>
                      Personnes morales de droit privé autres qu'organismes sociaux<br>
                      Société<br>
                      NOM SOCIETE &nbsp; complement &nbsp; <br>
                      LIEU DIT<br>
                      05100 &nbsp; BORDER TOWN</td>
                     <td class="principaleCol2"><a href="/bl-xemwebviewer/browse?docId=pes-aller&amp;etatId=PES_RecetteAller&amp;elementId=BordereauRecette&amp;collectivite=123456789&amp;budget=00&amp;path=[@added:primary-key='2019-01-7']&amp;xsl:param=(mandatId,8)&amp;xwvSession=<sessionId>">8</a></td>
                     <td class="principaleCol3">REDEVANCE 2019<br>
                      Titre ordinaire<br>
                      Fonctionnement<br xmlns=""><br>
                       Justifié par 1 documents :\s
                      <ul xmlns="" class="pj">
                       <li class="pj"><span class="badge badge-success">Présent</span>&nbsp; <a target="_blank" title="
                										Identifiant unique : asap-pj-id" href="/bl-xemwebviewer/browse-asap-content?pjId=asap-pj-id&amp;collectivite=123456789&amp;xwvSession=<sessionId>">asap_2019_27_Titre_8_Bor_7.xml</a></li>
                      </ul><br></td>
                     <td class="principaleCol4">757<br></td>
                     <td class="principaleCol5">95<br></td>
                     <td class="principaleCol6"><br></td>
                     <td class="text-right">1 234,56<br></td>
                     <td class="text-right">123,45<br></td>
                     <td class="text-right">1 358,01<br></td>
                    </tr>
                   </tbody>
                  </table>
                  <p><br></p>
                  <table class="totaux table table-striped" cellspacing="0">
                   <tbody>
                    <tr>
                     <td class="noborder" rowspan="5">Arrêté le présent bordereau à la somme de 1 358,01 euros. <br>
                      Comprenant le titre n°8<br><emp></emp><span class="text-danger">Ces éléments sont déduits du flux mais absence de signature électronique. </span><br><br>
                      L'ordonnateur [NomSignataire]<br>
                       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[Role]<br>
                       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
                       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[AdresseEMail]<br>
                       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;le</td>
                     <td class="totalLibelle">Total HT</td>
                     <td class="text-right">1 234,56&nbsp;</td>
                    </tr>
                    <tr>
                     <td class="totalLibelle">Total&nbsp;du&nbsp;présent&nbsp;bordereau</td>
                     <td class="text-right">1 358,01&nbsp;</td>
                    </tr>
                    <tr>
                     <td class="totalLibelle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dont TVA</td>
                     <td class="text-right">123,45&nbsp;</td>
                    </tr>
                    <tr>
                     <td class="totalLibelle">Total&nbsp;général&nbsp;au&nbsp;précédent&nbsp;bordereau</td>
                     <td class="text-right">-1 220,51&nbsp;</td>
                    </tr>
                    <tr>
                     <td class="totalLibelle">Cumul&nbsp;annuel</td>
                     <td class="text-right">14,05&nbsp;</td>
                    </tr>
                   </tbody>
                  </table>
                  <p><br></p><ls-back-top-top></ls-back-top-top>
                 </body>
                </html>\
                """;
        assertEquals(biffedExpectedString, biffedItemString);
    }


    @Test
    void whenBrowseAsapPJ_doReturnHtmlContent() throws Exception {
        MvcResult prepareResult = preparePes("pesWithASAP-minimal.xml");
        Map<String, List<String>> params = parseRedirectionParameters(prepareResult);
        params.put("pjId", singletonList("asap-pj-id"));

        final AtomicReference<MvcResult> requestResult = new AtomicReference<>();
        mockMvc.perform(
                        get("/browse-asap-content")
                                .params(CollectionUtils.toMultiValueMap(params))
                                .session((MockHttpSession) parseRedirectionSession(prepareResult))
                )
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(TEXT_HTML))
                .andDo(requestResult::set);

        // The result is not a valid XML file, we cannot simply test the xPath like we could the jsonPath.
        // Extracting it to a string, and use the regular insertions is simpler than create an HarmCrest Matcher.
        String responseString = requestResult.get().getResponse().getContentAsString(UTF_8);
        assertNotNull(responseString);
        String expected = """
                <html xmlns="http://www.w3.org/1999/xhtml" xmlns:added="http://projets.admisource.gouv.fr/xemelios/namespaces#added" xmlns:ano="http://projets.admisource.gouv.fr/xemelios/namespaces#anomally" xmlns:data="data.uri" xmlns:fn="http://projets.admisource.gouv.fr/xemelios/namespaces#functions" xmlns:n="http://www.minefi.gouv.fr/cp/helios/pes_v2/Rev0/aller" xml:lang="en" lang="en">
                 <head>
                  <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
                  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
                  <link rel="stylesheet" type="text/css" href="css/custom-xemelios.css">
                  <script src="ls-elements.js"></script>
                  <meta charset="UTF-8">
                 </head>
                 <body>
                  <div class="divpage">
                   <div class="demi">
                    <fieldset xmlns="">
                     <legend>Emetteur de la créance</legend>
                     <table class="noborder table table-striped">
                      <tbody>
                       <tr>
                        <td>COMMUNE DE X</td>
                       </tr>
                       <tr>
                        <td>BUDGET RANDOM</td>
                       </tr>
                       <tr>
                        <td>2 RUE DE LA PAIE</td>
                       </tr>
                       <tr>
                        <td>01220 &nbsp; DIVONNE LES BAINS</td>
                       </tr>
                       <tr>
                        <td>Téléphone : 04.05.06.07.08</td>
                       </tr>
                      </tbody>
                     </table>
                    </fieldset>
                    <fieldset xmlns="">
                     <legend>Destinataire de votre paiement</legend>
                     <table class="noborder table table-striped">
                      <tbody>
                       <tr>
                        <td>Centre des Finances Publiques</td>
                       </tr>
                       <tr>
                        <td>NONAME</td>
                       </tr>
                       <tr>
                        <td>1 RUE DE LA PAIE</td>
                       </tr>
                       <tr>
                        <td>01170 &nbsp; GEX</td>
                       </tr>
                      </tbody>
                     </table>
                    </fieldset>
                   </div>
                   <div class="demi">
                    <p xmlns="" class="thick">AVIS DES SOMMES A PAYER</p>
                    <br xmlns="">
                    <br>
                    <table xmlns="" class="noborder table table-striped">
                     <tbody>
                      <tr>
                       <td>Centre des Finances Publiques</td>
                      </tr>
                      <tr>
                       <td>NONAME</td>
                      </tr>
                      <tr>
                       <td>1 RUE DE LA PAIE</td>
                      </tr>
                      <tr>
                       <td>01170 &nbsp; GEX</td>
                      </tr>
                     </tbody>
                    </table>
                    <br xmlns="">
                    <br>
                    <table xmlns="" class="noborder table table-striped">
                     <tbody>
                      <tr>
                       <td>NOM SOCIETE &nbsp;</td>
                      </tr>
                      <tr>
                       <td>complement</td>
                      </tr>
                      <tr>
                       <td>LIEU DIT</td>
                      </tr>
                      <tr>
                       <td>05100 &nbsp; BORDER TOWN</td>
                      </tr>
                     </tbody>
                    </table>
                   </div>
                   <div class="seul">
                    Madame, Monsieur,\s
                    <br>
                     En application des articles L.252 A du livre des procédures fiscales et L.1617-5 du code général des collectivités territoriales, j'ai émis et rendu exécutoire un titre de recette pour recouvrer la créance dont les\s
                    <br>
                     caractéristiques sont les suivantes :
                   </div>
                   <div class="demi">
                    Références à rappeler\s
                    <table xmlns="" class="withborder table table-striped" cellpadding="0" cellspacing="0">
                     <tbody>
                      <tr>
                       <td>Budget</td>
                       <td>Exercice</td>
                       <td>N°bordereau</td>
                       <td>N°titre</td>
                      </tr>
                      <tr>
                       <td>14300</td>
                       <td>2019</td>
                       <td>7</td>
                       <td>8</td>
                      </tr>
                     </tbody>
                    </table>
                    <br xmlns="">
                    <br>
                     Date d'émission du titre de recette : 2019-09-10
                   </div>
                   <div class="demi">
                    <fieldset xmlns="">
                     <table class="noborder table table-striped">
                      <tbody>
                       <tr>
                        <td><span class="thick">Adresse de paiement par Internet : </span></td>
                       </tr>
                       <tr>
                        <td><span class="thick">Identifiant collectivité : </span></td>
                       </tr>
                       <tr>
                        <td><span class="thick">Référence : </span></td>
                       </tr>
                      </tbody>
                     </table>
                    </fieldset>
                   </div>
                   <div class="seul">
                    <table xmlns="" class="facture table table-striped" cellpadding="0" cellspacing="0">
                     <tbody>
                      <tr>
                       <td class="facturecolonne1">Objet</td>
                       <td class="facturecolonne2">Prix unitaire</td>
                       <td class="facturecolonne3">Qté. 1</td>
                       <td class="facturecolonne4">Qté. 2</td>
                       <td class="facturecolonne5">Montant total HT</td>
                       <td class="facturecolonne6">TVA</td>
                       <td class="facturecolonne7">Montant TTC</td>
                      </tr>
                      <tr>
                       <td class="facturecolonne1">Redevance 2019 - -</td>
                       <td class="facturecolonne2"></td>
                       <td class="facturecolonne3"></td>
                       <td class="facturecolonne4"></td>
                       <td class="facturecolonne5">1 234,56</td>
                       <td class="facturecolonne6">1 234,45</td>
                       <td class="facturecolonne7">1 345,67</td>
                      </tr>
                     </tbody>
                    </table>
                    <table xmlns="" class="tableau3 table table-striped">
                     <tbody>
                      <tr>
                       <td class="cell_titre_col" colspan="2">Total</td>
                      </tr>
                      <tr>
                       <td class="cell_tableau1">Montant HT</td>
                       <td class="cell_numeric_g">1 234,56</td>
                      </tr>
                      <tr>
                       <td class="cell_tableau1">Montant TVA</td>
                       <td class="cell_numeric_g">123,45</td>
                      </tr>
                      <tr>
                       <td class="cell_tableau1">Montant TTC</td>
                       <td class="cell_numeric_g">1 345,67</td>
                      </tr>
                     </tbody>
                    </table>
                   </div>
                   <div class="seul">
                    A compter du présent avis, vous disposez d'un délai de :\s
                    <br>
                     - trente jours pour payer cette somme au comptable public selon les modalités détaillées au verso ;\s
                    <br>
                     - deux mois pour éventuellement contester ce titre de recette, selon les modalités détaillées au verso.\s
                    <br>
                     Mes services se tiennent à votre disposition pour tout renseignement supplémentaire.\s
                    <br>
                     Je vous prie de croire, Madame, Monsieur, à l'assurance de ma considération distinguée.
                   </div>
                   <br>
                   <div class="seul">
                    <font xmlns="" size="10pt">SOMEONE &nbsp; Else,&nbsp; Maire</font>
                   </div>
                   <div class="seul">
                    <fieldset xmlns="">
                     <legend>Talon de paiement / TIP SEPA</legend>
                     <table class="noborder table table-striped">
                      <tbody>
                       <tr>
                        <td>Centre d'encaissement :</td>
                       </tr>
                       <tr>
                        <td>CENTRE D'ENCAISSEMENT</td>
                       </tr>
                       <tr>
                        <td>01170 &nbsp; GEX</td>
                       </tr>
                       <tr>
                        <td>Emetteur :</td>
                       </tr>
                       <tr>
                        <td>ICS :</td>
                       </tr>
                       <tr>
                        <td>RUM : TIPSEPA 001014143000000000800000119 T</td>
                       </tr>
                       <tr>
                        <td>n°émetteur = 850033</td>
                       </tr>
                       <tr>
                        <td>
                         <table class="table table-striped">
                          <tbody>
                           <tr>
                            <td>Lignes optiques :</td>
                            <td>000000143193</td>
                           </tr>
                           <tr>
                            <td>&nbsp;</td>
                            <td>850033000159 95100000000080000010010145946806 4409234</td>
                           </tr>
                          </tbody>
                         </table></td>
                       </tr>
                      </tbody>
                     </table>
                    </fieldset>
                   </div>
                   <div class="seul">
                    <p xmlns="">Comment régler votre dette auprès de l'organisme public&nbsp;: <br><br></p>
                    <ul>
                     <li>Si l'organisme public offre la possibilité de la payer par internet, au moyen d'une carte bancaire, vous êtes invité(e) à vous connecter à l'adresse électronique mentionnée dans le cadre concerné au recto&nbsp;;</li>
                     <li>Sinon, il vous est recommandé de payer par titre interbancaire de paiement (TIP), en détachant le talon en bas du recto du présent avis, en le datant et le signant dans l'encadré indiqué. Si vos coordonnées bancaires ne sont pas mentionnées en haut à gauche de ce TIP, joignez le relevé d'identité bancaire du compte sur lequel sera prélevée cette créance. Le tout est à envoyer à l'adresse mentionnée sur le TIP&nbsp;;</li>
                     <li>Si vous réglez par chèque, libellez-le à l'ordre du trésor public et joignez-le TIP non signé et non agrafé, sans aucun autre document. Le tout est à envoyer à l'adresse mentionnée sur le TIP&nbsp;;</li>
                     <li>Si vous réglez par virement bancaire, faites le vers le compte bancaire du comptable public (BIC/IBAN&nbsp;: &nbsp;) en indiquant, en zone objet / libellé les références à rappeler mentionnées au recto&nbsp;;</li>
                     <li>Si vous réglez en espèces (dans la limite de 300€) auprès du guichet du comptable public indiqué au recto ou auprès d'un autre Centre des Finances Publiques, munissez-vous du présent avis&nbsp;;</li>
                     <li>Si vous souhaitez que vos dettes futures soient prélevées automatiquement sur votre compte bancaire, et si la collectivité offre cette possibilité, la démarche est la suivante&nbsp;: .</li>
                    </ul>
                    <p></p>
                   </div>
                   <div class="seul">
                    <p xmlns="">Comment contester ou vous renseigner sur votre dette envers l'organisme public&nbsp;: <br><br></p>
                    <ul>
                     <li>Pour tout renseignement complémentaire sur la créance dont le paiement vous est réclamé, vous devez contacter le service émetteur de la créance indiqué au recto du présent avis&nbsp;;</li>
                     <li></li>
                     <li>Toute somme non acquittée dans le délai de 30 jours de la réception du présent avis fera l'objet de poursuites engagées par le comptable public indiqué au recto (seul celui-ci peut accorder un délai de paiement dans des cas exceptionnels dûment justifiés par vous). Pour contester ces poursuites, vous devez déposer un recours devant le juge de l'exécution mentionnés aux articles L. 213-5 et L. 213.6 du code de l'organisation judiciaire dans un délai de deux mois suivant la notification de l'acte contesté (cf 2° de l'article L.1617-5 du code général des collectivités locales).</li>
                    </ul>
                    <p></p>
                   </div>
                  </div><ls-back-top-top></ls-back-top-top>
                 </body>
                </html>\
                """;

        assertEquals(expected, responseString);
    }


    @Test
    void whenBrowsingPreviouslyStoredAsapPJ_doReturnHtmlContent() throws Exception {

        // This request should store the attachment in cache
        preparePes("pesWithASAP-minimal.xml");

        MvcResult prepareResult = preparePes("pesASAP-absentPJ.xml");
        Map<String, List<String>> params = parseRedirectionParameters(prepareResult);
        params.put("pjId", singletonList("asap-pj-id"));

        final AtomicReference<MvcResult> requestResult = new AtomicReference<>();
        mockMvc.perform(
                        get("/attachment")
                                .params(CollectionUtils.toMultiValueMap(params))
                                .session((MockHttpSession) parseRedirectionSession(prepareResult))
                )
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(TEXT_HTML))
                .andDo(requestResult::set);

        // The result is not a valid XML file, we cannot simply test the xPath like we could the jsonPath.
        // Extracting it to a string, and use the regular insertions is simpler than create an HarmCrest Matcher.
        String responseString = requestResult.get().getResponse().getContentAsString(UTF_8);
        assertNotNull(responseString);
        String expected = """
                <html xmlns="http://www.w3.org/1999/xhtml" xmlns:added="http://projets.admisource.gouv.fr/xemelios/namespaces#added" xmlns:ano="http://projets.admisource.gouv.fr/xemelios/namespaces#anomally" xmlns:data="data.uri" xmlns:fn="http://projets.admisource.gouv.fr/xemelios/namespaces#functions" xmlns:n="http://www.minefi.gouv.fr/cp/helios/pes_v2/Rev0/aller" xml:lang="en" lang="en">
                 <head>
                  <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
                  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
                  <link rel="stylesheet" type="text/css" href="css/custom-xemelios.css">
                  <script src="ls-elements.js"></script>
                  <meta charset="UTF-8">
                 </head>
                 <body>
                  <div class="divpage">
                   <div class="demi">
                    <fieldset xmlns="">
                     <legend>Emetteur de la créance</legend>
                     <table class="noborder table table-striped">
                      <tbody>
                       <tr>
                        <td>COMMUNE DE X</td>
                       </tr>
                       <tr>
                        <td>BUDGET RANDOM</td>
                       </tr>
                       <tr>
                        <td>2 RUE DE LA PAIE</td>
                       </tr>
                       <tr>
                        <td>01220 &nbsp; DIVONNE LES BAINS</td>
                       </tr>
                       <tr>
                        <td>Téléphone : 04.05.06.07.08</td>
                       </tr>
                      </tbody>
                     </table>
                    </fieldset>
                    <fieldset xmlns="">
                     <legend>Destinataire de votre paiement</legend>
                     <table class="noborder table table-striped">
                      <tbody>
                       <tr>
                        <td>Centre des Finances Publiques</td>
                       </tr>
                       <tr>
                        <td>NONAME</td>
                       </tr>
                       <tr>
                        <td>1 RUE DE LA PAIE</td>
                       </tr>
                       <tr>
                        <td>01170 &nbsp; GEX</td>
                       </tr>
                      </tbody>
                     </table>
                    </fieldset>
                   </div>
                   <div class="demi">
                    <p xmlns="" class="thick">AVIS DES SOMMES A PAYER</p>
                    <br xmlns="">
                    <br>
                    <table xmlns="" class="noborder table table-striped">
                     <tbody>
                      <tr>
                       <td>Centre des Finances Publiques</td>
                      </tr>
                      <tr>
                       <td>NONAME</td>
                      </tr>
                      <tr>
                       <td>1 RUE DE LA PAIE</td>
                      </tr>
                      <tr>
                       <td>01170 &nbsp; GEX</td>
                      </tr>
                     </tbody>
                    </table>
                    <br xmlns="">
                    <br>
                    <table xmlns="" class="noborder table table-striped">
                     <tbody>
                      <tr>
                       <td>NOM SOCIETE &nbsp;</td>
                      </tr>
                      <tr>
                       <td>complement</td>
                      </tr>
                      <tr>
                       <td>LIEU DIT</td>
                      </tr>
                      <tr>
                       <td>05100 &nbsp; BORDER TOWN</td>
                      </tr>
                     </tbody>
                    </table>
                   </div>
                   <div class="seul">
                    Madame, Monsieur,\s
                    <br>
                     En application des articles L.252 A du livre des procédures fiscales et L.1617-5 du code général des collectivités territoriales, j'ai émis et rendu exécutoire un titre de recette pour recouvrer la créance dont les\s
                    <br>
                     caractéristiques sont les suivantes :
                   </div>
                   <div class="demi">
                    Références à rappeler\s
                    <table xmlns="" class="withborder table table-striped" cellpadding="0" cellspacing="0">
                     <tbody>
                      <tr>
                       <td>Budget</td>
                       <td>Exercice</td>
                       <td>N°bordereau</td>
                       <td>N°titre</td>
                      </tr>
                      <tr>
                       <td>14300</td>
                       <td>2019</td>
                       <td>7</td>
                       <td>8</td>
                      </tr>
                     </tbody>
                    </table>
                    <br xmlns="">
                    <br>
                     Date d'émission du titre de recette : 2019-09-10
                   </div>
                   <div class="demi">
                    <fieldset xmlns="">
                     <table class="noborder table table-striped">
                      <tbody>
                       <tr>
                        <td><span class="thick">Adresse de paiement par Internet : </span></td>
                       </tr>
                       <tr>
                        <td><span class="thick">Identifiant collectivité : </span></td>
                       </tr>
                       <tr>
                        <td><span class="thick">Référence : </span></td>
                       </tr>
                      </tbody>
                     </table>
                    </fieldset>
                   </div>
                   <div class="seul">
                    <table xmlns="" class="facture table table-striped" cellpadding="0" cellspacing="0">
                     <tbody>
                      <tr>
                       <td class="facturecolonne1">Objet</td>
                       <td class="facturecolonne2">Prix unitaire</td>
                       <td class="facturecolonne3">Qté. 1</td>
                       <td class="facturecolonne4">Qté. 2</td>
                       <td class="facturecolonne5">Montant total HT</td>
                       <td class="facturecolonne6">TVA</td>
                       <td class="facturecolonne7">Montant TTC</td>
                      </tr>
                      <tr>
                       <td class="facturecolonne1">Redevance 2019 - -</td>
                       <td class="facturecolonne2"></td>
                       <td class="facturecolonne3"></td>
                       <td class="facturecolonne4"></td>
                       <td class="facturecolonne5">1 234,56</td>
                       <td class="facturecolonne6">1 234,45</td>
                       <td class="facturecolonne7">1 345,67</td>
                      </tr>
                     </tbody>
                    </table>
                    <table xmlns="" class="tableau3 table table-striped">
                     <tbody>
                      <tr>
                       <td class="cell_titre_col" colspan="2">Total</td>
                      </tr>
                      <tr>
                       <td class="cell_tableau1">Montant HT</td>
                       <td class="cell_numeric_g">1 234,56</td>
                      </tr>
                      <tr>
                       <td class="cell_tableau1">Montant TVA</td>
                       <td class="cell_numeric_g">123,45</td>
                      </tr>
                      <tr>
                       <td class="cell_tableau1">Montant TTC</td>
                       <td class="cell_numeric_g">1 345,67</td>
                      </tr>
                     </tbody>
                    </table>
                   </div>
                   <div class="seul">
                    A compter du présent avis, vous disposez d'un délai de :\s
                    <br>
                     - trente jours pour payer cette somme au comptable public selon les modalités détaillées au verso ;\s
                    <br>
                     - deux mois pour éventuellement contester ce titre de recette, selon les modalités détaillées au verso.\s
                    <br>
                     Mes services se tiennent à votre disposition pour tout renseignement supplémentaire.\s
                    <br>
                     Je vous prie de croire, Madame, Monsieur, à l'assurance de ma considération distinguée.
                   </div>
                   <br>
                   <div class="seul">
                    <font xmlns="" size="10pt">SOMEONE &nbsp; Else,&nbsp; Maire</font>
                   </div>
                   <div class="seul">
                    <fieldset xmlns="">
                     <legend>Talon de paiement / TIP SEPA</legend>
                     <table class="noborder table table-striped">
                      <tbody>
                       <tr>
                        <td>Centre d'encaissement :</td>
                       </tr>
                       <tr>
                        <td>CENTRE D'ENCAISSEMENT</td>
                       </tr>
                       <tr>
                        <td>01170 &nbsp; GEX</td>
                       </tr>
                       <tr>
                        <td>Emetteur :</td>
                       </tr>
                       <tr>
                        <td>ICS :</td>
                       </tr>
                       <tr>
                        <td>RUM : TIPSEPA 001014143000000000800000119 T</td>
                       </tr>
                       <tr>
                        <td>n°émetteur = 850033</td>
                       </tr>
                       <tr>
                        <td>
                         <table class="table table-striped">
                          <tbody>
                           <tr>
                            <td>Lignes optiques :</td>
                            <td>000000143193</td>
                           </tr>
                           <tr>
                            <td>&nbsp;</td>
                            <td>850033000159 95100000000080000010010145946806 4409234</td>
                           </tr>
                          </tbody>
                         </table></td>
                       </tr>
                      </tbody>
                     </table>
                    </fieldset>
                   </div>
                   <div class="seul">
                    <p xmlns="">Comment régler votre dette auprès de l'organisme public&nbsp;: <br><br></p>
                    <ul>
                     <li>Si l'organisme public offre la possibilité de la payer par internet, au moyen d'une carte bancaire, vous êtes invité(e) à vous connecter à l'adresse électronique mentionnée dans le cadre concerné au recto&nbsp;;</li>
                     <li>Sinon, il vous est recommandé de payer par titre interbancaire de paiement (TIP), en détachant le talon en bas du recto du présent avis, en le datant et le signant dans l'encadré indiqué. Si vos coordonnées bancaires ne sont pas mentionnées en haut à gauche de ce TIP, joignez le relevé d'identité bancaire du compte sur lequel sera prélevée cette créance. Le tout est à envoyer à l'adresse mentionnée sur le TIP&nbsp;;</li>
                     <li>Si vous réglez par chèque, libellez-le à l'ordre du trésor public et joignez-le TIP non signé et non agrafé, sans aucun autre document. Le tout est à envoyer à l'adresse mentionnée sur le TIP&nbsp;;</li>
                     <li>Si vous réglez par virement bancaire, faites le vers le compte bancaire du comptable public (BIC/IBAN&nbsp;: &nbsp;) en indiquant, en zone objet / libellé les références à rappeler mentionnées au recto&nbsp;;</li>
                     <li>Si vous réglez en espèces (dans la limite de 300€) auprès du guichet du comptable public indiqué au recto ou auprès d'un autre Centre des Finances Publiques, munissez-vous du présent avis&nbsp;;</li>
                     <li>Si vous souhaitez que vos dettes futures soient prélevées automatiquement sur votre compte bancaire, et si la collectivité offre cette possibilité, la démarche est la suivante&nbsp;: .</li>
                    </ul>
                    <p></p>
                   </div>
                   <div class="seul">
                    <p xmlns="">Comment contester ou vous renseigner sur votre dette envers l'organisme public&nbsp;: <br><br></p>
                    <ul>
                     <li>Pour tout renseignement complémentaire sur la créance dont le paiement vous est réclamé, vous devez contacter le service émetteur de la créance indiqué au recto du présent avis&nbsp;;</li>
                     <li></li>
                     <li>Toute somme non acquittée dans le délai de 30 jours de la réception du présent avis fera l'objet de poursuites engagées par le comptable public indiqué au recto (seul celui-ci peut accorder un délai de paiement dans des cas exceptionnels dûment justifiés par vous). Pour contester ces poursuites, vous devez déposer un recours devant le juge de l'exécution mentionnés aux articles L. 213-5 et L. 213.6 du code de l'organisation judiciaire dans un délai de deux mois suivant la notification de l'acte contesté (cf 2° de l'article L.1617-5 du code général des collectivités locales).</li>
                    </ul>
                    <p></p>
                   </div>
                  </div><ls-back-top-top></ls-back-top-top>
                 </body>
                </html>\
                """;

        assertEquals(expected, responseString);
    }


}
