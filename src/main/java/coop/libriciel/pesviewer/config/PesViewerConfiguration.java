/*
 * PES-Viewer
 * Copyright (C) 2019-2024 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.pesviewer.config;

import fr.gouv.finances.dgfip.xemelios.common.config.DocumentsModel;
import fr.gouv.finances.dgfip.xemelios.common.config.Loader;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.ArchiveInputStream;
import org.apache.commons.compress.archivers.zip.ZipArchiveInputStream;
import org.apache.commons.compress.utils.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.web.firewall.HttpFirewall;
import org.springframework.security.web.firewall.StrictHttpFirewall;
import org.springframework.util.FileSystemUtils;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.nio.file.StandardOpenOption.CREATE;


@Log4j2
@Configuration
public class PesViewerConfiguration {

    private static final String RESOURCES_PATH = "xemelios-resources";
    private static final String VERSION_FILE_NAME = "version.txt";
    private static final String XEM_RESOURCES_JAR_VERSION = "1.3";

    @Value("${retro.style.default:true}")
    boolean useDefaultStyle;

    @Value("${xemelios.resources.path:/fr/gouv/finances/xemelios-resources.zip}")
    private String zipResourcesPath;


    @Bean
    public HttpFirewall allowUrlEncodedSlashHttpFirewall() {
        StrictHttpFirewall firewall = new StrictHttpFirewall();
        firewall.setAllowUrlEncodedSlash(true);
        firewall.setAllowSemicolon(true);
        return firewall;
    }


    @Bean
    public DocumentsModel documentsModel() throws IOException, SAXException, ParserConfigurationException {
        // This function extract "xemelios-resources" directory, contained in Xemelios library jar
        this.prepareXemeliosResources();

        return Loader.getDocumentsInfos(this.baseXemRessourceFolder());
    }


    private String baseXemRessourceFolder() {
        return RESOURCES_PATH + "/documents-def";
    }


    private String getXemResourcesPackageVersion() {
        // Ideally we should have used fr.gouv.(...).Loader.class.getPackage().getImplementationVersion(),
        // but it returns null... So keeping version as a hardcoded value will do for now.
        return XEM_RESOURCES_JAR_VERSION;
    }


    private void prepareXemeliosResources() throws IOException {
        String xemPackVersion = this.getXemResourcesPackageVersion();
        boolean resourcesDirExists = false;
        boolean resourcesDirIsUpToDate = false;
        Path xemResourcesDir = Paths.get(RESOURCES_PATH);
        Path versionFile = xemResourcesDir.resolve(VERSION_FILE_NAME);
        if (Files.isDirectory(xemResourcesDir)) {
            resourcesDirExists = true;
            if (Files.exists(versionFile)) {
                try {
                    List<String> versionContentList = Files.readAllLines(versionFile);
                    String versionContent = (versionContentList.size() > 0) ? versionContentList.get(0) : "0";
                    resourcesDirIsUpToDate = versionContent.equals(xemPackVersion);
                } catch (IOException e) {
                    log.warn("Found version file in xemelios resources, but could not read it. error : ", e);
                }
            }
        }

        if (!resourcesDirIsUpToDate) {
            log.info("Extracting new XSLT resources...");
            if (resourcesDirExists) {
                log.info("Deleting previous XSLT resources first");
                FileSystemUtils.deleteRecursively(xemResourcesDir);
            }

            unzip(this.getClass().getResourceAsStream(zipResourcesPath), // The zip file, contained if Xemelios jar file
                    Files.createDirectory(xemResourcesDir).toFile()); // The local xemelios-resources directory

            log.info("XSLT resources extracted, add version file");
            Files.write(versionFile, xemPackVersion.getBytes(UTF_8), CREATE);

        }
        // Always copy modified style
        if (!useDefaultStyle) {
            // Copy all modified files to new directory
            String[] filesToCopy = new String[]{"analyse-flux", "general-view", "mandatBordereau", "mandatBordereauRecette", "mandatBordereauUtils", "facture"};
            for (String filename : filesToCopy) {
                try (InputStream xslFileInputStream = new ClassPathResource("static/" + filename + ".xsl").getInputStream();
                     OutputStream resourceFolderOutputStream = Files.newOutputStream(Paths.get(baseXemRessourceFolder() + "/PES_V2/" + filename + ".xsl"))) {
                    IOUtils.copy(xslFileInputStream, resourceFolderOutputStream);
                }
            }
        }
    }


    private void unzip(InputStream stream, File destDir) throws IOException {
        try (ArchiveInputStream i = new ZipArchiveInputStream(stream)) {
            ArchiveEntry entry;
            while ((entry = i.getNextEntry()) != null) {
                if (!i.canReadEntryData(entry)) {
                    continue;
                }
                String name = destDir.getCanonicalPath() + File.separator + entry.getName();
                File f = new File(name);
                if (entry.isDirectory()) {
                    if (!f.isDirectory() && !f.mkdirs()) {
                        throw new IOException("failed to create directory " + f);
                    }
                } else {
                    File parent = f.getParentFile();
                    if (!parent.isDirectory() && !parent.mkdirs()) {
                        throw new IOException("failed to create directory " + parent);
                    }
                    try (OutputStream o = Files.newOutputStream(f.toPath())) {
                        IOUtils.copy(i, o);
                    }
                }
            }
        }
    }


}
